%define www_prefix %_var/www/archive/


Name: archive-gui
Version: 1.0
Release: alt46
Summary: Web interface for Documents Archive
Group: Archiving/Other
License: GPL
Url: http://wiki.office.etersoft.ru/ASU/Arxiv
# Git: git.eter:/projects/archive-gui
Packager: Devaev Maxim <mdevaev@etersoft.ru>
Source: %name-%version.tar
BuildArch: noarch
BuildRequires: rpm-build-compat
Requires: python-module-pisa, python-module-django >= 1.2, python-module-django-dbbackend-mysql, python-module-flup
Requires: javascript-libs-extjs >= 3.2.1
%description
%summary


%prep
%setup


%install
mkdir -p %buildroot%_sysconfdir/cron.hourly/
mkdir -p %buildroot%www_prefix
cp -a %name/* %buildroot%www_prefix
mv %buildroot%www_prefix/archive-clear-out.sh %buildroot%_sysconfdir/cron.hourly/
mv %buildroot%www_prefix/settings.py.template %buildroot%www_prefix/settings.py
sed -i -e "s/@WWW_PREFIX@/`echo %www_prefix | sed -e 's/\//\\\\\\//g'`/g" %buildroot%_sysconfdir/cron.hourly/archive-clear-out.sh \
	%buildroot%www_prefix/settings.py \
	%buildroot%www_prefix/archive.fcgi
ln -s %_datadir/extjs %buildroot%www_prefix/media/ext
ln -s ./ %buildroot%www_prefix/archive


%files
%dir %www_prefix/
%www_prefix/main/
%www_prefix/file/
%www_prefix/media/
%www_prefix/WebGui/
%www_prefix/templates/
%www_prefix/time_control/
%www_prefix/func/
%www_prefix/__init__.py
%www_prefix/urls.py
%www_prefix/archive
%www_prefix/printer
%attr(0644,root,root) %config(noreplace) %www_prefix/settings.py
%attr(0700,root,root) %www_prefix/manage.py
%attr(0755,root,root) %www_prefix/archive.fcgi
%_sysconfdir/cron.hourly/archive-clear-out.sh


%post
ln -sf /usr/lib/python2.6/site-packages/django/contrib/admin/media %www_prefix/media_admin


%changelog
* Tue Apr 17 2012 Denis Baranov <baraka@altlinux.ru> 1.0-alt46
- add izvewenie form tpl
- fix date show

* Mon Apr 16 2012 Denis Baranov <baraka@altlinux.ru> 1.0-alt45
- add .gitignore
- add Journal izvewenia
- delite dynamic file

* Tue Oct 04 2011 Denis Baranov <baraka@altlinux.ru> 1.0-alt44
- Fix bug with Date
- small fix in views

* Thu Feb 24 2011 Timchenko Sergey <timka_s@etersoft.ru> 1.0-alt43
- add view of TestResultItems in SystemLog
- bugfix in files.py

* Thu Nov 11 2010 Devaev Maxim <mdevaev@etersoft.ru> 1.0-alt42
- add fileviewer to change
- bugFix in ChangeForm
- fix in critical
- fix in FileUpload -> File.model
- new Tab - filesViewer

* Thu Oct 21 2010 Devaev Maxim <mdevaev@etersoft.ru> 1.0-alt41
- add InvNumberJournal fix in forms
- fix Format of DocumentTypeList Bugfix in save reg_number in first operation Fix RegExp for DocumentSign validation Add Hide of DocumentViewer Fix in RequestList : format and sheets
- fix ind date_format fix in signvalidate
- merge branch 'master' of git.office:packages/archive-gui
- tChange: search by ChangeSign Now, in DocumentHistory, can download files to prev. stable version and from now stable version. Stable version can download only in Request Add verification of ChangeForm and RequestForm: selected documents - unique in form Bugfix in windowlib
- type_object.short_name will be able "blank"

* Thu Oct 14 2010 Devaev Maxim <mdevaev@etersoft.ru> 1.0-alt40
- add strong verification of node statuses in ChangeAdd
- add unique to WorkersDict bugfix in SignValidation New, more faster and stable version of DocumentViewer labelfix in Statuses Add new UserGroup - editor Add print to worklog in dict Add verification of ChangeNumber fix regexp of DocumentSign Add download to document history
- bugfix in ChangeAdd verification of Document Sign verification of doucment status in ChangeAdd add ChangeAdd to bar's and menu
- bugfix in InvNumber and Sign validate
- bugfix: status_code in SysLog Saving Change with str number Get only document in form in Change and Download
- fix
- move field of change file up
- new operation bugfix in invnumber_validation

* Wed Oct 06 2010 Devaev Maxim <mdevaev@etersoft.ru> 1.0-alt39
- add blanc = True for no required fields add class File into admin interface
- bugfix
- bugFix BugFix in ChangeViewer Warning message Remove critical/warning files
- bugfix new logic of checkers new logic of tchange and tstorage one style for models.py Admin -> only cricital check is off
- merge branch 'master' of git.office:packages/archive-gui
- new build 1.0-alt26 (with rpmlog script)

* Wed Sep 29 2010 Devaev Maxim <mdevaev@etersoft.ru> 1.0-alt38
- New build

* Fri Sep 24 2010 Devaev Maxim <mdevaev@etersoft.ri> 1.0-alt37
- Fixed bug with link creation

* Tue Sep 21 2010 Timchenko Sergey <timka_s@etersoft.ru> 1.0-alt36
- add altered base.html as base admin template
- add search field to admin interface for object, invnumbers, autors
- alter mylogin and logout view
- bugfix
- devel of UserSessions, Login,Logout, Middleware release User's group in interface
- dev of inv number bugfix in NK and Dict
- fix editObject
- journal of UserSession bugfix in time_control
- lost files
- merge branch 'master' of git.eter:/projects/archive-gui
- request Viewer
- templatefix
- user sessions -> to new module

* Thu Sep 09 2010 Timchenko Sergey <timka_s@etersoft.ru> 1.0-alt35
- bugfix in copy fix in Folder->DocumentViever
- fileSave -> function.py End of Dev TempStorage
- fix status of added folders/projects Fix js reload tree Viewed fix Fix Change Number Node Move Add confirm of one-click action's
- readOnly Fields Name and Oboznachenie in Document
- tempory Storage
- translate models names for django admin drop out unnecessary model classes from admin interface

* Tue Aug 24 2010 Devaev Maxim <mdevaev@etersoft.ru> 1.0-alt34
- Fixed bug with installation a printer directory

* Mon Aug 23 2010 Timchenko Sergey <timka_s@etersoft.ru> 1.0-alt33
- bugfix in InvNumber
- checkOffline bugfix in EditObject
- create iterative tree viewer new copyNode function bugfix
- dev of Opis' Print
- fields fix
- fix
- label fix
- log of Operator working
- new CheckCritical and CheckOffline
- print Izveshenie
- svg bugfix folders opis in menu/bars
- textfix
- translate label's Remove Scroll from forms

* Wed Aug 04 2010 Timchenko Sergey <timka_s@etersoft.ru> 1.0-alt32
- new build

* Mon Jul 26 2010 Timchenko Sergey <timka_s@etersoft.ru> 1.0-alt31
- bugfix

* Mon Jul 26 2010 Timchenko Sergey <timka_s@etersoft.ru> 1.0-alt30
- bugfix
- bugfix in request comments small optimaze
- dev of conctructor fix unwanted mouse event fix bug in dategroups show all menu/bar
- dev of GridConstructor ReBuild in they Viewer Navigation and Folder
- new constructor of Grids

* Fri Jul 23 2010 Timchenko Sergey <timka_s@etersoft.ru> 1.0-alt29
- add invnumber
- +AJAX login form +get Path without req opitmize, bugfix
- autoLogout New HardJournal
- correct invnumbers
- merge branch 'master' of git.eter:/projects/archive-gui
- new fields, bugfix
- new Py function FilterDate Working ReportTab

* Fri Jul 16 2010 Timchenko Sergey <timka_s@etersoft.ru> 1.0-alt28
- dev of get Node patch, to many function of tree in global lib - dataGroup, new DataRange function BugFix in Log Add Change/Request Journal

* Fri Jul 16 2010 Timchenko Sergey <timka_s@etersoft.ru> 1.0-alt27
- added fielde change to File model, correct filter for system journal
- fixed Django admin CSS bug
- merge branch 'master' of git.eter:/projects/archive-gui

* Thu Jul 15 2010 Timchenko Sergey <timka_s@etersoft.ru> 1.0-alt26
- - lengthened klgi field, altered downloaded report, added date
- new AJAX function Not changed store -> One global store Limit of Chars in KLGI beta Form of copy Folders(working, but slow) Fix bug of JS Label fix

* Wed Jul 14 2010 Vitaly Lipatov <pav@altlinux.ru> 1.0-alt25.1
- bugfix in download files
- bugfix in file-download
- merge branch 'master' of git.eter:/projects/archive-gui
- working version of download

* Wed Jul 14 2010 Devaev Maxim <mdevaev@etersoft.ru> 1.0-alt25
- New build

* Fri Jul 09 2010 Devaev Maxim <mdevaev@etersoft.ru> 1.0-alt24
- New build

* Fri Jul 09 2010 Devaev Maxim <mdevaev@etersoft.ru> 1.0-alt23
- New build

* Wed Jul 07 2010 Devaev Maxim <mdevaev@etersoft.ru> 1.0-alt22
- Testing build

* Tue Jul 06 2010 Devaev Maxim <mdevaev@etersoft.ru> 1.0-alt21
- Added python-module-flup to requires

* Tue Jul 06 2010 Devaev Maxim <mdevaev@etersoft.ru> 1.0-alt20
- New build

* Mon Jul 05 2010 Devaev Maxim <mdevaev@etersoft.ru> 1.0-alt19
- Fixed requires to extjs lib

* Fri Jul 02 2010 Devaev Maxim <mdevaev@etersoft.ru> 1.0-alt18
- Added BuildRequires

* Fri Jul 02 2010 Devaev Maxim <mdevaev@etersoft.ru> 1.0-alt17
- Fixed path to extjs
- Optomized seds

* Fri Jul 02 2010 Devaev Maxim <mdevaev@etersoft.ru> 1.0-alt16
- ExtJS moved to separate package javascript-libs-extjs

* Fri Jul 02 2010 Devaev Maxim <mdevaev@etersoft.ru> 1.0-alt15
- Fixed lighttpd rewrite urls feature

* Fri Jul 02 2010 Devaev Maxim <mdevaev@etersoft.ru> 1.0-alt14
- Added WebGui

* Fri Jul 02 2010 Devaev Maxim <mdevaev@etersoft.ru> 1.0-alt13
- Removed obsoleted code
- Fixed default permissions

* Fri Jul 02 2010 Devaev Maxim <mdevaev@etersoft.ru> 1.0-alt12
- Added macros for building package with different www_prefix
- Most universal configuration file

* Thu Jul 01 2010 Devaev Maxim <mdevaev@etersoft.ru> 1.0-alt11
- Renamed settings.py to settings.py.template

* Thu Jul 01 2010 Devaev Maxim <mdevaev@etersoft.ru> 1.0-alt10
- Added script for clean out folder

* Tue Jun 29 2010 Devaev Maxim <mdevaev@etersoft.ru> 1.0-alt9
- Removed building hacks
- Added requires for python

* Tue Jun 29 2010 Devaev Maxim <mdevaev@etersoft.ru> 1.0-alt8
- Added Ext JS 3.2.1 libs

* Tue Jun 29 2010 Devaev Maxim <mdevaev@etersoft.ru> 1.0-alt7
- Added media package

* Tue Jun 29 2010 Devaev Maxim <mdevaev@etersoft.ru> 1.0-alt6
- Added files package
- Modified settings.py, added IN and OUT folder options

* Tue Jun 29 2010 Devaev Maxim <mdevaev@etersoft.ru> 1.0-alt5
- Skipping self-requires

* Tue Jun 29 2010 Devaev Maxim <mdevaev@etersoft.ru> 1.0-alt4
- Views updated

* Mon Jun 28 2010 Devaev Maxim <mdevaev@etersoft.ru> 1.0-alt3
- Fixed settings.py and urls.py
- Fixed dos2unix encoding
- Added link for self-packaging

* Tue Jun 22 2010 Devaev Maxim <mdevaev@etersoft.ru> 1.0-alt2
- FHS fix

* Tue Jun 22 2010 Devaev Maxim <mdevaev@etersoft.ru> 1.0-alt1
- Initial build

