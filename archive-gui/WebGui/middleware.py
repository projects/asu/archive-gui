# -*- coding: utf-8 -*-
from django.conf import settings
from django.contrib import auth
from datetime import datetime, timedelta

class AutoLogout:
  def process_request(self, request):
    #if not request.user.is_authenticated():
      ##Can't log out if not logged in
      #return

    try:
	# разлогин только при запросе aj_checkAuth и просроченном last_touch
      if ((request.META['PATH_INFO'] == "archive/aj_checkAuth") and (datetime.now() - request.session['last_touch'] > timedelta( 0, settings.AUTO_LOGOUT_DELAY * 60, 0))):
        auth.logout(request)
        del request.session['last_touch']
        return
      
    except KeyError:
      pass

    if (request.META['PATH_INFO'] == "archive/aj_checkCriticProblem") or (request.META['PATH_INFO'] == "archive/aj_checkAuth"):
	    # не меняем значение last_touch
    	pass 
    else:
    	request.session['last_touch'] = datetime.now()
	
  def process_respons(self, request, response):
    if (request.META['PATH_INFO'] == "login" and request.method == 'POST'):
	    idSession = request.session.session_key 
	    if len(UserSession.objects.filter(idSession__exact = idSession)) == 0:
		us = UserSession (idSession = request.COOKIES["sessionid"], user = user, start = datetime.datetime.now())
		us.save()				
	