#!/bin/bash
OUT_FOLDER=`python -c 'import sys; sys.path.append("@WWW_PREFIX@"); import settings; print settings.OUT_FOLDER'`
[ "$?" -eq 0 ] || exit 1
[ -z "$OUT_FOLDER" ] || find $OUT_FOLDER -type f -mmin +30 -exec rm -f '{}' \;

