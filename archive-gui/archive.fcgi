#!/usr/bin/python
import sys, os
from django.core.servers.fastcgi import runfastcgi

os.environ["DJANGO_SETTINGS_MODULE"] = "settings"
sys.path.append("@WWW_PREFIX@")

runfastcgi(method="threaded", daemonize="false")
