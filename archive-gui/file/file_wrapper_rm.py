from django.core.servers.basehttp import FileWrapper
import os

class FileWrapperRm(FileWrapper) :
	def __init__(self, filelike, blksize = 8192) :
		FileWrapper.__init__(self, filelike, blksize)
		self.filelike_name = filelike.name

	def next(self) :
		try :
			return FileWrapper.next(self)
		except StopIteration :
			try :
				os.remove(self.filelike_name)
			except : pass
			raise StopIteration

