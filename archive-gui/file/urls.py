# -*- coding: utf-8 -*-
from django.conf.urls.defaults import *
from file.views import *

urlpatterns = patterns('',
    # Example:
    # (r'^ttt/', include('ttt.foo.urls')),


    # Uncomment the admin/doc line below and add 'django.contrib.admindocs' 
    # to INSTALLED_APPS to enable admin documentation:
    # (r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # (r'^admin/', include(admin.site.urls)),
	#(r'view', view),
	(r'getFileInfo/', getNewFileInfo),
	#(r'getFile/(?P<name>\d+)/', getFile),
	(r'getFile/', getFile),
	(r'getAllFile/', getAllFile),
)

