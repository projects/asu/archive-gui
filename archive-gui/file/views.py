# -*- coding: utf-8 -*-
# Create your views here.
from django.http import HttpResponse
from django.template import Context, loader
from django.template.loader import render_to_string
from main.models import *
from django.db import transaction
import os
import time
import sys
#import re
from operator import truth
#import hashlib
#from django.core.servers.basehttp import FileWrapper
import shutil
from django.db import models
from django.contrib.auth.decorators import login_required
import datetime

import hashlib
import zipfile
#import ho.pisa as pisa
import shlex, subprocess
import settings
from django.utils.encoding import force_unicode

def getFile (request):
	name = request.GET['name']
	mime = request.GET['mime']
	
	ff = open (OUT_FOLFER + name)
	
	#response = HttpResponse(FileWrapper(ff), content_type='image/jpg')
	response = HttpResponse(FileWrapper(ff), content_type=mime)
	#response['Content-Disposition'] = 'attachment; filename ='+ nnnn.object.klgi.__str__()
	response['Content-Disposition'] = 'attachment; filename = swewewe.jpg'
	
	return response
	
def getAllFile (request):
	ff = None
	try:
		op_id = request.REQUEST['op_id']
		op = HistoryOperation.objects.get( id = int(op_id) )
		ff = op.file
	except:
		try:
			node_id = request.REQUEST['node_id']
			node = Node.objects.get( id = int(node_id) )
			ff = node.listhistory.order_by('id').reverse()[0].file
		except:
			try:
				change_id = request.REQUEST['change_id']
				change = Change.objects.get( id = int(change_id) )
				ff = change.file
			except:
				pass

		
	if ff <> None:
		import shutil
		tmp_name = hashlib.sha1(  str( datetime.datetime.now() ) ).hexdigest()  + ff.ext
		shutil.copyfile(settings.IN_FOLDER + ff.file.name, settings.OUT_FOLDER + tmp_name)
		res = "{ file : '" + tmp_name +"'}"
	else:
		res= "{ file : false }"
		
	return HttpResponse(res)

def getFileInfo (request):
	node = request.GET['node']

	# определяем тип объекта... документ или список  и готовим файл для выкачивания
	nnnn = Node.objects.get(id = node)
	if nnnn.object.type.kind.canhavechildren:
		## список
		pass
	else:
		## документ
		ff = nnnn.lastoperation.file
		fd = open ( settings.OUT_FOLDER + "www", 'w')
		shutil.copyfileobj(ff.file, fd)
		
	
	t = loader.get_template('file_info.tpl')
	c = Context({
		'name': 'www',
		'success': 'true',
		'mime'	: ff.mime
	})
	return HttpResponse(t.render(c))

	return response


def getChildrenByNodeRecrusive(node, level):
	node.level = range(level)
	nodes_list = [node]
	nl = list(node.children.all())
	for i in nl:
		nodes_list = getChildrenByNodeRecrusive (i, level + 1) + nodes_list
	
	return nodes_list
	

def getNewFileInfo (request):
	
	type = request.REQUEST['type']
	fdate = datetime.datetime.strptime(request.REQUEST['date'],"%d-%m-%Y")
	fnumber = request.REQUEST['number']
	fcomments = request.REQUEST['msg']
	numberzakaz = int(request.REQUEST['inv'])
	user	= Autor.objects.get(id = int(request.REQUEST['a_name']))
	fdivision = Division.objects.get(id = int(request.REQUEST['a_group']))
	fbase = request.REQUEST['base']
	
	# создаем документ  Request
	r = Request( date = fdate, number = fnumber, comments = fcomments, numberzakaz = numberzakaz, user = user, division = fdivision, base = fbase )
	r.save()
	
	if type == "klgi":
		
		# получаем количество klgi - idnode
		count_items = int(request.REQUEST['klgi'])

		nodes_list = [] # список node
		for i in range (count_items):
			item = int(request.REQUEST['nid_klgi_'+str(i+1)])
			nodes_list.append(Node.objects.get(id = item))
	else:
		
		nodes_list = [] # список node
		root_node = Node.objects.get(id = int(request.REQUEST['nid']))
		nodes_list = getChildrenByNodeRecrusive (root_node, 0)
		
		#nodes_list.append(root_node)
		


		
	#TODO надо генерить случайное имя файла... пока номер документа
	
	filename = settings.OUT_FOLDER + fnumber
	
	zout = zipfile.ZipFile(filename+".zip", "w")
	
	for item in nodes_list:
		if item.lastoperation <> None and item.lastoperation.file <> None: 
			# создаем записи к документу  Request
			ri = RequestItem(request = r, node = item)
			ri.save()
			
			# добавляем файл в архив
			# TODO надо сделать проверку на наличие файла!!!
			ff = item.lastoperation.file 
			#zout.write(ff.file.name, arcname = item.lastoperation.file.hash)
			namez = item.object.klgi+ff.ext
			zout.write(settings.IN_FOLDER+ff.file.name, arcname = force_unicode(namez).encode("cp1251"))
		else:
			continue
		
		
	zout.close()
	
	###определяем тип объекта... документ или список  и готовим файл для выкачивания
	#nnnn = Node.objects.get(id = node);
	#if nnnn.object.type.kind.canhavechildren:
		### список
		#pass
	#else:
		### документ
		#ff = nnnn.lastoperation.file
		#fd = open ("/home/pav/git/archive-gui/archive/media/out/www", 'w')
		#shutil.copyfileobj(ff.file, fd)
	
	#template для файла
	#f = loader.get_template('list_pdf.tpl')
	cc = Context({
		'list'	: nodes_list
	})
	import codecs
	f = codecs.open(filename+'.doc', "w", "utf-8")
	f.write(render_to_string("list_pdf.tpl",cc))
	#sss = render_to_string("list_pdf.tpl",cc)
	#pdf = pisa.CreatePDF("Hello <strong>World</strong>", file('/home/pav/git/archive-gui/archive/media/out/www.pdf', "wb"))
	
	
	f.close()
	#TODO !!! падает при русских символах в html
	#subprocess.call(["xhtml2pdf", filename + ".shtml"])
	
	
	#template для формы
	t = loader.get_template('file_info.tpl')
	c = Context({
		'name'	: fnumber,
		'success': 'true',
		'mime'	: "application/zip",
		'list'	: nodes_list.reverse()
	})
	return HttpResponse(t.render(c))

	return response

