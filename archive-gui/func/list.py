# -*- coding: utf-8 -*-
from django.core.paginator import Paginator
from datetime import datetime

def ListFilterDate(request, list_all):
    try:
        date_from = int(request.REQUEST['from'])
        date_to = int(request.REQUEST['to'])
        start = datetime.fromtimestamp(date_from)
        end = datetime.fromtimestamp(date_to)
    except:
        list_res = list_all
    else:
        list_res = list_all.filter(date__range=(start, end))

    return list_res

def ListPaging(request, list_all):
    try:
        limit = int(request.REQUEST['limit'])
    except:
        limit = 1

    list_res = Paginator(list_all, limit)

    try:
        page = int(request.REQUEST['page'])
    except:
        try:
            start = int(request.REQUEST['start'])
        except:
            page = 1
        else:
            page = 1 + int( start / limit )

    try:
        list_page = list_res.page(page)
    except:
        page = list_res.num_pages
        list_page = list_res.page(page)

    return {'list':list_page.object_list, 'total':list_res.count, 'start':(page-1)*limit}