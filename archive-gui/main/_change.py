# -*- coding: utf-8 -*-
from django.http import HttpResponse
from django.db.models import Q
from main.models import *
from main.function import *
from main._log import *
import simplejson
import datetime

def aj_ChangeAdd(request):
    res = {'success' : 'false', 'msg' : 'Init' }
    req = HTTPDataControl(request, {'req':['date', 'number', 'a_group', 'a_name', 'format', 'sheets', 'msg', 'klgi'], 'file':['file']})

    if req == False:
        res['msg'] = 'Некоректные параметры'
    else:
        try:
            date = datetime.datetime.strptime(req['date'],"%d-%m-%Y")
            number = req['number']
            sheets = int(req['sheets'])
            format = FormatDoc.objects.get(id = int(req['format']))
            user = Autor.objects.get(id = int(req['a_name']))
            division = Division.objects.get(id = int(req['a_group']))
            comments = req['msg']
        except:
            res['msg'] = 'Некоректные параметры2'
        else:
            try:
                    inv = int(request.REQUEST['inv'])
            except:
                    inv = False
                    try:
                            reg = int(request.REQUEST['reg'])
                    except:
                            reg = 0
            else:
                    reg = 0
         
            count = int(req['klgi'])
            nodes = []
            try:
                for i in range(count+1)[1:count+1]:
                    n  = Node.objects.get(id = int(request.POST['nid_klgi_'+str(i)]))
                    nodes.append(n)
                    if n.status <> 's':
                        raise "Error"
            except:
                    res['msg'] = 'Ошибка в статусах документов'
            else:
                file = FileUpload(req['file'])
                change = Change (
                    date = date,
                    number = number,
                    reg_number = reg,
                    format = format,
                    sheets = sheets,
                    user = user,
                    division = division,
                    file = file,
                    comments = comments
                )
                change.save()
                
                if inv <> False:
                    inv = Invnumber (number = inv, content_object = change)
                    inv.save()
                    change.invnumber = inv
                    change.save()
                    operation = 3
                    status = 's'
                else:
                    operation = 4
                    status = 'o'
                
                i = 0
                for n in nodes:
                    i+=1
                    if status == 'o':
                        n_reg = str(reg) + '.' + str(i)
                    else:
                        n_reg = 0
                    f = FileUpload(request.FILES['file_klgi_'+str(i)])
                    op = HistoryOperation (reg_number = n_reg, date = date, operation_id = operation, file = f, change = change)
                    op.save()
                    n.reg_number = n_reg
                    n.status = status
                    if n_reg == 0:
                        n.lastoperation = op
                    n.listhistory.add (op)
                    n.save()
                
                log_addition (request, change, "создано новое извещение")
                res['success'] = 'true'
                res['msg'] = 'Извещение добавлено'

    return HttpResponse(simplejson.dumps(res))
    
    
def aj_ChangeAddFile(request):
    res = {'success' : 'false', 'msg' : 'Init' }
    req = HTTPDataControl(request, {'req':['change','klgi']})

    if req == False:
        res['msg'] = 'Некоректные параметры'
    else:
	try:
            change = Change.objects.get(id = int(req['change']), invnumber__id = None)
        except:
            res['msg'] = 'Несуществующее извещение'
        else:
    	    res['msg'] = 'Извещение найдено'
    	    currentNodes = changeChanges(change)
    	    currentNodesCount = 0
    	    for cn in currentNodes:
    		currentNodesCount = currentNodesCount+1
    	    count = int(req['klgi'])
            nodes = []
            try:
                for i in range(count+1)[1:count+1]:
                    n  = Node.objects.get(id = int(request.POST['nid_klgi_'+str(i)]))
                    nodes.append(n)
                    if n.status <> 's':
                        raise "Error"
            except:
                res['msg'] = 'Ошибка в статусах документов'
            else:
                i = 0
                for n in nodes:
                    i+=1
                    n_reg = str(change.reg_number) + '.' + str(currentNodesCount+1)
                    currentNodesCount = currentNodesCount+1
                    f = FileUpload(request.FILES['file_klgi_'+str(i)])
                    op = HistoryOperation (reg_number = n_reg, date = change.date, operation_id = 4, file = f, change = change)
                    op.save()
                    n.reg_number = n_reg
                    n.status = 'o'
                    n.listhistory.add (op)
                    n.save()
                
        	log_addition (request, change, "создано новое извещение")
                res['success'] = 'true'
                res['msg'] = 'Извещение добавлено'
    return HttpResponse(simplejson.dumps(res))
    
def aj_ChangeDeleteFile(request):
    res = {'success' : 'false', 'msg' : 'Init' }
    req = HTTPDataControl(request, {'req':['selectedContent', 'nodeId']})

    if req == False:
        res['msg'] = 'Некоректные параметры'
    else:
	try:
	    #lastOper = HistoryOperation.objects.get(change_id = int(request.POST['selectedContent']), node_link_id = int(request.POST['nodeId']))
	    #lastOper.delete()
	    
	    #prevOper = HistoryOperation.objects.get(change_id = None, operation_id = 1, node_link_id = int(request.POST['nodeId']))
	    
	    node = Node.objects.get(id = int(request.POST['nodeId']))
	    histoperlist = node.listhistory.all().iterator()
	    for ho in histoperlist:
		if (ho.operation_id==1):
		    op = HistoryOperation (reg_number = None, date = ho.date, operation_id = 9, file = ho.file, change = None)
	            op.save()
	            node = Node.objects.get(id = int(request.POST['nodeId']))
		    node.reg_number = '0'
	            node.status = 's'
	            node.listhistory.add (op)
	            node.save()
		else:
		    ho.delete()
            res['success'] = 'true'
            res['msg'] = 'Файл удалён'
        except:
            res['msg'] = 'Ошибка удаления'
    return HttpResponse(simplejson.dumps(res))