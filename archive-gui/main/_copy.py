# -*- coding: utf-8 -*-
from django.http import HttpResponse,HttpResponseRedirect
from django.template import Context, loader
from django.db import transaction
from main.models import *
from main._log import *
from main.function import *
import datetime

def CopyNode (request, new_project, new_folder, last_folder):
	tree = new_getAllTree(last_folder, True)
	level = 0
	parents = [new_folder]
	for ln in tree:
		if ln['level'] > level:
			parents.append(n)
			level += 1
		elif ln['level'] < level:
			parents.pop()
			level -= 1
			
		op = HistoryOperation (
			date = datetime.datetime.now(),
			operation = Operation.objects.get(id = 2)
		)
		op.save()
		
		ob = ln['node'].object
		n = Node (
			object = ob,
			lastoperation = op,
			parent = parents[ln['level']],
			project = new_project,
			status = 's'
		)
		n.save()
		
		op.node_link=n
		op.save()
		
		log_addition (request, ob, "создана копия объекта")

@transaction.commit_manually
def aj_CopyNode (request):
	res_success = 'false'
	res_msg = 'Init'
	res_node = 1
	res_parent = 1

	try:
		req_new_project_id = int(request.POST['to_pid'])
		req_new_folder_id = int(request.POST['to_nid'])
		req_last_folder_id = int(request.POST['nid'])
	except:
		res_msg = 'Некоректные параметры'
	else:
		try:
			new_project = Node.objects.get(id = req_new_project_id)
			new_folder = Node.objects.get(id = req_new_folder_id)
			last_folder = Node.objects.get(id = req_last_folder_id)
		except:
			res_msg = 'Некоторые ноды не существуют'
		else:
			cycle = False
			level = new_folder.parent
			while level <> False:
				if level == last_folder:
					cycle = True
					level = False
				else:
					try:
						level = level.parent
					except:
						level = False
			
			if cycle == True:
				transaction.rollback()
				res_msg = 'Нельзя копировать в дочерние изделия'
			else:
				try:
					CopyNode (request, new_project, new_folder, last_folder)
				except:
					transaction.rollback()
					res_msg = 'Ошибка в copyNode()'
				else:
					transaction.commit()
					res_success = 'true'
					res_msg = 'Изделие скопировано'
					res_node = new_folder.id
					res_parent = new_project.id

	t = loader.get_template('new/act_res.tpl')
	c = Context({
		'success': res_success,
		'msg': res_msg,
		'node': res_node,
		'parent': res_parent,
	})
	
	return HttpResponse(t.render(c))