# -*- coding: utf-8 -*-
from django.http import HttpResponse,HttpResponseRedirect
from django.template import Context, loader
from main.models import *
from main._log import *
import simplejson

def aj_DictDocumentTypeList (request):
	l = ObjectType.objects.filter( kind__id = 3).order_by('shortname')
	res = []
	for it in l:
		view = it.name
		if it.shortname <> '':
			view = it.shortname + " - " + view
		res.append({'id':it.id, 'name':it.name, 'short':it.shortname, 'view':view})
	return HttpResponse(simplejson.dumps(res))

def aj_DictAutorList (request):
	l = Autor.objects.all().order_by('name')
	res = []
	for it in l:
		res.append({'id':it.id, 'name':it.name})
	return HttpResponse(simplejson.dumps(res))

def aj_DictAutorAdd (request):
	res = { 'success':'false', 'msg':'Init' }
	
	try:
		req_name = request.REQUEST['name']
	except:
		res['msg'] = 'Некоректные параметры'
	else:
		a_find = Autor.objects.filter( name = unicode(req_name) ).count()
		if a_find > 0 :
			res['msg'] = 'Сотрудник с такой фамилией уже есть'
		else:
			try:
				a = Autor( name = req_name )
				a.save()
			except:
				res['msg'] = 'Ошибка при создании'
			else:
				res = {'success':'true','msg':'Сотрудник создан'}
	
	return HttpResponse(simplejson.dumps(res))

def aj_DictAutorEdit (request):
	res = { 'success' : 'false', 'msg' : 'Init' }
	
	try:
		req_id = request.REQUEST['id']
		req_name = request.REQUEST['name']
	except:
		res['msg'] = 'Некоректные параметры'
	else:
		try:
			a = Autor.objects.get( id = req_id )
		except:
			res['msg'] = 'Нет сотрудника'
		else:
			if req_name == a.name :
				res = { 'success' : 'true', 'msg' : 'Сотрудник не поменялся' }
			else:
				a_find = Autor.objects.filter( name = unicode(req_name) ).count()
				if a_find > 0 :
					res['msg'] = 'Сотрудник с такой фамилией уже есть'
				else:
					try:
						a.name = req_name
						a.save()
					except:
						res['msg'] = 'Ошибка при редактировании'
					else:
						res = { 'success' : 'true','msg' : 'Сотрудник отредактирован' }
	
	return HttpResponse(simplejson.dumps(res))