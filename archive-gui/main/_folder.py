# -*- coding: utf-8 -*-
from django.http import HttpResponse,HttpResponseRedirect
from django.template import Context, loader
from main.models import *
from main._log import *
import datetime

def FolderAdd (fr_parent, fr_name, fr_klgi, fr_desc):
	o = Object (name = fr_name, type = ObjectType.objects.get(id = 3), klgi = fr_klgi, comments = fr_desc)
	o.save ()

	op = HistoryOperation (operation = Operation.objects.get(id = 1), file = None, date = datetime.datetime.now())
	op.save()

	n = Node (object = o, parent = fr_parent, lastoperation = op, status ='s')
	n.save()
	
	n.project = fr_parent.project
	n.save()
	
	op.node_link = n
	op.save()
	
	return {'obj':o,'node':n}

def FolderEdit (fr, fr_name, fr_klgi, fr_desc):
	o = fr.object
	o.name = fr_name
	o.klgi = fr_klgi
	o.comments = fr_desc
	o.save()
	
	return {'obj':o,'node':fr}

def aj_FolderAdd (request):
	res_success = 'false'
	res_msg = 'Init'
	res_node = 1
	res_parent = 1
	
	try:
		req_parent_id = int(request.REQUEST['nid'])
		req_name = request.REQUEST['name']
		req_klgi = request.REQUEST['klgi']
		req_desc = request.REQUEST['desc']
	except:
		res_msg = 'Некоректные параметры'
	else:
		try:
			parent = Node.objects.get(id = req_parent_id)
		except:
			res_msg = 'Несуществующий родитель'
		else:
			if parent.object.type.id > 3:
				res_msg = 'Родитель не может быть документом'
			else:
				try:
					res = FolderAdd (parent, req_name, req_klgi, req_desc)
					log_addition (request, res['obj'], "Создано новое изделие")
				except:
					res_msg = 'Ошибка в FolderAdd() или log_addition()'
				else:
					res_success = 'true'
					res_msg = 'Изделие создано'
					res_node = res['node'].id
					res_parent = parent.id

	t = loader.get_template('new/act_res.tpl')
	c = Context({
		'success': res_success,
		'msg': res_msg,
		'node': res_node,
		'parent': res_parent,
	})
	
	return HttpResponse(t.render(c))

def aj_FolderEdit (request):
	res_success = 'false'
	res_msg = 'Init'
	res_node = 1
	res_parent = 1

	try:
		req_node_id = int(request.REQUEST['node'])
		req_name = request.REQUEST['name']
		req_klgi = request.REQUEST['klgi']
		req_desc = request.REQUEST['desc']
	except:
		res_msg = 'Некоректные параметры'
	else:
		try:
			node = Node.objects.get(id = req_node_id)
		except:
			res_msg = 'Несуществующий нод'
		else:
			if node.object.type.id <> 3:
				msg = 'Нод - не изделие'
			else:
				try:
					res = FolderEdit (node, req_name, req_klgi, req_desc)
					log_change (request, res['obj'], "Изменено изделие")
				except:
					res_msg = 'Ошибка в FolderEdit() или log_change()'
				else:
					res_success = 'true'
					res_msg = 'Изделие изменено'
					res_node = node.id
					res_parent = node.parent.id

	t = loader.get_template('new/act_res.tpl')
	c = Context({
		'success': res_success,
		'msg': res_msg,
		'node': res_node,
		'parent': res_parent,
	})
	
	return HttpResponse(t.render(c))