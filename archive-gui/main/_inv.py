# -*- coding: utf-8 -*-
from django.http import HttpResponse
from django.db.models import Q
from main.models import *
from main.function import *
import simplejson

def aj_InvNumberList(request):
    res_all = Invnumber.objects.order_by('number')

    try:
        query = request.REQUEST['query']
        if query == '':
            raise
    except:
        res_query = res_all
    else:
        res_query = res_all.filter(Q(number__contains = query))

    res_page = ListPaging(request, res_query)

    res_row = []
    for it in res_page['list']:
        e = {
            'id': it.id,
            'number': it.number,
            'type': 'error',
            'name': 'Ошибка',
            'obj_id': it.object_id,
            'parent_id': 0,
            'sign': 'Ошибка',
        }
     
        try:
            if it.content_type.model == 'object':
                e['type'] = 'object'
                e['name'] = it.content_object.type.name
                e['parent_id'] = it.content_object.nodes.all()[0].parent.object.id
                e['sign'] = it.content_object.klgi
            elif it.content_type.model == 'change':
                e['type'] = 'change'
                e['name'] = 'Извещение'
                e['sign'] = it.content_object.number
        except:
            pass
     
        res_row.append(e)

    res = {'total':res_page['total'], 'records':res_row}

    return HttpResponse(simplejson.dumps(res))