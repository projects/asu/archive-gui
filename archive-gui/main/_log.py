# -*- coding: utf-8 -*-
from django.contrib.admin.models import LogEntry, ADDITION, DELETION, CHANGE
from django.utils.encoding import force_unicode
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.models import User

def log_addition(request, object, message):
	LogEntry.objects.log_action(
		user_id         = request.user.pk,
		content_type_id = ContentType.objects.get_for_model(object).pk,
		object_id       = object.pk,
		object_repr     = force_unicode(object),
		action_flag     = ADDITION,
		change_message	= message
	)


def log_change(request, object, message):
	LogEntry.objects.log_action(
		user_id         = request.user.pk,
		content_type_id = ContentType.objects.get_for_model(object).pk,
		object_id       = object.pk,
		object_repr     = force_unicode(object),
		action_flag     = CHANGE,
		change_message	= message
	)

def log_deletion(request, object, message):
	LogEntry.objects.log_action(
		user_id         = request.user.pk,
		content_type_id = ContentType.objects.get_for_model(object).pk,
		object_id       = object.pk,
		object_repr     = force_unicode(object),
		action_flag     = DELETION,
		change_message	= message
	)