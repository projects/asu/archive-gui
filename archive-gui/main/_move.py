# -*- coding: utf-8 -*-
from django.http import HttpResponse,HttpResponseRedirect
from django.template import Context, loader
from django.db import transaction
from main.models import *
from main._log import *
from main.function import *
import datetime

def MoveNode (request, new_project, new_folder, last_folder):
	if last_folder <> new_folder:
		if last_folder.project <> new_project:
			last_folder.project = new_project
			tree = new_getAllTree(last_folder, True)
			
			for ln in tree:
				ln['node'].project = new_project
				ln['node'].save()
	
		last_folder.parent = new_folder
		last_folder.save()

@transaction.commit_manually
def aj_MoveNode (request):
	res_success = 'false'
	res_msg = 'Init'
	res_node = 1
	res_parent = 1

	try:
		req_new_project_id = int(request.POST['to_pid'])
		req_new_folder_id = int(request.POST['to_nid'])
		req_last_folder_id = int(request.POST['nid'])
	except:
		res_msg = 'Некоректные параметры'
	else:
		try:
			new_project = Node.objects.get(id = req_new_project_id)
			new_folder = Node.objects.get(id = req_new_folder_id)
			last_folder = Node.objects.get(id = req_last_folder_id)
		except:
			res_msg = 'Некоторые ноды не существуют'
		else:
			cycle = False
			level = new_folder.parent
			while level <> False:
				if level == last_folder:
					cycle = True
					level = False
				else:
					try:
						level = level.parent
					except:
						level = False
			
			if cycle == True:
				transaction.rollback()
				res_msg = 'Нельзя перемещать в дочерние изделия'
			else:
				last_parent = last_folder.parent
				try:
					MoveNode (request, new_project, new_folder, last_folder)
				except:
					transaction.rollback()
					res_msg = 'Ошибка в moveNode()'
				else:
					transaction.commit()
					res_success = 'true'
					res_msg = 'Изделие перенесено'
					res_node = last_parent.id
					res_parent = new_folder.id

	t = loader.get_template('new/act_res.tpl')
	c = Context({
		'success': res_success,
		'msg': res_msg,
		'node': res_node,
		'parent': res_parent,
	})
	
	return HttpResponse(t.render(c))