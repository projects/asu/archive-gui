# -*- coding: utf-8 -*-
from django.http import HttpResponse,HttpResponseRedirect
from django.template import Context, loader
from main.models import *
from main._log import *
import datetime

def ProjectAdd (pr_name, pr_desc):
	o = Object (name = pr_name, type = ObjectType.objects.get(id = 2), comments = pr_desc)
	o.save ()

	op = HistoryOperation (operation = Operation.objects.get(id = 1), file = None, date = datetime.datetime.now())	
	op.save()

	n_root = Node.objects.get(id = 1)
	n = Node (object = o, parent = n_root, lastoperation = op, status ='s')
	n.save()
	
	n.project = n
	n.save()
	
	op.node_link = n
	op.save()
	
	return {'obj':o,'node':n}

def ProjectEdit (pr, pr_name, pr_desc):
	o = pr.object
	o.name = pr_name
	o.comments = pr_desc
	o.save()
	
	return {'obj':o,'node':pr}

def aj_ProjectAdd (request):
	res_success = 'false'
	res_node = 1
	res_msg = 'Init'

	try:
		req_name = request.REQUEST['name']
		req_desc = request.REQUEST['desc']
	except:
		res_msg = 'Некоректные параметры'
	else:
		try:
			res = ProjectAdd (req_name, req_desc)
			log_addition (request, res['obj'], "Создан новый проект")
		except:
			res_msg = 'Ошибка в ProjectAdd() или log_addition()'
		else:
			res_success = 'true'
			res_msg = 'Проект создан'
			res_node = res['node'].id

	t = loader.get_template('new/act_res.tpl')
	c = Context({
		'success': res_success,
		'msg': res_msg,
		'node': res_node,
		'parent': 1,
	})
	
	return HttpResponse(t.render(c))

def aj_ProjectEdit (request):
	res_success = 'false'
	res_node = 1
	res_msg = 'Init'

	try:
		req_node_id = int(request.REQUEST['node'])
		req_name = request.REQUEST['name']
		req_desc = request.REQUEST['desc']
	except:
		res_msg = 'Некоректные параметры'
	else:
		try:
			node = Node.objects.get(id = req_node_id)
		except:
			res_msg = 'Не существующий нод'
		else:
			if node <> node.project:
				res_msg = 'Нод - не проект'
			else:
				try:
					res = ProjectEdit (node, req_name, req_desc)
					log_change (request, res['obj'], "Изменён проект")
				except:
					res_msg = 'Ошибка в ProjectEdit() или log_change()'
				else:
					res_success = 'true'
					res_msg = 'Проект изменён'
					res_node = node.id

	t = loader.get_template('new/act_res.tpl')
	c = Context({
		'success': res_success,
		'msg': res_msg,
		'node': res_node,
		'parent': 1,
	})
	
	return HttpResponse(t.render(c))