# -*- coding: utf-8 -*-
from django.http import HttpResponse
from django.db.models import Q
from main.models import *
from main.function import *
from main._log import *
import simplejson
import datetime

def aj_TChangeApply(request):
    res = {'success' : 'false', 'msg' : 'Init' }
    req = HTTPDataControl(request, {'req':['change','inv']})
    if req == False:
        res['msg'] = 'Некоректные параметры'
    else:
        try:
            change = Change.objects.get(id = int(req['change']), invnumber__id = None)
        except:
            res['msg'] = 'Несуществующее извещение'
        else:
            nodes = changeChanges(change)
            waiting = False
            for node in nodes:
                if node.status <> 'w':
                    waiting = True
                    break
         
            if waiting <> False:
                res['msg'] = 'Не все документы прошли НК'
            else:
                inv = Invnumber (number = int(req['inv']), content_object = change)
                inv.save()
                change.invnumber = inv
                change.save()
                for node in nodes:
                    last_op = node.listhistory.order_by('id').reverse()[0]
                    op = HistoryOperation (
                        reg_number = change.reg_number,
                        operation_id = 3,
                        file = last_op.file,
                        date = datetime.datetime.now(),
                        change = change
                    )
                    op.save()
                    node.status = 's'
                    node.lastoperation = op
                    node.listhistory.add (op)
                    node.save()
                res['success'] = 'true'
                res['msg'] = 'Извещение принято'

    return HttpResponse(simplejson.dumps(res))

def aj_TChangeList(request):
    res_all = Change.objects.filter(invnumber__id = None).order_by('reg_number').reverse()

    try:
        query = request.REQUEST['query']
        if query == '':
            raise
    except:
        res_query = res_all
    else:
        res_query = res_all.filter(Q(reg_number__contains = query) | Q(number__contains = query))

    res_page = ListPaging(request, res_query)

    res_row = []
    for c in res_page['list']:
        nodes = changeChanges(c)
        waiting = False
        res_node = []
        project_id = 0
        for it in nodes:
            res_node.append({
                'reg_number': it.reg_number,
                'status': it.status,
                'status_desc': it.get_status_display(),
                'obj_id': it.object.id,
                'name': it.object.name,
                'klgi': it.object.klgi,
                'desc': it.object.comments,
                't_name': it.object.type.name,
                'node_id': it.id,
                'p_id': it.parent.id,
                'p_name': it.parent.object.name,
                'pr_id': it.project.id,
                'pr_name': it.project.object.name,
                'gname': it.object.division.name,
                'aname': it.object.developer.name,
                'f_name': it.object.format.name,
                'sheets': it.object.sheets,
                'd_add': '-',
                'd_edit': '-'
            })
            project_id = it.project.id
            if it.status <> 'w':
                waiting = True

        res_row.append({
            'reg_number': c.reg_number,
            'number': c.number,
            'change_id': c.id,
            'gname': c.division.name,
            'aname': c.user.name,
            'waiting': waiting,
            'nodes': res_node,
            'project_id': project_id
        })

    res = {'total':res_page['total'], 'records':res_row}

    return HttpResponse(simplejson.dumps(res))
    
def aj_TChangeOne(request):
    res_all = Change.objects.filter(invnumber__id = None).order_by('reg_number').reverse()

    try:
        query = request.REQUEST['query']
        if query == '':
            raise
    except:
        res_query = res_all
    else:
        res_query = res_all.filter(Q(id__contains = query))

    res_page = ListPaging(request, res_query)

    res_row = []
    for c in res_page['list']:
        nodes = changeChanges(c)
        waiting = False
        res_node = []
        for it in nodes:
            res_row.append({
                'reg_number': it.reg_number,
                'status': it.status,
                'status_desc': it.get_status_display(),
                'obj_id': it.object.id,
                'name': it.object.name,
                'klgi': it.object.klgi,
                'desc': it.object.comments,
                't_name': it.object.type.name,
                'node_id': it.id,
                'p_id': it.parent.id,
                'p_name': it.parent.object.name,
                'pr_id': it.project.id,
                'pr_name': it.project.object.name,
                'gname': it.object.division.name,
                'aname': it.object.developer.name,
                'f_name': it.object.format.name,
                'sheets': it.object.sheets,
                'd_add': '-',
                'd_edit': '-',
                'change_id':c.id
            })

    res = {'total':res_page['total'], 'records':res_row}

    return HttpResponse(simplejson.dumps(res))