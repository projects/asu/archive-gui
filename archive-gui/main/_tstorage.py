# -*- coding: utf-8 -*-
from django.http import HttpResponse
from django.db.models import Q
from main.models import *
from main.function import *
from main._log import *
import simplejson
import datetime

def aj_TStorageNew(request):
    res = {'success' : 'false', 'msg' : 'Init' }
    req = HTTPDataControl(request, {'req':['node', 'reg'], 'file':['file']})

    if req == False:
        res['msg'] = 'Некоректные параметры'
    else:
        try:
            node = Node.objects.get(id = int(req['node']))
        except:
            res['msg'] = 'Несуществующий нод'
        else:
            if node.status <> 'c':
                res['msg'] = 'Не верный статус'
            else:
                file = FileUpload(req['file'])
                last_op = node.listhistory.order_by('id').reverse()[0]
                op = HistoryOperation (
                    reg_number = int(req['reg']),
                    operation_id = 4,
                    file = file,
                    date = datetime.datetime.now()
                )
                if last_op.change_id <> None:
                    op.change_id = last_op.change_id
                op.save()
                node.reg_number = int(req['reg'])
                node.listhistory.add (op)
                node.status = 'o'
                node.save()
                res['success'] = 'true'
                res['msg'] = 'Файл обновлен'

    return HttpResponse(simplejson.dumps(res))

def aj_TStorageApply(request):
    res = {'success' : 'false', 'msg' : 'Init' }
    req = HTTPDataControl(request, {'req':['node']})
    if req == False:
        res['msg'] = 'Некоректные параметры'
    else:
        try:
            node = Node.objects.get(id = int(req['node']))
            if node.object.invnumber_id == None:
                req['inv'] = int(request.REQUEST['inv'])
        except:
            res['msg'] = 'Несуществующий нод'
        else:
            if node.status <> 'p':
                res_msg = 'Не верный статус'
            else:
                if node.object.invnumber_id == None:
                    inv = Invnumber (number = int(req['inv']), content_object = node.object)
                    inv.save()
                    node.object.invnumber = inv
                    node.object.save()
                last_op = node.listhistory.order_by('id').reverse()[0]
                op = HistoryOperation (
                    reg_number = node.reg_number,
                    operation_id = 6,
                    file = last_op.file,
                    date = datetime.datetime.now()
                )
                if last_op.change_id <> None:
                    op.change_id = last_op.change_id
                    op.operation_id = 8
                    op.save()
                    node.status = 'w'
                    res['msg'] = 'НК пройдено. Документ ожидает извещения'
                else:
                    op.save()
                    node.lastoperation = op
                    node.status = 's'
                    res['msg'] = 'НК пройдено. Документ перемещён в архив'
                node.listhistory.add (op)
                node.save()
                res['success'] = 'true'

    return HttpResponse(simplejson.dumps(res))

def aj_TStorageCancel(request):
    res = {'success' : 'false', 'msg' : 'Init' }
    req = HTTPDataControl(request, {'req':['node']})
    if req == False:
        res['msg'] = 'Некоректные параметры'
    else:
        try:
            node = Node.objects.get(id = int(req['node']))
        except:
            res['msg'] = 'Несуществующий нод'
        else:
            if node.status <> 'p':
                res['msg'] = 'Не верный статус'
            else:
                last_op = node.listhistory.order_by('id').reverse()[0]
                op = HistoryOperation (
                    reg_number = node.reg_number,
                    operation_id = 7,
                    file = last_op.file,
                    date = datetime.datetime.now()
                )
                if last_op.change_id <> None:
                    op.change_id = last_op.change_id
                op.save()
                node.listhistory.add (op)
                node.status = 'c'
                node.save()
                res['success'] = 'true'
                res['msg'] = 'НК не пройдено. Документ аннулирован'

    return HttpResponse(simplejson.dumps(res))

def aj_TStoragePrint(request):
    res = {'success' : 'false', 'msg' : 'Init' }
    req = HTTPDataControl(request, {'req':['node']})
    if req == False:
        res['msg'] = 'Некоректные параметры'
    else:
        try:
            node = Node.objects.get(id = int(req['node']))
        except:
            res['msg'] = 'Несуществующий нод'
        else:
            if node.status <> 'o':
                res['msg'] = 'Не верный статус'
            else:
                last_op = node.listhistory.order_by('id').reverse()[0]
                op = HistoryOperation (
                    reg_number = node.reg_number,
                    operation_id = 5,
                    file = last_op.file,
                    date = datetime.datetime.now()
                )
                if last_op.change_id <> None:
                    op.change_id = last_op.change_id
                op.save()
                node.listhistory.add (op)
                node.status = 'p'
                node.save()
                res['success'] = 'true'
                res['msg'] = 'Документ передан в НК'

    return HttpResponse(simplejson.dumps(res))

def aj_TStorageList(request):
    res_all = Node.objects.filter(status__in = ['o','p','c','w'], object__type__kind__id = 3).order_by('reg_number').reverse()

    try:
        query = request.REQUEST['query']
        if query == '':
            raise
    except:
        res_query = res_all
    else:
        res_query = res_all.filter(Q(reg_number__startswith = query) | Q(object__klgi__contains = query))

    res_page = ListPaging(request, res_query)

    res_row = []
    num = res_page['start']
    for it in res_page['list']:
        num += 1
        status = it.status
        if status == 'p' and it.object.invnumber_id <> None:
            status = 'i'
        res_row.append({
            'reg_number': it.reg_number,
            'status': status,
            'status_desc': it.get_status_display(),
            'obj_id': it.object.id,
            'name': it.object.name,
            'klgi': it.object.klgi,
            'desc': it.object.comments,
            't_name': it.object.type.name,
            'node_id': it.id,
            'p_id': it.parent.id,
            'p_name': it.parent.object.name,
            'pr_id': it.project.id,
            'pr_name': it.project.object.name,
            'gname': it.object.division.name,
            'aname': it.object.developer.name,
            'f_name': it.object.format.name,
            'sheets': it.object.sheets,
            'd_add': '-',
            'd_edit': '-'
        })

    res = {'total':res_page['total'], 'records':res_row}

    return HttpResponse(simplejson.dumps(res))