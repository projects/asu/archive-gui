# -*- coding: utf-8 -*-
from main.models import *
from django.contrib import admin

class ObjectAdmin(admin.ModelAdmin):
	search_fields = ['klgi', 'name']

class InvnumberAdmin(admin.ModelAdmin):
	search_fields = ['number']

class AutorAdmin(admin.ModelAdmin):
	search_fields = ['name']

#admin.site.register(Node)
admin.site.register(Object, ObjectAdmin)
admin.site.register(ObjectType)
#admin.site.register(TypeKind)
admin.site.register(File)
admin.site.register(Operation)
admin.site.register(Invnumber, InvnumberAdmin)
#admin.site.register(HistoryOperation)
admin.site.register(Division)
admin.site.register(FormatDoc)
#admin.site.register(Change)
#admin.site.register(Request)
#admin.site.register(UserSession)
#admin.site.register(TestType)
#admin.site.register(Test)
#admin.site.register(HealthObject)
#admin.site.register(HealthObjectType)
admin.site.register(Autor, AutorAdmin)


