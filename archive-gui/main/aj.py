from django.http import HttpResponse
from main.models import *
import os
import simplejson
import time_control

def aj_InvValidate (request):
	req = 0
        try:
            req = int(request.REQUEST.get('inv'))
        except:
            pass
	
	valid = True
	inv = Invnumber.objects.filter(number = req).count()
	if inv <> None and inv > 0:
		valid = False
	
	res = { 'valid': valid, 'inv': req }
	return HttpResponse(simplejson.dumps(res))

def aj_SignValidate (request):
	req = request.REQUEST.get('sign')
	
	valid = True
	if req <> None and req <> 'None':
		obj = Object.objects.filter(klgi__iexact = req).count()
		ch = Change.objects.filter(number__iexact = req).count()
		if ( ( obj + ch ) > 0 ) :
			valid = False
	
	res = { 'valid': valid, 'sign': req }
	return HttpResponse(simplejson.dumps(res))


def aj_login (request):
	success = 'false'
	
	try:
		login = request.REQUEST['login']
		passw = request.REQUEST['pass']
	except:
		pass
	else:
		success = time_control.LogIn (request, login, passw)
	
	res = {'success': success}
	return HttpResponse(simplejson.dumps(res))
	
def aj_logout (request):
	success = time_control.LogOut (request, 'a')
	
	res = {'success': success}
	return HttpResponse(simplejson.dumps(res))
'''
def aj_checkCritical (request):
	status = 0
	if os.path.isfile(settings.LOCK_FOLDER+"warning"):
		status = 1
	if os.path.isfile(settings.LOCK_FOLDER+"critical"):
		status = 2
	
	login = True
	if request.user.id == None:
		login = False

	res = {'status': status, 'login': login}
	return HttpResponse(simplejson.dumps(res))
'''
def aj_RemoveStatuses (request):
	if os.path.isfile(settings.LOCK_FOLDER+"warning"):
		os.remove(settings.LOCK_FOLDER+"warning")
	if os.path.isfile(settings.LOCK_FOLDER+"critical"):
		os.remove(settings.LOCK_FOLDER+"critical")
	return HttpResponse()