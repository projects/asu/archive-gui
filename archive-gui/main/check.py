from django.http import HttpResponse
import settings
import os
import simplejson

def aj_check (request):
    if os.path.isfile(settings.LOCK_FOLDER+"warning"):
        warning = True
    else:
        warning = False

    if os.path.isfile(settings.LOCK_FOLDER+"critical"):
        critical = True
    else:
        critical = False

    if request.user.id <> None:
        auth = True
    else:
        auth = False

    res = {
        'warning': warning,
        'critical': critical,
        'auth': auth
    }
    return HttpResponse(simplejson.dumps(res))