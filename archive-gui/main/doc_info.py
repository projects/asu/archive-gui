# -*- coding: utf-8 -*-
from django.http import HttpResponse
from main.models import *
import simplejson

def aj_getDocumentInformation(request):
    res = 'error'
    try:
        node = Node.objects.get( id = int(request.REQUEST['node_id']), object__type__kind__id = 3 )
    except:
        pass
    else:
        res_history = []
        history_stable = node.listhistory.order_by('id').reverse().filter(operation__id__in = [1,2,3,6])
        try:
            history_stable_now = history_stable[0].id
        except:
            history_stable_now = 0
        try:
            history_stable_prev = history_stable[1].id
        except:
            history_stable_prev = 0
        history_list = node.listhistory.order_by('id').all()
        print history_stable_now
        print history_stable_prev
        
        
        for c in history_list:
            c_o ={
                #'id': 0,
                'op_name': c.operation.name,
                'date': c.date.strftime("%d-%m-%Y"),
                'ch_id': '-',
                'ch_number': '-',
                'reg_number': c.reg_number,
            }
            
            if c.id > history_stable_now or c.id <= history_stable_prev :
                c_o['id'] = c.id
            
            try:
                c_o['ch_id'] = c.change.id
                c_o['ch_number'] = c.change.number
            except:
                pass
            
            res_history.append(c_o)
        
        res_request = []
        request_list = node.requests.order_by('id').all()
        for r in request_list:
            res_request.append({
                'id': r.request.id,
                'date': r.request.date.strftime("%d-%m-%Y"),
                'number': r.request.number,
                'zakaz': r.request.numberzakaz,
                'user': r.request.user.name,
            })
        
        res_info = {
            'obj_id' :  node.object.id,
            'node_id' : node.id,
            'name' :    node.object.name,
            'klgi' :    node.object.klgi,
            'desc' :    node.object.comments,
            't_id' :    node.object.type.id,
            't_name' :  node.object.type.name,
            'p_id' :    '-',
            'p_name' :  '-',
            'pr_id' :   '-',
            'pr_name' : '-',
            'a_group' : node.object.division.id,
            'gname' :   node.object.division.name,
            'a_name' :  node.object.developer.id,
            'aname' :   node.object.developer.name,
            'f_id' :    '-',
            'f_name' :  '-',
            'sheets' :  node.object.sheets,
            'reg' :     node.reg_number,
            'd_add' :   node.listhistory.all()[0].date.strftime("%d-%m-%Y"),
            'd_edit' :  node.listhistory.all().order_by('id').reverse()[0].date.strftime("%d-%m-%Y"),
            'ch_number':'Origin',
            'inv' :     '-'
        }
        
        try:
            res_info['p_id'] = node.parent.id
            res_info['p_name'] = node.parent.object.klgi + ' ' + node.parent.object.name;
        except:
            pass
        
        try:
            res_info['pr_id'] = node.project.id
            res_info['pr_name'] = node.project.object.name
        except:
            pass
        
        try:
            res_info['f_id'] = node.object.format.id
            res_info['f_name'] = node.object.format.name
        except:
            pass
        
        try:
            res_info['ch_number'] = node.lastoperation.change.number
        except:
            pass
         
        try:
            res_info['inv'] = node.object.invnumber.number
        except:
            pass
        
        res = {
            'info':res_info,
            'history':{'records':res_history},
            'request':{'records':res_request}
        }
    
    return HttpResponse(simplejson.dumps(res))