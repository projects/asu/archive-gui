# -*- coding: utf-8 -*-
from django.http import HttpResponse
from django.db.models import Q
from main.models import *
from main.function import *
import simplejson
import datetime

def aj_FilesNew(request):
    res = {'success' : 'false', 'msg' : 'Init' }
    req = HTTPDataControl(request, {'req':['file_id'], 'file':['file']})

    if req == False:
        res['msg'] = 'Некоректные параметры'
    else:
        try:
            file = File.objects.get(id = int(req['file_id']))
        except:
            res['msg'] = 'Несуществующий файл'
        else:
            req_file = req['file']
            hs = hashlib.sha1(req_file.read()).hexdigest()
            mime = req_file.content_type
            ext = os.path.splitext(req_file.name)[-1]
            file.hash = hs
            file.file = req_file
            file.mime = mime
            file.correct = 1
            file.correct_time = None
            file.copied = 0
            file.copied_time = None
            file.ext = ext
            file.file.name = hs
            file.save()
            res['success'] = 'true'
            res['msg'] = 'Файл заменён'

    return HttpResponse(simplejson.dumps(res))


def aj_FilesList(request):
    res_all = File.objects.order_by('id').reverse()

    try:
        query = request.REQUEST['query']
        if query == '':
            raise
    except:
        res_query = res_all
    else:
        res_query = res_all.filter(
            Q(hash__contains = query) |
            Q(operations__node_link__object__klgi__contains = query) |
            Q(operations__change__number__contains = query) |
            Q(change__number__contains = query)
        )

    res_page = ListPaging(request, res_query)

    res_row = []
    for f in res_page['list']:
        res_row_op = []
        
        f_ops = HistoryOperation.objects.filter( file = f )
        for f_op in f_ops:
            op_res = {
                'id': f_op.id,
                'reg_number': f_op.reg_number,
                'date': f_op.date.strftime("%m-%d-%Y"),
                'operation': f_op.operation.name,
                'change': '-',
                'node': '-',
            }
         
            try:
                op_res['change'] = f_op.change.number
            except:
                pass
            try:
                op_res['node'] = f_op.node_link.object.klgi
            except:
                pass
         
            res_row_op.append(op_res)
     
        f_res = {
            'id': f.id,
            'hash': f.hash,
            'mime': f.mime,
            'info': res_row_op,
            'info_count': len(res_row_op),
            'f_date': '-',
            'f_operation': '-',
            'f_change': '-',
            'f_node': '-',
        }
     
        if len(res_row_op) > 0:
            f_res['f_date'] = res_row_op[0]['date']
            f_res['f_operation'] = res_row_op[0]['operation']
            f_res['f_change'] = res_row_op[0]['change']
            f_res['f_node'] = res_row_op[0]['node']
        else:
            try:
                ch = f.change.all()[0]
            except:
                pass
            else:
                f_res['f_date'] = ch.date.strftime("%m-%d-%Y")
                f_res['f_operation'] = "Внесение извещения"
                f_res['f_change'] = ch.number
     
        res_row.append(f_res)

    res = {'total':res_page['total'], 'records':res_row}

    return HttpResponse(simplejson.dumps(res))