from django.core.paginator import Paginator
from main.models import *
import datetime
import hashlib
import os
'''
from main.function import *
HTTPDataControl(HTTPDataControl_r,HTTPDataControl_c)

HTTPDataControl_c={'get':['one'], 'object':[{'var':'one','class':Node,'field':'id'}]}
import django
HTTPDataControl_r=django.http.HttpRequest()
HTTPDataControl_r.GET['one']=2
'''

def HTTPDataControl(request,conf):
    req = {}

    def Check(var_all, loc):
        for var in var_all:
            try:
                req[var] = loc[var]
            except:
                return False
        return True

    try:
        c = conf['post']
    except:
        pass
    else:
        if Check(c, request.POST) == False:
            return False

    try:
        c = conf['get']
    except:
        pass
    else:
        if Check(c, request.GET) == False:
            return False

    try:
        c = conf['req']
    except:
        pass
    else:
        if Check(c, request.REQUEST) == False:
            return False

    try:
        c = conf['file']
    except:
        pass
    else:
        if Check(c, request.FILES) == False:
            return False

    '''    try:
        c = conf['object']
    except:
        pass
    else:
        for it in c:
            var = it['var']
            cl = it['class']
            field = it['field']
            try:
                req[var] = cl.objects.get( {field : req[var]} )
            except:
                return False'''

    return req

def FileUpload(req_file):
    hs = hashlib.sha1(req_file.read()).hexdigest()
    mime = req_file.content_type
    ext = os.path.splitext(req_file.name)[-1]
    res = File ( hash = hs, file = req_file, mime = mime, correct = 1, correct_time = None, copied_time = None, ext = ext)
    res.file.name = hs
    res.save()

    return res

def ListPaging(request, list_all):
    try:
        limit = int(request.REQUEST['limit'])
    except:
        limit = 1

    list_res = Paginator(list_all, limit)

    try:
        page = int(request.REQUEST['page'])
    except:
        try:
            start = int(request.REQUEST['start'])
        except:
            page = 1
        else:
            page = 1 + int( start / limit )

    try:
        list_page = list_res.page(page)
    except:
        page = list_res.num_pages
        list_page = list_res.page(page)

    return {'list':list_page.object_list, 'total':list_res.count, 'start':(page-1)*limit}

def new_getAllTree (start_node, folder_first):
	level = 0
	stack = [[start_node]]
	folder = [[]]
	document = [[]]
	
	while level > -1:
		if stack[level]:
			node = stack[level].pop()
			node_type = node.object.type
			node_append = {'node':node,'level':level,'type':node_type}
			if node_type.id < 4:
				folder[level].append(node_append)
				node_child = list(node.children.all())
				if node_child:
					level += 1
					stack.append(node_child)
					folder.append([])
					document.append([])
			else:
				document[level].append(node_append)
		elif level > 0:
			stack.pop()
			level -= 1
			if folder_first:
				folder[level] += folder.pop() + document.pop()
			else:
				folder[level] += document.pop() + folder.pop()
		else:
			return folder[0]
	return False

def new_getProjects ():
	list = Node.objects.filter(object__type__id = 2)
	return list

def new_getAllFolders (query,project_id):
	if project_id > 0:
		list_full = Node.objects.filter(project__id = project_id).exclude(object__type__kind__id = 3)
	else:	list_full = Node.objects.exclude(object__type__kind__id = 3)
		
	list_res = []
		
	if query <> "":
		for item in list_full:
			if (item.object.name.find(query) <> -1) or (item.object.klgi > 0 and item.object.klgi.find(query) <> -1):
				list_res.append (item)
	else:	list_res = list_full
	
	return list_res

def new_getChildrens (type_kind_id, parent_id):
	if parent_id > 0:
		list_res = Node.objects.filter(parent__id = parent_id, object__type__kind__id = type_kind_id)
	else:	list_res = Node.objects.filter(object__type__kind__id = type_kind_id)
		
	return list_res

def new_getPathReq (node):
	if node.parent <> node.project:
		list_res = new_getPatch (node.parent)
	else:	list_res=[]
		
	list_res.append (node)
		
	return list_res

def new_getPath (node_id):
	node = Node.objects.get(id = node_id)

	list_res=[]
	list_res.append (node)

	while node <> node.project:
		node = node.parent
		list_res.append (node)
	
	list_res.reverse()	
	return list_res

def new_filterDate(list,date_from,date_to):
	start = datetime.datetime.fromtimestamp(int(date_from))
	end = datetime.datetime.fromtimestamp(int(date_to))
		
	list_res = list.filter(date__range=(start, end))
		
	return list_res

def changeChanges(change):
	nodes = {}
	changes = change.changes.all().iterator()
	for it in changes:
		nodes[it.node_link_id] = it.node_link
	return nodes.values()
