from django.http import HttpResponse,HttpResponseRedirect
from django.core.paginator import Paginator
#http://docs.djangoproject.com/en/dev/topics/pagination/
from django.template import Context, loader
from django.db.models import Q
from function import *
import simplejson

def aj_getDocumentsInProject (request):
	l = Node.objects.filter( object__type__kind__id = 3 )
	try:
		project = Node.objects.get( id = int(request.REQUEST['idproject']), object__type__id = 2)
	except:
		pass
	else:
		l = l.filter( project = project )
	
	try:
		status_check = request.REQUEST['status']
	except:
		pass
	else:
		if status_check == "true":
			print status_check
			l = l.filter(status = "s")

	
	try:
		query = request.REQUEST['query']
	except:
		pass
	else:
		if query <> "":
			l = l.filter(Q(object__klgi__contains = query) | Q(object__name__startswith = query))
	
	res = []
	for it in l:
		res.append({'id':it.id, 'name':(it.object.klgi + ' - ' + it.object.name)})
	
	return HttpResponse(simplejson.dumps(res))


def aj_getPath (request):
	node_id = int(request.REQUEST['id'])

	list = new_getPath(node_id)
	t = loader.get_template('new/list_path.tpl')
	c = Context({ 'list': list, })
	return HttpResponse(t.render(c))

def aj_getProjects (request):
	list = new_getProjects()
	t = loader.get_template('new/list_projects.tpl')
	c = Context({ 'list': list, })
	return HttpResponse(t.render(c))

def aj_getFoldersInProject (request):
	query = request.POST['query']
	project_id = int(request.POST['pr_id'])
	
	list = new_getAllFolders(query,project_id)
	t = loader.get_template('new/list_folders.tpl')
	c = Context({ 'list': list, })
	return HttpResponse(t.render(c))

def aj_getDocumentsInFolder (request):
	parent_id = int(request.POST['p_id'])
	
	list = new_getChildrens(3, parent_id)
	t = loader.get_template('new/list_documents.tpl')
	c = Context({ 'list': list, })
	return HttpResponse(t.render(c))

def aj_getReport (request):
	limit = int(request.REQUEST['limit'])
	start = int(request.REQUEST['start'])
	page = 1 + int( start / limit )
	
	try:
		report_type = request.REQUEST['type']
	except:
		report_type = 'request'

	if report_type == 'change':
		list = Change.objects.all()
	else:
		report_type = 'request'
		list = Request.objects.all()
	
	try:
		list = new_filterDate(list,request.REQUEST['dateFrom'],request.REQUEST['dateTo'])
	except:
		pass
	
	try:
		list = list.filter(user__id = int(request.REQUEST['autor']))
	except:
		pass
	
	list_res = Paginator(list, limit)
	try:
		list_page = list_res.page(page)
	except:
		list_page = list_res.page(list_res.num_pages)
	
	for i in list_page.object_list:
		i.date=i.date.strftime("%d-%m-%Y")
	
	t = loader.get_template('new/list_report.tpl')
	c = Context({ 'total':list_res.count, 'type':report_type,'list': list_page.object_list, })
	return HttpResponse(t.render(c))

def aj_getTestResults (request):
	limit = int(request.REQUEST['limit'])
	start = int(request.REQUEST['start'])
	page = 1 + int( start / limit )
	
	list = TestResult.objects.all().order_by('id').reverse();
	
	try:
		list = new_filterDate(list,request.REQUEST['dateFrom'],request.REQUEST['dateTo'])
	except:
		pass
	
	try:
		request.REQUEST['res0']
	except:
		list = list.exclude(status_code = 0)
	
	try: 
		request.REQUEST['res1']
	except:
		list = list.exclude(status_code = 1)
	
	try:
		request.REQUEST['res2']
	except:
		list = list.exclude(status_code = 2)
	
	try:
		if request.REQUEST['obj'] <> "false":
			list = list.filter(test__health_object__id = int(request.REQUEST['obj']))
	except:
		pass
	
	
	list_res = Paginator(list, limit)
	try:
		list_page = list_res.page(page)
	except:
		list_page = list_res.page(list_res.num_pages)
	
	res = { 'total':list_res.count, 'records':[] }
	for t_r in list_page.object_list:
		t_r_o = {
			"id": t_r.id,
			"name": t_r.test.name,
			"idtest": t_r.test.id,
			"date": t_r.date.strftime("%d-%m-%Y"),
			"status_message": t_r.status_message,
			"status_code": t_r.status_code,
			"items": [ ],
			"items_count": 0
		}
		
		for t_r_i in t_r.testitems.all():
			t_r_o['items'].append({ 'name': t_r_i.name, 'label': t_r_i.label })
			t_r_o['items_count'] += 1
		
		res['records'].append(t_r_o)
	
	return HttpResponse(simplejson.dumps(res))
def aj_ChangeList (request):
	limit = int(request.REQUEST['limit'])
	start = int(request.REQUEST['start'])
	page = 1 + int( start / limit )
	
	list = Change.objects.all()
	
	try:
		list = list.filter(number__contains = request.REQUEST['query'])
	except:
		pass
	
	list_res = Paginator(list, limit)
	try:
		list_page = list_res.page(page)
	except:
		list_page = list_res.page(list_res.num_pages)
	
	t = loader.get_template('new/list_report.tpl')
	c = Context({ 'total':list_res.count,'list': list_page.object_list, })
	return HttpResponse(t.render(c))
