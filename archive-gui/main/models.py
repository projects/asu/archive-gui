# -*- coding: utf-8 -*-
from django.db import models
from django.utils.encoding import force_unicode
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes import generic
from datetime import datetime
import settings

class  Node(models.Model):
    reg_number = models.CharField(max_length=200, null = True)
    status_choices = (
        ('o', u'Временное хранилище'),
        ('p', u'Передан в НК'),
        ('c', u'Аннулирован'),
        ('w', u'Ожидает извещение'),
        ('s', u'В Архиве'),
    )
    status = models.CharField(max_length = 1, choices = status_choices, default = 'o')
    object = models.ForeignKey("Object", related_name = 'nodes', null = True)
    parent = models.ForeignKey('self', related_name = 'children', null = True)
    comments = models.TextField(null = True, blank=True) 
    lastoperation = models.OneToOneField("HistoryOperation", null = True)
    project = models.ForeignKey('self', related_name = 'allchildren', null = True)
    
    def __unicode__(self):
        return self.object.name


class  Object(models.Model):
    name = models.CharField(db_index = True, max_length=200, verbose_name = "Наименование")
    type = models.ForeignKey("ObjectType", verbose_name = "Тип")
    comments = models.TextField(null = True, verbose_name = "Комментарий", blank=True)
    klgi = models.CharField(db_index = True, max_length=25, null = True, verbose_name = "Обозначение", blank=True)
    invnumber = models.ForeignKey("Invnumber", null = True, verbose_name = "Инвентарный номер", blank=True)
    sheets = models.PositiveIntegerField(null = True, verbose_name = "Листов", blank=True)
    format = models.ForeignKey("FormatDoc", null = True, verbose_name = "Формат", blank=True)
    developer = models.ForeignKey("Autor", null = True, verbose_name = "Разработчик", blank=True)
    division = models.ForeignKey("Division", null = True, verbose_name = "Подразделение", blank=True)
    
    def __unicode__(self):
        return self.name
    
    class Meta:
        verbose_name = "Объект"
        verbose_name_plural = "Объекты"


class File(models.Model):
    file =  models.FileField(upload_to = "%Y/%m/%d",  null = True)
    hash =  models.CharField(max_length=200)
    mime =  models.CharField(max_length=200, null = True)
    correct = models.BooleanField()
    copied  = models.BooleanField(default = False)
    correct_time = models.DateTimeField(null = True, default = False, blank=True)
    copied_time = models.DateTimeField(null = True, default = False, blank=True)
    ext = models.CharField(max_length=5, null = True)
    
    def __unicode__(self):
        return self.hash
    
    class Meta:
        verbose_name = "Файл"
        verbose_name_plural = "Файлы"


class ObjectType(models.Model):
    name = models.CharField(max_length=200, verbose_name = "Наименование")
    kind = models.ForeignKey("TypeKind", verbose_name = "Вид")
    shortname = models.CharField(max_length=5, verbose_name = "Сокращенное обозначение", blank=True)
    
    #parents = models.ManyToManyField('self', symmetrical = False, related_name = "children", null = True)
    comments = models.TextField(null = True, verbose_name = "Комментарий", blank=True)
    def __unicode__(self):
        return self.name
    
    class Meta:
        verbose_name = "Тип объекта"
        verbose_name_plural = "Типы объектов"


class TypeKind(models.Model):
    name = models.CharField(max_length=200, verbose_name = "Наименование")
    canhavechildren = models.BooleanField(verbose_name = "Дочерние объекты")
    comments = models.TextField(null = True, verbose_name = "Комментарий", blank=True)
    def __unicode__(self):
        return self.name
    
    class Meta:
        verbose_name = "Вид типа объекта"
        verbose_name_plural = "Виды типов объектов"


class Operation(models.Model):
    name = models.CharField(max_length=200, verbose_name = "Наименование")
    def __unicode__(self):
        return self.name
    
    class Meta:
        verbose_name = "Операция"
        verbose_name_plural = "Операции"


class HistoryOperation (models.Model):
    reg_number = models.CharField(max_length=200, null = True)
    date =  models.DateField('date creation', null = True)
    operation = models.ForeignKey("Operation", null = True)
    change = models.ForeignKey("Change", related_name = "changes",  null = True)
    node_link = models.ForeignKey("Node", related_name = "listhistory", null = True)
    #project = models.ForeignKey("Node", related_name = "listhistoryproject", null = True)
    #parentobject = models.ForeignKey("Node", related_name = "cc", null = True)
    #prevoperation = models.ForeignKey('self', null = True)
    file = models.ForeignKey('File', related_name = "operations", null = True)
    comments = models.TextField(null = True, blank=True)


class Autor (models.Model):
    name = models.CharField(max_length=200, verbose_name = "Наименование")
    
    def __unicode__(self):
        return self.name
    
    class Meta:
        verbose_name = "Разработчик"
        verbose_name_plural = "Разработчики"


class Division (models.Model):
    name = models.CharField(max_length=200, verbose_name = "Наименование")
    shortname = models.CharField(max_length=5, verbose_name = "Сокращенное наименование", blank = True)
    
    def __unicode__(self):
        return self.name
    
    class Meta:
        verbose_name = "Подразделение"
        verbose_name_plural = "Подразделения"


class FormatDoc(models.Model):
    name = models.CharField(max_length=200, verbose_name = "Наименование")
    shortname = models.CharField(max_length=5, verbose_name = "Сокращенное наименование")
    
    def __unicode__(self):
        return self.name
    
    class Meta:
        verbose_name = "Формат"
        verbose_name_plural = "Форматы"


class Change(models.Model):
    reg_number = models.CharField(max_length=200, null = True)
    date =  models.DateField('date creation', null = True)
    number = models.CharField(max_length=200, null = True)
    user = models.ForeignKey("Autor")
    division = models.ForeignKey("Division", null = True)
    invnumber = models.ForeignKey("Invnumber", unique=True, null = True)
    sheets = models.PositiveIntegerField(null = True)
    format = models.ForeignKey("FormatDoc", null = True)
    file = models.ForeignKey("File", related_name = "change", null = True)
    comments = models.TextField(null = True, blank = True)
    
    def __unicode__(self):
        return self.number


class Invnumber (models.Model):
    number = models.PositiveIntegerField(unique=True, verbose_name = "Номер")
    content_type = models.ForeignKey(ContentType, related_name = "invnumber")
    object_id = models.PositiveIntegerField()
    content_object = generic.GenericForeignKey('content_type', 'object_id')
    
    def __unicode__(self):
        return str(self.number)
    
    class Meta:
        verbose_name = "Инвентарный номер"
        verbose_name_plural = "Инвентарные номера"


class Request(models.Model):
    date =  models.DateField('date creation')
    number = models.PositiveIntegerField(null = True)
    numberzakaz = models.PositiveIntegerField()
    user = models.ForeignKey("Autor")
    division = models.ForeignKey("Division", null = True)
    comments = models.TextField(null = True, blank = True)
    base = models.TextField(null = True, blank = True)


class RequestItem(models.Model):
    request = models.ForeignKey("Request", related_name = "requestitems")
    node = models.ForeignKey("Node", related_name = "requests")


class HealthObject(models.Model):
    name = models.CharField(max_length=200)
    health_object_type = models.ForeignKey ("HealthObjectType")


class HealthObjectType(models.Model):
    name = models.CharField(max_length=200)


class Test(models.Model):
    name = models.CharField(max_length=200)
    health_object = models.ForeignKey ("HealthObject")
    test_type = models.ForeignKey ("TestType")


class TestType(models.Model):
    name = models.CharField(max_length=200)


class TestResult(models.Model):
    date =  models.DateTimeField('date creation')
    test = models.ForeignKey ("Test")
    status_code = models.PositiveIntegerField()
    status_message = models.CharField(max_length=200)


class TestResultItem(models.Model):
    test_result = models.ForeignKey ("TestResult", related_name = "testitems")
    label = models.CharField(max_length=200)
    name = models.CharField(max_length=200)
    message = models.TextField(null = True)