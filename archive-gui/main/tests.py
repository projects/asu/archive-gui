"""
This file demonstrates two different styles of tests (one doctest and one
unittest). These will both pass when you run "manage.py test".

Replace these with more appropriate tests for your application.
"""

import unittest
from django.test.client import Client


#http://localhost:8000/archive/aj_saveNewObject

#pid	2
#nid	2
#date	07/07/2010
#document	1
#inv	23
#user_name	1
#msg	123123
#name	eee
#tid	5
#klgi	None 11111111
#desc	fffgdfgdfg
#file


class TestSaveObject(unittest.TestCase):
    def setUp(self):
        # Every test needs a client.
        self.client = Client()
	self.client.login(username='pav', password='123')

    def test_details(self):
        # Issue a GET request.
        response = self.client.post('/archive/aj_saveNewObject/',{'pid': '2', 
								'nid': '2',
								'date':'07/07/2010',
								'document': '1',
								'inv': '23',
								'user_name' : '1',
								'msg':	'commettest_testtest_test',
								'name': 'test_test_name_obj',
								'tid': '5',
								'klgi': 'test klgi 999999999',
								'desc': 'descr test test test',
								'file': "test test"})

        # Check that the response is 200 OK.
        self.failUnlessEqual(response.status_code, 200)

        # Check that the rendered context succeed True.
        self.failUnlessEqual(len(response.context['succeed']), "True")
	
    def closeDown(self):
	self.client.logout()
