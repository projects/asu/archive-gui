# -*- coding: utf-8 -*-
from django.conf.urls.defaults import *
from main.views import *
from main.get import *
from main.doc_info import *
from main.files import *
from main._project import *
from main._folder import *
from main._dict import *
from main._copy import *
from main._move import *
from main._inv import *
from main._tstorage import *
from main._tchange import *
from main._change import *
from main.aj import *
from main.check import *
from time_control.aj import *
from file.views import *

urlpatterns = patterns('',
    # Example:
    # (r'^ttt/', include('ttt.foo.urls')),


    # Uncomment the admin/doc line below and add 'django.contrib.admindocs' 
    # to INSTALLED_APPS to enable admin documentation:
    # (r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # (r'^admin/', include(admin.site.urls)),
	#(r'view', view),
	(r'^main', main),
	(r'^mpanel', mp),
	(r'^log', viewlog),
#----------------------------------------
	(r'^aj_UserList$', aj_UserList),
	(r'^aj_getUserSession$', aj_getUserSession),
	
	(r'^aj_login$', aj_login),
	(r'^aj_logout$', aj_logout),
	(r'^aj_InvValidate$', aj_InvValidate),
	(r'^aj_SignValidate$', aj_SignValidate),
	(r'^aj_check$', aj_check),
	(r'^aj_RemoveStatuses$', aj_RemoveStatuses),
	
	(r'^aj_getPath$', aj_getPath),
	(r'^aj_getProjects$', aj_getProjects),
	(r'^aj_getFoldersInProject$', aj_getFoldersInProject),
	(r'^aj_getDocumentsInFolder$', aj_getDocumentsInFolder),
	(r'^aj_getReport$', aj_getReport),
	(r'^aj_getTestResults$', aj_getTestResults),
	(r'^aj_getDocumentsInProject$', aj_getDocumentsInProject),
	
	(r'^aj_getDocumentInformation$', aj_getDocumentInformation),
	
	(r'^aj_copyNode$', aj_CopyNode),
	(r'^aj_moveNode$', aj_MoveNode),
	
	(r'^aj_ChangeAdd$', aj_ChangeAdd),
	(r'^aj_ChangeAddFile$', aj_ChangeAddFile),
	(r'^aj_ChangeDeleteFile$', aj_ChangeDeleteFile),
	(r'^aj_ChangeList$', aj_ChangeList),
	
	(r'^aj_ProjectAdd$', aj_ProjectAdd),
	(r'^aj_ProjectEdit$', aj_ProjectEdit),
	(r'^aj_FolderAdd$', aj_FolderAdd),
	(r'^aj_FolderEdit$', aj_FolderEdit),
	
	(r'^aj_InvNumberList$', aj_InvNumberList),
	
	(r'^aj_TChangeList$', aj_TChangeList),
	(r'^aj_TChangeApply$', aj_TChangeApply),
	(r'^aj_TChangeOne$', aj_TChangeOne),
	
	(r'^aj_FilesList$', aj_FilesList),
	(r'^aj_FilesNew$', aj_FilesNew),
	
	(r'^aj_TStorageList$', aj_TStorageList),
	(r'^aj_TStoragePrint$', aj_TStoragePrint),
	(r'^aj_TStorageCancel$', aj_TStorageCancel),
	(r'^aj_TStorageApply$', aj_TStorageApply),
	(r'^aj_TStorageNew$', aj_TStorageNew),
	
	(r'^aj_DictDocumentTypeList$', aj_DictDocumentTypeList),
	(r'^aj_DictAutorList$', aj_DictAutorList),
	(r'^aj_DictAutorAdd$', aj_DictAutorAdd),
	(r'^aj_DictAutorEdit$', aj_DictAutorEdit),
	
#----------------------------------------
	(r'aj_getTreeChildren$', getTreeChildrenJSON),
	(r'aj_getContentChildren', getContentChildrenJSON),
	(r'aj_getContentNodes', getContentNodesJSON),
	(r'aj_getObjectInformationJSON', getObjectInformationJSON),
	(r'aj_getNodeInformationJSON', getNodeInformationJSON),
	(r'^getTypeObjects', getTypeObjects),
	(r'aj_getBriefInfoObjectHTML',  getBriefInfoObjectHTML),
	(r'aj_saveNewObjectFolder',  saveNewObjectFolder),
	(r'aj_saveNewObject', saveNewObject),
	(r'aj_editObjectFolder', editObjectFolder),
	(r'aj_editObject', editObject),
	#(r'aj_saveNewObjectFolder',  saveNewObjectFolder),
	#(r'aj_saveNewObject',  saveChange),
	(r'aj_saveCopyObject',  copyObject),
	(r'aj_saveChange',  saveChange),
	(r'aj_checkCriticProblem',  checkCriticProblem),
	(r'aj_getProjectsJSON',  getProjectsJSON),
	(r'aj_getParentObjectJSON',  getParentJSON),
	(r'aj_getHistoryObjectHTML',  getHistoryObjectHTML),
	(r'aj_getHistoryObjectJSON',  getHistoryObjectJSON),
	(r'aj_getHistoryRequestObjectHTML',  getHistoryRequestObjectHTML),
	(r'aj_getHistoryRequestObjectJSON',  getHistoryRequestObjectJSON),
	(r'aj_getProjectByNodeJSON',  getProjectByNodeJSON),
	(r'aj_getKlgiByNodeJSON',  getKlgiByNodeJSON),
	(r'aj_getTypeObjectJSON',  getTypeObjectJSON),
	(r'init', init),
	(r'test', test),
	#(r'getFileInfo/', getFileInfo),
	#(r'getFile/(?P<name>\d+)/', getFile),
	#(r'getFile/', getFile),
	(r'aj_getGridNavigationJSON', getGridNavigationJSON),
	(r'^aj_getHealthObjectTypesJSON', getHealthObjectTypesJSON),
	(r'^aj_getHealthObjectsJSON', getHealthObjectsJSON),
	(r'^aj_getTestTypesJSON', getTestTypesJSON),
	(r'^aj_getTestsJSON', getTestsJSON),
	(r'^aj_getTestResultsJSON', getTestResultsJSON),
	(r'^aj_getUsersJSON', getUsersJSON),
	(r'^aj_getDivisionsJSON', getDivisionsJSON),
	(r'^aj_getFormatsJSON', getFormatsJSON),
	
	
)

