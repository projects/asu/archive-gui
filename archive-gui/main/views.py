# -*- coding: utf-8 -*-
from django.http import HttpResponse,HttpResponseRedirect
from django.template import Context, loader
from main.models import *
from django.db import transaction
import os
import time
import sys
import re
from operator import truth
import hashlib
from django.core.servers.basehttp import FileWrapper
import shutil
from django.db import models
from django.contrib.auth.decorators import login_required
import datetime
from django.contrib.auth import views
import settings
from django.db.models import Q
from django.contrib.admin.models import LogEntry, ADDITION, DELETION, CHANGE
from django.utils.encoding import force_unicode
from django.contrib.contenttypes.models import ContentType
from main.function import *
import simplejson
from django.contrib.sessions.models import Session
import time_control
# Create your views here.

@login_required
def mini(request):
    t = loader.get_template('mini.tpl')
    c = Context()
    return HttpResponse(t.render(c))

@login_required
def viewlog(request):
    t = loader.get_template('viewlog.tpl')
    c = Context()
    return HttpResponse(t.render(c))


@login_required
def main(request):
    t = loader.get_template('index.html')
    typeslist = ObjectType.objects.all()
    c = Context({
        'listtypes': typeslist,
        'user': request.user
    })
    return HttpResponse(t.render(c))

@login_required
def mp(request):
    t = loader.get_template('mainpanel.tpl')
    c = Context()
    return HttpResponse(t.render(c))

@login_required
def getTreeChildrenJSON(request):
    node = request.POST['node']
    list = Node.objects.get(id = int(node)).children.exclude(object__type__kind__name = "документ")
    #list = Node.objects.filter (parent = Node.objects.filter(object_id = int(node))[0]).exclude(object_id = 1)

    t = loader.get_template('tree.tpl')
    c = Context({
        'list': list,
    })
    return HttpResponse(t.render(c))

@login_required
def getContentChildrenJSON (request):
    node = request.POST['node']
    #list = Node.objects.get(id = int(node)).children.all()
    list = Node.objects.get(id = int(node)).children.filter(object__type__kind__name = "документ")

    #list = Node.objects.filter(parent = Node.objects.filter(object_id = int(node))[0]).exclude(object_id = 1)
    t = loader.get_template('content_children.tpl')
    c = Context({
        'list': list,
    })
    return HttpResponse(t.render(c))

@login_required
def getDivisionsJSON (request):
   # node = request.REQUEST['node']
    #list = Node.objects.get(id = int(node)).children.all()
    list = Division.objects.all()

    #list = Node.objects.filter(parent = Node.objects.filter(object_id = int(node))[0]).exclude(object_id = 1)
    t = loader.get_template('division.tpl')
    c = Context({
        'list': list,
    })
    return HttpResponse(t.render(c))

@login_required
def getUsersJSON (request):
    #list = Node.objects.get(id = int(node)).children.all()
    list = User.objects.all()

    #list = Node.objects.filter(parent = Node.objects.filter(object_id = int(node))[0]).exclude(object_id = 1)
    t = loader.get_template('user.tpl')
    c = Context({
        'list': list,
    })
    return HttpResponse(t.render(c))

@login_required
def getFormatsJSON (request):
    #list = Node.objects.get(id = int(node)).children.all()
    list = FormatDoc.objects.all()

    #list = Node.objects.filter(parent = Node.objects.filter(object_id = int(node))[0]).exclude(object_id = 1)
    t = loader.get_template('division.tpl')
    c = Context({
        'list': list,
    })
    return HttpResponse(t.render(c))


def getGridNavigationJSON (request):
    #node = request.POST['node']
    start = int(request.REQUEST['start'])
    limit = int(request.REQUEST['limit'])
    try:
    	query = request.REQUEST['query']
	
    except:
	query = ""
	
    if  query == "":
	list = Object.objects.exclude(type__kind__id = 3)
    else:
    	#list = Object.objects.filter(klgi__startswith = query)
	list = Object.objects.filter(Q(klgi__contains = query) | Q(name__startswith = query)).exclude(type__kind__id = 3)
    

    #list = Node.objects.filter(parent = Node.objects.filter(object_id = int(node))[0]).exclude(object_id = 1)
    
    t = loader.get_template('content_objects.tpl')
    total = len(list)
    list_res = list[start:start+limit]
    c = Context({
    	'total': total,
        'list': list_res,
#	'callback': callback,
    })
    return HttpResponse(t.render(c))


def getTypeObjects (request):
    list = Node.objects.all()
    t = loader.get_template('type_objects.tpl')
    c = Context({
    	'list': list,
    })
    return HttpResponse(t.render(c))

def getHealthObjectTypesJSON (request):
    list = HealthObjectType.objects.all()
    t = loader.get_template('type_objects.tpl')
    c = Context({
    	'list': list,
    })
    return HttpResponse(t.render(c))

def getHealthObjectsJSON (request):
    try:
	    ftype = int(request.POST['ftype'])
    except:
	    ftype = ""
    
    if ftype == "":
	    list = HealthObject.objects.all()
    else:
	    #t = HealthObjectType.objects.get (id = ftype)
	    #list = HealthObject.objects.filter(type__id__exact = ftype)
	    list = HealthObjectType.objects.get(id = ftype).healthobject_set.all()
 
    t = loader.get_template('type_objects.tpl')
    c = Context({
    	'list': list,
    })
    return HttpResponse(t.render(c))


def getTestTypesJSON (request):
    list = TestType.objects.all()
    t = loader.get_template('type_objects.tpl')
    c = Context({
    	'list': list,
    })
    return HttpResponse(t.render(c))

def getTestsJSON (request):
    try:
	    ftype = int(request.POST['ftype'])
    except:
	    ftype = ""
    
    if ftype == "":
    	list = Test.objects.all()
    else:
	    list = TestType.objects.get(id = ftype).test_set.all()
	    
    t = loader.get_template('type_objects.tpl')
    c = Context({
    	'list': list,
    })
    return HttpResponse(t.render(c))

def getTestResultsJSON (request):

    fstart_date = int(request.REQUEST['date_from'])
    fend_date = int(request.REQUEST['date_to'])
    
    #res0 = int(request.REQUEST['res0'])
    #res1 = int(request.REQUEST['res1'])
    #res2 = int(request.REQUEST['res2'])
    
    
    start_date = datetime.datetime.fromtimestamp(fstart_date)
    end_date = datetime.datetime.fromtimestamp(fend_date)
    
    list = TestResult.objects.filter(date__range=(start_date, end_date))

    if request.REQUEST.get('res0', True):
	list.exclude(status_code = 0)
	
    if request.REQUEST.get('res1', True):
	list.exclude(status_code = 1)
	
    if request.REQUEST.get('res2', True):
	list.exclude(status_code = 2)

    if request.REQUEST.get('obj', False):
	list.filter(test__health_object__id = int(request.REQUEST['obj']))
    
    #try:
    	#fobjecttype = int(request.REQUEST['hObjType'])
	#wherelist.append("") 
	
    #except:
	#fobjecttype = False
    
    

    t = loader.get_template('test_results.tpl')
    c = Context({
    	'list': list,
    })
    return HttpResponse(t.render(c))

def getTypeObjectJSON (request):
    typekind = int(request.POST['ftypekind'])
    list = TypeKind.objects.get(id = typekind).objecttype_set.all()
    t = loader.get_template('type_objects.tpl')
    c = Context({
    	'list': list,
    })
    return HttpResponse(t.render(c))



def getTypeObjectJSON (request):
	typekind = int(request.POST['ftypekind'])
	list = TypeKind.objects.get(id = typekind).objecttype_set.all()
	t = loader.get_template('type_objects.tpl')
	c = Context({
		'list': list,
	})
	return HttpResponse(t.render(c))


def getObjectInformationJSON(request):
	id = request.REQUEST['id']
	item = Object.objects.get(id = int(id))
	comm = item.comments
	if comm <> None:
		comm = comm.replace(u"\n", u"\\n")
		comm = comm.replace(u'"', u'\"')
	
	t = loader.get_template('infoobject.tpl')
	c = Context({
		'item': item,
		"item_comments" : comm
	})
	return HttpResponse(t.render(c))


def getNodeInformationJSON(request):
	id = request.REQUEST['id']
	item = Node.objects.get(id = int(id))
	t = loader.get_template('infonode.tpl')
	comm = item.object.comments
	if comm <> None:
		comm = comm.replace(u"\n", u"\\n")
		comm = comm.replace(u'"', u'\"')
	
	c = Context({
		'item': item,
		"added": getFirstOperation(item.id).date.strftime("%d-%m-%Y"),
		"edited": getLastOperation(item.id).date.strftime("%d-%m-%Y"),
		"item_comments" : comm
		#"idparent": item.parent.id, #Вынесено в шаблон, что-бы не вызывало ошибок
		
	})
	return HttpResponse(t.render(c))

def getFirstOperation(node):
	res = Node.objects.get(id = int(node)).listhistory.all()[0]
	return res


def getLastOperation(node):
	res = Node.objects.get(id = int(node)).listhistory.all().reverse()[0]
	return res


def getContentNodesJSON(request):
	id = request.REQUEST['id']
	list = Object.objects.get(id = int(id)).nodes.all()
	t = loader.get_template('content_nodes.tpl')
	c = Context({
		"list": list,
	})
	return HttpResponse(t.render(c))


def getBriefInfoObjectHTML(request):
	node = request.POST['node']
	item = Node.objects.get(id = int(node))
	t = loader.get_template('briefinfoobject.tpl')
	c = Context({
		'item': item,
	})
	return HttpResponse(t.render(c))

@login_required
def getHistoryObjectHTML(request):
	node = request.POST['node']
	#list = HistoryOperation.objects.filter(linknode = Node.objects.get(id = int(node)))
	list = Node.objects.get(id = int(node)).listhistory.all()
	
	
	t = loader.get_template('historyobject.tpl')
	c = Context({
		'list': list,
	})
	return HttpResponse(t.render(c))

@login_required
def getHistoryObjectJSON(request):
	node = request.POST['node']
	#list = HistoryOperation.objects.filter(linknode = Node.objects.get(id = int(node)))
	list = Node.objects.get(id = int(node)).listhistory.order_by('id').all()
	
	
	t = loader.get_template('historyobjectJSON.tpl')
	c = Context({
		'list': list,
	})
	return HttpResponse(t.render(c))


@login_required
def getHistoryRequestObjectHTML(request):
	node = request.POST['node']
	#list = HistoryOperation.objects.filter(linknode = Node.objects.get(id = int(node)))
	list = Node.objects.get(id = int(node)).requests.all()
	
	
	t = loader.get_template('historyrequest.tpl')
	c = Context({
		'list': list,
	})
	return HttpResponse(t.render(c))


@login_required
def getHistoryRequestObjectJSON(request):
	node = request.POST['node']
	#list = HistoryOperation.objects.filter(linknode = Node.objects.get(id = int(node)))
	list = Node.objects.get(id = int(node)).requests.order_by('id').all()
	
	
	t = loader.get_template('historyrequestJSON.tpl')
	c = Context({
		'list': list,
	})
	return HttpResponse(t.render(c))



#@transaction.commit_manually
def saveChange (request):
	
	fcounter =  int(request.REQUEST['klgi'])
	fdate	= datetime.datetime.strptime(request.REQUEST['date'],"%d-%m-%Y")
	#fnumber = int(request.REQUEST[''])
	finv	= request.REQUEST['inv']
	fnumber	= request.REQUEST['number']
	
	fsheets = request.REQUEST['sheets']
	#fformat	= FormatDoc.objects.get(id = request.REQUEST['format'])
	fformat	= FormatDoc.objects.get(id = 1)
	user	= Autor.objects.get(id = int(request.REQUEST['a_name']))
	fdivision = Division.objects.get(id = int(request.REQUEST['a_group']))
	success = 'false'
	msg = "initial"
#	try:

	
	
	
	# создаем ссылку на инвентарный номер 
	# TODO надо проверять на повтор номера 
		
	inv = Invnumber (number = finv, content_object = user)
	inv.save()
	
	#создаем изменение
	ch = Change (number = fnumber, date = fdate, format = fformat, sheets = fsheets, user = user,  division = fdivision, invnumber = inv)

	
	ch.invnumber = inv
	ch.save()
	
	inv.content_object = ch
	inv.save()
	
	log_addition (request, ch, "создано новое извещение")
	
	# цикл по строкам документа
	for i in range(fcounter+1)[1:fcounter+1]:
		f = FileUpload(request.FILES['file_klgi_'+str(i)])
		
	# создаем операцию
		
		op = HistoryOperation (date = fdate, operation = Operation.objects.get(id = 3), file = f, change = ch)	
		op.save()
		
	# меняем данные для узла... указываем lastoperation !!!! не уверен что это надо

		fidp	= int(request.POST['nid_klgi_'+str(i)]) # опеределяем узел
		
		n  = Node.objects.get(id = fidp)
		n.lastoperation = op
		n.listhistory.add (op)
		n.save()
		
		op.linknode = n
		op.save()
		
			
	#except "InventNumberExist":
		#msg = "InvetNumber Exist:", sys.exc_info()[0]
		#transaction.rollback()
		#success = 'false'
		
	#except:
		#msg = "Unexpected error:", sys.exc_info()[0]
		#transaction.rollback()
		#success = 'false'
		###msg = "Unexpected error while save object"
	#else:
#	transaction.commit()
	success = 'true'
	msg = "Изменение сохранено успешно"	

	
	t = loader.get_template('actionresult.tpl')
	c = Context({
		'success': success,
		'message': msg,
		#'node'	: fidparent,
		#'newnode'	: n.id,
		#'newnode': 1
	})
	return HttpResponse(t.render(c))



#@transaction.commit_manually
def saveNewObject (request):
	try:
		fidparent 	= int(request.REQUEST['nid'])
		fidtype		= int(request.REQUEST['tid'])
		#ftypekind	= int(request.POST['ftypekind'])
		fproject	= int(request.REQUEST['pid'])
		fname 		= request.REQUEST['name']
		fopercomment	= request.REQUEST['msg']
		fitemcomment	= request.REQUEST['desc']
		fklgi		= request.REQUEST['klgi']
		ffile		= request.FILES['file']
		fdate		= datetime.datetime.strptime(request.REQUEST['date'],"%d-%m-%Y")
		user		= Autor.objects.get(id = int(request.REQUEST['a_name']))
		fdivision	= Division.objects.get(id = int(request.REQUEST['a_group']))
		format		= FormatDoc.objects.get(id = int(request.REQUEST['format']))
		sheets		= request.REQUEST['sheets']
		
	except:
		return HttpResponse('ERROR')
	
	try:
		finv = int(request.REQUEST['inv'])
	except:
		finv = False
		try:
			freg = int(request.REQUEST['reg'])
		except:
			freg = 0
	else:
		freg = 0
		
	
	#if fidparent == False:
		#fidparent = fproject

	success = 'false'
	msg = "initial"
	#if ftypekind == 3: # сохраняем ДОКУМЕНТ
	#try:
	
# создаем объект
	o = Object (name = fname, type = ObjectType.objects.get(id = fidtype), klgi = fklgi, comments = fitemcomment, developer = user, division = fdivision, sheets = sheets, format = format)
	o.save ()

#создаем файл
	f = FileUpload(ffile)

# создаем операцию
	if finv <> False:
		operation_id = Operation.objects.get(id=1)
		status = 's'
	else:
		operation_id = Operation.objects.get(id=4)
		status = 'o'
	op = HistoryOperation (date = fdate, operation = operation_id, file = f, comments = fopercomment, reg_number = freg)
	op.save()

# создаем узел
	n = Node (reg_number = freg, object = o, parent = Node.objects.get(id = fidparent), project=Node.objects.get(id = fproject), status = status)
	n.save()
	if finv <> False:
		n.lastoperation = op
	n.listhistory.add(op)
	n.save()	
	
	op.node_link = n
	op.save()

# создаем ссылку на инвентарный номер 
# TODO надо проверять на повтор номера
	if finv <> False:
		inv = Invnumber (number = finv, content_object = o)
		inv.save()
		
		o.invnumber = inv
		o.save()
	

# создаем лог в журнал действий пользователя 	
	log_addition (request, o, "создан новый документ")

#except InventNumberExist:
	#msg = "InvetNumber Exist:", sys.exc_info()[0]
	#transaction.rollback()
	#success = 'false'
	#newnode = 0

	#except:
		#msg = "Unexpected error:", sys.exc_info()[0]
		#transaction.rollback()
		#success = 'false'
		#newnode = 0
		##msg = "Unexpected error while save object"
	#else:
	transaction.commit()
	success = 'true'
	msg = "Данные сохранены"
	newnode = n.id

	
	t = loader.get_template('actionresult.tpl')
	c = Context({
		'success': success,
		'message': msg,
		'node'	: fidparent,
		'newnode': newnode,
	})
	
	return HttpResponse(t.render(c))

#@transaction.commit_manually
def editObject (request):
#	try:
	fid	 	= int(request.REQUEST['nid'])
	fidtype		= int(request.REQUEST['tid'])
	#ftypekind	= int(request.POST['ftypekind'])
	#fproject	= int(request.POST['pid'])
	fname 		= request.REQUEST['name']
	#fopercomment	= request.POST['msg']
	fitemcomment	= request.REQUEST['desc']
	fklgi		= request.REQUEST['klgi']
	fsheets		= int(request.REQUEST['sheets'])
	fformat		= FormatDoc.objects.get(id = int(request.REQUEST['format']))
	try:
		finv = int(request.REQUEST['inv'])
	except:
		finv = False
	user		= Autor.objects.get(id = int(request.REQUEST['a_name']))
	
	#ffile		= request.FILES['file']
	#fdate		= datetime.datetime.strptime(request.POST['date'],"%m/%d/%Y")
	#except:
		#pass
	
	#if fidparent == False:
		#fidparent = fproject
	
	success = 'false'
	msg = "initial"
	#if ftypekind == 3: # сохраняем ДОКУМЕНТ
	#try:
		# находим соответствующее изделие по КЛГИ. либо создаем новое
		
		
		
	# находим объект в базе
	n = Node.objects.get (id = fid)
	o = n.object
	
	o.name = fname
	o.type = ObjectType.objects.get(id = fidtype)
	o.klgi = fklgi
	o.sheets = fsheets
	o.format = fformat
	#o.developer = user		= user
	o.comments = fitemcomment


	if o.invnumber_id <> None:
		last_inv = o.invnumber
		if last_inv.number <> finv:
			if finv <> None or finv <> '' or finv <> "":
				new_inv = Invnumber (number = finv, content_object = o)
				new_inv.save()
				o.invnumber = new_inv
			else:
				o.invnumber_id = None
			o.save()
			last_inv.delete()
		else:
			o.save()
	else:
		o.save()
	
	#op = node.lastoperation
	newnode = fid
	fidparent = n.parent.id
	
	log_change (request, o, "изменён документ")

	
	#except:
		#msg = "Unexpected error:", sys.exc_info()[0]
		#transaction.rollback()
		#success = 'false'
		##msg = "Unexpected error while save object"
		#newnode = 0
		#fidparent = 0
	#else:
	#transaction.commit()
	success = 'true'
	msg = "Изменения сохранены"



	
	t = loader.get_template('actionresult.tpl')
	c = Context({
		'success': success,
		'message': msg,
		'node'	: fidparent,
		#'newnode'	: n.id,
		'newnode': newnode
	})
	return HttpResponse(t.render(c))



#@transaction.commit_manually
def saveNewObjectFolder (request):
	
	fidparent 	= int(request.POST['nid'])
	#fidtype		= int(request.POST['tid'])
	#ftypekind	= int(request.POST['ftypekind'])
	fproject	= int(request.POST['pid'])
	fname 		= request.POST['name']
	
	fitemcomment	= request.POST['desc']
	fklgi		= request.POST['klgi']

	
	#if fidparent == False:
		#fidparent = fproject

	success = 'false'
	msg = "initial"
	

#	try:
# создаем объект
	of = Object (name = fname, type = ObjectType.objects.get(id = 3), comments = fitemcomment, klgi = fklgi)

	of.save ()

# создаем операцию
	op = HistoryOperation (operation = Operation.objects.get(id = 1), file = None, date = datetime.datetime.now())	
	op.save()


## создаем узел
	nf = Node (object = of, parent = Node.objects.get(id = fidparent), lastoperation = op, project=Node.objects.get(id = fproject))
	nf.save()
	
	op.node_link = nf
	op.save()
	
	log_addition (request, of, "создано новое изделие")
	#except:
		#msg = "Unexpected error:", sys.exc_info()[0]
		#transaction.rollback()
		#success = 'false'
		#newnode = 0
		#msg = "Unexpected error while save object"
	#else:
	transaction.commit()
	success = 'true'
	msg = "Данные сохранены"
	newnode = nf.id



	
	t = loader.get_template('actionresult.tpl')
	c = Context({
		'success': success,
		'message': msg,
		'node'	: fidparent,
		'newnode': newnode,
	})
	
	return HttpResponse(t.render(c))

@transaction.commit_manually
def editObjectFolder (request):
	
	fid 	= int(request.POST['nid'])
	fidtype		= int(request.POST['tid'])
	#ftypekind	= int(request.POST['ftypekind'])
	#fproject	= int(request.POST['pid'])
	fname 		= request.POST['name']
	
	fitemcomment	= request.POST['desc']
	fklgi		= request.POST['klgi']

	
	#if fidparent == False:
		#fidparent = fproject

	success = 'false'
	msg = "initial"
	

	try:
	# создаем объект
		#of = Object (name = fname, type = ObjectType.objects.get(id = fidtype), comments = fitemcomment, klgi = fklgi)
	
		#of.save ()
		
		# находим объект в базе
		n = Node.objects.get (id = fid)
		o = n.object
		o.name = fname
		o.type = ObjectType.objects.get(id = fidtype)
		o.klgi = fklgi
		o.comments = fitemcomment
		
		o.save ()
		
		log_change (request, o, "изменено изделие")


	except:
		msg = "Unexpected error:", sys.exc_info()[0]
		transaction.rollback()
		success = 'false'
		#msg = "Unexpected error while save object"
	else:
		transaction.commit()
		success = 'true'
		msg = "Изделие создано...добавьте документы"
	
	
	t = loader.get_template('actionresult.tpl')
	c = Context({
		'success': success,
		'message': msg,
		'node'	: fidparent,
		#'newnode'	: n.id,
		#'newnode': 10
	})
	
	return HttpResponse(t.render(c))



# рекурсивная функция копирования узла
def fn_copyNodesChildren(request, node, newparent, newproject):
	#копируем
	op = HistoryOperation ( operation = Operation.objects.get(id = 2))
	op.date = datetime.datetime.now()
	op.save()
	
	# реальный объект
	ob = node.object
	
	# создаем новый узел со ссылкой на реальный объект, новый родитель и проект
	n = Node (object = ob, lastoperation = op, parent = newparent, project = newproject)
	
	n.save()
	
	op.node_link=n
	op.save()
	
	log_addition (request, ob, "создана копия объекта")
	
	#ищем потомков
	l = list(node.children.all())
	
	#цикл по потомкам
	for item in l:
		fn_copyNodesChildren(request, item, n, newproject)
	


@transaction.commit_manually
def copyObject (request):
	
	fidnewparent 	= int(request.POST['to_nid'])
	fidnewproject	= int(request.POST['to_pid'])
	fidnode		= int(request.POST['nid'])
	fchcopytree	= 'all'
	
	success = 'false'
	msg = "initial"

	try:
	
		if fchcopytree == 'all':
		
		# копируем все дерево узлов
			#q = Node.objects.get(id = fidnewproject)
			fn_copyNodesChildren(request, Node.objects.get(id = fidnode), Node.objects.get(id = fidnewparent), Node.objects.get(id = fidnewproject))	
		else:
		# копируем один узел
		# создаем операцию
			op = HistoryOperation (operation = Operation.objects.get(id = 2))	
			op.save()
			
			ob = Node.objects.get(id = fidnode).object
		
		# создаем узел со ссылкой на объект
			n = Node (object = ob, lastoperation = op, parent = Node.objects.get(id = fidnewparent), project=Node.objects.get(id = fidnewproject))
			
			n.save()
			
	except:
		msg = "Unexpected error:", sys.exc_info()[0]
		transaction.rollback()
		success = 'false'
		#msg = "Unexpected error while save object"
	else:
		transaction.commit()
		success = 'true'
		msg = "saved object"


	
	t = loader.get_template('actionresult.tpl')
	c = Context({
		'success': success,
		'message': msg,
		'node': fidnode,
		'newnode': fidnewparent,
	})
	
	return HttpResponse(t.render(c))



def checkCriticProblem (request):
	if os.path.isfile(settings.LOCK_FOLDER+"critical"):
		success = 'false'

	else:
		success = 'true'

	t = loader.get_template('actionresult.tpl')
	c = Context({
	'success': success,
	'message': '---'
	})
	return HttpResponse(t.render(c))
		

def getProjectsJSON (request):
    try:
      query 	= request.POST['query']
      list = ObjectType.objects.get(id = "2").object_set.filter(name__contains=query)
    except:
      list = ObjectType.objects.get(id = "2").object_set.all()

    t = loader.get_template('list_projects.tpl')
    c = Context({
    	'list': list,
    })
    return HttpResponse(t.render(c))	



def getParentJSON (request):

	query 	= request.POST['query']
	idproject 	= request.POST['idproject']
	
	#idproject = fn_getInCombo (project, 1)
	
	if idproject == "0":
		list_f = list(Node.objects.all())
	else:
		list_f = list(Node.objects.get(id = int(idproject)).allchildren.all())

	list_res = []
	
	if query <> "":
		for item in list_f:
			if (item.object.name.find(query) <> -1) or ( item.object.klgi  > 0 and item.object.klgi.find(query) <> -1):
				list_res.append (item)
	else:
		list_res = list_f
	
	t = loader.get_template('list_projects2.tpl')
	c = Context({
    		'list': list_res,
	})
	return HttpResponse(t.render(c))	


def getProjectByNodeJSON(request):
	node = request.POST['node']
	item = Node.objects.get(id = int(node)).project
	t = loader.get_template('project.tpl')
	c = Context({
		'item': item,
		'success': 'true'
	})
	return HttpResponse(t.render(c))
	

def getKlgiByNodeJSON(request):
	node = request.POST['node']
	item = Node.objects.get(id = int(node))
	t = loader.get_template('klgi.tpl')
	c = Context({
		'item': item,
		'success': 'true'
	})
	return HttpResponse(t.render(c))

	
	
#@transaction.commit_manually
def init (request):
	#try:
	
	from django.contrib.auth.models import Group, User
	
	g1=Group(name = 'Архив')
	g2=Group(name = 'Временное хранилище')
	g3=Group(name = 'Утверждение извещений')
	g4=Group(name = 'Отчёты')
	g5=Group(name = 'Системный журнал')
	g6=Group(name = 'Справочники')
	g7=Group(name = 'Сессии пользователей')
	g8=Group(name = 'Интерфейс администратора')
	g9=Group(name = 'Управление сервером')
	g10=Group(name = 'Управление проектами')
	g11=Group(name = 'Журнал Инв.номеров')
	g12=Group(name = 'Работа с файлами')
	g1.save()
	g2.save()
	g3.save()
	g4.save()
	g5.save()
	g6.save()
	g7.save()
	g8.save()
	g9.save()
	g10.save()
	g11.save()
	g12.save()
	
	
	u=User.objects.get(id=1)
	u.groups.add(g1,g2,g3,g4,g5,g6,g7,g8,g9,g10,g11,g12)
	
		
	user_default = Autor(name = 'Init')
	user_default.save()
	
	tk1 = TypeKind (name = "проект", canhavechildren = True)
	tk2 = TypeKind (name = "изделие", canhavechildren = True)
	tk3 = TypeKind (name = "документ", canhavechildren = False)
	
	tk1.save()
	tk2.save()
	tk3.save()
	
	t1 = ObjectType (name = "root", kind = tk1)
	t2 = ObjectType (name = "проект", kind = tk1)
	t3 = ObjectType (name = "изделие", kind = tk2)
#	t4 = ObjectType (name = "изделие типа B", kind = tk2)
	t5 = ObjectType (name = "Сборочный чертеж", kind = tk3, shortname="СБ")
	t6 = ObjectType (name = "Чертеж общего вида", kind = tk3, shortname="ВО")
	t7 = ObjectType (name = "Габаритный чертеж", kind = tk3, shortname="ГЧ")
	
	t1.save()
	t2.save()
	t3.save()
#	t4.save()
	t5.save()
	t6.save()
	t7.save()
	
	o1 = Operation(name = "Создание нового")
	o2 = Operation(name = "Создание нового на основе")
	o3 = Operation(name = "Внесение изменения")
	o4 = Operation(name = "Временное хранилище")
	o5 = Operation(name = "Передан НК")
	o6 = Operation(name = "Пройден НК, помещён в архив")
	o7 = Operation(name = "Не пройден НК, аннулирован")
	o8 = Operation(name = "Пройден НК, ожидает извещения")
	
	o1.save()
	o2.save()
	o3.save()
	o4.save()
	o5.save()
	o6.save()
	o7.save()
	o8.save()
	
			
	ob1 = Object (type = t1, name = "root", developer = user_default)
	ob2 = Object (type = t2, name = "22030", developer = user_default,  comments = "Здесь нужно написать комментарии к проекту. Цели, задачи, порядоку выполнения")
	ob3 = Object (type = t2, name = "55806", developer = user_default, comments = "Здесь нужно написать комментарии к проекту. Цели, задачи, порядоку выполнения") 
	
	ob1.save ()
	ob2.save ()
	ob3.save ()
	
	#inv1 = Invnumber (number = "1", content_object = ob1)
	#inv2 = Invnumber (number = "2", content_object = ob2)
	#inv3 = Invnumber (number = "3", content_object = ob3)
	
	#inv1.save()
	#inv2.save()
	#inv3.save()

	#------------------------
	
	n1 = Node (object = ob1, status ='s')
	n1.save()
	
	ho1 = HistoryOperation(operation = o1, date = datetime.datetime.now(), node_link = n1)
	ho1.save()
	
	n1.lastoperation = ho1
	n1.save()
	
	
	#------------------------
	n2 = Node (object = ob2, parent = n1, status ='s')
	n2.save()
	n2.project = n2
	n2.save()
	
	ho2 = HistoryOperation(operation = o1, date = datetime.datetime.now(), node_link = n2)
	ho2.save()

	
	n2.lastoperation = ho2
	n2.save()
	
	#------------------------
	n3 = Node (object = ob3, parent = n1, status ='s')
	n3.save()
	n3.project = n3
	n3.save()


	ho3 = HistoryOperation(operation = o1, date = datetime.datetime.now(), node_link = n3)
	ho3.save()
	
	n3.lastoperation = ho3
	n3.save()
		
	d1 = Division(name = "НИО-13")
	d1.save()
	
	f1 = FormatDoc(name = "A4 полный формат", shortname = "A4")
	f1.save()
	
	
	#inv = Invnumber (number = 0)
	#inv.save()
		#------------------------

		#hot1 = HealthObjectType(name = "Тип1")
		#hot2 = HealthObjectType(name = "Тип2")
		#hot1.save()
		#hot2.save()

		#ho1 = HealthObject(name = "Объект1-1",health_object_type=hot1)
		#ho2 = HealthObject(name = "Объект2-1",health_object_type=hot1)
		#ho3 = HealthObject(name = "Объект3-2",health_object_type=hot2)
		#ho1.save()
		#ho2.save()
		#ho3.save()

		#tt1 = TestType(name = "ТипТеста1")
		#tt2 = TestType(name = "ТипТеста2")
		#tt3 = TestType(name = "ТипТеста3")
		#tt1.save()
		#tt2.save()
		#tt3.save()
		
		#test1 = Test(name = "Тест №1",health_object=ho1,test_type=tt1)
		#test2 = Test(name = "Тест №2",health_object=ho1,test_type=tt2)
		#test3 = Test(name = "Тест №3",health_object=ho1,test_type=tt3)
		#test4 = Test(name = "Тест №4",health_object=ho2,test_type=tt1)
		#test5 = Test(name = "Тест №5",health_object=ho2,test_type=tt2)
		#test6 = Test(name = "Тест №6",health_object=ho2,test_type=tt3)
		#test7 = Test(name = "Тест №7",health_object=ho3,test_type=tt1)
		#test8 = Test(name = "Тест №8",health_object=ho3,test_type=tt2)
		#test9 = Test(name = "Тест №9",health_object=ho3,test_type=tt3)
		#test1.save()
		#test2.save()
		#test3.save()
		#test4.save()
		#test5.save()
		#test6.save()
		#test7.save()
		#test8.save()
		#test9.save()
		
		#tr1=TestResult(date='2010-06-06', test=test2, status_code=0, status_message='2341423')
		#tr1.save()
		
		
	
	#except:
		#msg = "Unexpected error:", sys.exc_info()[0]
		#transaction.rollback()
		#success = 'false'
		##msg = "Unexpected error while save object"
	#else:

	success = 'true'
	msg = "saved object"	
	
	return HttpResponse(msg)
	
	
	
def mylogin(request, template_name=""):
	if request.method == 'GET':
		return views.login(request, template_name)
	elif request.method == 'POST':
		username = request.POST['username']
		password = request.POST['password']
		next = request.POST['next']
		
		time_control.LogIn (request, username, password)
		return HttpResponseRedirect (next)


def mylogout(request, template_name=""):
	time_control.LogOut (request, 'c')
	return views.logout(request, '/archive/main', template_name)

def  viewmain(request):
	
	return main(request)

	
def test (request):
    list = Node.objects.all()
    t = loader.get_template('combobox.html')
    c = Context({
    	'list': list,
    })
    return HttpResponse(t.render(c))

def log_addition(request, object, message):
	LogEntry.objects.log_action(
        user_id         = request.user.pk,
        content_type_id = ContentType.objects.get_for_model(object).pk,
        object_id       = object.pk,
        object_repr     = force_unicode(object),
        action_flag     = ADDITION,
	change_message	= message
    )


def log_change(request, object, message):
	LogEntry.objects.log_action(
        user_id         = request.user.pk,
        content_type_id = ContentType.objects.get_for_model(object).pk,
        object_id       = object.pk,
        object_repr     = force_unicode(object),
        action_flag     = CHANGE,
	change_message	= message
    )
    
	
def log_deletion(request, object, message):
	LogEntry.objects.log_action(
        user_id         = request.user.pk,
        content_type_id = ContentType.objects.get_for_model(object).pk,
        object_id       = object.pk,
        object_repr     = force_unicode(object),
        action_flag     = DELETION,
	change_message	= message
    )
