//Проверка критических ошибок
function Check(){
	var object=this;
	var interval=1000*5;
	
	var checkers={
		warning: new CheckWarning(),
		critical: new CheckCritical(),
		auth: new CheckAuth()
	};
	
	
	var check=function (result){
		for( c in checkers ){
			checkers[c].check(result[c]);
		}
	}
	
	var touch=function (){
		CoreAJAX({ url: 'aj_check', param: {}, succ: check });
	}
	
	Ext.TaskMgr.start({run: touch, interval: interval});
	touch();
}