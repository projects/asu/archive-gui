//Локализация
Ext.MessageBox.buttonText={
	cancel: "Отмена",
	no: "Нет",
	ok: "ОК",
	yes: "Да",
}

if(!index) var index={};

//---------------------------------- SRATR of Debug function -------------------------------
//Проверка
function a(){
	alert(true);
}

//Просмотр JavaScript объекта
function CoreViewObject(obj){
	var msg='';
	for(val in obj){
		msg+=val+':'+obj[val]+"\r\n";
	}
	Ext.MessageBox.alert('Ошибка:', msg);
}

var core=this;
//Наличие библиотек
function CoreIssetLib(list){
	for(name in list){
		if((!core[name])){
			alert('None '+name);
			return false;
		}
	}
	return true;
}
//---------------------------------- END of Debug function -------------------------------

function CoreMask(el){ el.mask("Загрузка данных", "x-mask-loading"); }
function CoreUnMask(el){ el.unmask(); }

//Склеивание ассоциативных массивов
function CoreMergeArray(){
	var i, obj, row, res={};
	for(i=0; obj=arguments[i]; i++){
		for (row in obj){
			res[row]=obj[row];
		}
	}
	return res;
}

//Унифицированный AJAX запрос
function CoreAJAX(conf){
	Ext.Ajax.request({
		url: conf.url,
		params: conf.param,
		success: function(result) {
			index.offline.succ();
			if(conf.succ) conf.succ(Ext.util.JSON.decode(result.responseText));
		},
		failure: function() {
			index.offline.fail();
			if(conf.fail) conf.fail();
		},
	});
}

function CoreAJAXResponser(conf){
	conf.succ=conf.fail=function(res){
		if(res){
			if(res.success) index.worklog.add(res.msg, 'ok');
			else Ext.MessageBox.alert('Ошибка:', 'Ошибка сервера!');
		}
	}
	CoreAJAX(conf);
}

//AJAX запрос получения инфо о объекте или ноде
function getNodeInfo(id,succ,fail,type){
	if(!type) type='Node';
	CoreAJAX({url:'aj_get'+type+'InformationJSON',param:{'id':id},succ:succ,fail:fail});
}

//Подтверждение одно-кликовых действий
function CoreConfirm(msg, callback){
	Ext.Msg.confirm(
		'Внимание! Подтвердите операцию!',
		msg,
		function (res){
			if(res=='yes'){
				if(callback) callback();
			}
		}
	);
}

//Дополнительный ввод одного поля для завершения действия
function CorePrompt(msg, callback){
	Ext.Msg.prompt(
		'Внимание! Завершение операции!',
		msg,
		function (res, val){
			if(res=='ok'){
				if(callback) callback(val);
			}
		}
	);
}

