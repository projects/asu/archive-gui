//Группа выбора промежутка дат
function CoreLibDateGroup(){
	var object=this;
	
	var curr_date=new Date(), lweek_date=new Date(curr_date.getTime()-1000*24*60*60*7);
	
	var from=new Ext.form.DateField({
		name:'date_from',
		fieldLabel: 'От',
		format: 'd-m-Y',
		editable: false,
		listeners:{
			change: function(th,val){
				to.setMinValue(val);
			}
		}
	});
	
	this.fromValue=function(){
		var curr=from.getValue(), res=0;
		if(curr) res=parseInt(curr.getTime()/1000);
		return res;
	}
	
	var to=new Ext.form.DateField({
		xtype: 'datefield',
		name:'date_to',
		fieldLabel: 'До',
		format: 'd-m-Y',
		editable: false,
		listeners:{
			change: function(th,val){
				from.setMaxValue(val);
			}
		}
	});
	
	this.toValue=function(){
		var curr=to.getValue(), res=0;
		if(curr) res=parseInt(curr.getTime()/1000);
		return res;
	}

	this.autoSet=function(){
		from.setValue(lweek_date);
		from.setMaxValue(curr_date);
		to.setValue(curr_date);
		to.setMinValue(lweek_date);
	}
	
	this.group=new Ext.form.FieldSet({
		xtype: 'fieldset',
		style: 'padding:0 5px; margin:0 5px;',
		title: 'Период',
		items:[
			from,
			to
		],
		listeners:{
			afterrender: object.autoSet
		}
	});
}