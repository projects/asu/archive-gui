//Проверка связи
function Offline(){
	var object=this;
	var offline=false;
	
	var win=new Ext.Window({
		closable: false,
		draggable: false,
		resizable: false,
		plain: true,
		modal: true,
		autoHeight: true,
		width: 400,
		frame: true,
		title: 'Внимание',
		html: '<center><b><font color="red">Потеряна связь с сервером!<br>'+
			'Необходимо позвать администратора</font></b></center>'
	});
	
	var fail=function (){
		if(!offline){
			offline=true;
			index.worklog.add('Связь с сервером потеряна','sys');
			win.show();
		}
	}
	
	var succ=function (){
		if(offline){
			offline=false;
			index.worklog.add('Связь с сервером восстановлена','ok');
			win.hide();
		}
	}
	
	this.fail=fail;
	this.succ=succ;
}