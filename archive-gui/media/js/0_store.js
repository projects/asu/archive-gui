if(!index) var index={};

index.store_report_types=new Ext.data.ArrayStore({
	data:[
		['change',	'Изменения',	'Составил',	'Номер извещения',	'Инвентарный номер'],
		['request',	'Запросы',	'Затребовал',	'Номер наряда',		'Заказ №'],
	],
	fields:['eng', "name", "autor", "number", "inv"],
});

index.store_hobject_types=new Ext.data.JsonStore({
	url: 'aj_getHealthObjectTypesJSON',
	fields: ['id', 'name'],
	autoLoad:true,
});

index.store_hobjects=new Ext.data.JsonStore({
	url: 'aj_getHealthObjectsJSON',
	fields: ['id', 'name'],
	autoLoad:true,
});

index.store_test_types=new Ext.data.JsonStore({
	url: 'aj_getTestTypesJSON',
	fields: ['id', 'name'],
	autoLoad:true,
})

index.store_projects=new Ext.data.JsonStore({
	url: 'aj_getProjects',
	fields	:["id", "name"],
	autoLoad: true,
});

index.store_document_types=new Ext.data.JsonStore({
	url: 'aj_DictDocumentTypeList',
	fields: ["id", "view", "name", "short"],
	autoLoad: true,
});

index.store_divisions=new Ext.data.JsonStore({
	url: 'aj_getDivisionsJSON',
	fields: ["id", "name"],
	autoLoad: true,
});

index.store_autors=new Ext.data.JsonStore({
	url: 'aj_DictAutorList',
	fields: ["id", "name"],
	autoLoad: true,
});

index.store_users=new Ext.data.JsonStore({
	url: 'aj_UserList',
	fields: ['id', 'name'],
	autoLoad:true,
});

index.store_formats=new Ext.data.JsonStore({
	url: 'aj_getFormatsJSON',
	fields: ["id", "name"],
	autoLoad: true,
});


