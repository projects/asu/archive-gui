function Change(){
	var object=this;
	var parent_id=0;
	var obj_id=0;
	
	var menuObject=new Ext.menu.Menu({
		items: [
			{itemId:'view_change',
			 text:'Показать Извещение',
			 handler:function(){ 
				var win = window.open(
					"/printer/change?id="+obj_id,
					"Извещение",
					"width:27cm,status=no"
				);
				win.focus()
			 },
			},
			{itemId:'view',
			 text:'Посмотреть файл',
			 handler:function(){
				CoreAJAX({
					url:'/file/getAllFile/',
					param:{'change_id':obj_id},
					succ: function(res){
						if ( res.file ){
							var win = window.open(
								"/media/out/"+res.file,
								"",
								"width:27cm,status=no"
							);
							win.focus();
						}
					}
				});
			 }
			}
		]
	});
	
	var enterGrid=new LibGrid({
		title: 'Журнал извещений',
		url: "aj_ChangeList",
		fields: ['number','id','group','autor','nodes'],
		page: index.change_config.result_page,
		search: true,
		column: [
			{name: "Обозначение", field: 'number', width: 1},
			{name: "Отдел/Цех", field: 'group', width: 1},
			{name: "Разработчик", field: 'autor', width: 1},
		],
		rowcontext: function(row, evtObj){
				obj_id = row.get('id');
				menuObject.showAt(evtObj.getXY());
		},
	});

	this.container=new Ext.Container({
		layout: 'hbox',
		layoutConfig: {
			align : 'stretch',
		},
		defaults: {flex:1, hideBorders:true},
		items:[
			enterGrid.grid
		],
	});
	
	this.refresh=enterGrid.load;
}