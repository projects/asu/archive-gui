function Dict(){
	var object=this;
	
	var idField=new Ext.form.TextField({name:'id', hidden:true, emptyText:1});
	var nameField=new Ext.form.TextField({name:'name', fieldLabel:'Сотрудник', allowBlank:false, anchor:'95%'});
	
	var autorClear=function (){
		idField.setValue(1);
		nameField.setValue('');
		nameField.validate();
	}
	
	var autorSubmit=function(type){
		var req={};
		req.success=req.failure=autorResult;
		if(type=='Add') req.url='aj_DictAutorAdd';
		else if(type=='Edit') req.url='aj_DictAutorEdit';
		autorMask();
		autorForm.getForm().submit(req);
	}
	
	var autorResult=function(form, action){
		autorUnmask();
		if(action.result){
			if(action.result.success){
				if((action.result.success=="false") && (action.result.msg)){
					Ext.MessageBox.alert('Ошибка:', action.result.msg);
				}
				else{
					autorClear();
					index.store_autors.load();
					index.worklog.add(action.result.msg,'ok');
				}
				
			}
			else Ext.MessageBox.alert('Ошибка:', 'Ошибка сервера!');
		}
		else if((action.response) && (action.response.status=='404')) alert('404');
	}
	
	//Кнопка очистки формы
	var resetButton={ text: 'Сбросить', handler: autorClear }
	//Кнопка создания нового
	var addButton={ text:'Создать', handler: function(){ autorSubmit('Add'); } }
	//Кнопка сохранения изменения
	var editButton={ text:'Сохранить', handler: function(){ autorSubmit('Edit'); } }
	
	var autorForm = new Ext.form.FormPanel({
		title:'Редактирование',
		region:'south',
		frame:true,
		autoHeight:true,
		items:[
			idField,
			nameField
		],
		buttons:[resetButton,editButton,addButton]
	});
	
	var autorGrid = new Ext.grid.GridPanel({
		title:'Сотрудники',
		region:'center',
		store:index.store_autors,
		columns: [
			{header: "Сотрудник", sortable: true, dataIndex: 'name'},
		],
		
		viewConfig: {forceFit:true},
		autoScroll: true,
		columnLines: true,
		
		enableColumnHide: false,
		enableColumnMove: false,
		enableDragDrop: false,
		enableHdMenu: false,
		
		listeners: {
			rowclick: function(grid, rowIndex, evtObj){
				evtObj.stopEvent();
				var autor=index.store_autors.getAt(rowIndex);
				idField.setValue(autor.get('id'));
				nameField.setValue(autor.get('name'));
			}
		}
	});
	
	var autorMask=function(){ CoreMask(autorPanel.el); }
	var autorUnmask=function(){ CoreUnMask(autorPanel.el); }
	var autorPanel = new Ext.Panel({
		layout: 'border',
		items:[
			autorGrid, autorForm
		],
	});

	this.container=new Ext.Container({
		layout:'hbox',
		layoutConfig: {
			align : 'stretch',
		},
		defaults: {flex:1, hideBorders:true},
		items:[
			autorPanel,
		],
	});

}