function Files(){
	var object=this;
	var file_id=0;
	
	var menuNew=new Ext.menu.Menu({
		items: [
			{itemId:'nk_new',
			 text:'Заменить файл',
			 handler:function(){
				index.form_files_new.view(file_id);
			 },
			}
		]
	});
	
	var enterGrid=new LibGrid({
		title: 'Файлы в хранилище',
		url: "aj_FilesList",
		fields: ['id','hash','mime','info', 'info_count', 'f_date', 'f_change', 'f_operation', 'f_node'],
		page: index.files_config.result_page,
		search: true,
		expander: new Ext.XTemplate(
			'<tpl if="info_count">'+
			'<p><table border=1 width=100%>'+
			'<tr><td>Рег.номер</td><td>Дата</td><td>Операция</td><td>Извещение</td><td>Документ</td></tr>'+
			'<tpl for="info">'+
			'<tr><td>{reg_number}</td><td>{date}</td><td>{operation}</td><td>{change}</td><td>{node}</td></tr>'+
			'</tpl></table></p></tpl>'
		),
		column: [
			{name: "Дата", field: 'f_date', width: 1},
			{name: "Операция", field: 'f_operation', width: 2},
			{name: "Извещение", field: 'f_change', width: 2},
			{name: "Документ", field: 'f_node', width: 2},
			{name: "Тип", field: 'mime', width: 2},
		],
		rowcontext: function(row, evtObj){
			file_id=row.get('id');
			menuNew.showAt(evtObj.getXY());
		},
	});
	
	this.container=new Ext.Container({
		layout: 'fit',
		defaults: {flex:1, hideBorders:true},
		items: enterGrid.grid
	});
	
	this.refresh=enterGrid.load;
}