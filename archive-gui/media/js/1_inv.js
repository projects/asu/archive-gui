function Inv(){
	var object=this;
	var parent_id=0;
	
	var menuObject=new Ext.menu.Menu({
		items: [
			{itemId:'view_parent',
				text:'Посмотреть изделие',
				handler:function(){
					if (index.viewer_folder){
						index.monitor_panel.setActiveTab(0);
						index.viewer_folder.view(parent_id,'Object');
					}
				}
			}
		]
	});
	
	var enterGrid=new LibGrid({
		title: 'Журнал инвентарных номеров',
		url: "aj_InvNumberList",
		fields: ['id', 'number', 'type', 'name', 'object_id', 'parent_id', 'sign'],
		page: index.inv_config.result_page,
		search: true,
		column: [
			{name: "Номер", field: 'number', width: 1},
			{name: "Тип", field: 'name', width: 1},
			{name: "Обозначение", field: 'sign', width: 1},
		],
		rowcontext: function(row, evtObj){
			if( row.get('type') == 'object' ){
				parent_id = row.get('parent_id');
				menuObject.showAt(evtObj.getXY());
			}
		},
	});

	this.container=new Ext.Container({
		layout: 'hbox',
		layoutConfig: {
			align : 'stretch',
		},
		defaults: {flex:1, hideBorders:true},
		items:[
			enterGrid.grid
		],
	});
	
	this.refresh=enterGrid.load;
}