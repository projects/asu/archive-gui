function TChange(){
	var object=this;
	var change_id=0;
	var project_id=0;
	
	var menuNK=new Ext.menu.Menu({
		items: [
			{itemId:'nk_apply',
				text:'Документы прошли НК -> Принять изменения',
				handler:function(){
					index.form_tchange_apply.view(change_id);
				}
			},
			{itemId:'add_change',
				text:'Изменить документы',
				handler:function(){ 
					index.form_files_change.view(change_id, project_id); 
				}
			},
		]
	});
	var menuCH=new Ext.menu.Menu({
		items: [
			{itemId:'add_change',
				text:'Изменить документы',
				handler:function(){ 
					index.form_files_change.view(change_id, project_id); 
				}
			},
		]
	});
	
	var enterGrid=new LibGrid({
		title: 'Извещения на проверке',
		url: "aj_TChangeList",
		fields: ['reg_number','number','change_id','gname','aname','waiting','nodes', 'project_id'],
		page: index.tchange_config.result_page,
		search: true,
		expander: new Ext.XTemplate(
			'<p><table border=1 width=100%>'+
			'<tr><td>Номер</td><td>Статус</td><td>Наименование</td><td>Обозначение</td></tr>'+
			'<tpl for="nodes">'+
			'<tr><td>{reg_number}</td><td>{status_desc}</td><td>{name}</td><td>{klgi}</td></tr>'+
			'</tpl></table></p>'
		),
		column: [
			{name: "Номер", field: 'reg_number', width: 1},
			{name: "Обозначение", field: 'number', width: 1},
			{name: "Статус", field: 'waiting', width: 1,
			 renderer: function (c){
				if(!c) return '<font color="green">Готово к принятию</font>';
				else return '<b><font color="red">Ожидает документы</font></b>';
			 }
			},
			{name: "Отдел/Цех", field: 'gname', width: 1},
			{name: "Разработчик", field: 'aname', width: 1},
		],
		rowcontext: function(row, evtObj){
			if(!row.get('waiting')){
				change_id=row.get('change_id');
				project_id=row.get('project_id');
				menuNK.showAt(evtObj.getXY());
			}
			if(row.get('waiting')){
				change_id=row.get('change_id');
				project_id=row.get('project_id');
				menuCH.showAt(evtObj.getXY());
			}
		},
	});

	this.container=new Ext.Container({
		layout: 'hbox',
		layoutConfig: {
			align : 'stretch',
		},
		defaults: {flex:1, hideBorders:true},
		items:[
			enterGrid.grid
		],
	});
	
	this.refresh=enterGrid.load;
}