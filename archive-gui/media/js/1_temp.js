function Temp(){
	var object=this;
	
	var menu=function() {
		var node_id=0, node_klgi=0;
		
		var view_file={
			itemId:'view',
			text:'Посмотреть файл',
			handler:function(){
				CoreAJAX({
					url:'/file/getAllFile/',
					param:{'node_id':node_id},
					succ: function(res){
						if ( res.file ){
							var win = window.open(
								"/media/out/"+res.file,
								"",
								"width:27cm,status=no"
							);
							win.focus();
						}
					}
				});
			}
		}
		
		var nk_print=function(){
			CoreConfirm(
				'Вы уверены, что '+node_klgi+' передан в НК?',
				function (){
					CoreAJAXResponser({ url:'aj_TStoragePrint', param:{'node':node_id} });
					index.temp.refresh();
				}
			);
		}
		
		var nk_apply=function(){
			CoreConfirm(
				'Вы уверены, '+node_klgi+' прошёл НК?',
				function (){
					CoreAJAXResponser({ url:'aj_TStorageApply', param:{'node':node_id} });
					index.temp.refresh();
				}
			);
		}
		
		var nk_cancel=function(){
			CoreConfirm(
				'Вы уверены, '+node_klgi+' не прошёл НК?',
				function (){
					CoreAJAXResponser({ url:'aj_TStorageCancel', param:{'node':node_id} });
					index.temp.refresh();
				}
			);
		}
		
		var menuNK=new Ext.menu.Menu({
			items: [
				{itemId:'nk_apply',
				 text:'Пройден НК -> Поместить в архив',
				 handler:function(){
					index.form_temp_apply.view(node_id);
				 }
				},
				{itemId:'nk_cancel',
				 text:'Не пройден НК -> Аннулировать',
				 handler:nk_cancel,
				},
				view_file
			]
		});
		
                var menuNKChange=new Ext.menu.Menu({
			items: [
				{itemId:'nk_apply',
				 text:'Пройден НК -> Ожидать извещения',
				 handler:nk_apply
				},
				{itemId:'nk_cancel',
				 text:'Не пройден НК -> Аннулировать',
				 handler:nk_cancel,
				},
				view_file
			]
		});
		
		var menuPrint=new Ext.menu.Menu({
			items: [
				{itemId:'nk_print',
				 text:'Передан в НК',
				 handler:nk_print,
				},
				view_file
			]
		});
		
		var menuNew=new Ext.menu.Menu({
			items: [
				{itemId:'nk_new',
				 text:'Заменить файл',
				 handler:function(){
					index.form_temp_new.view(node_id);
				 },
				}
			]
		});
		
		this.view=function(type,id,klgi,XY){
			var menu;
			
			if(type && id && klgi){
				node_id=id;
				node_klgi=klgi;
				if(type=='o') menu=menuPrint;
				else if(type=='p') menu=menuNK;
				else if(type=='c') menu=menuNew;
				else if(type=='i') menu=menuNKChange;
				
				if(menu) menu.showAt(XY);
			}
		}
	}
	
	var enterMenu=new menu();
	
	var enterGrid=new LibGrid({
		title: 'Поступления',
		url: "aj_TStorageList",
		fields: ['reg_number','status','status_desc','node_id','pr_name','p_name','name','t_name',
			'klgi','gname','aname','f_name','sheets'],
		page: index.temp_config.result_page,
		search: true,
		column: [
			{name: "Номер", field: 'reg_number', width: 1},
			{name: "Статус", field: 'status_desc', width: 1},
			{name: "Проект", field: 'pr_name', width: 1},
			{name: "Применение", field: 'p_name', width: 1},
			{name: "Наименование", field: 'name', width: 1},
			{name: "Обозначение", field: 'klgi', width: 1},
			{name: "Отдел/Цех", field: 'gname', width: 1},
			{name: "Разработчик", field: 'aname', width: 1},
			{name: "Формат", field: 'f_name', width: 1},
			{name: "Кол-во листов", field: 'sheets', width: 1}
		],
		rowcontext: function(row, evtObj){
			enterMenu.view(row.get('status'),row.get('node_id'),row.get('klgi'),evtObj.getXY());
		},
	});

	this.container=new Ext.Container({
		layout: 'hbox',
		layoutConfig: {
			align : 'stretch',
		},
		defaults: {flex:1, hideBorders:true},
		items:[
			enterGrid.grid
		],
	});
	
	this.refresh=enterGrid.load;
}