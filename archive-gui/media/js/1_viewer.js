function Viewer(){
	var object=this;
	
	this.container=new Ext.Container({
		layout: 'border',
		defaults: {split: true, hideBorders:true},
		items:[
			{title:'Навигация',
			 region:'west',
			 layout: 'fit',
			 animCollapse:false,
			 collapsible: true,
			 collapseMode: 'mini',
			 width: 300,
			 minWidth: 200,
			 maxWidth: 400,
			 items: index.viewer_navigation.container
			},
			{title:'Изделие',
			 region:'center',
			 layout: 'fit',
			 items: index.viewer_folder.container,
			},
			{title:'Документ',
			 width:400,
			 region:'east',
			 layout: 'fit',
			 items: index.viewer_document.container
			},
		],
	});

}