function Worklog(){
	var store=new Ext.data.ArrayStore({
		idIndex: 0,
		fields: ['time', 'msg']
	});
	
	var add=function(msg,type){
		var color, m;
		if(!type) color='black';
		else if(type=='ok') color='green';
		else if(type=='sys') color='red';
		else color='yellow';
		
		m = new store.recordType({
			time: (new Date()).format('G:i:s'),
			msg: '<font color="'+color+'">'+msg+'</font>'
		});
		store.insert(0, m);
	}
	
	var list=new Ext.list.ListView({
		store: store,
		autoScroll: true,
		emptyText: 'Сообщений не было',
		columns: [
			{header: "Время", width:.10, dataIndex: 'time'},
			{header: "Сообщение", dataIndex: 'msg'},
		]
	});
	
	this.container=list;
	this.add=add;
	
	add('Начало работы','ok');
}