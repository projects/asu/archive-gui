Ext.EventManager.on("body", 'selectstart', function(e){ e.stopEvent(); });
Ext.EventManager.on("body", 'contextmenu', function(e){ e.stopEvent(); });

index.form_login=new FormLogin();
index.form_download=new FormDownload();
index.form_file=new FormFile();
index.form_project=new FormProject();
index.form_folder=new FormFolder();
index.form_document=new FormDocument();
index.form_copy=new FormCopy();
index.form_move=new FormMove();
index.form_temp_new=new FormTStorageFile();
index.form_temp_apply=new FormTStorageApply();
index.form_tchange_apply=new FormTChangeApply();
index.form_files_new=new FormFilesFile();
index.form_files_change=new FormFilesChange();

index.worklog=new Worklog();

index.check=new Check();
index.offline=new Offline();
index.monitor_items=[]

if(archive_groups.viewer){
	index.viewer_config={'navigation_page':20}
	index.viewer_menu=new ViewerMenu();
	index.viewer_navigation=new ViewerNavigation();
	index.viewer_folder=new ViewerFolder();
	index.viewer_document=new ViewerDocument();
	index.viewer=new Viewer();
	index.monitor_items.push({title: 'Архив', items: index.viewer.container });
}

if(archive_groups.temp){
	index.temp_config={'result_page':20}
	index.temp=new Temp();
	index.monitor_items.push({title: 'Временное хранилище', items: index.temp.container });
}

if(archive_groups.tchange){
	index.tchange_config={'result_page':20}
	index.tchange=new TChange();
	index.monitor_items.push({title: 'Утверждение извещений', items: index.tchange.container });
}

if(archive_groups.tchange){
	index.change_config={'result_page':20}
	index.change=new Change();
	index.monitor_items.push({title: 'Журнал Извещений', items: index.change.container });
}

if(archive_groups.inv){
	index.inv_config={'result_page':20}
	index.inv=new Inv();
	index.monitor_items.push({title: 'Журнал Инв.Номеров', items: index.inv.container });
}


if(archive_groups.report){
	index.report_config={'result_page':20}
	index.report_menu=new ReportMenu();
	index.report_search=new ReportSearch();
	index.report_result=new ReportResult();
	index.report=new Report();
	index.monitor_items.push({title: 'Отчёты', items: index.report.container });
}

if(archive_groups.syslog){
	index.syslog_config={'result_page':20}
	index.syslog_search=new SyslogSearch();
	index.syslog_result=new SyslogResult();
	index.syslog=new Syslog();
	index.monitor_items.push({title: 'Системный Журнал', items: index.syslog.container });
}

if(archive_groups.dict){
	index.dict=new Dict();
	index.monitor_items.push({title: 'Справочники', items: index.dict.container });
}

if(archive_groups.time){
	index.time_config={'result_page':20}
	index.time_search=new TimeSearch();
	index.time_result=new TimeResult();
	index.time=new Time();
	index.monitor_items.push({title: 'Сессии пользователей', items: index.time.container });
}

if(archive_groups.files){
	index.files_config={'result_page':20}
	index.files=new Files();
	index.monitor_items.push({title: 'Работа с файлами', items: index.files.container });
}

if(archive_groups.admin){
	index.monitor_items.push({title: 'Интерфейс администратора', html: '<iframe width=100% height=100% src="/admin">' });
}

index.monitor_panel=new Ext.TabPanel({
	region: 'center',
	viewConfig: {forceFit:true},
	defaults:{layout: 'fit'},
	frame: false,
	activeTab: 0,
	items: index.monitor_items,
});
	
index.monitor=new Ext.Viewport({
	layout: 'border',
	title: "Архив документов",
	defaults: { split: true },
	items: [
		index.monitor_panel,
		{title: "Сообщения",
		 region: 'south',
		 height: 150,
		 minHeight: 100,
		 maxHeight: 200,
		 layout: 'fit',
		 items: index.worklog.container
		},
		{xtype:'container',
		 region	: 'north',
		 height: 40,
		 style:'background-image:url("./media/back.png"); background-repeat:repeat;',
		 html:	'<table width=100%><tr>'+
		 	'<td><img src="./media/logo_name.png" style="padding-top:5px;padding-left:15px;"></td>'+
		 	'<td align="right">'+Ext.get('user-tools').dom.innerHTML+'</td>'+
		 	'</tr></table>',
		 split: false,
		}
	]
});

if(archive_groups.viewer){
	index.viewer_folder.view(1,'Node');
	index.viewer_document.view(1);
}