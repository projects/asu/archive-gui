//Проверка критических ошибок
function CheckCritical(){
	var critical=false;
	
	var win=new Ext.Window({
		closable: false,
		draggable: false,
		resizable: false,
		plain: true,
		modal: true,
		autoHeight: true,
		width: 400,
		frame: true,
		title: 'Внимание',
		html: '<center><b><font color="red">Критическая ошибка!<br>'+
			'Необходимо позвать администратора</font></b></center>'
	});

	var start=function(){
		critical=true;
		index.worklog.add('Критическая ошибка системы','sys');
		if(!archive_groups.server) win.show();
	}
	
	var end=function(){
		critical=false;
		index.worklog.add('Исправлена критическая ошибка системы','ok');
		win.hide();
	}
	
	var check=function (st){
		if ( !st && critical ) end();
		else if ( st && !critical ) start();
	}
	
	this.check=check;
}