//Авто разлогин
function CheckAuth(){
	var auth=true;
	var interval=1000*10, period=1000*60*5, last=(new Date()).getTime();
	
	var auto_logout=function (){
		if(auth && ((last+period)<(new Date()).getTime())) logout();
	}
	
	var touch=function (){
		last=(new Date()).getTime();
	}
	
	var login=function (){
		touch();
		auth=true;
		index.worklog.add('Вход в систему','ok');
	}
	
	var logout=function (){
		auth=false;
		index.worklog.add('Автоматический выход из системы','sys');
		index.form_login.view(true);
		CoreAJAX({url:'aj_logout',param:{}});
	}
	
	var check=function (st){
		if ( !st && auth ) logout();
		else if ( st && !auth ) login();
	}
	
	this.check=check;
	
	Ext.EventManager.on('body','keydown',touch);
	Ext.EventManager.on('body','mousedown',touch);
	Ext.EventManager.on('body','mousemove',touch);
	
	Ext.TaskMgr.start({run: auto_logout, interval: interval});
	touch();
}