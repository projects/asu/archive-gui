//Проверка критических ошибок
function CheckWarning(){
	var warning=false;
	
	var win=new Ext.Window({
		closable: false,
		draggable: false,
		resizable: false,
		autoHeight: true,
		width: 400,
		frame: true,
		y: 0,
		html: '<center><b><font color="red">Сбой в системе! &nbsp;&nbsp;&nbsp;&nbsp;'+
			'Необходимо позвать администратора</font></b></center>'
	});
	
	var start=function(){
		warning=true;
		Ext.Msg.alert(
			'Внимание',
			'<b><font color="red">Произошёл сбой в системе!<br>'+
			'Необходимо позвать администратора,<br>'+
			'Но вы можете продолжить работу</font></b>'
		);
		index.worklog.add('Сбой в системе','sys');
		win.show();
	}
	
	var end=function(){
		warning=false;
		index.worklog.add('Устранён сбой в системе','ok');
		win.hide();
	}
	
	var check=function (st){
		if ( !st && warning) end();
		else if ( st && !warning) start();
	}
	
	this.check=check;
}