//Данные о бумажном носителе
function FormLibDocumentGroup(type){
	var object=this;
	var Title=false, numberTitle=false, invTitle=false, autorTitle=false;
	var groupCombo=false, autorCombo=false, formatCombo=false, msgArea=false;
	var dateField=false, numberField=false, invField=false, autorField=false, sheetsField=false, baseField=false;
		
	if(type=='Document'){
		Title='Документ';
		invTitle='Инвентарный номер';
		autorTitle='Разработчик';
	}
	else if(type=='Download'){
		Title='Наряд';
		numberTitle='Номер наряда';
		numberField=true;
		invTitle='Заказ №';
		autorTitle='Затребовал';
		baseField=true;
	}
	else if(type=='File'){
		Title='Извещение';
		numberTitle='Номер извещения';
		numberField=true;
		invTitle='Инвентарный номер';
		autorTitle='Составил';
	}

	dateField=new Ext.form.DateField({name:'date', fieldLabel:'Дата', format: 'd-m-Y', allowBlank:false});
	if(numberTitle=='Номер извещения'){
		numberField=new FormLibSignField("number");
	}
	else if(numberField){
		numberField={
                    field:new Ext.form.NumberField({ name:'number', fieldLabel:numberTitle, allowBlank:false, maxLength: 10, anchor:'95%' }),
                    clear:function(){ numberField.field.setValue(); numberField.field.validate(); },
                    set:function(value){ numberField.field.setValue(value); },
                };
	}
	if(invTitle=='Инвентарный номер'){
		invField=new FormLibInvField(true);
	}
	else invField={
		field:new Ext.form.NumberField({name:'inv', fieldLabel:invTitle, allowBlank:false, maxLength: 10, anchor:'95%'}),
		clear:function(){ invField.field.setValue(); invField.field.validate(); },
		set:function(value){ invField.field.setValue(value); },
	};
	groupCombo=new Ext.form.ComboBox({
		valueField: 'id',
		hiddenName:'a_group',
		store: index.store_divisions,
		triggerAction: 'all',
		emptyText:'Выберите Отдел/Цех',
		allowBlank:false,
		mode:'local',
		editable: false,
		displayField: 'name',
		fieldLabel: 'Отдел/Цех',
		anchor:'95%',
	});
	autorCombo=new Ext.form.ComboBox({
		valueField: 'id',
		hiddenName:'a_name',
		store: index.store_autors,
		triggerAction: 'all',
		emptyText:'Выберите кто '+autorTitle,
		allowBlank:false,
		mode:'local',
		forceSelection: true,
		displayField: 'name',
		fieldLabel: autorTitle,
		anchor:'95%',
	});
	formatCombo=new Ext.form.ComboBox({
		valueField: 'id',
		hiddenName:'format',
		store: index.store_formats,
		triggerAction: 'all',
		emptyText:'Выберите формат',
		allowBlank:false,
		mode:'local',
		editable: false,
		displayField: 'name',
		fieldLabel: 'Формат листов',
		anchor:'95%',
	});
	sheetsField=new Ext.form.NumberField({name:'sheets', fieldLabel:'Кол-во листов', allowBlank:false, anchor:'95%'});
	if(baseField){
		baseField=new Ext.form.TextField({name:'base', fieldLabel:'Основание', allowBlank:false, anchor:'95%'});
	}
	msgArea=new Ext.form.TextArea({hideLabel: true, name: 'msg', anchor:'95%'});


//Объект extJS для рендера
	this.container=new Ext.form.FieldSet({
		title: Title,
		items:[
			dateField,
			numberField ? numberField.field : false,
			invField.field,
			groupCombo,
			autorCombo,
			formatCombo,
			sheetsField,
			baseField,
			{xtype: 'label',fieldLabel:'Коментарий'},
			msgArea,
		],
	});


//Показ формы
	this.show=function(){
		dateField.enable();
		/*if(numberField){
			numberField.enable();
		}*/
		//invField.enable();
		groupCombo.enable();
		autorCombo.enable();
		formatCombo.enable();
		sheetsField.enable();
		if(baseField){
			baseField.enable();
		}
		msgArea.enable();
		object.container.enable().show(false);
	}
//Скрытие формы
	this.hide=function(){
		dateField.disable();
		/*if(numberField){
			numberField.disable();
		}*/
		//invField.disable();
		groupCombo.disable();
		autorCombo.disable();
		formatCombo.disable();
		sheetsField.disable();
		if(baseField){
			baseField.disable();
		}
		msgArea.disable();
		object.container.disable().hide();
	}
//Очистка формы
	this.clear=function(){
		dateField.setValue(new Date());
		if(numberField) numberField.clear();
		invField.clear();
		groupCombo.clearValue();
		groupCombo.validate();
		autorCombo.clearValue();
		autorCombo.validate();
		formatCombo.clearValue();
		formatCombo.validate();
		sheetsField.setValue();
		sheetsField.validate();
		if(baseField){
			baseField.setValue();
			baseField.validate();
		}
		msgArea.setValue()
		msgArea.validate();
	}
//Заполнение формы
	this.set=function(data){
		dateField.setValue(data.d_add);
		if(numberField) numberField.set(data.number);
		invField.set(data.inv);
		groupCombo.setValue(data.a_group);
		groupCombo.setRawValue(data.gname);
		autorCombo.setValue(data.a_name);
		autorCombo.setRawValue(data.aname);
		formatCombo.setValue(data.f_id);
		formatCombo.setRawValue(data.f_name);
		sheetsField.setValue(data.sheets);
		if(baseField) baseField.setValue(data.base);
		msgArea.setValue(data.msg);
	}
//Фокус в начало формы
	this.focus=function(){
		if(numberField) numberField.field.focus();
		else invField.field.focus();
	}
}