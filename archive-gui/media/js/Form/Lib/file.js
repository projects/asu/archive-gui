//Загрузка файла документа
function FormLibFileGroup(){
	var object=this;

//Поле файла
	var fileField=new Ext.ux.form.FileUploadField({hideLabel:true, allowBlank:false, name:'file', anchor:'95%'});


//Объект extJS для рендера
	this.container=new Ext.form.FieldSet({
		title: 'Файл с данными',
		items:[fileField],
	});


//Показ формы
	this.show=function(){
		fileField.enable();
		object.container.enable().show(false);
	}
///Скрытие формы
	this.hide=function(){
		fileField.disable();
		object.container.disable().hide();
	}
//Очистка формы
	this.clear=function(){
		fileField.setValue();
		fileField.validate();
	}
//Заполнение формы
	this.set=function(){ alert('newFileDocumentGroup(): None Set()'); }
//Фокус в начало формы
	this.focus=function(){ alert('newFileDocumentGroup(): None Focus()'); }
}