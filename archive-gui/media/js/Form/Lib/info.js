//Информация о документе
function FormLibInfoGroup(type){
	var object=this;
	var typeCombo=false, klgiReg=false, klgiCal=false, descArea=false;
	var nameField=false, klgiField=false;

//Основные данные
//	//Поле Наименование
	nameField=new Ext.form.TextField({name:'name', fieldLabel:'Наименование', allowBlank:false, anchor:'95%'});
//	//Поле КЛГИ
	if(type=='Project') klgiField={field: false};
	else klgiField=new FormLibSignField();
//	//Combo-box выбора типа изделия
	if(type=='Document'){
//	//Автоподстановка полей
		klgiReg=/^([\S]+)([ ]{1,}[\S]{0,3})$/;
		typeCalc=function(co,tid){
// 			//var tid=index.store_document_types.getById(typeCombo.getValue());
			if(tid){
				var t_klgi=tid.get('short');
				var klgi_a=klgi_n=klgiField.get();
				var klgi_arr=klgiReg.exec(klgi_a);
				if(klgi_arr) klgi_n=klgi_arr[1];
				klgiField.setValue(klgi_n+' '+t_klgi);
				nameField.setValue(tid.get('name'));
			}
		}
		
		nameField.setReadOnly(true);
		typeCombo=new Ext.form.ComboBox({
			valueField: 'id',
			hiddenName:'tid',
			store: index.store_document_types,
			triggerAction: 'all',
			emptyText:'Выберите тип документа',
			allowBlank:false,
			mode:'local',
			forceSelection: true,
			displayField: 'view',
			fieldLabel: 'Тип документа',
			anchor:'95%',
			listeners:{select: typeCalc},
			
		});
	}
//	//Поле Описания
	descArea=new Ext.form.TextArea({hideLabel: true, name: 'desc', anchor:'95%'});


//Объект extJS для рендера
	this.container=new Ext.form.FieldSet({
		title: 'Основные данные',
		items:[
			nameField,
			typeCombo,
			klgiField.field,
			{xtype: 'label',fieldLabel:'Описание'},
			descArea,
		],
	});


//Показ формы
	this.show=function(){ alert('newInfoGroup(): None Show()'); }
//Скрытие формы
	this.hide=function(){ alert('newInfoGroup(): None Hide()'); }
//Очистка формы
	this.clear=function(){
		nameField.setValue();
		nameField.validate();
		if(typeCombo){
			typeCombo.clearValue();
			typeCombo.validate();
		}
		if(klgiField.field) klgiField.clear();
		descArea.setValue();
	}
//Заполнение формы
	this.set=function(Edit,n_name,n_klgi,n_desc,t_id,t_name){
		nameField.setValue(n_name);
		if(klgiField.field) klgiField.set(n_klgi);
//	//	//Заполнение формы
		if(Edit){
			if(typeCombo) typeCombo.setValue(t_id);
			descArea.setValue(n_desc);
		}
	}
//Фокус в начало формы
	this.focus=function(){ alert('newInfoGroup(): None Focus()'); }
}