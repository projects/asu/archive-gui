function FormLibInvField(inv_null){
	var num_edit=false, num_res=false, num_valid=false, num=false;
	
	var messages={
		'null': '<div>&nbsp;</div><br>',
		'now': '<div>Текущий инвентарный номер</div><br>',
		'blank': '<div style="color:red">Необходимо задать инвентарный номер</div><br>',
		'request': '<div style="color:orange">Идёт проверка инвентарного номера</div><br>',
		'valid': '<div style="color:green">Данный инвентарный номер - свободен</div><br>',
		'invalid': '<div style="color:red">Данный инвентарный номер уже используется</div><br>',
	};
	
	var msg=new Ext.Container({ html:'<div>&nbsp;</div><br>', style:'text-align:center;width:100%;' });

	var validate=function(value){
		if(value!=''){
			if(value==num_edit){
				msg.el.dom.innerHTML=messages['now'];
				return true;
			}
			else if(value==num){
				if(num_res){
					if(num_valid) msg.el.dom.innerHTML=messages['valid'];
					else msg.el.dom.innerHTML=messages['invalid'];
					return num_valid;
				}
				else return false;
			}
			else{
				num_res=num_valid=false;
				num=value;
				CoreAJAX({
					url:'aj_InvValidate',
					param:{'inv':value},
					succ:function(result){
						if(result.inv==num){
							num_res=true;
							if(result.valid) num_valid=true;
							field.validate();
						}
					}
				});
				msg.el.dom.innerHTML=messages['request'];
				return false;
			}
		}
		else{
			if(!inv_null){
				msg.el.dom.innerHTML=messages['blank'];
				return false;
			}
			else{
				msg.el.dom.innerHTML=messages['null'];
				return true;
			}
		}
	}

	if(!inv_null) inv_null=false;
	var field=new Ext.form.NumberField({
		name:'inv',
		fieldLabel:'Инвентарный номер',
		allowBlank: inv_null,
		anchor:'95%',
		validator:validate
	});

	this.field=new Ext.Container({
		layout:'form',
		items:[field, msg],
		anchor:'100%'
	});
	this.set=function(value){
		num_edit=value;
		num=false;
		field.setValue(value);
	};
	this.clear=function(){
		num_edit=num=false;
		field.setValue();
		field.validate();
	};
}