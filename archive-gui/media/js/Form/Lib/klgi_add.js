//КЛГИ
function FormLibKlgiGroupAdd(handler,title_group,fileEnable,statusCheck, change_id){
	if(!title_group) title_group='Выборка по КЛГИ';
	if(!statusCheck) statusCheck = false;
	if(!fileEnable) fileEnable=false;
	
	var object=this;
//Создание объекта Combo-box
	var Combos=['none'], Files=['none'], Values={};
	this.ComboStore=new Ext.data.JsonStore({
		url: 'aj_getDocumentsInProject',
		baseParams: {'idproject':0,'query':'','status':statusCheck},
		fields: ["id", "name"],
		autoLoad:true,
	});
	var ComboValid=function(value){
		if ( value == '' ) return false;
		else if (Combos[klgi].getValue() == value){
			if ( parseInt(value) != value ){
				var n = object.ComboStore.find('name',value);
				var t_n = object.ComboStore.getAt(n);
				if ( t_n ) value = t_n.get('id');
				else alert('error of change form: validation');
			}
			if (Values[value] && (Values[value] != klgi)){
				Ext.MessageBox.alert('Ошибка:', 'Поле с таким значением уже есть!');
				Combos[klgi].clearValue();
				return false;
			}
			return true
		}
		else return true;
	}
	var Combo=function(kid){
		var combo=new Ext.form.ComboBox({
			name: "name",
			valueField: 'id',
			hiddenName: 'nid_klgi_'+kid,
			hiddenValue:'id',
			store: object.ComboStore,
			triggerAction: 'all',
			emptyText:'Наберите часть КЛГИ...',
			minChars: 1,
			allowBlank:false,
			forceSelection: true,
			displayField: 'name',
			fieldLabel: kid+') Документ',
			loadingText: 'Идет запрос на сервер...',
			validator: ComboValid,
			//anchor: '100%'
		});
		Combos.push(combo);

		if(!fileEnable) return combo;
		else{
			var file=new Ext.ux.form.FileUploadField({
				hideLabel:true,
				name:'file_klgi_'+kid,
				allowBlank:false,
				anchor:'95%',
			});
			Files.push(file);

			return new Ext.Container({
				layout:'form',
				items:[combo,file],
				anchor:'100%'
			});
		}
	}
//Добавить Combo-box
	var AddCombo=function(){
		var val = Combos[klgi].getValue();
		if((val=='') || (Values[val]) || (fileEnable && (Files[klgi].getValue()==''))){
			Ext.MessageBox.alert('Ошибка:', 'Перед добавлением нового поля - заполните предыдущее!');
		}
		else{
			Values[val] = klgi;
			Combos[klgi].setReadOnly(true);
			if(fileEnable) Files[klgi].setReadOnly(true);
			klgi++;
			object.container.add(Combo(klgi));
			object.container.get(0).setValue(klgi);
			Combos[klgi].validate();
			if(fileEnable) Files[klgi].validate();
			object.container.doLayout();
			if(handler) handler();
		}
	}
//Удалить Combo-box
	var DelCombo=function(){
		var val = Combos[klgi].getValue();
		if(klgi==1) Ext.MessageBox.alert('Ошибка:', 'Нельзя удалить все поля!');
		else{
			Values[val]=false;
			object.container.remove(klgi);
			klgi--;
			Combos.pop();
			Combos[klgi].setReadOnly(false);
			if(fileEnable){
				Files.pop();
				Files[klgi].setReadOnly(false);
			}
			object.container.get(0).setValue(klgi);
		}
	}
	
	//Сохраняем изменения
	var klgi=1;


//Объект extJS для рендера
	this.container=new Ext.form.FieldSet({
		title: title_group,
		defaultType: 'textfield',
		items:[
			{name:'klgi',hidden:true,emptyText:1},
			Combo(1)
		],
		buttons:[
			{text: 'Удалить поле', handler: DelCombo,},
			{text: 'Добавить поле', handler: AddCombo,},
		],
	});


//Показ формы
	this.show=function(){
		for(i=klgi;i>=1;i--) Combos[i].enable();
		if(fileEnable){ for(i=klgi;i>=1;i--){ Files[i].enable(); } }
		object.container.enable().show(false);
	}
//Скрытие формы
	this.hide=function(){
		for(i=klgi;i>=1;i--) Combos[i].disable();
		if(fileEnable){ for(i=klgi;i>=1;i--){ Files[i].disable(); } }
		object.container.disable().hide();
	}
//Очистка формы
	this.clear=function(){
		Values={};
		for(i=klgi;i>1;i--) DelCombo();
		Combos[1].clearValue();
		Combos[1].validate();
		if(fileEnable) Files[1].setValue('').validate();
	}
//Заполнение первого эл-та формы
	this.set=function(n_id){
		Combos[1].setValue(n_id).validate();
	}
}