//КЛГИ
function FormLibKlgiContent(handler,title_group){
	if(!title_group) title_group='Выборка по КЛГИ';
	var object=this;
	var selectedContentId=0;
	var nodeId=0;
	
	
	this.selectedId = null;
	
	var menuContent=new Ext.menu.Menu({
		items: [
			{itemId:'remove_content',
				text:'Удалить вложение',
				handler:function(){ 
				    Ext.Msg.confirm('Удаление вложения', 'Вы точно хотите удалить выбранный файл?', function(btn){
					if (btn=='yes'){
					    Ext.Ajax.request({
						url: 'aj_ChangeDeleteFile',
						success: function(){
						    Ext.Msg.alert("Удаление вложения", 'Вложение удалено.');
						    object.store.load();
						},
						failure: function(){
						    Ext.Msg.alert("Ошибка", 'Произошла ошибка. Обратитесь к администратору.');
						},
						params: { 
						    selectedContent: selectedContentId,
						    nodeId: nodeId
						}
					    }); 
					}
				    });
				}
			},
		]
	});
	
	this.store=new Ext.data.JsonStore({
		url: "aj_TChangeOne",
		root: "records",
		totalProperty: "total",
		fields: ['obj_id','reg_number','change_id','name','klgi','status_desc', 'node_id']
	});
	
	//Таблица
	this.grid=new Ext.grid.GridPanel ({
		title: 'Содержание извещения',
		store: object.store,
		height: 280,
		colModel: new Ext.grid.ColumnModel({
		    defaults: {
			width: 120,
			sortable: true
		    },
    		    columns: [
			{header: "Номер", dataIndex: 'reg_number'},
			{header: "Статус", dataIndex: 'status_desc'},
			{header: "Наименование", dataIndex: 'name'},
			{header: "Обозначение", dtaIndex: 'klgi'}
		    ],
		}),
		viewConfig: {
		        forceFit: true
		},
		listeners: {
			rowcontextmenu: function(grid, rowIndex, event){
				selectedContentId=object.store.getAt(rowIndex).get('change_id');
				nodeId=object.store.getAt(rowIndex).get('node_id');
				menuContent.showAt(event.getXY());
			}
		}
	});


	RefreshContent = function(){
	    object.store.load();
	};
	
//Объект extJS для рендера
	this.container=new Ext.form.FieldSet({
		title: title_group,
		defaultType: 'textfield',
		height: 300,
		items:[
			this.grid
		],
		buttons:[
			{text: 'Обновить', handler: RefreshContent}
		],
	});
	
//Показ формы
	this.show=function(){
		object.container.enable().show(false);
	}
//Скрытие формы
	this.hide=function(){
		object.container.disable().hide();
	}
//Очистка формы
	this.clear=function(){
	}
}