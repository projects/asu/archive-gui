//Применение
function FormLibNodeGroup(title_group,title_folder,field_name){
	if(!title_group) title_group='Свойства';
	if(!title_folder) title_folder='Применение';
	else title_folder='Изделие';
	if(!field_name) field_name='';
	var object=this;
		
//Combo-box выбора проекта
	var project=new Ext.form.ComboBox({
		name: "pname",
		valueField: 'id',
		hiddenName:field_name+'pid',
		hiddenValue:'id',
		store: index.store_projects,
		triggerAction: 'all',
		emptyText:'Выберите проект',
		allowBlank:false,
		mode:'local',
		editable: false,
		displayField: "name",
		fieldLabel: 'Проект',
		loadingText: 'Идет запрос на сервер...',
		anchor:'95%',
		listeners: {
			select: function() {
				var pid=project.getValue();
				folder.enable();
				folder.clearValue();
				folder.store.setBaseParam('pr_id',pid);
				folder.store.load({params:{'pr_id':pid}});
			}
		}
	});
//Combo-box выбора Нода
	var folder=new Ext.form.ComboBox({
		name: "nname",
		valueField: 'id',
		hiddenName:field_name+'nid',
		hiddenValue:'id',
		store: new Ext.data.JsonStore({
			url: 'aj_getFoldersInProject',
			baseParams: {query:''},
			fields	:["id", "name"],
		}),
		triggerAction: 'all',
		emptyText:'Наберите часть названия...',
		minChars: 1,
		allowBlank:false,
		forceSelection: true,
		displayField: 'name',
		fieldLabel: title_folder,
		loadingText: 'Идет запрос на сервер...',
		anchor:'95%',
	});


//Объект extJS для рендера
	this.container=new Ext.form.FieldSet({
		title: title_group,
		items:[project,folder],
	});


//Показ формы
	this.show=function(){
		project.enable();
		folder.enable();
		object.container.enable().show(false);
	}
//Скрытие формы
	this.hide=function(){
		project.disable();
		folder.disable();
		object.container.disable().hide();
	}
//Очистка формы
	this.clear=function(){
		project.setReadOnly(false);
		project.clearValue();
		project.validate();
		project.enable();

		folder.setReadOnly(false);
		folder.clearValue();
		folder.validate();
		folder.disable();
	}
//Заполнение формы
	this.set=function(pr_id,pr_name,n_id,n_name,n_klgi){
//	//Заполнение проекта и изделия
		project.setValue(pr_id);
		project.setRawValue(pr_name);
		folder.enable();
		folder.store.setBaseParam('pr_id',pr_id);
		folder.store.load({params:{'pr_id':pr_id}});
		folder.setValue(n_id);
		folder.setRawValue(n_name+" - ["+n_klgi+"]");
//	//Блокируем редактирование
		//if(Edit){
			project.setReadOnly(true);
			folder.setReadOnly(true);
	//	}
	}
}