function FormLibSignField(field_name){
	var num_edit=false, num_res=false, num_valid=false, num=false;
	
	var messages={
		'now': '<div>Текущее обозначение</div><br>',
		'blank': '<div style="color:red">Необходимо задать обозначение</div><br>',
		'request': '<div style="color:orange">Идёт проверка обозначения</div><br>',
		'valid': '<div style="color:green">Данное обозначение - свободно</div><br>',
		'invalid': '<div style="color:red">Данное обозначение уже используется</div><br>',
	};
	
	var msg=new Ext.Container({ html:'<div>&nbsp;</div><br>', style:'text-align:center;width:100%;' });

	var validate=function(value){
		if(value!=''){
			if(value==num_edit){
				msg.el.dom.innerHTML=messages['now'];
				return true;
			}
			else if(value==num){
				if(num_res){
					if(num_valid) msg.el.dom.innerHTML=messages['valid'];
					else msg.el.dom.innerHTML=messages['invalid'];
					return num_valid;
				}
				else return false;
			}
			else{
				num_res=num_valid=false;
				num=value;
				CoreAJAX({
					url:'aj_SignValidate',
					param:{'sign':value},
					succ:function(result){
						if(result.sign==num){
							num_res=true;
							if(result.valid) num_valid=true;
							field.validate();
						}
					}
				});
				msg.el.dom.innerHTML=messages['request'];
				return false;
			}
		}
		else{
			msg.el.dom.innerHTML=messages['blank'];
			return false;
		}
	}

	if(!field_name) field_name = 'klgi';
	var field=new Ext.form.TextField({
		name:field_name,
		fieldLabel:'Обозначение',
		allowBlank:false,
		anchor:'95%',
		maxLength:25,
		maxLengthText:'Максимальная длинна - 25 символов',
		validator:validate
	});

	this.field=new Ext.Container({
		layout:'form',
		items:[field, msg],
		anchor:'100%'
	});
	this.get=function(){
		return field.getValue();
	}
	this.set=function(value){
		num_edit=value;
		num=false;
		field.setValue(value);
	};
	this.setValue=function(value){
		field.setValue(value);
	}
	this.ReadOnly=function(){
		field.setReadOnly(true);
	}
	this.clear=function(){
		num=false;
		field.setValue();
		field.validate();
	};
}