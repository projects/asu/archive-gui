//Создание универсального окна
function FormLibWindow(conf){
	if(!conf.items) conf.items=[];
	if(!conf.View) conf.View=function(){ alert('View'); };
	if(!conf.Clear) conf.Clear=function(){ alert('Clear'); };

	if(!conf.Submit) conf.Submit=function(){ alert('Submit'); };
	if(!conf.SubmitURL) conf.SubmitURL="";
	if(!conf.SubmitMSG) conf.SubmitMSG='';
	if(!conf.SubmitTITLE) conf.SubmitTITLE='New window';

	if(!conf.Edit) conf.Edit=function(){ alert('Edit'); };
	if(!conf.EditURL) conf.EditURL="";
	if(!conf.EditMSG) conf.EditMSG='';
	if(!conf.EditTITLE) conf.EditTITLE='New window';

	if(!conf.fileUpload) conf.fileUpload=false;
	if(!conf.w_width) conf.w_width=400;
	if(!conf.w_height) conf.w_height=500;

	var winType='Submit';

	var winSubmit=function(){
		var req={};
		req.success=req.failure=winSubmitRes;
		if(winType=='Edit') req.url=conf.EditURL;
		else if(winType=='Submit'){
			req.url=conf.SubmitURL;
			if(conf.fileUpload) req.timeout=100000;
		}
		
		var win_form=winBody.getForm()
		if(win_form.isValid()){
			mask();
			win_form.submit(req);
		}
	}

	var winSubmitRes=function(form, action){
		unmask();
		if(action.result){
			if(action.result.success){
				if((action.result.success=="false") && (action.result.msg)){
					Ext.MessageBox.alert('Ошибка:', action.result.msg);
				}
				else{
					conf.Clear();
					win.hide();
					if(winType=='Submit'){
						conf.Submit(form, action);
						index.worklog.add(conf.SubmitMSG,'ok');
					}
					else if(winType=='Edit'){
						conf.Edit(form, action);
						index.worklog.add(conf.EditMSG,'ok');
					}
				}
				
			}
			else Ext.MessageBox.alert('Ошибка:', 'Ошибка сервера!');
		}
		else if((action.response) && (action.response.status=='404')) alert('404');
	}

	var winBody=new Ext.FormPanel({
		frame:true,
		fileUpload:conf.fileUpload,
		items: conf.items,
	});


	var winButtonCancel=new Ext.Button({text: 'Отмена', handler: function(){ win.hide(); }});
	var winButtonSubmit=new Ext.Button({text: 'Сохранить', handler: winSubmit});
	var win=new Ext.Window({
		plain: true,
		modal: true,
		width: conf.w_width,
		//height: conf.w_height,
		frame: true,
		autoScroll: true,
		boxMaxHeight: Ext.lib.Dom.getViewportHeight(),
		title : conf.SubmitTITLE,
		items: [winBody],
		closeAction: 'hide',
		buttons: [winButtonCancel, winButtonSubmit],
	});

	var mask=function(){ win.setWidth(conf.w_width+1); CoreMask(win.el); }
	var unmask=function(){ win.setWidth(conf.w_width); CoreUnMask(win.el); }
	this.winView=function(id,type){
		if((!type) || (type=='Submit')){
			winType='Submit';
			win.setTitle(conf.SubmitTITLE);
		}
		else if(type=='Edit'){
			winType='Edit';
			win.setTitle(conf.EditTITLE);
		}

		win.show(false);
		mask();
		conf.Clear();
		if(id) getNodeInfo(id,winViewUpd,unmask);
		else{
			win.syncSize();
			win.center();
			unmask();
		}
	}
	
	var winViewUpd=function(result){
		conf.View(result);
		win.syncSize();
		win.center();
		unmask();
	}
}