function FormDocument(){
	var object=this;


//Регистрационный номер
	var regField=new Ext.form.NumberField({name:'reg', fieldLabel:'Регистрационный номер', anchor:'95%'});
	
	var regGroup=new Ext.form.FieldSet({
		title: 'Временное хранилище',
		items:[regField],
	});
	
	
//Извещение
	var docGroup=new FormLibDocumentGroup('Document');
//Применение
	var nodeGroup=new FormLibNodeGroup();
//Основные данные
	var infoGroup=new FormLibInfoGroup('Document');
//Новый файл
	var fileGroup=new FormLibFileGroup();


//Форма
	var panel={
		Submit:function(form, action){
			index.viewer_folder.refresh(action.result.n_id,action.result.p_id);
			index.viewer_document.view(action.result.n_id);
		},
		SubmitURL:"aj_saveNewObject",
		SubmitMSG:"Создан новый документ!",
		SubmitTITLE:"Добавление нового документа",
		Edit:function(form, action){
			index.viewer_folder.refresh(action.result.n_id,action.result.p_id);
			index.viewer_document.refresh(action.result.n_id);
		},
		EditURL:"aj_editObject",
		EditMSG:"Отредактирован докумен!",
		EditTITLE:"Редактирование документа",
		fileUpload:true,
		items:[
			nodeGroup.container,
			regGroup,
			docGroup.container,
			infoGroup.container,
			fileGroup.container,
		],
	};
//	//Очистка формы
	panel.Clear=function(){
		regGroup.show();
		regField.setValue();
//	//	//Очистка проекта и изделия
		nodeGroup.clear();
//	//	//Очистка извещения
		docGroup.show();
		docGroup.clear();
		docGroup.focus();
//	//	//Очистка полей формы
		infoGroup.clear();
//	//	//Очистка поля файла
		fileGroup.show();
		fileGroup.clear();
	}
//	//Обработка загруженных данных
	panel.View=function(result){
		var pr_id=result.node.pr_id, pr_name=result.node.pr_name;
		var n_id=result.node.id, n_name=result.obj.name, n_klgi=result.obj.klgi, n_desc=result.obj.desc;
		var t_id=result.obj.t_id, t_name=result.obj.t_name;
//	//	//Заполнение проекта и изделия
		nodeGroup.set(pr_id,pr_name,n_id,n_name,n_klgi);
//	//	//Заполнение основных данных
		infoGroup.set(viewEdit,n_name,n_klgi,n_desc,t_id,t_name);
//	//	//Заполнение формы
		if(viewEdit){
//	//	//Скрытие извещения, и поля файла
			if(result.node.reg) regField.setValue(result.node.reg);
			else regGroup.hide();
			docGroup.set(CoreMergeArray(result.obj,result.node));
			fileGroup.hide();
		}
	}
//	//Объект extJS
	var win=new FormLibWindow(panel);



//Просмотр
	var viewEdit=false, winType='Submit';
	this.view=function(id,edit){
		if(edit){
			viewEdit=true;
			winType='Edit';
		}
		else{
			viewEdit=false;
			winType='Submit';
		}
		if(!id) id=false;
		win.winView(id,winType);
	}
}