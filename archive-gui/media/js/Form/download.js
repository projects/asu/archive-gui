function FormDownload(){
	var object=this;
		
//Элемент-заглушка, для авто-прокрутки страницы
	var freeSpace=new Ext.form.TextField ({}), freeSpaceEnable=true;
	var freeSpaceScroll=function(){
		if(!freeSpaceEnable) freeSpaceEnable=true;
		else{
			freeSpace.show(false);
			freeSpace.focus();
			freeSpace.hide();
		}
	}
		
//Извещение
	var docGroup=new FormLibDocumentGroup('Download');
//Поиск по КЛГИ
	var klgiGroup=new FormLibKlgiGroup(freeSpaceScroll);
//Поиск по Ноду
	var nodeGroup=new FormLibNodeGroup('Выборка по Проекту',true);
		
//Выбор режима поиск
	var choiceGroup=new Ext.form.FieldSet({
		title:'Критерий поиска',
		items:[
			{xtype: 'radiogroup',
			 hideLabel: true,
			 items: [
				{boxLabel:'КЛГИ', name:'type', inputValue:'klgi', checked:true},
				{boxLabel:'Проект', name:'type', inputValue:'node'},
			 ],
			 listeners: {
				change: function(radio, set) {
					var val=set.getGroupValue();
					
					if(val=='klgi'){
						nodeGroup.hide();
						klgiGroup.show();
					}
					else if(val=='node'){
						klgiGroup.hide();
						nodeGroup.show();
					}
					freeSpaceScroll();
				}
			 }
			}
		]
	});

//Форма
	var panel={
		Submit:function(){},
		SubmitURL:"/file/getFileInfo/",
		SubmitMSG:"Добавлен наряд на изготовление копии",
		SubmitTITLE:"Добавление наряда на изготовление копии",
		items:[
			docGroup.container,
			choiceGroup,
			klgiGroup.container,
			nodeGroup.container,
			freeSpace,
		],
	};
//	//Отправка формы
	panel.Submit=function(form, action){
		document.getElementById('downloadZIP').src='/media/out/'+action.result.name+'.zip';
		document.getElementById('downloadPDF').src='/media/out/'+action.result.name+'.doc';
	}
//	//Очистка формы
	panel.Clear=function(){
//	//	//Очистка извещения
		docGroup.clear();
//	//	//Выбор поиска по КЛГИ
		choiceGroup.get(0).setValue('klgi');
//	//	//Очистка списка КЛГИ
		klgiGroup.clear();
//	//	//Очистка проекта и изделия
		nodeGroup.clear();
		nodeGroup.hide();
//	//	//Прокрутка в начало формы
		freeSpace.hide();
		freeSpaceEnable=false;
		docGroup.focus();
	}
//	//Обработка загруженных данных
	panel.View=function(result){
		var pr_id=result.node.pr_id, pr_name=result.node.pr_name;
		var n_id=result.node.id, n_name=result.obj.name, n_klgi=result.obj.klgi;
//	//	//Выбор поиска по Ноду
		choiceGroup.get(0).setValue('node');
//	//	//Заполнение проекта и изделия
		nodeGroup.set(pr_id,pr_name,n_id,n_name,n_klgi);
//	//	//Показ и прокрутка в начало формы
		freeSpaceEnable=false;
		docGroup.focus();
	}
//	//Объект extJS
	var win=new FormLibWindow(panel);



//Просмотр
	this.view=function(id){
		if(!id) id=false;
		win.winView(id);
	}
}