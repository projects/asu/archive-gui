function FormFile(){
	var object=this;
		
//Элемент-заглушка, для авто-прокрутки страницы
	var freeSpace=new Ext.form.TextField ({}), freeSpaceEnable=true;
	var freeSpaceScroll=function(){
		if(!freeSpaceEnable) freeSpaceEnable=true;
		else{
			freeSpace.show(false);
			freeSpace.focus();
			freeSpace.hide();
		}
	}


//Регистрационный номер
	var regField=new Ext.form.NumberField({name:'reg', fieldLabel:'Регистрационный номер', anchor:'95%'});
	
	var regGroup=new Ext.form.FieldSet({
		title: 'Временное хранилище',
		items:[regField],
	});

//Извещение
	var docGroup=new FormLibDocumentGroup('File');
//Изменения
	var filesGroup=new FormLibKlgiGroup(freeSpaceScroll,'Изменения',true,true);
//Новый файл
	var fileGroup=new FormLibFileGroup();

//Выбор проекта
	var projectCombo=new Ext.form.ComboBox({
		name: "pname",
		valueField: 'id',
		hiddenName:'pid',
		hiddenValue:'id',
		store: index.store_projects,
		triggerAction: 'all',
		emptyText:'Выберите проект',
		minChars: 1,
		forceSelection: true,
		valueField: 'id',
		displayField: "name",
		hideLabel: true,
		allowBlank:false,
		mode:'local',
		loadingText: 'Идет запрос на сервер...',
		anchor:'95%',
		listeners: {
			select: function() {
				var pid=projectCombo.getValue();
				filesGroup.clear();
				filesGroup.ComboStore.setBaseParam('idproject',pid);
				filesGroup.ComboStore.load({params:{'idproject':pid}});
			}
		}
	});
	var projectGroup=new Ext.form.FieldSet({
		title: 'Проект',
		defaultType: 'textfield',
		items:[projectCombo]
	});


//Форма
	var panel={
		Submit:function(){},
		SubmitURL:"aj_ChangeAdd",
		SubmitMSG:"Введено извещение!",
		SubmitTITLE:"Ввод извещения",
		fileUpload:true,
		items:[
			projectGroup,
			regGroup,
			fileGroup.container,
			docGroup.container,
			filesGroup.container,
			freeSpace,
		],
	};
//	//Очистка формы
	panel.Clear=function(){
		regGroup.show();
		regField.setValue();
//	//	//Очистка проекта
		projectCombo.clearValue();
		projectCombo.validate();
		projectCombo.setReadOnly(false);
//	//	//Очистка извещения
		docGroup.clear();
//	//	//Очистка изменений
		filesGroup.clear();
//	//	//Прокрутка в начало формы
		freeSpace.hide();
		freeSpaceEnable=false;
		fileGroup.clear();
		docGroup.focus();
	}
//	//Обработка загруженных данных
	panel.View=function(result){
		var pr_id=result.node.pr_id, n_id=result.node.id;
		projectCombo.setValue(pr_id);
		projectCombo.setReadOnly(true);
		filesGroup.ComboStore.setBaseParam('idproject',pr_id);
		filesGroup.ComboStore.load({params:{'idproject':pr_id}});
		filesGroup.set(n_id);
	}
//	//Объект extJS
	var win=new FormLibWindow(panel);



//Просмотр
	this.view=function(id){
		if(!id) id=false;
		win.winView(id);
	}
}