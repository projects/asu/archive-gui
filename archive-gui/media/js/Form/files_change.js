function FormFilesChange(){
	var object=this;
	var selectedId=null;

//Элемент-заглушка, для авто-прокрутки страницы
	var freeSpace=new Ext.form.TextField ({}), freeSpaceEnable=true;
	var freeSpaceScroll=function(){
		if(!freeSpaceEnable) freeSpaceEnable=true;
		else{
			freeSpace.show(false);
			freeSpace.focus();
			freeSpace.hide();
		}
	}

	var fileChange=new Ext.form.TextField({name:'change', hidden:true});
//Изменения
	var filesGroup=new FormLibKlgiGroupAdd(freeSpaceScroll,'Изменения',true,true);
	
	var filesContent=new FormLibKlgiContent(freeSpaceScroll,'Содержание');
	
//Новый файл
	var fileGroup=new FormLibFileGroup();

//Форма
	var panel={
		Submit:function(form, action){ index.files.refresh(); },
		SubmitURL:"aj_ChangeAddFile",
		SubmitMSG:"Документы изменены!",
		SubmitTITLE:"Изменение документов в извещении",
		fileUpload:true,
		width: 500,
		items:[
			fileChange,
			filesGroup.container,
			filesContent.container
		],
	};
//	//Очистка формы
	panel.Clear=function(){
		fileGroup.clear();
		filesGroup.clear();
	}
//	//Обработка загруженных данных
	panel.View=function(result){ }
//	//Объект extJS
	var win=new FormLibWindow(panel);

//Просмотр
	this.view=function(id, project_id){
		filesGroup
		filesContent
		fileChange.setValue(id);
		
		filesGroup.ComboStore.setBaseParam('idproject',project_id);
		filesGroup.ComboStore.load({params:{'idproject':project_id}});
		
		filesContent.store.setBaseParam('query', id);
		filesContent.store.load();
		win.winView(0,'Submit');
	};
}