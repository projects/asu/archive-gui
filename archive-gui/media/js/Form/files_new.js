function FormFilesFile(){
	var object=this;

	var fileField=new Ext.form.TextField({name:'file_id', hidden:true});
//Новый файл
	var fileGroup=new FormLibFileGroup();


//Форма
	var panel={
		Submit:function(form, action){ index.files.refresh(); },
		SubmitURL:"aj_FilesNew",
		SubmitMSG:"Заменён файл в хранилище!",
		SubmitTITLE:"Замена файла в хранилище",
		fileUpload:true,
		items:[
			fileField,
			fileGroup.container,
		],
	};
//	//Очистка формы
	panel.Clear=function(){
		fileGroup.clear();
	}
//	//Обработка загруженных данных
	panel.View=function(result){ }
//	//Объект extJS
	var win=new FormLibWindow(panel);

//Просмотр
	this.view=function(id){
		win.winView(0,'Submit');
		fileField.setValue(id);
	}
}