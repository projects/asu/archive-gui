function FormFolder(){
	var object=this;

	var idField=new Ext.form.TextField({name:'node',hidden:true,emptyText:1});
//Применение
	var nodeGroup=new FormLibNodeGroup();
//Основные данные
	var infoGroup=new FormLibInfoGroup('Folder');


//Форма
	var panel={
		Submit:function(form, action){
			index.viewer_navigation.tree_view(action.result.parent);
			index.viewer_folder.refresh(action.result.node,action.result.parent);
		},
		SubmitURL:"aj_FolderAdd",
		SubmitMSG:"Создано новое изделие!",
		SubmitTITLE:"Добавление нового изделия",
		Edit:function(form, action){
			index.viewer_navigation.tree_view(action.result.parent);
			index.viewer_folder.refresh(action.result.node,action.result.parent);
		},
		EditURL:"aj_FolderEdit",
		EditMSG:"Отредактировано изделие!",
		EditTITLE:"Редактирование изделия",
		items:[
			idField,
			nodeGroup.container,
			infoGroup.container,
		],
	};
//	//Очистка формы
	panel.Clear=function(){
		idField.setValue(1);
//	//	//Очистка проекта и изделия
		nodeGroup.clear();
//	//	//Очистка полей формы
		infoGroup.clear();
	}
//	//Обработка загруженных данных
	panel.View=function(result){
		var pr_id=result.node.pr_id, pr_name=result.node.pr_name;
		var n_id=result.node.id, n_name=result.obj.name, n_klgi=result.obj.klgi, n_desc=result.obj.desc;
//	//	//Заполнение проекта и изделия
		nodeGroup.set(pr_id,pr_name,n_id,n_name,n_klgi);
//	//	//Заполнение основных данных
		if(viewEdit) idField.setValue(n_id);
		else n_klgi='';
		infoGroup.set(viewEdit,n_name,n_klgi,n_desc);
	}
//	//Объект extJS
	var win=new FormLibWindow(panel);



//Просмотр
	var viewEdit=false, winType='Submit';
	this.view=function(id,edit){
		if(edit){
			viewEdit=true;
			winType='Edit';
		}
		else{
			viewEdit=false;
			winType='Submit';
		}
		if(!id) id=false;
		win.winView(id,winType);
	}
}