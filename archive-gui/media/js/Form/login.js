function FormLogin(){
	var object=this;
	var user_name=0;
	
	var loginField=new Ext.form.TextField({fieldLabel:'Имя пользователя', name:'username', allowBlank:false, anchor:'95%'});
	var passField=new Ext.form.TextField({fieldLabel:'Пароль', name:'password', inputType:'password', allowBlank:false, anchor:'95%'});
	var helpLabel=new Ext.form.Label({
		html:'<center>В связи с неактивностью и в целях обеспечения безопасности,'+
		'<br>был произведён автоматический выход из системы!'+
		'<br>Для продолжения работы необходим повторный вход.<br><br></center>',
		style: 'color:red; font-weight: bold;',
		anchor:'95%'
	});	
	var errorLabel=new Ext.form.Label({
		html:'<center>Имя пользователя или пароль не подходят.<br>Пожалуйста, попробуйте еще раз.<br><br></center>',
		style: 'color:red; font-weight: bold;',
		anchor:'95%'
	});

	var winBody=new Ext.FormPanel({
		frame:true,
		style:'align:center;',
		items: [
			helpLabel,
			errorLabel,
			loginField,
			passField
		],
	});

	var winSubmit=function(){
		if(loginField.isValid() && passField.isValid()){
			user_name=loginField.getValue();
			mask();
			CoreAJAX({
				url:'aj_login',
				param:{'login':user_name,'pass':passField.getValue()},
				succ:winSubmitRes,
				fail:winSubmitRes
			});
		}
	}

	var winSubmitRes=function(result){
		if(result.success=="true"){
			if(archive_user_name!=user_name){
				document.location.href = document.location.href;
			}
			else{
				loginField.setValue();
				loginField.validate();
				passField.setValue();
				passField.validate();
				win.hide();
			}
		}
		else{
			helpLabel.hide();
			errorLabel.show();
			win.syncSize();
		}
		unmask();
	}

	var winButtonSubmit=new Ext.Button({text: 'Войти', handler: winSubmit});
	var win=new Ext.Window({
		closable: false,
		draggable: false,
		resizable: false,
		plain: true,
		modal: true,
		autoHeight: true,
		width: 400,
		frame: true,
		title: 'Вход в систему',
		items: [winBody],
		buttons:[winButtonSubmit],
	});
	
	var mask=function(){ CoreMask(win.el); }
	var unmask=function(){ CoreUnMask(win.el); }
	this.view=function(help){
		win.show();
		if(help) helpLabel.show();
		else helpLabel.hide();
		errorLabel.hide();
		win.syncSize();
	}
}