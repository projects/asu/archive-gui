function FormMove(){
	var object=this;

	var fromGroup=new FormLibNodeGroup('Перемещаемое изделие',true);
	var toGroup=new FormLibNodeGroup('Применение',false,'to_');

//Форма
	var panel={
		Submit:function(form, action){
			index.viewer_navigation.tree_view(action.result.node);
			index.viewer_navigation.tree_view(action.result.parent);
		},
		SubmitURL:"aj_moveNode",
		SubmitMSG:"Изделие перенесено!",
		SubmitTITLE:"Перемещение изделия",
		items:[
			fromGroup.container,
			toGroup.container,
		],
	};
//	//Очистка формы
	panel.Clear=function(){
		fromGroup.clear();
		toGroup.clear();
	}
//	//Обработка загруженных данных
	panel.View=function(result){
		var pr_id=result.node.pr_id, pr_name=result.node.pr_name;
		var n_id=result.node.id, n_name=result.obj.name, n_klgi=result.obj.klgi;
//	//	//Заполнение основных данных
		fromGroup.set(pr_id,pr_name,n_id,n_name,n_klgi);
	}
//	//Объект extJS
	var win=new FormLibWindow(panel);



//Просмотр
	var viewEdit=false, winType='Submit';
	this.view=function(id){
		if(!id) id=false;
		win.winView(id);
	}
}