function FormProject(){
	var object=this;

	var idField=new Ext.form.TextField({name:'node',hidden:true,emptyText:1});
//Основные данные
	var infoGroup=new FormLibInfoGroup('Project');


//Форма
	var panel={
		Submit:function(form, action){
			index.store_projects.load();
			index.viewer_navigation.tree_view(1);
			index.viewer_folder.refresh(action.result.node,1);
		},
		SubmitURL:"aj_ProjectAdd",
		SubmitMSG:"Создан новый проект!",
		SubmitTITLE:"Добавление нового проекта",
		Edit:function(form, action){
			index.store_projects.load();
			index.viewer_navigation.tree_view(1);
			index.viewer_folder.refresh(action.result.node,1);
		},
		EditURL:"aj_ProjectEdit",
		EditMSG:"Отредактирован проект!",
		EditTITLE:"Редактирование проекта",
		items:[
			idField,
			infoGroup.container,
		],
	};
//	//Очистка формы
	panel.Clear=function(){
		idField.setValue(1);
//	//	//Очистка полей формы
		infoGroup.clear();
	}
//	//Обработка загруженных данных
	panel.View=function(result){
		var n_id=result.node.id, n_name=result.obj.name, n_desc=result.obj.desc;
//	//	//Заполнение основных данных
		if(viewEdit){
			infoGroup.set(viewEdit,n_name,'n_klgi',n_desc);
			idField.setValue(n_id);
		}
	}
//	//Объект extJS
	var win=new FormLibWindow(panel);



//Просмотр
	var viewEdit=false, winType='Submit';
	this.view=function(id){
		if(id!=1){
			viewEdit=true;
			winType='Edit';
		}
		else{
			viewEdit=false;
			winType='Submit';
		}
		if(!id) id=false;
		win.winView(id,winType);
	}
}