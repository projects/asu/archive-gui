function FormTChangeApply(){
	var object=this;

	var changeField=new Ext.form.TextField({name:'change', hidden:true});
	var invField=new FormLibInvField();

//Форма
	var panel={
		Submit:function(form, action){ index.tchange.refresh(); },
		SubmitURL:"aj_TChangeApply",
		SubmitMSG:"Извещение подтверждено, изменения приняты в архив!",
		SubmitTITLE:"Принятие изменений в архив",
		fileUpload:true,
		items:[
			changeField,
			invField.field,
		],
	};
//	//Очистка формы
	panel.Clear=function(){
		changeField.setValue();
		invField.clear();
	}
//	//Обработка загруженных данных
	panel.View=function(result){ }
//	//Объект extJS
	var win=new FormLibWindow(panel);

//Просмотр
	this.view=function(id){
		win.winView(0,'Submit');
		changeField.setValue(id);
	}
}