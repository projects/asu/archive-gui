function FormTStorageApply(){
	var object=this;

	var nodeField=new Ext.form.TextField({name:'node', hidden:true});
	var invField=new FormLibInvField();

//Форма
	var panel={
		Submit:function(form, action){ index.temp.refresh(); },
		SubmitURL:"aj_TStorageApply",
		SubmitMSG:"Документ перемещён в архив!",
		SubmitTITLE:"Перемещение документа в архив",
		fileUpload:true,
		items:[
			nodeField,
			invField.field,
		],
	};
//	//Очистка формы
	panel.Clear=function(){
		nodeField.setValue();
		invField.clear();
	}
//	//Обработка загруженных данных
	panel.View=function(result){ }
//	//Объект extJS
	var win=new FormLibWindow(panel);

//Просмотр
	this.view=function(id){
		win.winView(0,'Submit');
		nodeField.setValue(id);
	}
}