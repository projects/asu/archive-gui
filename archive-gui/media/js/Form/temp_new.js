function FormTStorageFile(){
	var object=this;

	var nodeField=new Ext.form.TextField({name:'node', hidden:true});
	
//Регистрационный номер
	var regField=new Ext.form.NumberField({name:'reg', fieldLabel:'Регистрационный номер', allowBlank:false, anchor:'95%'});
	
	var regGroup=new Ext.form.FieldSet({
		title: 'Временное хранилище',
		items:[regField],
	});

//Новый файл
	var fileGroup=new FormLibFileGroup();


//Форма
	var panel={
		Submit:function(form, action){ index.temp.refresh(); },
		SubmitURL:"aj_TStorageNew",
		SubmitMSG:"Обновлён файл во временном хранилище!",
		SubmitTITLE:"Обновление файла во временном хранилище",
		fileUpload:true,
		items:[
			nodeField,
			regGroup,
			fileGroup.container,
		],
	};
//	//Очистка формы
	panel.Clear=function(){
		regField.setValue('');
		fileGroup.clear();
	}
//	//Обработка загруженных данных
	panel.View=function(result){ }
//	//Объект extJS
	var win=new FormLibWindow(panel);

//Просмотр
	this.view=function(id){
		win.winView(0,'Submit');
		nodeField.setValue(id);
	}
}