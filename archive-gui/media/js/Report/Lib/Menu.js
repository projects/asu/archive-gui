var ReportMenu=function() {
	var obj_id=0;

	var menuChange=new Ext.menu.Menu({
		items: [
			{itemId:'view_change',
			 text:'Показать Извещение',
			 handler:function(){ 
				var win = window.open(
					"/printer/change?id="+obj_id,
					"Извещение",
					"width:27cm,status=no"
				);
				win.focus()
			 },
			},
			{itemId:'view',
			 text:'Посмотреть файл',
			 handler:function(){
				CoreAJAX({
					url:'/file/getAllFile/',
					param:{'change_id':obj_id},
					succ: function(res){
						if ( res.file ){
							var win = window.open(
								"/media/out/"+res.file,
								"",
								"width:27cm,status=no"
							);
							win.focus();
						}
					}
				});
			 }
			}
		]
	});

	var menuRequest=new Ext.menu.Menu({
		items: [
			{itemId:'view_request',
			 text:'Показать Наряд',
			 handler:function(){ 
				var win = window.open(
					"/printer/request?id="+obj_id,
					"Наряд",
					"width:27cm,status=no"
				);
				win.focus()
			 },
			},
		]
	});

	this.view=function(type,id,XY){
		var menu;
		
		if(type && id){
			obj_id=id;	
			if(type=='change') menu=menuChange;
			else if(type=='request') menu=menuRequest;
			
			if(menu) menu.showAt(XY);
		}
	}
}