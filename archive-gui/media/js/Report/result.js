function ReportResult(){
	var object=this;

	var grid_conf={
		title: 'Отчёт',
		url: "aj_getReport",
		fields: ['id','type','date','number','inv','group','autor','format','sheets','base','msg'],
		page: index.report_config.result_page,
		expander: new Ext.XTemplate(
			'<p><br><tpl if="base"><b>Основание</b>: {base}<br></tpl>'+
			'<b>Комментарий</b>: {msg}</p>'
		),
		column:[
			{name: "Дата", field: 'date', width: 30},
			{name: "Номер", field: 'number', width: 50,
			 names: {'request':'Номер наряда','change':'Номер извещения'}, param_rename: 'type'},
			{name: "№", field: 'inv', width: 50,
			 names: {'request':'Заказ №','change':'Инвентарный номер'}, param_rename: 'type'},
			{name: "Отдел/Цех", field: 'group', width: 50},
			{name: "Сотрудник", field: 'autor', width: 50,
			 names: {'request':'Затребовал','change':'Составил'}, param_rename: 'type', param_hide:'autor'},
			{name: "Формат листов", field: 'format', width: 50},
			{name: "Кол-во листов", field: 'sheets', width: 50},
		],
		request:[
			{name: "Тип", param: 'type', type: 'id', store: index.store_report_types, store_field:'eng'},
			{name: "Сотрудник", param: 'autor', type: 'id', store: index.store_autors,
			 names: {'request':'Затребовал','change':'Составил'}, param_rename: 'type'},
			{name: "С", param: 'dateFrom', type: 'date'},
			{name: "До", param: 'dateTo', type: 'date'},
		],
		rowcontext: function(row, evtObj){
			index.report_menu.view(row.get('type'),row.get('id'),evtObj.getXY());
		},
	};
	
	var grid=new ConstructorGridResult(grid_conf);

	this.region=new Ext.Container({
		layout:'fit',
		region:'center',
		items:[grid.grid]
	});
	this.view=grid.load;
}