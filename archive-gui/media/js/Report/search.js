function ReportSearch(){
	var object=this;
		
	//Выбор тип отчёта
	var typeCombo=new Ext.form.ComboBox({
		name: "tname",
		valueField: 'eng',
		hiddenName:'type',
		store: index.store_report_types,
		triggerAction: 'all',
		emptyText:'Выберите тип отчёта',
		allowBlank:false,
		mode:'local',
		editable: false,
		displayField: 'name',
		fieldLabel: 'Тип отчёта',
		value:'request',
		anchor:'95%',
	});
	
	//Выбор сотрудника
	var autorCombo=new Ext.form.ComboBox({
		name: "aname",
		valueField: 'id',
		hiddenName:'autor',
		store: index.store_autors,
		triggerAction: 'all',
		emptyText:'Выберите сотрудника',
		mode:'local',
		editable: false,
		displayField: 'name',
		fieldLabel: 'Сотрудник',
		anchor:'95%',
	});
	
	//Группа параметров отчёта	
	var typeGroup=new Ext.form.FieldSet({
		style: 'padding:0 5px; margin:0 5px;',
		title: 'Параметры отчёта',
		items:[
			typeCombo,
			autorCombo
		],
	});
	
	//Группа выбора даты
	var dateGroup=new CoreLibDateGroup();


	//Кнопка очистки поиска
	var resetButton={
		text: 'Сбросить',
		handler: function() {
			typeCombo.setValue('request');
			autorCombo.clearValue();
			dateGroup.autoSet();
		}
	}
	//Кнопка поиска
	var searchButton={
		text: 'Поиск',
		handler: function(){			
			var type=typeCombo.getValue();
			var autor=autorCombo.getValue();
			var date_from=dateGroup.fromValue();
			var date_to=dateGroup.toValue();
			
			
			if(type!=''){
				var param={};
				
				param.type=type;
				if(autor!='') param.autor=autor;
				if(date_from) param.dateFrom=date_from;
				if(date_to) param.dateTo=date_to+24*60*60-1;
			
				index.report_result.view(param);
			}
		}
	}

//Объект extJS для рендера
	this.region=new Ext.FormPanel({
		title: 'Поиск',
		region:'north',
		height:'150',
		animCollapse: false,
		collapsible: true,
		collapseMode: 'mini',
		items:[
			{layout: 'column',
			 border: false,
			 items:[typeGroup,dateGroup.group],
			 anchor:'100% 100%'
			}
		],
		buttons:[resetButton,searchButton]
	});
}