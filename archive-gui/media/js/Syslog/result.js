function SyslogResult(){
	var object=this;

	var grid_conf={
		title: 'Журнал',
		url: "aj_getTestResults",
		fields: ['id','date','idtest','name','status_code','status_message','items','items_count'],
		page: index.report_config.result_page,
		expander: new Ext.XTemplate(
			'<p>'+
				'<tpl if="status_message"><br><b>Сообщение</b>: {status_message}</tpl>'+
				'<tpl if="items_count"><br><b>Содержание</b></tpl>'+
				'<tpl for="items">'+
					'<br>&nbsp; &nbsp; &nbsp;<b>{label}</b>: {name}'+
				'</tpl>'+
			'</p>'
		),
		column:[
			{name: "ID", field: 'id', width: 10},
			{name: "Дата", field: 'date'},
			{name: "Тест", field: 'name'},
			{name: "Результат", field: 'status_code',
			 renderer: function (c){
				if(c==0) return '<font color="green">OK</font>';
				else if(c==1) return '<font color="orange">Warning</font>';
				else if(c==2) return '<b><font color="red">Error</font></b>';
			 }
			},
		],
		request:[
			{name: "Объект", param: 'obj', type: 'text'},
			{name: "Тип Объекта", param: 'tobj', type: 'id', store: index.store_hobjects_types},
			{name: "Тип Теста", param: 'ttest', type: 'id', store: index.store_test_types},
			{name: "С", param: 'dateFrom', type: 'date'},
			{name: "До", param: 'dateTo', type: 'date'},
		],
	};
	
	var grid=new ConstructorGridResult(grid_conf);

	this.region=new Ext.Container({
		layout:'fit',
		region:'center',
		items:[grid.grid]
	});
	this.view=grid.load;
}