function SyslogSearch(){
	var object=this;
		
	//Выбор типа объекта
	var tobjCombo=new Ext.form.ComboBox({
		name: "obj_tname",
		valueField: 'id',
		hiddenName:'obj_type',
		store: index.store_hobject_types,
		triggerAction: 'all',
		emptyText:'Выберите тип объектов',
		mode:'local',
		editable: false,
		displayField: 'name',
		fieldLabel: 'Тип объектов',
		anchor:'95%',
		listeners: {
			select: function() { objCombo.clearValue(); }
		},
		disabled:true,
	});
	//Выбор объекта
	var objCombo=new Ext.form.ComboBox({
		name: "oname",
		valueField: 'id',
		hiddenName:'obj',
		store: index.store_hobjects,
		triggerAction: 'all',
		emptyText:'Выберите объект',
		mode:'local',
		editable: false,
		displayField: 'name',
		fieldLabel: 'Объект',
		anchor:'95%',
		listeners: {
			select: function() { tobjCombo.clearValue(); }
		},
	});	
	//Группа выбора объекта	
	var typeGroup=new Ext.form.FieldSet({
		style: 'padding:0 5px; margin:0 5px;',
		title: 'Объект',
		items:[
			tobjCombo,
			objCombo
		],
	});
	
	//Выбор типа объекта
	var ttestCombo=new Ext.form.ComboBox({
		name: "t_tname",
		valueField: 'id',
		hiddenName:'t_type',
		store: index.store_test_types,
		triggerAction: 'all',
		emptyText:'Выберите тип тестов',
		mode:'local',
		editable: false,
		displayField: 'name',
		fieldLabel: 'Тип тестов',
		anchor:'95%',
	});
	var resCheck=new Ext.form.CheckboxGroup({
		fieldLabel: 'Результат',
		items: [
			{boxLabel: '<font color="green">OK</font>', name: 'res0', checked: true},
			{boxLabel: '<font color="orange">Warning</font>', name: 'res1', checked: true},
			{boxLabel: '<b><font color="red">Error</font></b>', name: 'res2', checked: true},
		],
	});
	//Группа выбора теста	
	var testGroup=new Ext.form.FieldSet({
		style: 'padding:0 5px; margin:0 5px;',
		title: 'Тест',
		items:[
			ttestCombo,
			resCheck
		],
		width:'350'
	});	
	
	//Группа выбора даты
	var dateGroup=new CoreLibDateGroup();


	//Кнопка очистки поиска
	var resetButton={
		text: 'Сбросить',
		handler: function() {
			tobjCombo.clearValue();
			objCombo.clearValue();
			ttestCombo.clearValue();
			resCheck.setValue([true,true,true]);
			dateGroup.autoSet();
		}
	}
	//Кнопка поиска
	var searchButton={
		text: 'Поиск',
		handler: function(){			
			var tobj=tobjCombo.getValue();
			var obj=objCombo.getValue();
			var ttest=ttestCombo.getValue();
			var res=resCheck.getValue();
			var date_from=dateGroup.fromValue();
			var date_to=dateGroup.toValue();
			
			var param={};
			
			if(obj!='') param.obj=obj;
			else if(tobj!='') param.tobj=tobj;
			if(ttest!='') param.ttest=ttest;
			if(res[0]) param[res[0].getName()]=1;
			if(res[1]) param[res[1].getName()]=1;
			if(res[2]) param[res[2].getName()]=1;
			if(date_from) param.dateFrom=date_from;
			if(date_to) param.dateTo=date_to+24*60*60-1;
			
			index.syslog_result.view(param);
		}
	}

//Объект extJS для рендера
	this.region=new Ext.FormPanel({
		title: 'Поиск',
		region:'north',
		height:'150',
		animCollapse: false,
		collapsible: true,
		collapseMode: 'mini',
		items:[
			{layout: 'column',
			 border: false,
			 items:[typeGroup,testGroup,dateGroup.group],
			 anchor:'100% 100%'
			}
		],
		buttons:[resetButton,searchButton]
	});
}