function TimeResult(){
	var object=this;

	var grid_conf={
		title: 'Сессии',
		url: "aj_getUserSession",
		fields: ['id','start','finish','user','closed'],
		page: index.time_config.result_page,
		column:[
			{name: "ID", field: 'id', width: 10},
			{name: "Начало сессии", field: 'start'},
			{name: "Последнее время", field: 'finish'},
			{name: "Пользователь", field: 'user'},
			{name: "Статус", field: 'closed',
			 renderer: function (c){
				if(c==0) return '<font color="green">Открыта</font>';
				else if(c==1) return '<font color="orange">Закрыта</font>';
			 }
			},
		],
		request:[
			{name: "Пользователь", param: 'tobj', type: 'id', store: index.store_users},
			{name: "С", param: 'dateFrom', type: 'date'},
			{name: "До", param: 'dateTo', type: 'date'},
		],
	};
	
	var grid=new ConstructorGridResult(grid_conf);

	this.region=new Ext.Container({
		layout:'fit',
		region:'center',
		items:[grid.grid]
	});
	this.view=grid.load;
}