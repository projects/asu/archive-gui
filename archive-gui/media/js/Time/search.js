function TimeSearch(){
	var object=this;
		
	//Выбор статус сессии
	var closedCheck=new Ext.form.CheckboxGroup({
		fieldLabel: 'Статус',
		items: [
			{boxLabel: '<font color="green">Открыта</font>', name: 'closed0', checked: true},
			{boxLabel: '<font color="orange">Закрыта</font>', name: 'closed1', checked: true},
		],
	});
	
	//Выбор пользователя
	var userCombo=new Ext.form.ComboBox({
		name: "aname",
		valueField: 'id',
		hiddenName:'user',
		store: index.store_users,
		triggerAction: 'all',
		emptyText:'Выберите пользователя',
		mode:'local',
		editable: false,
		displayField: 'name',
		fieldLabel: 'Пользователь',
		anchor:'95%',
	});
	
	//Группа параметров поиска	
	var typeGroup=new Ext.form.FieldSet({
		style: 'padding:0 5px; margin:0 5px;',
		title: 'Параметры отчёта',
		items:[
			userCombo,
			closedCheck
		],
	});
	
	//Группа выбора даты
	var dateGroup=new CoreLibDateGroup();


	//Кнопка очистки поиска
	var resetButton={
		text: 'Сбросить',
		handler: function() {
			closedCheck.setValue([true,true]);
			userCombo.clearValue();
			dateGroup.autoSet();
		}
	}
	//Кнопка поиска
	var searchButton={
		text: 'Поиск',
		handler: function(){
			window.closedCheck=closedCheck;
			var cl=closedCheck.getValue();
			var user=userCombo.getValue();
			var date_from=dateGroup.fromValue();
			var date_to=dateGroup.toValue();
			
			var param={};
			
			if(user!='') param.user=user;
			if(cl[0]) param[cl[0].getName()]=1;
			if(cl[1]) param[cl[1].getName()]=1;
			window.cl=cl;
			if(date_from) param.dateFrom=date_from;
			if(date_to) param.dateTo=date_to+24*60*60-1;
			
			index.time_result.view(param);
		}
	}

//Объект extJS для рендера
	this.region=new Ext.FormPanel({
		title: 'Поиск',
		region:'north',
		height:'150',
		animCollapse: false,
		collapsible: true,
		collapseMode: 'mini',
		items:[
			{layout: 'column',
			 border: false,
			 items:[typeGroup,dateGroup.group],
			 anchor:'100% 100%'
			}
		],
		buttons:[resetButton,searchButton]
	});
}