var ViewerBar=function() {
	var node_id=0;


	var barRoot=new Ext.ButtonGroup({
		frame:false,
		items: [
			{itemId:'add_project',
			 text:'Добавить проект',
			 handler:function(){ index.form_project.view(node_id); },
			 disabled: archive_groups.project ? false : true
			},{xtype: 'tbseparator'},
			{itemId:'add_change',
			 text:'Внести извещение',
			 handler:function(){ index.form_file.view(); }
			},
		]
	});

	var barProject=new Ext.ButtonGroup({
		frame:false,
		items: [
			{itemId:'opis_project',
			 text:'Опись проекта',
			 handler:function(){ 
				var win = window.open(
					"/printer/opis?id="+node_id,
					"Опись",
					"width:27cm,status=no"
				);
				win.focus()
			 },
			},{xtype: 'tbseparator'},
			{itemId:'edit_project',
			 text:'Редактировать проект',
			 handler:function(){ index.form_project.view(node_id); },
			 disabled: archive_groups.project ? false : true
			},{xtype: 'tbseparator'},
			{itemId:'add_change',
			 text:'Внести извещение',
			 handler:function(){ index.form_file.view(); }
			},{xtype: 'tbseparator'},
			{itemId:'add_folder',
			 text:'Добавить изделие',
			 handler:function(){ index.form_folder.view(node_id); },
			 disabled: archive_groups.project ? false : true
			},{xtype: 'tbseparator'},
			{itemId:'get_project',
			 text:'Выдать копию документов проекта',
			 handler:function(){ index.form_download.view(node_id); },
			},
		]
	});

	var barFolder=new Ext.ButtonGroup({
		frame:false,
		items: [
			{itemId:'opis_folder',
			 text:'Опись изделия',
			 handler:function(){ 
				var win = window.open(
					"/printer/opis?id="+node_id,
					"Опись",
					"width:27cm,status=no"
				);
				win.focus()
			 },
			},{xtype: 'tbseparator'},
			{itemId:'edit_folder',
			 text:'Редактировать изделие',
			 handler:function(){ index.form_folder.view(node_id,true); },
			},{xtype: 'tbseparator'},
			{itemId:'add_change',
			 text:'Внести извещение',
			 handler:function(){ index.form_file.view(); }
			},{xtype: 'tbseparator'},
			{text: 'Добавить',
			 menu: [
				{itemId:'add_folder',
				 text:'Добавить изделие',
				 handler:function(){ index.form_folder.view(node_id); },
				},
				{itemId:'add_document',
				 text:'Добавить документ',
				 handler:function(){ index.form_document.view(node_id); },
				}
			 ]
			},{xtype: 'tbseparator'},
			{itemId:'get_project',
			 text:'Выдать копию документов изделия',
			 handler:function(){ index.form_download.view(node_id); },
			},{xtype: 'tbseparator'},
			{itemId:'copy_folder',
			 text:'Скопировать изделие в другой проект',
			 handler:function(){ index.form_copy.view(node_id); },
			},{xtype: 'tbseparator'},
			{itemId:'move_folder',
			 text:'Переместить изделие',
			 handler:function(){ index.form_copy.view(node_id); },
			},
		]
	});

	var barDocument=new Ext.ButtonGroup({
		frame:false,
		items: [
			{itemId:'edit_document',
			 text:'Редактировать документ',
			 handler:function(){ index.form_document.view(node_id,true); },
			},{xtype: 'tbseparator'},
			{itemId:'change_document',
			 text:'Внести изменение',
			 handler:function(){ index.form_file.view(node_id); },
			},{xtype: 'tbseparator'},
			{itemId:'get_document',
			 text:'Выдать копию документа',
			 handler:function(){ index.form_download.view(node_id); },
			},
		]
	});

	this.bar=new Ext.Toolbar({
		hideBorders:true,
		items: [
			barRoot,
			barProject,
			barFolder,
			barDocument,
		]
	});
		
	this.view=function(id,project,havechildren,XY){
		barRoot.hide();
		barProject.hide();
		barFolder.hide();
		barDocument.hide();

		if(id){
			node_id=id;	
			if(id=='1') barRoot.show(false);
			else if(project) barProject.show();
			else if(havechildren) barFolder.show();
			else barDocument.show();
		}
	}
}