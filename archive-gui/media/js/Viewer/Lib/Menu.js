var ViewerMenu=function() {
	var node_id=0;


	var menuRoot=new Ext.menu.Menu({
		items: [
			{itemId:'add_project',
			 text:'Добавить проект',
			 handler:function(){ index.form_project.view(node_id); },
			 disabled: archive_groups.project ? false : true
			},'-',
			{itemId:'add_change',
			 text:'Внести извещение',
			 handler:function(){ index.form_file.view(); }
			},
		]
	});

	var menuProject=new Ext.menu.Menu({
		items: [
			{itemId:'info_project',
			 text:'Информация о проекте',
			 handler:function(){ index.viewer_folder.view(node_id,'Node'); },
			},
			{itemId:'opis_project',
			 text:'Опись проекта',
			 handler:function(){ 
				var win = window.open(
					"/printer/opis?id="+node_id,
					"Опись",
					"width:27cm,status=no"
				);
				win.focus()
			 },
			},
			{itemId:'edit_project',
			 text:'Редактировать проект',
			 handler:function(){ index.form_project.view(node_id,true); },
			 disabled: archive_groups.project ? false : true
			},'-',
			{itemId:'add_change',
			 text:'Внести извещение',
			 handler:function(){ index.form_file.view(); }
			},
			{itemId:'add_folder',
			 text:'Добавить изделие',
			 handler:function(){ index.form_folder.view(node_id); },
			 disabled: archive_groups.project ? false : true
			},'-',
			{itemId:'get_project',
			 text:'Выдать копию документов проекта',
			 handler:function(){ index.form_download.view(node_id); },
			},
		]
	});

	var menuFolder=new Ext.menu.Menu({	
		items: [
			{itemId:'info_folder',
			 text:'Информация о изделии',
			 handler:function(){ index.viewer_folder.view(node_id,'Node'); },
			},
			{itemId:'opis_folder',
			 text:'Опись изделия',
			 handler:function(){ 
				var win = window.open(
					"/printer/opis?id="+node_id,
					"Опись",
					"width:27cm,status=no"
				);
				win.focus()
			 },
			},
			{itemId:'edit_folder',
			 text:'Редактировать изделие',
			 handler:function(){ index.form_folder.view(node_id, true); },
			},'-',
			{itemId:'add_change',
			 text:'Внести извещение',
			 handler:function(){ index.form_file.view(); }
			},
			{itemId:'add_folder',
			 text:'Добавить изделие',
			 handler:function(){ index.form_folder.view(node_id); },
			},
			{itemId:'add_document',
			 text:'Добавить документ',
			 handler:function(){ index.form_document.view(node_id); },
			},'-',
			{itemId:'get_project',
			 text:'Выдать копию документов изделия',
			 handler:function(){ index.form_download.view(node_id); },
			},'-',
			{itemId:'copy_folder',
			 text:'Скопировать изделие в другой проект',
			 handler:function(){ index.form_copy.view(node_id); },
			},
			{itemId:'move_folder',
			 text:'Переместить изделие',
			 handler:function(){ index.form_move.view(node_id); },
			},
		]
	});

	var menuDocument=new Ext.menu.Menu({
		items: [
			{itemId:'info_document',
			 text:'Информация о документе',
			 handler:function(){ index.viewer_document.view(node_id); },
			},
			{itemId:'edit_document',
			 text:'Редактировать документ',
			 handler:function(){ index.form_document.view(node_id,true); },
			},'-',
			{itemId:'change_document',
			 text:'Внести изменение',
			 handler:function(){ index.form_file.view(node_id); },
			},'-',
			{itemId:'get_document',
			 text:'Выдать копию документа',
			 handler:function(){ index.form_download.view(node_id); },
			},
		]
	});
		
	this.view=function(id,project,havechildren,XY){
		var menu;
		
		if(id){
			node_id=id;	
			if(id=='1') menu=menuRoot;
			else if(project) menu=menuProject;
			else if(havechildren) menu=menuFolder;
			else menu=menuDocument;

			menu.showAt(XY);
		}
	}
}