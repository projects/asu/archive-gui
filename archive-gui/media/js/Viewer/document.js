function ViewerDocument(){
	var object=this;
	var var_id=0;

	var documentBar=new ViewerBar();

	var docData={};
	var docTpl=new Ext.Template(
		'<table width=100% border=1 style="border-collapse: collapse;">'+
			'<tr><td width=30%>Проект:</td><td>{pr_name}</td></tr>'+
			'<tr><td>Применение:</td><td>{p_name}</td></tr>'+
			'<tr><td>Наименование:</td><td>{name}</td></tr>'+
			'<tr><td>Тип документа:</td><td>{t_name}</td></tr>'+
			'<tr><td>Обозначение:</td><td>{klgi}</td></tr>'+
			'<tr><td>Инв. номер:</td><td>{inv}</td></tr>'+
			'<tr><td>Отдел/Цех:</td><td>{gname}</td></tr>'+
			'<tr><td>Разработчик:</td><td>{aname}</td></tr>'+
			'<tr><td>Формат:</td><td>{f_name}</td></tr>'+
			'<tr><td>Кол-во листов:</td><td>{sheets}</td></tr>'+
			'<tr><td>Поступление:</td><td>{d_add}</td></tr>'+
			'<tr><td>Версия:</td><td>{ch_number}</td></tr>'+
			'<tr><td>Изменён:</td><td>{d_edit}</td></tr>'+
			'<tr><td colspan=2>Описание:</td></tr>'+
			'<tr><td colspan=2>{desc}</td></tr>'+
		'</table>'
	);

	var docRegion = new Ext.Panel({
		title: 'Информация о документе',
		autoScroll:true,
		html:'132'
	});
	
	var menu=function() {
		var op_id=0;
		var menuView=new Ext.menu.Menu({
			items: [{
				itemId:'view',
				text:'Посмотреть файл',
				handler:function(){
					CoreAJAX({
						url:'/file/getAllFile/',
						param:{'op_id':op_id},
						succ: function(res){
							if ( res.file ){
								var win = window.open(
									"/media/out/"+res.file,
									"",
									"width:27cm,status=no"
								);
								win.focus();
							}
						}
					});
				}
			}]
		});
		
		var menuClose=new Ext.menu.Menu({
			items: [{
				itemId:'view',
				text:'Просмотр - запрещён',
			}]
		});
		
		this.view=function(id,XY){
			var menu;
			if (!id) menu = menuClose;
			else{
				op_id = id;
				menu = menuView;
			}
			
			menu.showAt(XY);
		}
	}
	
	var changeMenu=new menu();
	
	var changeGrid = new LibGrid({
		title: 'История Изменений',
		url: "aj_getHistoryObjectJSON",
		fields: ['id','op_name','date','ch_id','ch_number','reg_number'],
		column: [
			{name: "Дата", field: 'date', width: 20},
			{name: "Операция", field: 'op_name', width: 35},
			{name: "Извещение", field: 'ch_number', width: 30},
			{name: "Рег.№", field: 'reg_number', width: 15},
		],
		rowcontext: function(row, evtObj){
			changeMenu.view(row.get('id'), evtObj.getXY());
		},
	});
	
	var changeRegion = changeGrid.grid;
	
	var downloadGrid = new LibGrid({
		title: 'История Запросов',
		url: "aj_getHistoryRequestObjectJSON",
		fields: ['id','date','number','zakaz','user'],
		column: [
			{name: "Дата", field: 'date'},
			{name: "Наряд №", field: 'number'},
			{name: "Заказ №", field: 'zakaz'},
			{name: "Затребовал", field: 'user'},
		],
	});
	
	var downloadRegion = downloadGrid.grid;

//Объект extJS для рендера
	var containerBody=new Ext.Panel({
		hideBorders:true,
		layout:'accordion',
		layoutConfig: {
			hideCollapseTool: true,
			titleCollapse: true,
		},
		tbar: documentBar.bar,
		items: [
			docRegion,
			changeRegion,
			downloadRegion
		]
	});
	this.container=new Ext.Container({
		hideBorders:true,
		layout:'fit',
		items: [containerBody],
	});
	documentBar.view(0);
	
//Функция просмотра документа
	var mask=function(){ CoreMask(object.container.el); }
	var unmask=function(){ object.container.doLayout(); CoreUnMask(object.container.el); }
	this.view=function(id){
		var_id=id;
		documentBar.view(0);
		mask();
		containerBody.hide();
		if (var_id != 0){
			CoreAJAX({
				url:'aj_getDocumentInformation',
				param:{'node_id':var_id},
				succ: viewUpd
			});
		}
		else unmask();
	}
//Функция обновления, т.к. произошли изменения
	this.refresh=function (n_id){
		//var id=0;
		//if(var_id==n_id){
			var_id=0; id=n_id;
		//}
		if(id) object.view(id);
	}
//	//Обработка загруженных данных
	var viewUpd=function(result){
		if(result.info && var_id==result.info.node_id){
			docTpl.overwrite(docRegion.getLayoutTarget(),result.info);
			changeGrid.store.loadData(result.history);
			downloadGrid.store.loadData(result.request);
			documentBar.view(var_id);
			containerBody.show(false);
			unmask();
		}
		else unmask();
	}
}