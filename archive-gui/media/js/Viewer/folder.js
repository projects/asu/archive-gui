function ViewerFolder(){
	var object=this;
	var var_id=0, var_type='';
	
	var folderBar=new ViewerBar();

//Информации об объекте
//	//Массив данных, доступен извне
	var infoData={id:'ID',name:'Имя',klgi:'Обозначение',t_id:'ID Типа',t_name:'Название типа',desc:'Описание'}
//	//Темплей вывода
	var infoTpl=new Ext.Template(
		'<table width=100% border=1 style="border-collapse: collapse;"><tr>'+
			//'<td width=20%><b>ID объекта:</b></td><td width=35%>{id}</td>'+
			'<td><b>Название:</b></td><td colspan=3>{name}</td>'+
		'</tr><tr>'+
			'<td width=20%><b>Обозначение:</b></td><td width=35%>{klgi}</td>'+
			'<td><b>Тип:</b></td><td>{t_name}</td>'+
		'</tr><tr>'+
			'<td><b>Описание:</b></td><td colspan=3>{desc}</td>'+
		'</tr></table>'
	);
//	//Объект панели
	var infoRegion=new Ext.Panel({
		title: 'Общая информация',
		autoHeight:true,
		html:'',
	});
	
//Информация о экземпляре(документе)	
//	//Массив данных, доступен извне
	var docData={id:'ID',pr_id:'ID родителя',pr_name:'Имя родителя',d_add:'Добавлен',d_edit:'Изменён',inv:'Инв. номер'}
//	//Темплей вывода
	var docTpl=new Ext.Template(
		'<table width=100% border=1 style="border-collapse: collapse;"><tr>'+
			'<td width=20%><b>Проект:</b></td><td width=35%>{pr_name}</td>'+
			'<td width=20%><b>Поступление:</b></td><td>{d_add}</td>'+		
		'</tr><tr>'+
			'<td><b>Применяемость:</b></td><td>{p_name}</td>'+
			'<td><b>Изменён:</b></td><td>{d_edit}</td>'+
//		'</tr><tr>'+
//			'<td><b>Инв. номер:</b></td><td>{inv}</td>'+
//			'<td><b>Скачать:</b></td><td><a href="#" onclick="index.form_download.view({id});">link</a></td>'+
		'</tr></table>'
	);
//	//Объект панели
	var docRegion=new Ext.Panel({
		title: 'Дополнительная информация',
		autoHeight:true,
		html:'',
	});
	
	
	var loader=function(){ viewFin(); }
//Блок с таблицей экземпляров объекта
//	//Таблица экзепляров
	var nodeGrid=new LibGrid({
		title: 'Использование в проектах',
		url: "aj_getContentNodes",
		fields: ['id','pr_id','pr_name','p_id','p_name','inv','date','havechildren','isproject'],
		column: [
			{name: "Проект", field: 'pr_name', width: 30},
			{name: "Применение", field: 'p_name', width: 30},
//			{name: "Инв. номер", field: 'inv'},
			{name: "Дата", field: 'date'},
		],
		loader: loader,
		rowclick: function(row){
			object.view(row.get('id'),'Node');
		},
		rowcontext: function(row, evtObj){
			index.viewer_menu.view(row.get('id'),row.get('isproject'),row.get('havechildren'),evtObj.getXY());
		},
	});
//	//Объект таблицы экзепляров
	var nodeRegion=nodeGrid.grid;
	
//Блок с таблицей детей экзепляра
//	//Таблица детей экзепляра
	var childGrid=new LibGrid({
		title: 'Документы',
		url: "aj_getContentChildren",
		fields: ['id','klgi','name','t_name','ch_number'],
		column: [
			{name: "Обозначение", field: 'klgi', width: 3},
			{name: "Название", field: 'name', width: 5},
			{name: "Тип документа", field: 't_name', width: 3},
			{name: "Версия", field: 'ch_number', width: 1},
		],
		loader: loader,
		rowclick: function(row){
			index.viewer_document.view(row.get('id'));
		},
		rowcontext: function(row, evtObj){
			index.viewer_menu.view(row.get('id'),0,0,evtObj.getXY());
		},
	});
//	//Панель таблицы детей
	var childRegion=childGrid.grid;
	
//	//Контейнер для таблиц
	var gridRegion=new Ext.Container({
		flex:1,
		layout:'anchor',
		hideBorders:true,
		defaults: { anchor:'100% 100%' },
		items:[
			nodeRegion,
			childRegion
		],
	});

//Объект extJS для рендера
	var containerBody=new Ext.FormPanel({
		hideBorders:true,
		layout:'vbox',
		layoutConfig: {
			align : 'stretch',
		},
		tbar: folderBar.bar,
		items: [
			infoRegion,
			docRegion,
			gridRegion,
		],
	});
	this.container=new Ext.Container({
		hideBorders:true,
		layout:'fit',
		items: [containerBody],
	});
	folderBar.view(0);
	
//Функция просмотра объекта/экземпляра
	var mask=function(){  CoreMask(object.container.el); }
	var unmask=function(){ object.container.doLayout(); CoreUnMask(object.container.el); }
	this.view=function (id,type){
		if((!type) || (type!='Object')) type='Node';
		if(!id) id=0;
	
		if(!(var_id==id && var_type==type)){
			index.viewer_document.view(0);
			if(type=='Node') index.viewer_navigation.tree_view(id,true);

			var_id=id; var_type=type;
			
			mask();
			containerBody.hide();
			folderBar.view(0);
			docRegion.hide();
			nodeRegion.hide();
			childRegion.hide();
			
			getNodeInfo(id,viewUpd,unmask,type);
		}
	}
//Функция обновления, т.к. произошли изменения
	this.refresh=function (n_id,p_id){
		var id=0,type='Node';
		if(var_id==n_id){
			var_id=0; id=n_id;
			if(var_type=='Object') type='Object';
		}
		else if(var_id==p_id){
			var_id=0; id=p_id;
			if(var_type=='Object') type='Object';
		}
		if(id) object.view(id,type);
	}
//	//Обработка загруженных данных
	var viewUpd=function(result){
		if(((var_type=='Object') && (var_id==result.obj.id)) || ((var_type=='Node') && (var_id==result.node.id))){
			infoData=result.obj;
			docData=result.node;
				
			infoTpl.overwrite(infoRegion.getLayoutTarget(),infoData);
				
			if(var_type=='Object'){
				nodeRegion.show(false);
				nodeGrid.load({'id': var_id});
			}
			else{
				folderBar.view(docData.id, docData.isproject, docData.havechildren);
				
				docRegion.show(false);
				docTpl.overwrite(docRegion.getLayoutTarget(),docData);
				if(docData.havechildren){
					childRegion.show(false);
					childGrid.load({'node': var_id});
				}
				else viewFin();
			}
		}
	}
//	//Завершающая стадия обработка
	var viewFin=function(){
		containerBody.show(false);
		unmask();
	}
}