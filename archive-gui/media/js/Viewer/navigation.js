function ViewerNavigation(){
//Поиск по КЛГИ
	var klgiGrid=new LibGrid({
		title: 'Поиск',
		url: "aj_getGridNavigationJSON",
		fields: ['id','klgi','text'],
		page: index.viewer_config.navigation_page,
		search: true,
		column: [
			{name: "Обозначение", field: 'klgi', width: 1},
			{name: "Название", field: 'text', width: 2},
		],
		rowclick: function(row){
			index.viewer_folder.view(row.get('id'),'Object');
		},
	});
	var klgiTab=klgiGrid.grid;
		
//Дерево
	var treeTab=new Ext.tree.TreePanel({
		title: 'Структура по проектам',
		autoScroll : true,
		listeners: {
			click: function(node){
				index.viewer_folder.view(node.id,'Node');
			},
			contextmenu: function(node,evtObj){
				evtObj.stopEvent();
				index.viewer_menu.view(
					node.attributes['id'],
					node.attributes['isproject'],
					node.attributes['havechildren'],
					evtObj.getXY()
				);
			},
		},
		loader : new Ext.tree.TreeLoader({
			url: "aj_getTreeChildren",
			clearOnLoad: true,
			preloadChildren: true
		}),
		root : { text : 'Проекты', id : '1', expanded : true },
		animate: false,
	});

//Объект extJS для рендера
	this.container=new Ext.TabPanel({
		activeTab: 1,
		defaults:{autoScroll: true},
		items:[
			klgiTab,
			treeTab,
		]
	});

//Открытие строк в дереве, type - дерево не менялось
	this.tree_view=function(id,type){
		var node=treeTab.getNodeById(id);
		if(node){
			if((!type) && (node.expanded)) treeTab.getLoader().load(node);
			node.expand();
		}
	}
}