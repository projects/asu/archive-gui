
Ext.onReady(function() {


// next module

//Данные о бумажном носителе
function FormLibDocumentGroup(type){
	var object=this;
	var Title=false, numberTitle=false, invTitle=false, autorTitle=false;
	var groupCombo=false, autorCombo=false, formatCombo=false, msgArea=false;
	var dateField=false, numberField=false, invField=false, autorField=false, sheetsField=false, baseField=false;
		
	if(type=='Document'){
		Title='Документ';
		invTitle='Инвентарный номер';
		autorTitle='Разработчик';
	}
	else if(type=='Download'){
		Title='Наряд';
		numberTitle='Номер наряда';
		numberField=true;
		invTitle='Заказ №';
		autorTitle='Затребовал';
		baseField=true;
	}
	else if(type=='File'){
		Title='Извещение';
		numberTitle='Номер извещения';
		numberField=true;
		invTitle='Инвентарный номер';
		autorTitle='Составил';
	}

	dateField=new Ext.form.DateField({name:'date', fieldLabel:'Дата', format: 'd-m-Y', allowBlank:false});
	if(numberTitle=='Номер извещения'){
		numberField=new FormLibSignField("number");
	}
	else if(numberField){
		numberField={
                    field:new Ext.form.NumberField({ name:'number', fieldLabel:numberTitle, allowBlank:false, maxLength: 10, anchor:'95%' }),
                    clear:function(){ numberField.field.setValue(); numberField.field.validate(); },
                    set:function(value){ numberField.field.setValue(value); },
                };
	}
	if(invTitle=='Инвентарный номер'){
		invField=new FormLibInvField(true);
	}
	else invField={
		field:new Ext.form.NumberField({name:'inv', fieldLabel:invTitle, allowBlank:false, maxLength: 10, anchor:'95%'}),
		clear:function(){ invField.field.setValue(); invField.field.validate(); },
		set:function(value){ invField.field.setValue(value); },
	};
	groupCombo=new Ext.form.ComboBox({
		valueField: 'id',
		hiddenName:'a_group',
		store: index.store_divisions,
		triggerAction: 'all',
		emptyText:'Выберите Отдел/Цех',
		allowBlank:false,
		mode:'local',
		editable: false,
		displayField: 'name',
		fieldLabel: 'Отдел/Цех',
		anchor:'95%',
	});
	autorCombo=new Ext.form.ComboBox({
		valueField: 'id',
		hiddenName:'a_name',
		store: index.store_autors,
		triggerAction: 'all',
		emptyText:'Выберите кто '+autorTitle,
		allowBlank:false,
		mode:'local',
		forceSelection: true,
		displayField: 'name',
		fieldLabel: autorTitle,
		anchor:'95%',
	});
	formatCombo=new Ext.form.ComboBox({
		valueField: 'id',
		hiddenName:'format',
		store: index.store_formats,
		triggerAction: 'all',
		emptyText:'Выберите формат',
		allowBlank:false,
		mode:'local',
		editable: false,
		displayField: 'name',
		fieldLabel: 'Формат листов',
		anchor:'95%',
	});
	sheetsField=new Ext.form.NumberField({name:'sheets', fieldLabel:'Кол-во листов', allowBlank:false, anchor:'95%'});
	if(baseField){
		baseField=new Ext.form.TextField({name:'base', fieldLabel:'Основание', allowBlank:false, anchor:'95%'});
	}
	msgArea=new Ext.form.TextArea({hideLabel: true, name: 'msg', anchor:'95%'});


//Объект extJS для рендера
	this.container=new Ext.form.FieldSet({
		title: Title,
		items:[
			dateField,
			numberField ? numberField.field : false,
			invField.field,
			groupCombo,
			autorCombo,
			formatCombo,
			sheetsField,
			baseField,
			{xtype: 'label',fieldLabel:'Коментарий'},
			msgArea,
		],
	});


//Показ формы
	this.show=function(){
		dateField.enable();
		/*if(numberField){
			numberField.enable();
		}*/
		//invField.enable();
		groupCombo.enable();
		autorCombo.enable();
		formatCombo.enable();
		sheetsField.enable();
		if(baseField){
			baseField.enable();
		}
		msgArea.enable();
		object.container.enable().show(false);
	}
//Скрытие формы
	this.hide=function(){
		dateField.disable();
		/*if(numberField){
			numberField.disable();
		}*/
		//invField.disable();
		groupCombo.disable();
		autorCombo.disable();
		formatCombo.disable();
		sheetsField.disable();
		if(baseField){
			baseField.disable();
		}
		msgArea.disable();
		object.container.disable().hide();
	}
//Очистка формы
	this.clear=function(){
		dateField.setValue(new Date());
		if(numberField) numberField.clear();
		invField.clear();
		groupCombo.clearValue();
		groupCombo.validate();
		autorCombo.clearValue();
		autorCombo.validate();
		formatCombo.clearValue();
		formatCombo.validate();
		sheetsField.setValue();
		sheetsField.validate();
		if(baseField){
			baseField.setValue();
			baseField.validate();
		}
		msgArea.setValue()
		msgArea.validate();
	}
//Заполнение формы
	this.set=function(data){
		dateField.setValue(data.d_add);
		if(numberField) numberField.set(data.number);
		invField.set(data.inv);
		groupCombo.setValue(data.a_group);
		groupCombo.setRawValue(data.gname);
		autorCombo.setValue(data.a_name);
		autorCombo.setRawValue(data.aname);
		formatCombo.setValue(data.f_id);
		formatCombo.setRawValue(data.f_name);
		sheetsField.setValue(data.sheets);
		if(baseField) baseField.setValue(data.base);
		msgArea.setValue(data.msg);
	}
//Фокус в начало формы
	this.focus=function(){
		if(numberField) numberField.field.focus();
		else invField.field.focus();
	}
}
// next module

//Загрузка файла документа
function FormLibFileGroup(){
	var object=this;

//Поле файла
	var fileField=new Ext.ux.form.FileUploadField({hideLabel:true, allowBlank:false, name:'file', anchor:'95%'});


//Объект extJS для рендера
	this.container=new Ext.form.FieldSet({
		title: 'Файл с данными',
		items:[fileField],
	});


//Показ формы
	this.show=function(){
		fileField.enable();
		object.container.enable().show(false);
	}
///Скрытие формы
	this.hide=function(){
		fileField.disable();
		object.container.disable().hide();
	}
//Очистка формы
	this.clear=function(){
		fileField.setValue();
		fileField.validate();
	}
//Заполнение формы
	this.set=function(){ alert('newFileDocumentGroup(): None Set()'); }
//Фокус в начало формы
	this.focus=function(){ alert('newFileDocumentGroup(): None Focus()'); }
}
// next module

//Информация о документе
function FormLibInfoGroup(type){
	var object=this;
	var typeCombo=false, klgiReg=false, klgiCal=false, descArea=false;
	var nameField=false, klgiField=false;

//Основные данные
//	//Поле Наименование
	nameField=new Ext.form.TextField({name:'name', fieldLabel:'Наименование', allowBlank:false, anchor:'95%'});
//	//Поле КЛГИ
	if(type=='Project') klgiField={field: false};
	else klgiField=new FormLibSignField();
//	//Combo-box выбора типа изделия
	if(type=='Document'){
//	//Автоподстановка полей
		klgiReg=/^([\S]+)([ ]{1,}[\S]{0,3})$/;
		typeCalc=function(co,tid){
// 			//var tid=index.store_document_types.getById(typeCombo.getValue());
			if(tid){
				var t_klgi=tid.get('short');
				var klgi_a=klgi_n=klgiField.get();
				var klgi_arr=klgiReg.exec(klgi_a);
				if(klgi_arr) klgi_n=klgi_arr[1];
				klgiField.setValue(klgi_n+' '+t_klgi);
				nameField.setValue(tid.get('name'));
			}
		}
		
		nameField.setReadOnly(true);
		typeCombo=new Ext.form.ComboBox({
			valueField: 'id',
			hiddenName:'tid',
			store: index.store_document_types,
			triggerAction: 'all',
			emptyText:'Выберите тип документа',
			allowBlank:false,
			mode:'local',
			forceSelection: true,
			displayField: 'view',
			fieldLabel: 'Тип документа',
			anchor:'95%',
			listeners:{select: typeCalc},
			
		});
	}
//	//Поле Описания
	descArea=new Ext.form.TextArea({hideLabel: true, name: 'desc', anchor:'95%'});


//Объект extJS для рендера
	this.container=new Ext.form.FieldSet({
		title: 'Основные данные',
		items:[
			nameField,
			typeCombo,
			klgiField.field,
			{xtype: 'label',fieldLabel:'Описание'},
			descArea,
		],
	});


//Показ формы
	this.show=function(){ alert('newInfoGroup(): None Show()'); }
//Скрытие формы
	this.hide=function(){ alert('newInfoGroup(): None Hide()'); }
//Очистка формы
	this.clear=function(){
		nameField.setValue();
		nameField.validate();
		if(typeCombo){
			typeCombo.clearValue();
			typeCombo.validate();
		}
		if(klgiField.field) klgiField.clear();
		descArea.setValue();
	}
//Заполнение формы
	this.set=function(Edit,n_name,n_klgi,n_desc,t_id,t_name){
		nameField.setValue(n_name);
		if(klgiField.field) klgiField.set(n_klgi);
//	//	//Заполнение формы
		if(Edit){
			if(typeCombo) typeCombo.setValue(t_id);
			descArea.setValue(n_desc);
		}
	}
//Фокус в начало формы
	this.focus=function(){ alert('newInfoGroup(): None Focus()'); }
}
// next module

function FormLibInvField(inv_null){
	var num_edit=false, num_res=false, num_valid=false, num=false;
	
	var messages={
		'null': '<div>&nbsp;</div><br>',
		'now': '<div>Текущий инвентарный номер</div><br>',
		'blank': '<div style="color:red">Необходимо задать инвентарный номер</div><br>',
		'request': '<div style="color:orange">Идёт проверка инвентарного номера</div><br>',
		'valid': '<div style="color:green">Данный инвентарный номер - свободен</div><br>',
		'invalid': '<div style="color:red">Данный инвентарный номер уже используется</div><br>',
	};
	
	var msg=new Ext.Container({ html:'<div>&nbsp;</div><br>', style:'text-align:center;width:100%;' });

	var validate=function(value){
		if(value!=''){
			if(value==num_edit){
				msg.el.dom.innerHTML=messages['now'];
				return true;
			}
			else if(value==num){
				if(num_res){
					if(num_valid) msg.el.dom.innerHTML=messages['valid'];
					else msg.el.dom.innerHTML=messages['invalid'];
					return num_valid;
				}
				else return false;
			}
			else{
				num_res=num_valid=false;
				num=value;
				CoreAJAX({
					url:'aj_InvValidate',
					param:{'inv':value},
					succ:function(result){
						if(result.inv==num){
							num_res=true;
							if(result.valid) num_valid=true;
							field.validate();
						}
					}
				});
				msg.el.dom.innerHTML=messages['request'];
				return false;
			}
		}
		else{
			if(!inv_null){
				msg.el.dom.innerHTML=messages['blank'];
				return false;
			}
			else{
				msg.el.dom.innerHTML=messages['null'];
				return true;
			}
		}
	}

	if(!inv_null) inv_null=false;
	var field=new Ext.form.NumberField({
		name:'inv',
		fieldLabel:'Инвентарный номер',
		allowBlank: inv_null,
		anchor:'95%',
		validator:validate
	});

	this.field=new Ext.Container({
		layout:'form',
		items:[field, msg],
		anchor:'100%'
	});
	this.set=function(value){
		num_edit=value;
		num=false;
		field.setValue(value);
	};
	this.clear=function(){
		num_edit=num=false;
		field.setValue();
		field.validate();
	};
}
// next module

//КЛГИ
function FormLibKlgiGroupAdd(handler,title_group,fileEnable,statusCheck, change_id){
	if(!title_group) title_group='Выборка по КЛГИ';
	if(!statusCheck) statusCheck = false;
	if(!fileEnable) fileEnable=false;
	
	var object=this;
//Создание объекта Combo-box
	var Combos=['none'], Files=['none'], Values={};
	this.ComboStore=new Ext.data.JsonStore({
		url: 'aj_getDocumentsInProject',
		baseParams: {'idproject':0,'query':'','status':statusCheck},
		fields: ["id", "name"],
		autoLoad:true,
	});
	var ComboValid=function(value){
		if ( value == '' ) return false;
		else if (Combos[klgi].getValue() == value){
			if ( parseInt(value) != value ){
				var n = object.ComboStore.find('name',value);
				var t_n = object.ComboStore.getAt(n);
				if ( t_n ) value = t_n.get('id');
				else alert('error of change form: validation');
			}
			if (Values[value] && (Values[value] != klgi)){
				Ext.MessageBox.alert('Ошибка:', 'Поле с таким значением уже есть!');
				Combos[klgi].clearValue();
				return false;
			}
			return true
		}
		else return true;
	}
	var Combo=function(kid){
		var combo=new Ext.form.ComboBox({
			name: "name",
			valueField: 'id',
			hiddenName: 'nid_klgi_'+kid,
			hiddenValue:'id',
			store: object.ComboStore,
			triggerAction: 'all',
			emptyText:'Наберите часть КЛГИ...',
			minChars: 1,
			allowBlank:false,
			forceSelection: true,
			displayField: 'name',
			fieldLabel: kid+') Документ',
			loadingText: 'Идет запрос на сервер...',
			validator: ComboValid,
			//anchor: '100%'
		});
		Combos.push(combo);

		if(!fileEnable) return combo;
		else{
			var file=new Ext.ux.form.FileUploadField({
				hideLabel:true,
				name:'file_klgi_'+kid,
				allowBlank:false,
				anchor:'95%',
			});
			Files.push(file);

			return new Ext.Container({
				layout:'form',
				items:[combo,file],
				anchor:'100%'
			});
		}
	}
//Добавить Combo-box
	var AddCombo=function(){
		var val = Combos[klgi].getValue();
		if((val=='') || (Values[val]) || (fileEnable && (Files[klgi].getValue()==''))){
			Ext.MessageBox.alert('Ошибка:', 'Перед добавлением нового поля - заполните предыдущее!');
		}
		else{
			Values[val] = klgi;
			Combos[klgi].setReadOnly(true);
			if(fileEnable) Files[klgi].setReadOnly(true);
			klgi++;
			object.container.add(Combo(klgi));
			object.container.get(0).setValue(klgi);
			Combos[klgi].validate();
			if(fileEnable) Files[klgi].validate();
			object.container.doLayout();
			if(handler) handler();
		}
	}
//Удалить Combo-box
	var DelCombo=function(){
		var val = Combos[klgi].getValue();
		if(klgi==1) Ext.MessageBox.alert('Ошибка:', 'Нельзя удалить все поля!');
		else{
			Values[val]=false;
			object.container.remove(klgi);
			klgi--;
			Combos.pop();
			Combos[klgi].setReadOnly(false);
			if(fileEnable){
				Files.pop();
				Files[klgi].setReadOnly(false);
			}
			object.container.get(0).setValue(klgi);
		}
	}
	
	//Сохраняем изменения
	var klgi=1;


//Объект extJS для рендера
	this.container=new Ext.form.FieldSet({
		title: title_group,
		defaultType: 'textfield',
		items:[
			{name:'klgi',hidden:true,emptyText:1},
			Combo(1)
		],
		buttons:[
			{text: 'Удалить поле', handler: DelCombo,},
			{text: 'Добавить поле', handler: AddCombo,},
		],
	});


//Показ формы
	this.show=function(){
		for(i=klgi;i>=1;i--) Combos[i].enable();
		if(fileEnable){ for(i=klgi;i>=1;i--){ Files[i].enable(); } }
		object.container.enable().show(false);
	}
//Скрытие формы
	this.hide=function(){
		for(i=klgi;i>=1;i--) Combos[i].disable();
		if(fileEnable){ for(i=klgi;i>=1;i--){ Files[i].disable(); } }
		object.container.disable().hide();
	}
//Очистка формы
	this.clear=function(){
		Values={};
		for(i=klgi;i>1;i--) DelCombo();
		Combos[1].clearValue();
		Combos[1].validate();
		if(fileEnable) Files[1].setValue('').validate();
	}
//Заполнение первого эл-та формы
	this.set=function(n_id){
		Combos[1].setValue(n_id).validate();
	}
}
// next module

//КЛГИ
function FormLibKlgiContent(handler,title_group){
	if(!title_group) title_group='Выборка по КЛГИ';
	var object=this;
	var selectedContentId=0;
	var nodeId=0;
	
	
	this.selectedId = null;
	
	var menuContent=new Ext.menu.Menu({
		items: [
			{itemId:'remove_content',
				text:'Удалить вложение',
				handler:function(){ 
				    Ext.Msg.confirm('Удаление вложения', 'Вы точно хотите удалить выбранный файл?', function(btn){
					if (btn=='yes'){
					    Ext.Ajax.request({
						url: 'aj_ChangeDeleteFile',
						success: function(){
						    Ext.Msg.alert("Удаление вложения", 'Вложение удалено.');
						    object.store.load();
						},
						failure: function(){
						    Ext.Msg.alert("Ошибка", 'Произошла ошибка. Обратитесь к администратору.');
						},
						params: { 
						    selectedContent: selectedContentId,
						    nodeId: nodeId
						}
					    }); 
					}
				    });
				}
			},
		]
	});
	
	this.store=new Ext.data.JsonStore({
		url: "aj_TChangeOne",
		root: "records",
		totalProperty: "total",
		fields: ['obj_id','reg_number','change_id','name','klgi','status_desc', 'node_id']
	});
	
	//Таблица
	this.grid=new Ext.grid.GridPanel ({
		title: 'Содержание извещения',
		store: object.store,
		height: 280,
		colModel: new Ext.grid.ColumnModel({
		    defaults: {
			width: 120,
			sortable: true
		    },
    		    columns: [
			{header: "Номер", dataIndex: 'reg_number'},
			{header: "Статус", dataIndex: 'status_desc'},
			{header: "Наименование", dataIndex: 'name'},
			{header: "Обозначение", dtaIndex: 'klgi'}
		    ],
		}),
		viewConfig: {
		        forceFit: true
		},
		listeners: {
			rowcontextmenu: function(grid, rowIndex, event){
				selectedContentId=object.store.getAt(rowIndex).get('change_id');
				nodeId=object.store.getAt(rowIndex).get('node_id');
				menuContent.showAt(event.getXY());
			}
		}
	});


	RefreshContent = function(){
	    object.store.load();
	};
	
//Объект extJS для рендера
	this.container=new Ext.form.FieldSet({
		title: title_group,
		defaultType: 'textfield',
		height: 300,
		items:[
			this.grid
		],
		buttons:[
			{text: 'Обновить', handler: RefreshContent}
		],
	});
	
//Показ формы
	this.show=function(){
		object.container.enable().show(false);
	}
//Скрытие формы
	this.hide=function(){
		object.container.disable().hide();
	}
//Очистка формы
	this.clear=function(){
	}
}
// next module

//КЛГИ
function FormLibKlgiGroup(handler,title_group,fileEnable,statusCheck, change_id){
	if(!title_group) title_group='Выборка по КЛГИ';
	if(!statusCheck) statusCheck = false;
	if(!fileEnable) fileEnable=false;
	
	var object=this;
//Создание объекта Combo-box
	var Combos=['none'], Files=['none'], Values={};
	this.ComboStore=new Ext.data.JsonStore({
		url: 'aj_getDocumentsInProject',
		baseParams: {'idproject':0,'query':'','status':statusCheck},
		fields: ["id", "name"],
		autoLoad:true,
	});
	var ComboValid=function(value){
		if ( value == '' ) return false;
		else if (Combos[klgi].getValue() == value){
			if ( parseInt(value) != value ){
				var n = object.ComboStore.find('name',value);
				var t_n = object.ComboStore.getAt(n);
				if ( t_n ) value = t_n.get('id');
				else alert('error of change form: validation');
			}
			if (Values[value] && (Values[value] != klgi)){
				Ext.MessageBox.alert('Ошибка:', 'Поле с таким значением уже есть!');
				Combos[klgi].clearValue();
				return false;
			}
			return true
		}
		else return true;
	}
	var Combo=function(kid){
		var combo=new Ext.form.ComboBox({
			name: "name",
			valueField: 'id',
			hiddenName: 'nid_klgi_'+kid,
			hiddenValue:'id',
			store: object.ComboStore,
			triggerAction: 'all',
			emptyText:'Наберите часть КЛГИ...',
			minChars: 1,
			allowBlank:false,
			forceSelection: true,
			displayField: 'name',
			fieldLabel: kid+') Документ',
			loadingText: 'Идет запрос на сервер...',
			validator: ComboValid,
			//anchor: '100%'
		});
		Combos.push(combo);

		if(!fileEnable) return combo;
		else{
			var file=new Ext.ux.form.FileUploadField({
				hideLabel:true,
				name:'file_klgi_'+kid,
				allowBlank:false,
				anchor:'95%',
			});
			Files.push(file);

			return new Ext.Container({
				layout:'form',
				items:[combo,file],
				anchor:'100%'
			});
		}
	}
//Добавить Combo-box
	var AddCombo=function(){
		var val = Combos[klgi].getValue();
		if((val=='') || (Values[val]) || (fileEnable && (Files[klgi].getValue()==''))){
			Ext.MessageBox.alert('Ошибка:', 'Перед добавлением нового поля - заполните предыдущее!');
		}
		else{
			Values[val] = klgi;
			Combos[klgi].setReadOnly(true);
			if(fileEnable) Files[klgi].setReadOnly(true);
			klgi++;
			object.container.add(Combo(klgi));
			object.container.get(0).setValue(klgi);
			Combos[klgi].validate();
			if(fileEnable) Files[klgi].validate();
			object.container.doLayout();
			if(handler) handler();
		}
	}
//Удалить Combo-box
	var DelCombo=function(){
		var val = Combos[klgi].getValue();
		if(klgi==1) Ext.MessageBox.alert('Ошибка:', 'Нельзя удалить все поля!');
		else{
			Values[val]=false;
			object.container.remove(klgi);
			klgi--;
			Combos.pop();
			Combos[klgi].setReadOnly(false);
			if(fileEnable){
				Files.pop();
				Files[klgi].setReadOnly(false);
			}
			object.container.get(0).setValue(klgi);
		}
	}
	
	var klgi=1;


//Объект extJS для рендера
	this.container=new Ext.form.FieldSet({
		title: title_group,
		defaultType: 'textfield',
		items:[
			{name:'klgi',hidden:true,emptyText:1},
			Combo(1),
		],
		buttons:[
			{text: 'Удалить поле', handler: DelCombo,},
			{text: 'Добавить поле', handler: AddCombo,},
		],
	});


//Показ формы
	this.show=function(){
		for(i=klgi;i>=1;i--) Combos[i].enable();
		if(fileEnable){ for(i=klgi;i>=1;i--){ Files[i].enable(); } }
		object.container.enable().show(false);
	}
//Скрытие формы
	this.hide=function(){
		for(i=klgi;i>=1;i--) Combos[i].disable();
		if(fileEnable){ for(i=klgi;i>=1;i--){ Files[i].disable(); } }
		object.container.disable().hide();
	}
//Очистка формы
	this.clear=function(){
		Values={};
		for(i=klgi;i>1;i--) DelCombo();
		Combos[1].clearValue();
		Combos[1].validate();
		if(fileEnable) Files[1].setValue('').validate();
	}
//Заполнение первого эл-та формы
	this.set=function(n_id){
		Combos[1].setValue(n_id).validate();
	}
}
// next module

//Применение
function FormLibNodeGroup(title_group,title_folder,field_name){
	if(!title_group) title_group='Свойства';
	if(!title_folder) title_folder='Применение';
	else title_folder='Изделие';
	if(!field_name) field_name='';
	var object=this;
		
//Combo-box выбора проекта
	var project=new Ext.form.ComboBox({
		name: "pname",
		valueField: 'id',
		hiddenName:field_name+'pid',
		hiddenValue:'id',
		store: index.store_projects,
		triggerAction: 'all',
		emptyText:'Выберите проект',
		allowBlank:false,
		mode:'local',
		editable: false,
		displayField: "name",
		fieldLabel: 'Проект',
		loadingText: 'Идет запрос на сервер...',
		anchor:'95%',
		listeners: {
			select: function() {
				var pid=project.getValue();
				folder.enable();
				folder.clearValue();
				folder.store.setBaseParam('pr_id',pid);
				folder.store.load({params:{'pr_id':pid}});
			}
		}
	});
//Combo-box выбора Нода
	var folder=new Ext.form.ComboBox({
		name: "nname",
		valueField: 'id',
		hiddenName:field_name+'nid',
		hiddenValue:'id',
		store: new Ext.data.JsonStore({
			url: 'aj_getFoldersInProject',
			baseParams: {query:''},
			fields	:["id", "name"],
		}),
		triggerAction: 'all',
		emptyText:'Наберите часть названия...',
		minChars: 1,
		allowBlank:false,
		forceSelection: true,
		displayField: 'name',
		fieldLabel: title_folder,
		loadingText: 'Идет запрос на сервер...',
		anchor:'95%',
	});


//Объект extJS для рендера
	this.container=new Ext.form.FieldSet({
		title: title_group,
		items:[project,folder],
	});


//Показ формы
	this.show=function(){
		project.enable();
		folder.enable();
		object.container.enable().show(false);
	}
//Скрытие формы
	this.hide=function(){
		project.disable();
		folder.disable();
		object.container.disable().hide();
	}
//Очистка формы
	this.clear=function(){
		project.setReadOnly(false);
		project.clearValue();
		project.validate();
		project.enable();

		folder.setReadOnly(false);
		folder.clearValue();
		folder.validate();
		folder.disable();
	}
//Заполнение формы
	this.set=function(pr_id,pr_name,n_id,n_name,n_klgi){
//	//Заполнение проекта и изделия
		project.setValue(pr_id);
		project.setRawValue(pr_name);
		folder.enable();
		folder.store.setBaseParam('pr_id',pr_id);
		folder.store.load({params:{'pr_id':pr_id}});
		folder.setValue(n_id);
		folder.setRawValue(n_name+" - ["+n_klgi+"]");
//	//Блокируем редактирование
		//if(Edit){
			project.setReadOnly(true);
			folder.setReadOnly(true);
	//	}
	}
}
// next module

function FormLibSignField(field_name){
	var num_edit=false, num_res=false, num_valid=false, num=false;
	
	var messages={
		'now': '<div>Текущее обозначение</div><br>',
		'blank': '<div style="color:red">Необходимо задать обозначение</div><br>',
		'request': '<div style="color:orange">Идёт проверка обозначения</div><br>',
		'valid': '<div style="color:green">Данное обозначение - свободно</div><br>',
		'invalid': '<div style="color:red">Данное обозначение уже используется</div><br>',
	};
	
	var msg=new Ext.Container({ html:'<div>&nbsp;</div><br>', style:'text-align:center;width:100%;' });

	var validate=function(value){
		if(value!=''){
			if(value==num_edit){
				msg.el.dom.innerHTML=messages['now'];
				return true;
			}
			else if(value==num){
				if(num_res){
					if(num_valid) msg.el.dom.innerHTML=messages['valid'];
					else msg.el.dom.innerHTML=messages['invalid'];
					return num_valid;
				}
				else return false;
			}
			else{
				num_res=num_valid=false;
				num=value;
				CoreAJAX({
					url:'aj_SignValidate',
					param:{'sign':value},
					succ:function(result){
						if(result.sign==num){
							num_res=true;
							if(result.valid) num_valid=true;
							field.validate();
						}
					}
				});
				msg.el.dom.innerHTML=messages['request'];
				return false;
			}
		}
		else{
			msg.el.dom.innerHTML=messages['blank'];
			return false;
		}
	}

	if(!field_name) field_name = 'klgi';
	var field=new Ext.form.TextField({
		name:field_name,
		fieldLabel:'Обозначение',
		allowBlank:false,
		anchor:'95%',
		maxLength:25,
		maxLengthText:'Максимальная длинна - 25 символов',
		validator:validate
	});

	this.field=new Ext.Container({
		layout:'form',
		items:[field, msg],
		anchor:'100%'
	});
	this.get=function(){
		return field.getValue();
	}
	this.set=function(value){
		num_edit=value;
		num=false;
		field.setValue(value);
	};
	this.setValue=function(value){
		field.setValue(value);
	}
	this.ReadOnly=function(){
		field.setReadOnly(true);
	}
	this.clear=function(){
		num=false;
		field.setValue();
		field.validate();
	};
}
// next module

//Создание универсального окна
function FormLibWindow(conf){
	if(!conf.items) conf.items=[];
	if(!conf.View) conf.View=function(){ alert('View'); };
	if(!conf.Clear) conf.Clear=function(){ alert('Clear'); };

	if(!conf.Submit) conf.Submit=function(){ alert('Submit'); };
	if(!conf.SubmitURL) conf.SubmitURL="";
	if(!conf.SubmitMSG) conf.SubmitMSG='';
	if(!conf.SubmitTITLE) conf.SubmitTITLE='New window';

	if(!conf.Edit) conf.Edit=function(){ alert('Edit'); };
	if(!conf.EditURL) conf.EditURL="";
	if(!conf.EditMSG) conf.EditMSG='';
	if(!conf.EditTITLE) conf.EditTITLE='New window';

	if(!conf.fileUpload) conf.fileUpload=false;
	if(!conf.w_width) conf.w_width=400;
	if(!conf.w_height) conf.w_height=500;

	var winType='Submit';

	var winSubmit=function(){
		var req={};
		req.success=req.failure=winSubmitRes;
		if(winType=='Edit') req.url=conf.EditURL;
		else if(winType=='Submit'){
			req.url=conf.SubmitURL;
			if(conf.fileUpload) req.timeout=100000;
		}
		
		var win_form=winBody.getForm()
		if(win_form.isValid()){
			mask();
			win_form.submit(req);
		}
	}

	var winSubmitRes=function(form, action){
		unmask();
		if(action.result){
			if(action.result.success){
				if((action.result.success=="false") && (action.result.msg)){
					Ext.MessageBox.alert('Ошибка:', action.result.msg);
				}
				else{
					conf.Clear();
					win.hide();
					if(winType=='Submit'){
						conf.Submit(form, action);
						index.worklog.add(conf.SubmitMSG,'ok');
					}
					else if(winType=='Edit'){
						conf.Edit(form, action);
						index.worklog.add(conf.EditMSG,'ok');
					}
				}
				
			}
			else Ext.MessageBox.alert('Ошибка:', 'Ошибка сервера!');
		}
		else if((action.response) && (action.response.status=='404')) alert('404');
	}

	var winBody=new Ext.FormPanel({
		frame:true,
		fileUpload:conf.fileUpload,
		items: conf.items,
	});


	var winButtonCancel=new Ext.Button({text: 'Отмена', handler: function(){ win.hide(); }});
	var winButtonSubmit=new Ext.Button({text: 'Сохранить', handler: winSubmit});
	var win=new Ext.Window({
		plain: true,
		modal: true,
		width: conf.w_width,
		//height: conf.w_height,
		frame: true,
		autoScroll: true,
		boxMaxHeight: Ext.lib.Dom.getViewportHeight(),
		title : conf.SubmitTITLE,
		items: [winBody],
		closeAction: 'hide',
		buttons: [winButtonCancel, winButtonSubmit],
	});

	var mask=function(){ win.setWidth(conf.w_width+1); CoreMask(win.el); }
	var unmask=function(){ win.setWidth(conf.w_width); CoreUnMask(win.el); }
	this.winView=function(id,type){
		if((!type) || (type=='Submit')){
			winType='Submit';
			win.setTitle(conf.SubmitTITLE);
		}
		else if(type=='Edit'){
			winType='Edit';
			win.setTitle(conf.EditTITLE);
		}

		win.show(false);
		mask();
		conf.Clear();
		if(id) getNodeInfo(id,winViewUpd,unmask);
		else{
			win.syncSize();
			win.center();
			unmask();
		}
	}
	
	var winViewUpd=function(result){
		conf.View(result);
		win.syncSize();
		win.center();
		unmask();
	}
}
// next module

var ReportMenu=function() {
	var obj_id=0;

	var menuChange=new Ext.menu.Menu({
		items: [
			{itemId:'view_change',
			 text:'Показать Извещение',
			 handler:function(){ 
				var win = window.open(
					"/printer/change?id="+obj_id,
					"Извещение",
					"width:27cm,status=no"
				);
				win.focus()
			 },
			},
			{itemId:'view',
			 text:'Посмотреть файл',
			 handler:function(){
				CoreAJAX({
					url:'/file/getAllFile/',
					param:{'change_id':obj_id},
					succ: function(res){
						if ( res.file ){
							var win = window.open(
								"/media/out/"+res.file,
								"",
								"width:27cm,status=no"
							);
							win.focus();
						}
					}
				});
			 }
			}
		]
	});

	var menuRequest=new Ext.menu.Menu({
		items: [
			{itemId:'view_request',
			 text:'Показать Наряд',
			 handler:function(){ 
				var win = window.open(
					"/printer/request?id="+obj_id,
					"Наряд",
					"width:27cm,status=no"
				);
				win.focus()
			 },
			},
		]
	});

	this.view=function(type,id,XY){
		var menu;
		
		if(type && id){
			obj_id=id;	
			if(type=='change') menu=menuChange;
			else if(type=='request') menu=menuRequest;
			
			if(menu) menu.showAt(XY);
		}
	}
}
// next module

var ViewerBar=function() {
	var node_id=0;


	var barRoot=new Ext.ButtonGroup({
		frame:false,
		items: [
			{itemId:'add_project',
			 text:'Добавить проект',
			 handler:function(){ index.form_project.view(node_id); },
			 disabled: archive_groups.project ? false : true
			},{xtype: 'tbseparator'},
			{itemId:'add_change',
			 text:'Внести извещение',
			 handler:function(){ index.form_file.view(); }
			},
		]
	});

	var barProject=new Ext.ButtonGroup({
		frame:false,
		items: [
			{itemId:'opis_project',
			 text:'Опись проекта',
			 handler:function(){ 
				var win = window.open(
					"/printer/opis?id="+node_id,
					"Опись",
					"width:27cm,status=no"
				);
				win.focus()
			 },
			},{xtype: 'tbseparator'},
			{itemId:'edit_project',
			 text:'Редактировать проект',
			 handler:function(){ index.form_project.view(node_id); },
			 disabled: archive_groups.project ? false : true
			},{xtype: 'tbseparator'},
			{itemId:'add_change',
			 text:'Внести извещение',
			 handler:function(){ index.form_file.view(); }
			},{xtype: 'tbseparator'},
			{itemId:'add_folder',
			 text:'Добавить изделие',
			 handler:function(){ index.form_folder.view(node_id); },
			 disabled: archive_groups.project ? false : true
			},{xtype: 'tbseparator'},
			{itemId:'get_project',
			 text:'Выдать копию документов проекта',
			 handler:function(){ index.form_download.view(node_id); },
			},
		]
	});

	var barFolder=new Ext.ButtonGroup({
		frame:false,
		items: [
			{itemId:'opis_folder',
			 text:'Опись изделия',
			 handler:function(){ 
				var win = window.open(
					"/printer/opis?id="+node_id,
					"Опись",
					"width:27cm,status=no"
				);
				win.focus()
			 },
			},{xtype: 'tbseparator'},
			{itemId:'edit_folder',
			 text:'Редактировать изделие',
			 handler:function(){ index.form_folder.view(node_id,true); },
			},{xtype: 'tbseparator'},
			{itemId:'add_change',
			 text:'Внести извещение',
			 handler:function(){ index.form_file.view(); }
			},{xtype: 'tbseparator'},
			{text: 'Добавить',
			 menu: [
				{itemId:'add_folder',
				 text:'Добавить изделие',
				 handler:function(){ index.form_folder.view(node_id); },
				},
				{itemId:'add_document',
				 text:'Добавить документ',
				 handler:function(){ index.form_document.view(node_id); },
				}
			 ]
			},{xtype: 'tbseparator'},
			{itemId:'get_project',
			 text:'Выдать копию документов изделия',
			 handler:function(){ index.form_download.view(node_id); },
			},{xtype: 'tbseparator'},
			{itemId:'copy_folder',
			 text:'Скопировать изделие в другой проект',
			 handler:function(){ index.form_copy.view(node_id); },
			},{xtype: 'tbseparator'},
			{itemId:'move_folder',
			 text:'Переместить изделие',
			 handler:function(){ index.form_copy.view(node_id); },
			},
		]
	});

	var barDocument=new Ext.ButtonGroup({
		frame:false,
		items: [
			{itemId:'edit_document',
			 text:'Редактировать документ',
			 handler:function(){ index.form_document.view(node_id,true); },
			},{xtype: 'tbseparator'},
			{itemId:'change_document',
			 text:'Внести изменение',
			 handler:function(){ index.form_file.view(node_id); },
			},{xtype: 'tbseparator'},
			{itemId:'get_document',
			 text:'Выдать копию документа',
			 handler:function(){ index.form_download.view(node_id); },
			},
		]
	});

	this.bar=new Ext.Toolbar({
		hideBorders:true,
		items: [
			barRoot,
			barProject,
			barFolder,
			barDocument,
		]
	});
		
	this.view=function(id,project,havechildren,XY){
		barRoot.hide();
		barProject.hide();
		barFolder.hide();
		barDocument.hide();

		if(id){
			node_id=id;	
			if(id=='1') barRoot.show(false);
			else if(project) barProject.show();
			else if(havechildren) barFolder.show();
			else barDocument.show();
		}
	}
}
// next module

var ViewerMenu=function() {
	var node_id=0;


	var menuRoot=new Ext.menu.Menu({
		items: [
			{itemId:'add_project',
			 text:'Добавить проект',
			 handler:function(){ index.form_project.view(node_id); },
			 disabled: archive_groups.project ? false : true
			},'-',
			{itemId:'add_change',
			 text:'Внести извещение',
			 handler:function(){ index.form_file.view(); }
			},
		]
	});

	var menuProject=new Ext.menu.Menu({
		items: [
			{itemId:'info_project',
			 text:'Информация о проекте',
			 handler:function(){ index.viewer_folder.view(node_id,'Node'); },
			},
			{itemId:'opis_project',
			 text:'Опись проекта',
			 handler:function(){ 
				var win = window.open(
					"/printer/opis?id="+node_id,
					"Опись",
					"width:27cm,status=no"
				);
				win.focus()
			 },
			},
			{itemId:'edit_project',
			 text:'Редактировать проект',
			 handler:function(){ index.form_project.view(node_id,true); },
			 disabled: archive_groups.project ? false : true
			},'-',
			{itemId:'add_change',
			 text:'Внести извещение',
			 handler:function(){ index.form_file.view(); }
			},
			{itemId:'add_folder',
			 text:'Добавить изделие',
			 handler:function(){ index.form_folder.view(node_id); },
			 disabled: archive_groups.project ? false : true
			},'-',
			{itemId:'get_project',
			 text:'Выдать копию документов проекта',
			 handler:function(){ index.form_download.view(node_id); },
			},
		]
	});

	var menuFolder=new Ext.menu.Menu({	
		items: [
			{itemId:'info_folder',
			 text:'Информация о изделии',
			 handler:function(){ index.viewer_folder.view(node_id,'Node'); },
			},
			{itemId:'opis_folder',
			 text:'Опись изделия',
			 handler:function(){ 
				var win = window.open(
					"/printer/opis?id="+node_id,
					"Опись",
					"width:27cm,status=no"
				);
				win.focus()
			 },
			},
			{itemId:'edit_folder',
			 text:'Редактировать изделие',
			 handler:function(){ index.form_folder.view(node_id, true); },
			},'-',
			{itemId:'add_change',
			 text:'Внести извещение',
			 handler:function(){ index.form_file.view(); }
			},
			{itemId:'add_folder',
			 text:'Добавить изделие',
			 handler:function(){ index.form_folder.view(node_id); },
			},
			{itemId:'add_document',
			 text:'Добавить документ',
			 handler:function(){ index.form_document.view(node_id); },
			},'-',
			{itemId:'get_project',
			 text:'Выдать копию документов изделия',
			 handler:function(){ index.form_download.view(node_id); },
			},'-',
			{itemId:'copy_folder',
			 text:'Скопировать изделие в другой проект',
			 handler:function(){ index.form_copy.view(node_id); },
			},
			{itemId:'move_folder',
			 text:'Переместить изделие',
			 handler:function(){ index.form_move.view(node_id); },
			},
		]
	});

	var menuDocument=new Ext.menu.Menu({
		items: [
			{itemId:'info_document',
			 text:'Информация о документе',
			 handler:function(){ index.viewer_document.view(node_id); },
			},
			{itemId:'edit_document',
			 text:'Редактировать документ',
			 handler:function(){ index.form_document.view(node_id,true); },
			},'-',
			{itemId:'change_document',
			 text:'Внести изменение',
			 handler:function(){ index.form_file.view(node_id); },
			},'-',
			{itemId:'get_document',
			 text:'Выдать копию документа',
			 handler:function(){ index.form_download.view(node_id); },
			},
		]
	});
		
	this.view=function(id,project,havechildren,XY){
		var menu;
		
		if(id){
			node_id=id;	
			if(id=='1') menu=menuRoot;
			else if(project) menu=menuProject;
			else if(havechildren) menu=menuFolder;
			else menu=menuDocument;

			menu.showAt(XY);
		}
	}
}
// next module

//Проверка критических ошибок
function CheckCritical(){
	var critical=false;
	
	var win=new Ext.Window({
		closable: false,
		draggable: false,
		resizable: false,
		plain: true,
		modal: true,
		autoHeight: true,
		width: 400,
		frame: true,
		title: 'Внимание',
		html: '<center><b><font color="red">Критическая ошибка!<br>'+
			'Необходимо позвать администратора</font></b></center>'
	});

	var start=function(){
		critical=true;
		index.worklog.add('Критическая ошибка системы','sys');
		if(!archive_groups.server) win.show();
	}
	
	var end=function(){
		critical=false;
		index.worklog.add('Исправлена критическая ошибка системы','ok');
		win.hide();
	}
	
	var check=function (st){
		if ( !st && critical ) end();
		else if ( st && !critical ) start();
	}
	
	this.check=check;
}
// next module

//Авто разлогин
function CheckAuth(){
	var auth=true;
	var interval=1000*10, period=1000*60*5, last=(new Date()).getTime();
	
	var auto_logout=function (){
		if(auth && ((last+period)<(new Date()).getTime())) logout();
	}
	
	var touch=function (){
		last=(new Date()).getTime();
	}
	
	var login=function (){
		touch();
		auth=true;
		index.worklog.add('Вход в систему','ok');
	}
	
	var logout=function (){
		auth=false;
		index.worklog.add('Автоматический выход из системы','sys');
		index.form_login.view(true);
		CoreAJAX({url:'aj_logout',param:{}});
	}
	
	var check=function (st){
		if ( !st && auth ) logout();
		else if ( st && !auth ) login();
	}
	
	this.check=check;
	
	Ext.EventManager.on('body','keydown',touch);
	Ext.EventManager.on('body','mousedown',touch);
	Ext.EventManager.on('body','mousemove',touch);
	
	Ext.TaskMgr.start({run: auto_logout, interval: interval});
	touch();
}
// next module

//Проверка критических ошибок
function CheckWarning(){
	var warning=false;
	
	var win=new Ext.Window({
		closable: false,
		draggable: false,
		resizable: false,
		autoHeight: true,
		width: 400,
		frame: true,
		y: 0,
		html: '<center><b><font color="red">Сбой в системе! &nbsp;&nbsp;&nbsp;&nbsp;'+
			'Необходимо позвать администратора</font></b></center>'
	});
	
	var start=function(){
		warning=true;
		Ext.Msg.alert(
			'Внимание',
			'<b><font color="red">Произошёл сбой в системе!<br>'+
			'Необходимо позвать администратора,<br>'+
			'Но вы можете продолжить работу</font></b>'
		);
		index.worklog.add('Сбой в системе','sys');
		win.show();
	}
	
	var end=function(){
		warning=false;
		index.worklog.add('Устранён сбой в системе','ok');
		win.hide();
	}
	
	var check=function (st){
		if ( !st && warning) end();
		else if ( st && !warning) start();
	}
	
	this.check=check;
}
// next module

//Генератор обработчика вывода имени поля запроса
function LibGenParamName(par){
	if(par.param_rename){
		var names=par.names, param=par.param_rename;
		return function (req){ return names[req[param]]; }
	}
	else if(par.name){
		var name=par.name;
		return function (req){ return name; }
	}
	else{
		return function (req){ return 'None Name'; }
	}
}

//Генератор обработчика вывода значения поля запроса
function LibGenParamValue(par){
	var param=par.param;
	if(par.type=='id'){
		var store=par.store;
		if(par.store_field){
			var s_field=par.store_field;
			return function(req){
				var s_id=store.findExact(s_field,req[param]);
				return store.getAt(s_id).data.name;
			}
		}
		else{
			return function (req){ return store.getById(req[param]).data.name; }
		}
	}
	else if(par.type=='text'){
		return function (req){ return req[param]; }
	}
	else if(par.type=='date'){
		return function (req){ return new Date(req[param]*1000).format('d-m-Y'); }
	}
	else if(par.type=='date_a'){
		return function (req){ return new Date(req[param]*1000).format('d-m-Y H:i:s'); }
	}
	else{
		return function (req){ return 'None Value'; }
	}
}

//Генератор обработчика вывода имени колонки в зависимости от запроса
function LibGenParamCol(col){
	var name=col.name, names=col.names;
	return function (val){
		if(val) name=names[val];
		return name;
	}
}
/*	conf={
		title:заголовок панельки,
		url:откуда брать рез-ты,
		fields:[поля],
		+-search:Использовать-ли встроенную строку поиска(если да - нет метода load, за ненадобностью),
		+-page:кол-во на страницу(false-не использовать),
		+-expander:темплей для экспандера(false-не использовать),
		column:[колонки
			{
				name:заголовок
				field:поле,
				+-width:ширина
				+-renderer:функция рендера
			}
		],
		loader: func(object,res,req_all) Функция, работающая при загрузке
		rowclick: func(row, event) Событие RowClick,
		rowcontext: func(row, event) Событие RowClick,
	}
*/

//Генератор таблиц
function LibGrid(conf){
	var object=this;

//	//Добавление Пагинации к запросу
	var paging=function(obj){
		obj.limit=conf.page;
		obj.start=0;
		return obj;
	}

//	//Хранилище
	this.store=new Ext.data.JsonStore({
		url: conf.url,
		root: "records",
		totalProperty: "total",
		fields: conf.fields,
	});
	if(conf.page) object.store.baseParams=paging({});

//	//Доп.информация о строке
	var grid_expander=false, columns=[];
	if(conf.expander){
		grid_expander=new Ext.grid.RowExpander({tpl: conf.expander});
		columns=[grid_expander];
	}

//	//Колонки
	for(i=0;i<conf.column.length;i++){
		var col=conf.column[i];
		var res={header:col.name, dataIndex:col.field, sortable: true};
		if(col.width) res.width=col.width;
		if(col.renderer) res.renderer=col.renderer;
		columns.push(res);
	}
	this.column=new Ext.grid.ColumnModel(columns);

//	//Строка поиска
	var grid_tbar=false;
	if(conf.search) {
		grid_tbar=[
			'Поиск: ',
			' ',
			new Ext.ux.form.SearchField({store: object.store})
		];
	}

//	//Постраничная навигация
	var grid_bbar=false;
	if(conf.page){
		grid_bbar=new Ext.PagingToolbar({
			pageSize: conf.page,
			store: object.store,
			displayInfo: true,
			displayMsg: '{0} - {1} из {2}',
			emptyMsg: "Нет объектов",
		});
	}

//	//Событие-Левый клик на записи
	var grid_e_rowclick=function(grid, rowIndex, evtObj){
		evtObj.stopEvent();
		if(conf.rowclick) conf.rowclick(object.store.getAt(rowIndex), evtObj);
	};

//	//Событие-Правый клик на записи
	var grid_e_rowcontext=function(grid, rowIndex, evtObj){
		evtObj.stopEvent();
		if(conf.rowcontext) conf.rowcontext(object.store.getAt(rowIndex), evtObj);
	}

//	//Таблица
	this.grid=new Ext.grid.GridPanel ({
		title: conf.title,
		store: object.store,
		colModel: object.column,
		
		viewConfig: {forceFit:true},
		autoScroll: true,
		columnLines: true,
		
		enableColumnHide: false,
		enableColumnMove: false,
		enableDragDrop: false,
		enableHdMenu: false,
		
		plugins: grid_expander,
		tbar: grid_tbar,
		bbar: grid_bbar,
		
		listeners: {
			rowclick: grid_e_rowclick,
			rowcontextmenu: grid_e_rowcontext,
		},
	});

//	//Функция загрузки
	var mask=function(){ CoreMask(object.grid.el); }
	var unmask=function(){ object.grid.doLayout(); CoreUnMask(object.grid.el); }
	this.load=function(param){
		mask();
		if(!param) param={};
		if(conf.page) param=paging(param);
		object.store.baseParams=param;
		object.store.load({callback:load_fin});
	}

//	//Завершающая стадия обработка
	var load_fin=function(res,req_all){
		if(conf.loader) conf.loader(object,res,req_all);
		unmask();
	}
}

/*	conf={
		title:заголовок таблицы,
		url:источник,
		fields:[поля],
		+-page:кол-во на страницу(false-не использовать),
		+-expander:темплей для экспандера(false-не использовать),
		column:[колонки
			{
				name:заголовок
				field:поле,
				+-width:ширина
				+-renderer:функция рендера

				+-param_rename:имя параметра, в зависимости от значения которого вычисляется заголовок колонки
				+-names:{значение:результат}
				+-param_hide:если это поле задано в запросе, то скрывается колонка
			}
		],
		loader: func(object,res,req_all) Событие Загрузка
		rowclick: func(row, event) Событие RowClick,
		rowcontext: func(row, event) Событие RowClick,

		request:[расшифровка запроса
			{
				name:описание параметра,
				param:имя параметра,
				type:
					text - просто текст,
					date - дата ДД/ММ/ГГГГ,
					date_a - date+время,
					id - запись в хранилище store, по полю id либо, если задано, по полю store_field
				+-store:хранилище откуда взять имя
				+-store_field:поле в хранилище

				+-param_rename:	имя параметра, в зависимости от значения которого вычисляется надпись
				+-names:{значение:результат}

			}
		],
	}
*/

//Конструктор таблицы - результата поиска
function ConstructorGridResult(conf){
	var object=this;
	var gen_name=LibGenParamName, gen_value=LibGenParamValue, gen_col=LibGenParamCol;
	var column_id=0;

//	//Подготовка конфигурации
	var grid_conf={
		title: conf.title+' ( ):',
		url: conf.url,
		fields: conf.fields,
		search: false,
		column: [],
	};
	if(conf.page) grid_conf.page=conf.page;
	if(conf.expander){
		column_id=1;
		grid_conf.expander=conf.expander;
	}
	if(conf.rowclick) grid_conf.rowclick=conf.rowclick;
	if(conf.rowcontext) grid_conf.rowcontext=conf.rowcontext;

//	//Присваивание полям - обработчиков, и переформирование колонок
	var params={};
	for(i=0;i<conf.request.length;i++){
		var par=conf.request[i];
		params[par.param]={'name':gen_name(par),'value':gen_value(par),'rename':[],'hide':[]};
	}
	for(i=0;i<conf.column.length;i++,column_id++){
		var col=conf.column[i];
		if(col.param_rename)
			params[col.param_rename].rename.push({'cid':column_id,'rename':gen_col(col)});
		if(col.param_hide)
			params[col.param_hide].hide.push({'cid':column_id});
		
		var conf_col={name:col.name,field:col.field};
		if(col.width) conf_col.width=col.width;
		if(col.renderer) conf_col.renderer=col.renderer;
		grid_conf.column.push(conf_col);
	}

//	//Обработчик загрузки
	var loader=function(grid_object,res,req_all){
		if(res.length==0) Ext.MessageBox.alert('Результат поиска:', 'По вашему запросу данных не найдено');
		
		var title=[];
		title.push(conf.title+' ( ');
		if(req_all && req_all.params){
			var req=req_all.params;
			for(par_name in params){
				var par=params[par_name], c_hide=false, c_rename=false;
				if(req[par_name]){
					c_hide=true, c_rename=req[par_name];
					title.push(par.name(req)+': '+par.value(req)+'; ');
				}
				
				for(i=0;i<par.rename.length;i++)
					grid_object.column.setColumnHeader(par.rename[i].cid, par.rename[i].rename(c_rename));
				for(i=0;i<par.hide.length;i++)
					grid_object.column.setHidden(par.hide[i].cid, c_hide);
			}
		}
		title.push('):');
		grid_object.grid.setTitle(title.join(''));
	}

//	//Туннель для загрузчика
	if(!conf.loader) grid_conf.loader=loader;
	else{
		grid_conf.loader=function (object,res,req_all){
			loader(object,res,req_all);
			conf.loader(object,res,req_all);
		}
	}

	return new LibGrid(grid_conf);
}
// next module

function FormCopy(){
	var object=this;

	var fromGroup=new FormLibNodeGroup('Копируемое изделие',true);
	var toGroup=new FormLibNodeGroup('Применение',false,'to_');

//Форма
	var panel={
		Submit:function(form, action){
			index.viewer_navigation.tree_view(action.result.node);
			index.viewer_navigation.tree_view(action.result.parent);
		},
		SubmitURL:"aj_copyNode",
		SubmitMSG:"Скопировано изделие!",
		SubmitTITLE:"Копирование изделия",
		items:[
			fromGroup.container,
			toGroup.container,
		],
	};
//	//Очистка формы
	panel.Clear=function(){
		fromGroup.clear();
		toGroup.clear();
	}
//	//Обработка загруженных данных
	panel.View=function(result){
		var pr_id=result.node.pr_id, pr_name=result.node.pr_name;
		var n_id=result.node.id, n_name=result.obj.name, n_klgi=result.obj.klgi;
//	//	//Заполнение основных данных
		fromGroup.set(pr_id,pr_name,n_id,n_name,n_klgi);
	}
//	//Объект extJS
	var win=new FormLibWindow(panel);



//Просмотр
	var viewEdit=false, winType='Submit';
	this.view=function(id){
		if(!id) id=false;
		win.winView(id);
	}
}
// next module

function FormDocument(){
	var object=this;


//Регистрационный номер
	var regField=new Ext.form.NumberField({name:'reg', fieldLabel:'Регистрационный номер', anchor:'95%'});
	
	var regGroup=new Ext.form.FieldSet({
		title: 'Временное хранилище',
		items:[regField],
	});
	
	
//Извещение
	var docGroup=new FormLibDocumentGroup('Document');
//Применение
	var nodeGroup=new FormLibNodeGroup();
//Основные данные
	var infoGroup=new FormLibInfoGroup('Document');
//Новый файл
	var fileGroup=new FormLibFileGroup();


//Форма
	var panel={
		Submit:function(form, action){
			index.viewer_folder.refresh(action.result.n_id,action.result.p_id);
			index.viewer_document.view(action.result.n_id);
		},
		SubmitURL:"aj_saveNewObject",
		SubmitMSG:"Создан новый документ!",
		SubmitTITLE:"Добавление нового документа",
		Edit:function(form, action){
			index.viewer_folder.refresh(action.result.n_id,action.result.p_id);
			index.viewer_document.refresh(action.result.n_id);
		},
		EditURL:"aj_editObject",
		EditMSG:"Отредактирован докумен!",
		EditTITLE:"Редактирование документа",
		fileUpload:true,
		items:[
			nodeGroup.container,
			regGroup,
			docGroup.container,
			infoGroup.container,
			fileGroup.container,
		],
	};
//	//Очистка формы
	panel.Clear=function(){
		regGroup.show();
		regField.setValue();
//	//	//Очистка проекта и изделия
		nodeGroup.clear();
//	//	//Очистка извещения
		docGroup.show();
		docGroup.clear();
		docGroup.focus();
//	//	//Очистка полей формы
		infoGroup.clear();
//	//	//Очистка поля файла
		fileGroup.show();
		fileGroup.clear();
	}
//	//Обработка загруженных данных
	panel.View=function(result){
		var pr_id=result.node.pr_id, pr_name=result.node.pr_name;
		var n_id=result.node.id, n_name=result.obj.name, n_klgi=result.obj.klgi, n_desc=result.obj.desc;
		var t_id=result.obj.t_id, t_name=result.obj.t_name;
//	//	//Заполнение проекта и изделия
		nodeGroup.set(pr_id,pr_name,n_id,n_name,n_klgi);
//	//	//Заполнение основных данных
		infoGroup.set(viewEdit,n_name,n_klgi,n_desc,t_id,t_name);
//	//	//Заполнение формы
		if(viewEdit){
//	//	//Скрытие извещения, и поля файла
			if(result.node.reg) regField.setValue(result.node.reg);
			else regGroup.hide();
			docGroup.set(CoreMergeArray(result.obj,result.node));
			fileGroup.hide();
		}
	}
//	//Объект extJS
	var win=new FormLibWindow(panel);



//Просмотр
	var viewEdit=false, winType='Submit';
	this.view=function(id,edit){
		if(edit){
			viewEdit=true;
			winType='Edit';
		}
		else{
			viewEdit=false;
			winType='Submit';
		}
		if(!id) id=false;
		win.winView(id,winType);
	}
}
// next module

function FormDownload(){
	var object=this;
		
//Элемент-заглушка, для авто-прокрутки страницы
	var freeSpace=new Ext.form.TextField ({}), freeSpaceEnable=true;
	var freeSpaceScroll=function(){
		if(!freeSpaceEnable) freeSpaceEnable=true;
		else{
			freeSpace.show(false);
			freeSpace.focus();
			freeSpace.hide();
		}
	}
		
//Извещение
	var docGroup=new FormLibDocumentGroup('Download');
//Поиск по КЛГИ
	var klgiGroup=new FormLibKlgiGroup(freeSpaceScroll);
//Поиск по Ноду
	var nodeGroup=new FormLibNodeGroup('Выборка по Проекту',true);
		
//Выбор режима поиск
	var choiceGroup=new Ext.form.FieldSet({
		title:'Критерий поиска',
		items:[
			{xtype: 'radiogroup',
			 hideLabel: true,
			 items: [
				{boxLabel:'КЛГИ', name:'type', inputValue:'klgi', checked:true},
				{boxLabel:'Проект', name:'type', inputValue:'node'},
			 ],
			 listeners: {
				change: function(radio, set) {
					var val=set.getGroupValue();
					
					if(val=='klgi'){
						nodeGroup.hide();
						klgiGroup.show();
					}
					else if(val=='node'){
						klgiGroup.hide();
						nodeGroup.show();
					}
					freeSpaceScroll();
				}
			 }
			}
		]
	});

//Форма
	var panel={
		Submit:function(){},
		SubmitURL:"/file/getFileInfo/",
		SubmitMSG:"Добавлен наряд на изготовление копии",
		SubmitTITLE:"Добавление наряда на изготовление копии",
		items:[
			docGroup.container,
			choiceGroup,
			klgiGroup.container,
			nodeGroup.container,
			freeSpace,
		],
	};
//	//Отправка формы
	panel.Submit=function(form, action){
		document.getElementById('downloadZIP').src='/media/out/'+action.result.name+'.zip';
		document.getElementById('downloadPDF').src='/media/out/'+action.result.name+'.doc';
	}
//	//Очистка формы
	panel.Clear=function(){
//	//	//Очистка извещения
		docGroup.clear();
//	//	//Выбор поиска по КЛГИ
		choiceGroup.get(0).setValue('klgi');
//	//	//Очистка списка КЛГИ
		klgiGroup.clear();
//	//	//Очистка проекта и изделия
		nodeGroup.clear();
		nodeGroup.hide();
//	//	//Прокрутка в начало формы
		freeSpace.hide();
		freeSpaceEnable=false;
		docGroup.focus();
	}
//	//Обработка загруженных данных
	panel.View=function(result){
		var pr_id=result.node.pr_id, pr_name=result.node.pr_name;
		var n_id=result.node.id, n_name=result.obj.name, n_klgi=result.obj.klgi;
//	//	//Выбор поиска по Ноду
		choiceGroup.get(0).setValue('node');
//	//	//Заполнение проекта и изделия
		nodeGroup.set(pr_id,pr_name,n_id,n_name,n_klgi);
//	//	//Показ и прокрутка в начало формы
		freeSpaceEnable=false;
		docGroup.focus();
	}
//	//Объект extJS
	var win=new FormLibWindow(panel);



//Просмотр
	this.view=function(id){
		if(!id) id=false;
		win.winView(id);
	}
}
// next module

function FormFile(){
	var object=this;
		
//Элемент-заглушка, для авто-прокрутки страницы
	var freeSpace=new Ext.form.TextField ({}), freeSpaceEnable=true;
	var freeSpaceScroll=function(){
		if(!freeSpaceEnable) freeSpaceEnable=true;
		else{
			freeSpace.show(false);
			freeSpace.focus();
			freeSpace.hide();
		}
	}


//Регистрационный номер
	var regField=new Ext.form.NumberField({name:'reg', fieldLabel:'Регистрационный номер', anchor:'95%'});
	
	var regGroup=new Ext.form.FieldSet({
		title: 'Временное хранилище',
		items:[regField],
	});

//Извещение
	var docGroup=new FormLibDocumentGroup('File');
//Изменения
	var filesGroup=new FormLibKlgiGroup(freeSpaceScroll,'Изменения',true,true);
//Новый файл
	var fileGroup=new FormLibFileGroup();

//Выбор проекта
	var projectCombo=new Ext.form.ComboBox({
		name: "pname",
		valueField: 'id',
		hiddenName:'pid',
		hiddenValue:'id',
		store: index.store_projects,
		triggerAction: 'all',
		emptyText:'Выберите проект',
		minChars: 1,
		forceSelection: true,
		valueField: 'id',
		displayField: "name",
		hideLabel: true,
		allowBlank:false,
		mode:'local',
		loadingText: 'Идет запрос на сервер...',
		anchor:'95%',
		listeners: {
			select: function() {
				var pid=projectCombo.getValue();
				filesGroup.clear();
				filesGroup.ComboStore.setBaseParam('idproject',pid);
				filesGroup.ComboStore.load({params:{'idproject':pid}});
			}
		}
	});
	var projectGroup=new Ext.form.FieldSet({
		title: 'Проект',
		defaultType: 'textfield',
		items:[projectCombo]
	});


//Форма
	var panel={
		Submit:function(){},
		SubmitURL:"aj_ChangeAdd",
		SubmitMSG:"Введено извещение!",
		SubmitTITLE:"Ввод извещения",
		fileUpload:true,
		items:[
			projectGroup,
			regGroup,
			fileGroup.container,
			docGroup.container,
			filesGroup.container,
			freeSpace,
		],
	};
//	//Очистка формы
	panel.Clear=function(){
		regGroup.show();
		regField.setValue();
//	//	//Очистка проекта
		projectCombo.clearValue();
		projectCombo.validate();
		projectCombo.setReadOnly(false);
//	//	//Очистка извещения
		docGroup.clear();
//	//	//Очистка изменений
		filesGroup.clear();
//	//	//Прокрутка в начало формы
		freeSpace.hide();
		freeSpaceEnable=false;
		fileGroup.clear();
		docGroup.focus();
	}
//	//Обработка загруженных данных
	panel.View=function(result){
		var pr_id=result.node.pr_id, n_id=result.node.id;
		projectCombo.setValue(pr_id);
		projectCombo.setReadOnly(true);
		filesGroup.ComboStore.setBaseParam('idproject',pr_id);
		filesGroup.ComboStore.load({params:{'idproject':pr_id}});
		filesGroup.set(n_id);
	}
//	//Объект extJS
	var win=new FormLibWindow(panel);



//Просмотр
	this.view=function(id){
		if(!id) id=false;
		win.winView(id);
	}
}
// next module

function FormFilesChange(){
	var object=this;
	var selectedId=null;

//Элемент-заглушка, для авто-прокрутки страницы
	var freeSpace=new Ext.form.TextField ({}), freeSpaceEnable=true;
	var freeSpaceScroll=function(){
		if(!freeSpaceEnable) freeSpaceEnable=true;
		else{
			freeSpace.show(false);
			freeSpace.focus();
			freeSpace.hide();
		}
	}

	var fileChange=new Ext.form.TextField({name:'change', hidden:true});
//Изменения
	var filesGroup=new FormLibKlgiGroupAdd(freeSpaceScroll,'Изменения',true,true);
	
	var filesContent=new FormLibKlgiContent(freeSpaceScroll,'Содержание');
	
//Новый файл
	var fileGroup=new FormLibFileGroup();

//Форма
	var panel={
		Submit:function(form, action){ index.files.refresh(); },
		SubmitURL:"aj_ChangeAddFile",
		SubmitMSG:"Документы изменены!",
		SubmitTITLE:"Изменение документов в извещении",
		fileUpload:true,
		width: 500,
		items:[
			fileChange,
			filesGroup.container,
			filesContent.container
		],
	};
//	//Очистка формы
	panel.Clear=function(){
		fileGroup.clear();
		filesGroup.clear();
	}
//	//Обработка загруженных данных
	panel.View=function(result){ }
//	//Объект extJS
	var win=new FormLibWindow(panel);

//Просмотр
	this.view=function(id, project_id){
		filesGroup
		filesContent
		fileChange.setValue(id);
		
		filesGroup.ComboStore.setBaseParam('idproject',project_id);
		filesGroup.ComboStore.load({params:{'idproject':project_id}});
		
		filesContent.store.setBaseParam('query', id);
		filesContent.store.load();
		win.winView(0,'Submit');
	};
}
// next module

function FormFilesFile(){
	var object=this;

	var fileField=new Ext.form.TextField({name:'file_id', hidden:true});
//Новый файл
	var fileGroup=new FormLibFileGroup();


//Форма
	var panel={
		Submit:function(form, action){ index.files.refresh(); },
		SubmitURL:"aj_FilesNew",
		SubmitMSG:"Заменён файл в хранилище!",
		SubmitTITLE:"Замена файла в хранилище",
		fileUpload:true,
		items:[
			fileField,
			fileGroup.container,
		],
	};
//	//Очистка формы
	panel.Clear=function(){
		fileGroup.clear();
	}
//	//Обработка загруженных данных
	panel.View=function(result){ }
//	//Объект extJS
	var win=new FormLibWindow(panel);

//Просмотр
	this.view=function(id){
		win.winView(0,'Submit');
		fileField.setValue(id);
	}
}
// next module

function FormFolder(){
	var object=this;

	var idField=new Ext.form.TextField({name:'node',hidden:true,emptyText:1});
//Применение
	var nodeGroup=new FormLibNodeGroup();
//Основные данные
	var infoGroup=new FormLibInfoGroup('Folder');


//Форма
	var panel={
		Submit:function(form, action){
			index.viewer_navigation.tree_view(action.result.parent);
			index.viewer_folder.refresh(action.result.node,action.result.parent);
		},
		SubmitURL:"aj_FolderAdd",
		SubmitMSG:"Создано новое изделие!",
		SubmitTITLE:"Добавление нового изделия",
		Edit:function(form, action){
			index.viewer_navigation.tree_view(action.result.parent);
			index.viewer_folder.refresh(action.result.node,action.result.parent);
		},
		EditURL:"aj_FolderEdit",
		EditMSG:"Отредактировано изделие!",
		EditTITLE:"Редактирование изделия",
		items:[
			idField,
			nodeGroup.container,
			infoGroup.container,
		],
	};
//	//Очистка формы
	panel.Clear=function(){
		idField.setValue(1);
//	//	//Очистка проекта и изделия
		nodeGroup.clear();
//	//	//Очистка полей формы
		infoGroup.clear();
	}
//	//Обработка загруженных данных
	panel.View=function(result){
		var pr_id=result.node.pr_id, pr_name=result.node.pr_name;
		var n_id=result.node.id, n_name=result.obj.name, n_klgi=result.obj.klgi, n_desc=result.obj.desc;
//	//	//Заполнение проекта и изделия
		nodeGroup.set(pr_id,pr_name,n_id,n_name,n_klgi);
//	//	//Заполнение основных данных
		if(viewEdit) idField.setValue(n_id);
		else n_klgi='';
		infoGroup.set(viewEdit,n_name,n_klgi,n_desc);
	}
//	//Объект extJS
	var win=new FormLibWindow(panel);



//Просмотр
	var viewEdit=false, winType='Submit';
	this.view=function(id,edit){
		if(edit){
			viewEdit=true;
			winType='Edit';
		}
		else{
			viewEdit=false;
			winType='Submit';
		}
		if(!id) id=false;
		win.winView(id,winType);
	}
}
// next module

function FormLogin(){
	var object=this;
	var user_name=0;
	
	var loginField=new Ext.form.TextField({fieldLabel:'Имя пользователя', name:'username', allowBlank:false, anchor:'95%'});
	var passField=new Ext.form.TextField({fieldLabel:'Пароль', name:'password', inputType:'password', allowBlank:false, anchor:'95%'});
	var helpLabel=new Ext.form.Label({
		html:'<center>В связи с неактивностью и в целях обеспечения безопасности,'+
		'<br>был произведён автоматический выход из системы!'+
		'<br>Для продолжения работы необходим повторный вход.<br><br></center>',
		style: 'color:red; font-weight: bold;',
		anchor:'95%'
	});	
	var errorLabel=new Ext.form.Label({
		html:'<center>Имя пользователя или пароль не подходят.<br>Пожалуйста, попробуйте еще раз.<br><br></center>',
		style: 'color:red; font-weight: bold;',
		anchor:'95%'
	});

	var winBody=new Ext.FormPanel({
		frame:true,
		style:'align:center;',
		items: [
			helpLabel,
			errorLabel,
			loginField,
			passField
		],
	});

	var winSubmit=function(){
		if(loginField.isValid() && passField.isValid()){
			user_name=loginField.getValue();
			mask();
			CoreAJAX({
				url:'aj_login',
				param:{'login':user_name,'pass':passField.getValue()},
				succ:winSubmitRes,
				fail:winSubmitRes
			});
		}
	}

	var winSubmitRes=function(result){
		if(result.success=="true"){
			if(archive_user_name!=user_name){
				document.location.href = document.location.href;
			}
			else{
				loginField.setValue();
				loginField.validate();
				passField.setValue();
				passField.validate();
				win.hide();
			}
		}
		else{
			helpLabel.hide();
			errorLabel.show();
			win.syncSize();
		}
		unmask();
	}

	var winButtonSubmit=new Ext.Button({text: 'Войти', handler: winSubmit});
	var win=new Ext.Window({
		closable: false,
		draggable: false,
		resizable: false,
		plain: true,
		modal: true,
		autoHeight: true,
		width: 400,
		frame: true,
		title: 'Вход в систему',
		items: [winBody],
		buttons:[winButtonSubmit],
	});
	
	var mask=function(){ CoreMask(win.el); }
	var unmask=function(){ CoreUnMask(win.el); }
	this.view=function(help){
		win.show();
		if(help) helpLabel.show();
		else helpLabel.hide();
		errorLabel.hide();
		win.syncSize();
	}
}
// next module

function FormMove(){
	var object=this;

	var fromGroup=new FormLibNodeGroup('Перемещаемое изделие',true);
	var toGroup=new FormLibNodeGroup('Применение',false,'to_');

//Форма
	var panel={
		Submit:function(form, action){
			index.viewer_navigation.tree_view(action.result.node);
			index.viewer_navigation.tree_view(action.result.parent);
		},
		SubmitURL:"aj_moveNode",
		SubmitMSG:"Изделие перенесено!",
		SubmitTITLE:"Перемещение изделия",
		items:[
			fromGroup.container,
			toGroup.container,
		],
	};
//	//Очистка формы
	panel.Clear=function(){
		fromGroup.clear();
		toGroup.clear();
	}
//	//Обработка загруженных данных
	panel.View=function(result){
		var pr_id=result.node.pr_id, pr_name=result.node.pr_name;
		var n_id=result.node.id, n_name=result.obj.name, n_klgi=result.obj.klgi;
//	//	//Заполнение основных данных
		fromGroup.set(pr_id,pr_name,n_id,n_name,n_klgi);
	}
//	//Объект extJS
	var win=new FormLibWindow(panel);



//Просмотр
	var viewEdit=false, winType='Submit';
	this.view=function(id){
		if(!id) id=false;
		win.winView(id);
	}
}
// next module

function FormProject(){
	var object=this;

	var idField=new Ext.form.TextField({name:'node',hidden:true,emptyText:1});
//Основные данные
	var infoGroup=new FormLibInfoGroup('Project');


//Форма
	var panel={
		Submit:function(form, action){
			index.store_projects.load();
			index.viewer_navigation.tree_view(1);
			index.viewer_folder.refresh(action.result.node,1);
		},
		SubmitURL:"aj_ProjectAdd",
		SubmitMSG:"Создан новый проект!",
		SubmitTITLE:"Добавление нового проекта",
		Edit:function(form, action){
			index.store_projects.load();
			index.viewer_navigation.tree_view(1);
			index.viewer_folder.refresh(action.result.node,1);
		},
		EditURL:"aj_ProjectEdit",
		EditMSG:"Отредактирован проект!",
		EditTITLE:"Редактирование проекта",
		items:[
			idField,
			infoGroup.container,
		],
	};
//	//Очистка формы
	panel.Clear=function(){
		idField.setValue(1);
//	//	//Очистка полей формы
		infoGroup.clear();
	}
//	//Обработка загруженных данных
	panel.View=function(result){
		var n_id=result.node.id, n_name=result.obj.name, n_desc=result.obj.desc;
//	//	//Заполнение основных данных
		if(viewEdit){
			infoGroup.set(viewEdit,n_name,'n_klgi',n_desc);
			idField.setValue(n_id);
		}
	}
//	//Объект extJS
	var win=new FormLibWindow(panel);



//Просмотр
	var viewEdit=false, winType='Submit';
	this.view=function(id){
		if(id!=1){
			viewEdit=true;
			winType='Edit';
		}
		else{
			viewEdit=false;
			winType='Submit';
		}
		if(!id) id=false;
		win.winView(id,winType);
	}
}
// next module

function FormTChangeApply(){
	var object=this;

	var changeField=new Ext.form.TextField({name:'change', hidden:true});
	var invField=new FormLibInvField();

//Форма
	var panel={
		Submit:function(form, action){ index.tchange.refresh(); },
		SubmitURL:"aj_TChangeApply",
		SubmitMSG:"Извещение подтверждено, изменения приняты в архив!",
		SubmitTITLE:"Принятие изменений в архив",
		fileUpload:true,
		items:[
			changeField,
			invField.field,
		],
	};
//	//Очистка формы
	panel.Clear=function(){
		changeField.setValue();
		invField.clear();
	}
//	//Обработка загруженных данных
	panel.View=function(result){ }
//	//Объект extJS
	var win=new FormLibWindow(panel);

//Просмотр
	this.view=function(id){
		win.winView(0,'Submit');
		changeField.setValue(id);
	}
}
// next module

function FormTStorageApply(){
	var object=this;

	var nodeField=new Ext.form.TextField({name:'node', hidden:true});
	var invField=new FormLibInvField();

//Форма
	var panel={
		Submit:function(form, action){ index.temp.refresh(); },
		SubmitURL:"aj_TStorageApply",
		SubmitMSG:"Документ перемещён в архив!",
		SubmitTITLE:"Перемещение документа в архив",
		fileUpload:true,
		items:[
			nodeField,
			invField.field,
		],
	};
//	//Очистка формы
	panel.Clear=function(){
		nodeField.setValue();
		invField.clear();
	}
//	//Обработка загруженных данных
	panel.View=function(result){ }
//	//Объект extJS
	var win=new FormLibWindow(panel);

//Просмотр
	this.view=function(id){
		win.winView(0,'Submit');
		nodeField.setValue(id);
	}
}
// next module

function FormTStorageFile(){
	var object=this;

	var nodeField=new Ext.form.TextField({name:'node', hidden:true});
	
//Регистрационный номер
	var regField=new Ext.form.NumberField({name:'reg', fieldLabel:'Регистрационный номер', allowBlank:false, anchor:'95%'});
	
	var regGroup=new Ext.form.FieldSet({
		title: 'Временное хранилище',
		items:[regField],
	});

//Новый файл
	var fileGroup=new FormLibFileGroup();


//Форма
	var panel={
		Submit:function(form, action){ index.temp.refresh(); },
		SubmitURL:"aj_TStorageNew",
		SubmitMSG:"Обновлён файл во временном хранилище!",
		SubmitTITLE:"Обновление файла во временном хранилище",
		fileUpload:true,
		items:[
			nodeField,
			regGroup,
			fileGroup.container,
		],
	};
//	//Очистка формы
	panel.Clear=function(){
		regField.setValue('');
		fileGroup.clear();
	}
//	//Обработка загруженных данных
	panel.View=function(result){ }
//	//Объект extJS
	var win=new FormLibWindow(panel);

//Просмотр
	this.view=function(id){
		win.winView(0,'Submit');
		nodeField.setValue(id);
	}
}
// next module

function ReportResult(){
	var object=this;

	var grid_conf={
		title: 'Отчёт',
		url: "aj_getReport",
		fields: ['id','type','date','number','inv','group','autor','format','sheets','base','msg'],
		page: index.report_config.result_page,
		expander: new Ext.XTemplate(
			'<p><br><tpl if="base"><b>Основание</b>: {base}<br></tpl>'+
			'<b>Комментарий</b>: {msg}</p>'
		),
		column:[
			{name: "Дата", field: 'date', width: 30},
			{name: "Номер", field: 'number', width: 50,
			 names: {'request':'Номер наряда','change':'Номер извещения'}, param_rename: 'type'},
			{name: "№", field: 'inv', width: 50,
			 names: {'request':'Заказ №','change':'Инвентарный номер'}, param_rename: 'type'},
			{name: "Отдел/Цех", field: 'group', width: 50},
			{name: "Сотрудник", field: 'autor', width: 50,
			 names: {'request':'Затребовал','change':'Составил'}, param_rename: 'type', param_hide:'autor'},
			{name: "Формат листов", field: 'format', width: 50},
			{name: "Кол-во листов", field: 'sheets', width: 50},
		],
		request:[
			{name: "Тип", param: 'type', type: 'id', store: index.store_report_types, store_field:'eng'},
			{name: "Сотрудник", param: 'autor', type: 'id', store: index.store_autors,
			 names: {'request':'Затребовал','change':'Составил'}, param_rename: 'type'},
			{name: "С", param: 'dateFrom', type: 'date'},
			{name: "До", param: 'dateTo', type: 'date'},
		],
		rowcontext: function(row, evtObj){
			index.report_menu.view(row.get('type'),row.get('id'),evtObj.getXY());
		},
	};
	
	var grid=new ConstructorGridResult(grid_conf);

	this.region=new Ext.Container({
		layout:'fit',
		region:'center',
		items:[grid.grid]
	});
	this.view=grid.load;
}
// next module

function ReportSearch(){
	var object=this;
		
	//Выбор тип отчёта
	var typeCombo=new Ext.form.ComboBox({
		name: "tname",
		valueField: 'eng',
		hiddenName:'type',
		store: index.store_report_types,
		triggerAction: 'all',
		emptyText:'Выберите тип отчёта',
		allowBlank:false,
		mode:'local',
		editable: false,
		displayField: 'name',
		fieldLabel: 'Тип отчёта',
		value:'request',
		anchor:'95%',
	});
	
	//Выбор сотрудника
	var autorCombo=new Ext.form.ComboBox({
		name: "aname",
		valueField: 'id',
		hiddenName:'autor',
		store: index.store_autors,
		triggerAction: 'all',
		emptyText:'Выберите сотрудника',
		mode:'local',
		editable: false,
		displayField: 'name',
		fieldLabel: 'Сотрудник',
		anchor:'95%',
	});
	
	//Группа параметров отчёта	
	var typeGroup=new Ext.form.FieldSet({
		style: 'padding:0 5px; margin:0 5px;',
		title: 'Параметры отчёта',
		items:[
			typeCombo,
			autorCombo
		],
	});
	
	//Группа выбора даты
	var dateGroup=new CoreLibDateGroup();


	//Кнопка очистки поиска
	var resetButton={
		text: 'Сбросить',
		handler: function() {
			typeCombo.setValue('request');
			autorCombo.clearValue();
			dateGroup.autoSet();
		}
	}
	//Кнопка поиска
	var searchButton={
		text: 'Поиск',
		handler: function(){			
			var type=typeCombo.getValue();
			var autor=autorCombo.getValue();
			var date_from=dateGroup.fromValue();
			var date_to=dateGroup.toValue();
			
			
			if(type!=''){
				var param={};
				
				param.type=type;
				if(autor!='') param.autor=autor;
				if(date_from) param.dateFrom=date_from;
				if(date_to) param.dateTo=date_to+24*60*60-1;
			
				index.report_result.view(param);
			}
		}
	}

//Объект extJS для рендера
	this.region=new Ext.FormPanel({
		title: 'Поиск',
		region:'north',
		height:'150',
		animCollapse: false,
		collapsible: true,
		collapseMode: 'mini',
		items:[
			{layout: 'column',
			 border: false,
			 items:[typeGroup,dateGroup.group],
			 anchor:'100% 100%'
			}
		],
		buttons:[resetButton,searchButton]
	});
}
// next module

function SyslogResult(){
	var object=this;

	var grid_conf={
		title: 'Журнал',
		url: "aj_getTestResults",
		fields: ['id','date','idtest','name','status_code','status_message','items','items_count'],
		page: index.report_config.result_page,
		expander: new Ext.XTemplate(
			'<p>'+
				'<tpl if="status_message"><br><b>Сообщение</b>: {status_message}</tpl>'+
				'<tpl if="items_count"><br><b>Содержание</b></tpl>'+
				'<tpl for="items">'+
					'<br>&nbsp; &nbsp; &nbsp;<b>{label}</b>: {name}'+
				'</tpl>'+
			'</p>'
		),
		column:[
			{name: "ID", field: 'id', width: 10},
			{name: "Дата", field: 'date'},
			{name: "Тест", field: 'name'},
			{name: "Результат", field: 'status_code',
			 renderer: function (c){
				if(c==0) return '<font color="green">OK</font>';
				else if(c==1) return '<font color="orange">Warning</font>';
				else if(c==2) return '<b><font color="red">Error</font></b>';
			 }
			},
		],
		request:[
			{name: "Объект", param: 'obj', type: 'text'},
			{name: "Тип Объекта", param: 'tobj', type: 'id', store: index.store_hobjects_types},
			{name: "Тип Теста", param: 'ttest', type: 'id', store: index.store_test_types},
			{name: "С", param: 'dateFrom', type: 'date'},
			{name: "До", param: 'dateTo', type: 'date'},
		],
	};
	
	var grid=new ConstructorGridResult(grid_conf);

	this.region=new Ext.Container({
		layout:'fit',
		region:'center',
		items:[grid.grid]
	});
	this.view=grid.load;
}
// next module

function SyslogSearch(){
	var object=this;
		
	//Выбор типа объекта
	var tobjCombo=new Ext.form.ComboBox({
		name: "obj_tname",
		valueField: 'id',
		hiddenName:'obj_type',
		store: index.store_hobject_types,
		triggerAction: 'all',
		emptyText:'Выберите тип объектов',
		mode:'local',
		editable: false,
		displayField: 'name',
		fieldLabel: 'Тип объектов',
		anchor:'95%',
		listeners: {
			select: function() { objCombo.clearValue(); }
		},
		disabled:true,
	});
	//Выбор объекта
	var objCombo=new Ext.form.ComboBox({
		name: "oname",
		valueField: 'id',
		hiddenName:'obj',
		store: index.store_hobjects,
		triggerAction: 'all',
		emptyText:'Выберите объект',
		mode:'local',
		editable: false,
		displayField: 'name',
		fieldLabel: 'Объект',
		anchor:'95%',
		listeners: {
			select: function() { tobjCombo.clearValue(); }
		},
	});	
	//Группа выбора объекта	
	var typeGroup=new Ext.form.FieldSet({
		style: 'padding:0 5px; margin:0 5px;',
		title: 'Объект',
		items:[
			tobjCombo,
			objCombo
		],
	});
	
	//Выбор типа объекта
	var ttestCombo=new Ext.form.ComboBox({
		name: "t_tname",
		valueField: 'id',
		hiddenName:'t_type',
		store: index.store_test_types,
		triggerAction: 'all',
		emptyText:'Выберите тип тестов',
		mode:'local',
		editable: false,
		displayField: 'name',
		fieldLabel: 'Тип тестов',
		anchor:'95%',
	});
	var resCheck=new Ext.form.CheckboxGroup({
		fieldLabel: 'Результат',
		items: [
			{boxLabel: '<font color="green">OK</font>', name: 'res0', checked: true},
			{boxLabel: '<font color="orange">Warning</font>', name: 'res1', checked: true},
			{boxLabel: '<b><font color="red">Error</font></b>', name: 'res2', checked: true},
		],
	});
	//Группа выбора теста	
	var testGroup=new Ext.form.FieldSet({
		style: 'padding:0 5px; margin:0 5px;',
		title: 'Тест',
		items:[
			ttestCombo,
			resCheck
		],
		width:'350'
	});	
	
	//Группа выбора даты
	var dateGroup=new CoreLibDateGroup();


	//Кнопка очистки поиска
	var resetButton={
		text: 'Сбросить',
		handler: function() {
			tobjCombo.clearValue();
			objCombo.clearValue();
			ttestCombo.clearValue();
			resCheck.setValue([true,true,true]);
			dateGroup.autoSet();
		}
	}
	//Кнопка поиска
	var searchButton={
		text: 'Поиск',
		handler: function(){			
			var tobj=tobjCombo.getValue();
			var obj=objCombo.getValue();
			var ttest=ttestCombo.getValue();
			var res=resCheck.getValue();
			var date_from=dateGroup.fromValue();
			var date_to=dateGroup.toValue();
			
			var param={};
			
			if(obj!='') param.obj=obj;
			else if(tobj!='') param.tobj=tobj;
			if(ttest!='') param.ttest=ttest;
			if(res[0]) param[res[0].getName()]=1;
			if(res[1]) param[res[1].getName()]=1;
			if(res[2]) param[res[2].getName()]=1;
			if(date_from) param.dateFrom=date_from;
			if(date_to) param.dateTo=date_to+24*60*60-1;
			
			index.syslog_result.view(param);
		}
	}

//Объект extJS для рендера
	this.region=new Ext.FormPanel({
		title: 'Поиск',
		region:'north',
		height:'150',
		animCollapse: false,
		collapsible: true,
		collapseMode: 'mini',
		items:[
			{layout: 'column',
			 border: false,
			 items:[typeGroup,testGroup,dateGroup.group],
			 anchor:'100% 100%'
			}
		],
		buttons:[resetButton,searchButton]
	});
}
// next module

function TimeResult(){
	var object=this;

	var grid_conf={
		title: 'Сессии',
		url: "aj_getUserSession",
		fields: ['id','start','finish','user','closed'],
		page: index.time_config.result_page,
		column:[
			{name: "ID", field: 'id', width: 10},
			{name: "Начало сессии", field: 'start'},
			{name: "Последнее время", field: 'finish'},
			{name: "Пользователь", field: 'user'},
			{name: "Статус", field: 'closed',
			 renderer: function (c){
				if(c==0) return '<font color="green">Открыта</font>';
				else if(c==1) return '<font color="orange">Закрыта</font>';
			 }
			},
		],
		request:[
			{name: "Пользователь", param: 'tobj', type: 'id', store: index.store_users},
			{name: "С", param: 'dateFrom', type: 'date'},
			{name: "До", param: 'dateTo', type: 'date'},
		],
	};
	
	var grid=new ConstructorGridResult(grid_conf);

	this.region=new Ext.Container({
		layout:'fit',
		region:'center',
		items:[grid.grid]
	});
	this.view=grid.load;
}
// next module

function TimeSearch(){
	var object=this;
		
	//Выбор статус сессии
	var closedCheck=new Ext.form.CheckboxGroup({
		fieldLabel: 'Статус',
		items: [
			{boxLabel: '<font color="green">Открыта</font>', name: 'closed0', checked: true},
			{boxLabel: '<font color="orange">Закрыта</font>', name: 'closed1', checked: true},
		],
	});
	
	//Выбор пользователя
	var userCombo=new Ext.form.ComboBox({
		name: "aname",
		valueField: 'id',
		hiddenName:'user',
		store: index.store_users,
		triggerAction: 'all',
		emptyText:'Выберите пользователя',
		mode:'local',
		editable: false,
		displayField: 'name',
		fieldLabel: 'Пользователь',
		anchor:'95%',
	});
	
	//Группа параметров поиска	
	var typeGroup=new Ext.form.FieldSet({
		style: 'padding:0 5px; margin:0 5px;',
		title: 'Параметры отчёта',
		items:[
			userCombo,
			closedCheck
		],
	});
	
	//Группа выбора даты
	var dateGroup=new CoreLibDateGroup();


	//Кнопка очистки поиска
	var resetButton={
		text: 'Сбросить',
		handler: function() {
			closedCheck.setValue([true,true]);
			userCombo.clearValue();
			dateGroup.autoSet();
		}
	}
	//Кнопка поиска
	var searchButton={
		text: 'Поиск',
		handler: function(){
			window.closedCheck=closedCheck;
			var cl=closedCheck.getValue();
			var user=userCombo.getValue();
			var date_from=dateGroup.fromValue();
			var date_to=dateGroup.toValue();
			
			var param={};
			
			if(user!='') param.user=user;
			if(cl[0]) param[cl[0].getName()]=1;
			if(cl[1]) param[cl[1].getName()]=1;
			window.cl=cl;
			if(date_from) param.dateFrom=date_from;
			if(date_to) param.dateTo=date_to+24*60*60-1;
			
			index.time_result.view(param);
		}
	}

//Объект extJS для рендера
	this.region=new Ext.FormPanel({
		title: 'Поиск',
		region:'north',
		height:'150',
		animCollapse: false,
		collapsible: true,
		collapseMode: 'mini',
		items:[
			{layout: 'column',
			 border: false,
			 items:[typeGroup,dateGroup.group],
			 anchor:'100% 100%'
			}
		],
		buttons:[resetButton,searchButton]
	});
}
// next module

function ViewerDocument(){
	var object=this;
	var var_id=0;

	var documentBar=new ViewerBar();

	var docData={};
	var docTpl=new Ext.Template(
		'<table width=100% border=1 style="border-collapse: collapse;">'+
			'<tr><td width=30%>Проект:</td><td>{pr_name}</td></tr>'+
			'<tr><td>Применение:</td><td>{p_name}</td></tr>'+
			'<tr><td>Наименование:</td><td>{name}</td></tr>'+
			'<tr><td>Тип документа:</td><td>{t_name}</td></tr>'+
			'<tr><td>Обозначение:</td><td>{klgi}</td></tr>'+
			'<tr><td>Инв. номер:</td><td>{inv}</td></tr>'+
			'<tr><td>Отдел/Цех:</td><td>{gname}</td></tr>'+
			'<tr><td>Разработчик:</td><td>{aname}</td></tr>'+
			'<tr><td>Формат:</td><td>{f_name}</td></tr>'+
			'<tr><td>Кол-во листов:</td><td>{sheets}</td></tr>'+
			'<tr><td>Поступление:</td><td>{d_add}</td></tr>'+
			'<tr><td>Версия:</td><td>{ch_number}</td></tr>'+
			'<tr><td>Изменён:</td><td>{d_edit}</td></tr>'+
			'<tr><td colspan=2>Описание:</td></tr>'+
			'<tr><td colspan=2>{desc}</td></tr>'+
		'</table>'
	);

	var docRegion = new Ext.Panel({
		title: 'Информация о документе',
		autoScroll:true,
		html:'132'
	});
	
	var menu=function() {
		var op_id=0;
		var menuView=new Ext.menu.Menu({
			items: [{
				itemId:'view',
				text:'Посмотреть файл',
				handler:function(){
					CoreAJAX({
						url:'/file/getAllFile/',
						param:{'op_id':op_id},
						succ: function(res){
							if ( res.file ){
								var win = window.open(
									"/media/out/"+res.file,
									"",
									"width:27cm,status=no"
								);
								win.focus();
							}
						}
					});
				}
			}]
		});
		
		var menuClose=new Ext.menu.Menu({
			items: [{
				itemId:'view',
				text:'Просмотр - запрещён',
			}]
		});
		
		this.view=function(id,XY){
			var menu;
			if (!id) menu = menuClose;
			else{
				op_id = id;
				menu = menuView;
			}
			
			menu.showAt(XY);
		}
	}
	
	var changeMenu=new menu();
	
	var changeGrid = new LibGrid({
		title: 'История Изменений',
		url: "aj_getHistoryObjectJSON",
		fields: ['id','op_name','date','ch_id','ch_number','reg_number'],
		column: [
			{name: "Дата", field: 'date', width: 20},
			{name: "Операция", field: 'op_name', width: 35},
			{name: "Извещение", field: 'ch_number', width: 30},
			{name: "Рег.№", field: 'reg_number', width: 15},
		],
		rowcontext: function(row, evtObj){
			changeMenu.view(row.get('id'), evtObj.getXY());
		},
	});
	
	var changeRegion = changeGrid.grid;
	
	var downloadGrid = new LibGrid({
		title: 'История Запросов',
		url: "aj_getHistoryRequestObjectJSON",
		fields: ['id','date','number','zakaz','user'],
		column: [
			{name: "Дата", field: 'date'},
			{name: "Наряд №", field: 'number'},
			{name: "Заказ №", field: 'zakaz'},
			{name: "Затребовал", field: 'user'},
		],
	});
	
	var downloadRegion = downloadGrid.grid;

//Объект extJS для рендера
	var containerBody=new Ext.Panel({
		hideBorders:true,
		layout:'accordion',
		layoutConfig: {
			hideCollapseTool: true,
			titleCollapse: true,
		},
		tbar: documentBar.bar,
		items: [
			docRegion,
			changeRegion,
			downloadRegion
		]
	});
	this.container=new Ext.Container({
		hideBorders:true,
		layout:'fit',
		items: [containerBody],
	});
	documentBar.view(0);
	
//Функция просмотра документа
	var mask=function(){ CoreMask(object.container.el); }
	var unmask=function(){ object.container.doLayout(); CoreUnMask(object.container.el); }
	this.view=function(id){
		var_id=id;
		documentBar.view(0);
		mask();
		containerBody.hide();
		if (var_id != 0){
			CoreAJAX({
				url:'aj_getDocumentInformation',
				param:{'node_id':var_id},
				succ: viewUpd
			});
		}
		else unmask();
	}
//Функция обновления, т.к. произошли изменения
	this.refresh=function (n_id){
		//var id=0;
		//if(var_id==n_id){
			var_id=0; id=n_id;
		//}
		if(id) object.view(id);
	}
//	//Обработка загруженных данных
	var viewUpd=function(result){
		if(result.info && var_id==result.info.node_id){
			docTpl.overwrite(docRegion.getLayoutTarget(),result.info);
			changeGrid.store.loadData(result.history);
			downloadGrid.store.loadData(result.request);
			documentBar.view(var_id);
			containerBody.show(false);
			unmask();
		}
		else unmask();
	}
}
// next module

function ViewerFolder(){
	var object=this;
	var var_id=0, var_type='';
	
	var folderBar=new ViewerBar();

//Информации об объекте
//	//Массив данных, доступен извне
	var infoData={id:'ID',name:'Имя',klgi:'Обозначение',t_id:'ID Типа',t_name:'Название типа',desc:'Описание'}
//	//Темплей вывода
	var infoTpl=new Ext.Template(
		'<table width=100% border=1 style="border-collapse: collapse;"><tr>'+
			//'<td width=20%><b>ID объекта:</b></td><td width=35%>{id}</td>'+
			'<td><b>Название:</b></td><td colspan=3>{name}</td>'+
		'</tr><tr>'+
			'<td width=20%><b>Обозначение:</b></td><td width=35%>{klgi}</td>'+
			'<td><b>Тип:</b></td><td>{t_name}</td>'+
		'</tr><tr>'+
			'<td><b>Описание:</b></td><td colspan=3>{desc}</td>'+
		'</tr></table>'
	);
//	//Объект панели
	var infoRegion=new Ext.Panel({
		title: 'Общая информация',
		autoHeight:true,
		html:'',
	});
	
//Информация о экземпляре(документе)	
//	//Массив данных, доступен извне
	var docData={id:'ID',pr_id:'ID родителя',pr_name:'Имя родителя',d_add:'Добавлен',d_edit:'Изменён',inv:'Инв. номер'}
//	//Темплей вывода
	var docTpl=new Ext.Template(
		'<table width=100% border=1 style="border-collapse: collapse;"><tr>'+
			'<td width=20%><b>Проект:</b></td><td width=35%>{pr_name}</td>'+
			'<td width=20%><b>Поступление:</b></td><td>{d_add}</td>'+		
		'</tr><tr>'+
			'<td><b>Применяемость:</b></td><td>{p_name}</td>'+
			'<td><b>Изменён:</b></td><td>{d_edit}</td>'+
//		'</tr><tr>'+
//			'<td><b>Инв. номер:</b></td><td>{inv}</td>'+
//			'<td><b>Скачать:</b></td><td><a href="#" onclick="index.form_download.view({id});">link</a></td>'+
		'</tr></table>'
	);
//	//Объект панели
	var docRegion=new Ext.Panel({
		title: 'Дополнительная информация',
		autoHeight:true,
		html:'',
	});
	
	
	var loader=function(){ viewFin(); }
//Блок с таблицей экземпляров объекта
//	//Таблица экзепляров
	var nodeGrid=new LibGrid({
		title: 'Использование в проектах',
		url: "aj_getContentNodes",
		fields: ['id','pr_id','pr_name','p_id','p_name','inv','date','havechildren','isproject'],
		column: [
			{name: "Проект", field: 'pr_name', width: 30},
			{name: "Применение", field: 'p_name', width: 30},
//			{name: "Инв. номер", field: 'inv'},
			{name: "Дата", field: 'date'},
		],
		loader: loader,
		rowclick: function(row){
			object.view(row.get('id'),'Node');
		},
		rowcontext: function(row, evtObj){
			index.viewer_menu.view(row.get('id'),row.get('isproject'),row.get('havechildren'),evtObj.getXY());
		},
	});
//	//Объект таблицы экзепляров
	var nodeRegion=nodeGrid.grid;
	
//Блок с таблицей детей экзепляра
//	//Таблица детей экзепляра
	var childGrid=new LibGrid({
		title: 'Документы',
		url: "aj_getContentChildren",
		fields: ['id','klgi','name','t_name','ch_number'],
		column: [
			{name: "Обозначение", field: 'klgi', width: 3},
			{name: "Название", field: 'name', width: 5},
			{name: "Тип документа", field: 't_name', width: 3},
			{name: "Версия", field: 'ch_number', width: 1},
		],
		loader: loader,
		rowclick: function(row){
			index.viewer_document.view(row.get('id'));
		},
		rowcontext: function(row, evtObj){
			index.viewer_menu.view(row.get('id'),0,0,evtObj.getXY());
		},
	});
//	//Панель таблицы детей
	var childRegion=childGrid.grid;
	
//	//Контейнер для таблиц
	var gridRegion=new Ext.Container({
		flex:1,
		layout:'anchor',
		hideBorders:true,
		defaults: { anchor:'100% 100%' },
		items:[
			nodeRegion,
			childRegion
		],
	});

//Объект extJS для рендера
	var containerBody=new Ext.FormPanel({
		hideBorders:true,
		layout:'vbox',
		layoutConfig: {
			align : 'stretch',
		},
		tbar: folderBar.bar,
		items: [
			infoRegion,
			docRegion,
			gridRegion,
		],
	});
	this.container=new Ext.Container({
		hideBorders:true,
		layout:'fit',
		items: [containerBody],
	});
	folderBar.view(0);
	
//Функция просмотра объекта/экземпляра
	var mask=function(){  CoreMask(object.container.el); }
	var unmask=function(){ object.container.doLayout(); CoreUnMask(object.container.el); }
	this.view=function (id,type){
		if((!type) || (type!='Object')) type='Node';
		if(!id) id=0;
	
		if(!(var_id==id && var_type==type)){
			index.viewer_document.view(0);
			if(type=='Node') index.viewer_navigation.tree_view(id,true);

			var_id=id; var_type=type;
			
			mask();
			containerBody.hide();
			folderBar.view(0);
			docRegion.hide();
			nodeRegion.hide();
			childRegion.hide();
			
			getNodeInfo(id,viewUpd,unmask,type);
		}
	}
//Функция обновления, т.к. произошли изменения
	this.refresh=function (n_id,p_id){
		var id=0,type='Node';
		if(var_id==n_id){
			var_id=0; id=n_id;
			if(var_type=='Object') type='Object';
		}
		else if(var_id==p_id){
			var_id=0; id=p_id;
			if(var_type=='Object') type='Object';
		}
		if(id) object.view(id,type);
	}
//	//Обработка загруженных данных
	var viewUpd=function(result){
		if(((var_type=='Object') && (var_id==result.obj.id)) || ((var_type=='Node') && (var_id==result.node.id))){
			infoData=result.obj;
			docData=result.node;
				
			infoTpl.overwrite(infoRegion.getLayoutTarget(),infoData);
				
			if(var_type=='Object'){
				nodeRegion.show(false);
				nodeGrid.load({'id': var_id});
			}
			else{
				folderBar.view(docData.id, docData.isproject, docData.havechildren);
				
				docRegion.show(false);
				docTpl.overwrite(docRegion.getLayoutTarget(),docData);
				if(docData.havechildren){
					childRegion.show(false);
					childGrid.load({'node': var_id});
				}
				else viewFin();
			}
		}
	}
//	//Завершающая стадия обработка
	var viewFin=function(){
		containerBody.show(false);
		unmask();
	}
}
// next module

function ViewerNavigation(){
//Поиск по КЛГИ
	var klgiGrid=new LibGrid({
		title: 'Поиск',
		url: "aj_getGridNavigationJSON",
		fields: ['id','klgi','text'],
		page: index.viewer_config.navigation_page,
		search: true,
		column: [
			{name: "Обозначение", field: 'klgi', width: 1},
			{name: "Название", field: 'text', width: 2},
		],
		rowclick: function(row){
			index.viewer_folder.view(row.get('id'),'Object');
		},
	});
	var klgiTab=klgiGrid.grid;
		
//Дерево
	var treeTab=new Ext.tree.TreePanel({
		title: 'Структура по проектам',
		autoScroll : true,
		listeners: {
			click: function(node){
				index.viewer_folder.view(node.id,'Node');
			},
			contextmenu: function(node,evtObj){
				evtObj.stopEvent();
				index.viewer_menu.view(
					node.attributes['id'],
					node.attributes['isproject'],
					node.attributes['havechildren'],
					evtObj.getXY()
				);
			},
		},
		loader : new Ext.tree.TreeLoader({
			url: "aj_getTreeChildren",
			clearOnLoad: true,
			preloadChildren: true
		}),
		root : { text : 'Проекты', id : '1', expanded : true },
		animate: false,
	});

//Объект extJS для рендера
	this.container=new Ext.TabPanel({
		activeTab: 1,
		defaults:{autoScroll: true},
		items:[
			klgiTab,
			treeTab,
		]
	});

//Открытие строк в дереве, type - дерево не менялось
	this.tree_view=function(id,type){
		var node=treeTab.getNodeById(id);
		if(node){
			if((!type) && (node.expanded)) treeTab.getLoader().load(node);
			node.expand();
		}
	}
}
// next module

//Проверка критических ошибок
function Check(){
	var object=this;
	var interval=1000*5;
	
	var checkers={
		warning: new CheckWarning(),
		critical: new CheckCritical(),
		auth: new CheckAuth()
	};
	
	
	var check=function (result){
		for( c in checkers ){
			checkers[c].check(result[c]);
		}
	}
	
	var touch=function (){
		CoreAJAX({ url: 'aj_check', param: {}, succ: check });
	}
	
	Ext.TaskMgr.start({run: touch, interval: interval});
	touch();
}
// next module

//Локализация
Ext.MessageBox.buttonText={
	cancel: "Отмена",
	no: "Нет",
	ok: "ОК",
	yes: "Да",
}

if(!index) var index={};

//---------------------------------- SRATR of Debug function -------------------------------
//Проверка
function a(){
	alert(true);
}

//Просмотр JavaScript объекта
function CoreViewObject(obj){
	var msg='';
	for(val in obj){
		msg+=val+':'+obj[val]+"\r\n";
	}
	Ext.MessageBox.alert('Ошибка:', msg);
}

var core=this;
//Наличие библиотек
function CoreIssetLib(list){
	for(name in list){
		if((!core[name])){
			alert('None '+name);
			return false;
		}
	}
	return true;
}
//---------------------------------- END of Debug function -------------------------------

function CoreMask(el){ el.mask("Загрузка данных", "x-mask-loading"); }
function CoreUnMask(el){ el.unmask(); }

//Склеивание ассоциативных массивов
function CoreMergeArray(){
	var i, obj, row, res={};
	for(i=0; obj=arguments[i]; i++){
		for (row in obj){
			res[row]=obj[row];
		}
	}
	return res;
}

//Унифицированный AJAX запрос
function CoreAJAX(conf){
	Ext.Ajax.request({
		url: conf.url,
		params: conf.param,
		success: function(result) {
			index.offline.succ();
			if(conf.succ) conf.succ(Ext.util.JSON.decode(result.responseText));
		},
		failure: function() {
			index.offline.fail();
			if(conf.fail) conf.fail();
		},
	});
}

function CoreAJAXResponser(conf){
	conf.succ=conf.fail=function(res){
		if(res){
			if(res.success) index.worklog.add(res.msg, 'ok');
			else Ext.MessageBox.alert('Ошибка:', 'Ошибка сервера!');
		}
	}
	CoreAJAX(conf);
}

//AJAX запрос получения инфо о объекте или ноде
function getNodeInfo(id,succ,fail,type){
	if(!type) type='Node';
	CoreAJAX({url:'aj_get'+type+'InformationJSON',param:{'id':id},succ:succ,fail:fail});
}

//Подтверждение одно-кликовых действий
function CoreConfirm(msg, callback){
	Ext.Msg.confirm(
		'Внимание! Подтвердите операцию!',
		msg,
		function (res){
			if(res=='yes'){
				if(callback) callback();
			}
		}
	);
}

//Дополнительный ввод одного поля для завершения действия
function CorePrompt(msg, callback){
	Ext.Msg.prompt(
		'Внимание! Завершение операции!',
		msg,
		function (res, val){
			if(res=='ok'){
				if(callback) callback(val);
			}
		}
	);
}


// next module

//Группа выбора промежутка дат
function CoreLibDateGroup(){
	var object=this;
	
	var curr_date=new Date(), lweek_date=new Date(curr_date.getTime()-1000*24*60*60*7);
	
	var from=new Ext.form.DateField({
		name:'date_from',
		fieldLabel: 'От',
		format: 'd-m-Y',
		editable: false,
		listeners:{
			change: function(th,val){
				to.setMinValue(val);
			}
		}
	});
	
	this.fromValue=function(){
		var curr=from.getValue(), res=0;
		if(curr) res=parseInt(curr.getTime()/1000);
		return res;
	}
	
	var to=new Ext.form.DateField({
		xtype: 'datefield',
		name:'date_to',
		fieldLabel: 'До',
		format: 'd-m-Y',
		editable: false,
		listeners:{
			change: function(th,val){
				from.setMaxValue(val);
			}
		}
	});
	
	this.toValue=function(){
		var curr=to.getValue(), res=0;
		if(curr) res=parseInt(curr.getTime()/1000);
		return res;
	}

	this.autoSet=function(){
		from.setValue(lweek_date);
		from.setMaxValue(curr_date);
		to.setValue(curr_date);
		to.setMinValue(lweek_date);
	}
	
	this.group=new Ext.form.FieldSet({
		xtype: 'fieldset',
		style: 'padding:0 5px; margin:0 5px;',
		title: 'Период',
		items:[
			from,
			to
		],
		listeners:{
			afterrender: object.autoSet
		}
	});
}
// next module

//Проверка связи
function Offline(){
	var object=this;
	var offline=false;
	
	var win=new Ext.Window({
		closable: false,
		draggable: false,
		resizable: false,
		plain: true,
		modal: true,
		autoHeight: true,
		width: 400,
		frame: true,
		title: 'Внимание',
		html: '<center><b><font color="red">Потеряна связь с сервером!<br>'+
			'Необходимо позвать администратора</font></b></center>'
	});
	
	var fail=function (){
		if(!offline){
			offline=true;
			index.worklog.add('Связь с сервером потеряна','sys');
			win.show();
		}
	}
	
	var succ=function (){
		if(offline){
			offline=false;
			index.worklog.add('Связь с сервером восстановлена','ok');
			win.hide();
		}
	}
	
	this.fail=fail;
	this.succ=succ;
}
// next module

if(!index) var index={};

index.store_report_types=new Ext.data.ArrayStore({
	data:[
		['change',	'Изменения',	'Составил',	'Номер извещения',	'Инвентарный номер'],
		['request',	'Запросы',	'Затребовал',	'Номер наряда',		'Заказ №'],
	],
	fields:['eng', "name", "autor", "number", "inv"],
});

index.store_hobject_types=new Ext.data.JsonStore({
	url: 'aj_getHealthObjectTypesJSON',
	fields: ['id', 'name'],
	autoLoad:true,
});

index.store_hobjects=new Ext.data.JsonStore({
	url: 'aj_getHealthObjectsJSON',
	fields: ['id', 'name'],
	autoLoad:true,
});

index.store_test_types=new Ext.data.JsonStore({
	url: 'aj_getTestTypesJSON',
	fields: ['id', 'name'],
	autoLoad:true,
})

index.store_projects=new Ext.data.JsonStore({
	url: 'aj_getProjects',
	fields	:["id", "name"],
	autoLoad: true,
});

index.store_document_types=new Ext.data.JsonStore({
	url: 'aj_DictDocumentTypeList',
	fields: ["id", "view", "name", "short"],
	autoLoad: true,
});

index.store_divisions=new Ext.data.JsonStore({
	url: 'aj_getDivisionsJSON',
	fields: ["id", "name"],
	autoLoad: true,
});

index.store_autors=new Ext.data.JsonStore({
	url: 'aj_DictAutorList',
	fields: ["id", "name"],
	autoLoad: true,
});

index.store_users=new Ext.data.JsonStore({
	url: 'aj_UserList',
	fields: ['id', 'name'],
	autoLoad:true,
});

index.store_formats=new Ext.data.JsonStore({
	url: 'aj_getFormatsJSON',
	fields: ["id", "name"],
	autoLoad: true,
});



// next module

function Change(){
	var object=this;
	var parent_id=0;
	var obj_id=0;
	
	var menuObject=new Ext.menu.Menu({
		items: [
			{itemId:'view_change',
			 text:'Показать Извещение',
			 handler:function(){ 
				var win = window.open(
					"/printer/change?id="+obj_id,
					"Извещение",
					"width:27cm,status=no"
				);
				win.focus()
			 },
			},
			{itemId:'view',
			 text:'Посмотреть файл',
			 handler:function(){
				CoreAJAX({
					url:'/file/getAllFile/',
					param:{'change_id':obj_id},
					succ: function(res){
						if ( res.file ){
							var win = window.open(
								"/media/out/"+res.file,
								"",
								"width:27cm,status=no"
							);
							win.focus();
						}
					}
				});
			 }
			}
		]
	});
	
	var enterGrid=new LibGrid({
		title: 'Журнал извещений',
		url: "aj_ChangeList",
		fields: ['number','id','group','autor','nodes'],
		page: index.change_config.result_page,
		search: true,
		column: [
			{name: "Обозначение", field: 'number', width: 1},
			{name: "Отдел/Цех", field: 'group', width: 1},
			{name: "Разработчик", field: 'autor', width: 1},
		],
		rowcontext: function(row, evtObj){
				obj_id = row.get('id');
				menuObject.showAt(evtObj.getXY());
		},
	});

	this.container=new Ext.Container({
		layout: 'hbox',
		layoutConfig: {
			align : 'stretch',
		},
		defaults: {flex:1, hideBorders:true},
		items:[
			enterGrid.grid
		],
	});
	
	this.refresh=enterGrid.load;
}
// next module

function Dict(){
	var object=this;
	
	var idField=new Ext.form.TextField({name:'id', hidden:true, emptyText:1});
	var nameField=new Ext.form.TextField({name:'name', fieldLabel:'Сотрудник', allowBlank:false, anchor:'95%'});
	
	var autorClear=function (){
		idField.setValue(1);
		nameField.setValue('');
		nameField.validate();
	}
	
	var autorSubmit=function(type){
		var req={};
		req.success=req.failure=autorResult;
		if(type=='Add') req.url='aj_DictAutorAdd';
		else if(type=='Edit') req.url='aj_DictAutorEdit';
		autorMask();
		autorForm.getForm().submit(req);
	}
	
	var autorResult=function(form, action){
		autorUnmask();
		if(action.result){
			if(action.result.success){
				if((action.result.success=="false") && (action.result.msg)){
					Ext.MessageBox.alert('Ошибка:', action.result.msg);
				}
				else{
					autorClear();
					index.store_autors.load();
					index.worklog.add(action.result.msg,'ok');
				}
				
			}
			else Ext.MessageBox.alert('Ошибка:', 'Ошибка сервера!');
		}
		else if((action.response) && (action.response.status=='404')) alert('404');
	}
	
	//Кнопка очистки формы
	var resetButton={ text: 'Сбросить', handler: autorClear }
	//Кнопка создания нового
	var addButton={ text:'Создать', handler: function(){ autorSubmit('Add'); } }
	//Кнопка сохранения изменения
	var editButton={ text:'Сохранить', handler: function(){ autorSubmit('Edit'); } }
	
	var autorForm = new Ext.form.FormPanel({
		title:'Редактирование',
		region:'south',
		frame:true,
		autoHeight:true,
		items:[
			idField,
			nameField
		],
		buttons:[resetButton,editButton,addButton]
	});
	
	var autorGrid = new Ext.grid.GridPanel({
		title:'Сотрудники',
		region:'center',
		store:index.store_autors,
		columns: [
			{header: "Сотрудник", sortable: true, dataIndex: 'name'},
		],
		
		viewConfig: {forceFit:true},
		autoScroll: true,
		columnLines: true,
		
		enableColumnHide: false,
		enableColumnMove: false,
		enableDragDrop: false,
		enableHdMenu: false,
		
		listeners: {
			rowclick: function(grid, rowIndex, evtObj){
				evtObj.stopEvent();
				var autor=index.store_autors.getAt(rowIndex);
				idField.setValue(autor.get('id'));
				nameField.setValue(autor.get('name'));
			}
		}
	});
	
	var autorMask=function(){ CoreMask(autorPanel.el); }
	var autorUnmask=function(){ CoreUnMask(autorPanel.el); }
	var autorPanel = new Ext.Panel({
		layout: 'border',
		items:[
			autorGrid, autorForm
		],
	});

	this.container=new Ext.Container({
		layout:'hbox',
		layoutConfig: {
			align : 'stretch',
		},
		defaults: {flex:1, hideBorders:true},
		items:[
			autorPanel,
		],
	});

}
// next module

function Files(){
	var object=this;
	var file_id=0;
	
	var menuNew=new Ext.menu.Menu({
		items: [
			{itemId:'nk_new',
			 text:'Заменить файл',
			 handler:function(){
				index.form_files_new.view(file_id);
			 },
			}
		]
	});
	
	var enterGrid=new LibGrid({
		title: 'Файлы в хранилище',
		url: "aj_FilesList",
		fields: ['id','hash','mime','info', 'info_count', 'f_date', 'f_change', 'f_operation', 'f_node'],
		page: index.files_config.result_page,
		search: true,
		expander: new Ext.XTemplate(
			'<tpl if="info_count">'+
			'<p><table border=1 width=100%>'+
			'<tr><td>Рег.номер</td><td>Дата</td><td>Операция</td><td>Извещение</td><td>Документ</td></tr>'+
			'<tpl for="info">'+
			'<tr><td>{reg_number}</td><td>{date}</td><td>{operation}</td><td>{change}</td><td>{node}</td></tr>'+
			'</tpl></table></p></tpl>'
		),
		column: [
			{name: "Дата", field: 'f_date', width: 1},
			{name: "Операция", field: 'f_operation', width: 2},
			{name: "Извещение", field: 'f_change', width: 2},
			{name: "Документ", field: 'f_node', width: 2},
			{name: "Тип", field: 'mime', width: 2},
		],
		rowcontext: function(row, evtObj){
			file_id=row.get('id');
			menuNew.showAt(evtObj.getXY());
		},
	});
	
	this.container=new Ext.Container({
		layout: 'fit',
		defaults: {flex:1, hideBorders:true},
		items: enterGrid.grid
	});
	
	this.refresh=enterGrid.load;
}
// next module

function Inv(){
	var object=this;
	var parent_id=0;
	
	var menuObject=new Ext.menu.Menu({
		items: [
			{itemId:'view_parent',
				text:'Посмотреть изделие',
				handler:function(){
					if (index.viewer_folder){
						index.monitor_panel.setActiveTab(0);
						index.viewer_folder.view(parent_id,'Object');
					}
				}
			}
		]
	});
	
	var enterGrid=new LibGrid({
		title: 'Журнал инвентарных номеров',
		url: "aj_InvNumberList",
		fields: ['id', 'number', 'type', 'name', 'object_id', 'parent_id', 'sign'],
		page: index.inv_config.result_page,
		search: true,
		column: [
			{name: "Номер", field: 'number', width: 1},
			{name: "Тип", field: 'name', width: 1},
			{name: "Обозначение", field: 'sign', width: 1},
		],
		rowcontext: function(row, evtObj){
			if( row.get('type') == 'object' ){
				parent_id = row.get('parent_id');
				menuObject.showAt(evtObj.getXY());
			}
		},
	});

	this.container=new Ext.Container({
		layout: 'hbox',
		layoutConfig: {
			align : 'stretch',
		},
		defaults: {flex:1, hideBorders:true},
		items:[
			enterGrid.grid
		],
	});
	
	this.refresh=enterGrid.load;
}
// next module

function Report(){
	var object=this;
	
	this.container=new Ext.Container({
		layout:'border',
		items:[
			index.report_search.region,
			index.report_result.region
		],
	});
}
// next module

function Syslog(){
	var object=this;
	
	this.container=new Ext.Container({
		layout:'border',
		items:[
			index.syslog_search.region,
			index.syslog_result.region
		],
	});
}
// next module

function TChange(){
	var object=this;
	var change_id=0;
	var project_id=0;
	
	var menuNK=new Ext.menu.Menu({
		items: [
			{itemId:'nk_apply',
				text:'Документы прошли НК -> Принять изменения',
				handler:function(){
					index.form_tchange_apply.view(change_id);
				}
			},
			{itemId:'add_change',
				text:'Изменить документы',
				handler:function(){ 
					index.form_files_change.view(change_id, project_id); 
				}
			},
		]
	});
	var menuCH=new Ext.menu.Menu({
		items: [
			{itemId:'add_change',
				text:'Изменить документы',
				handler:function(){ 
					index.form_files_change.view(change_id, project_id); 
				}
			},
		]
	});
	
	var enterGrid=new LibGrid({
		title: 'Извещения на проверке',
		url: "aj_TChangeList",
		fields: ['reg_number','number','change_id','gname','aname','waiting','nodes', 'project_id'],
		page: index.tchange_config.result_page,
		search: true,
		expander: new Ext.XTemplate(
			'<p><table border=1 width=100%>'+
			'<tr><td>Номер</td><td>Статус</td><td>Наименование</td><td>Обозначение</td></tr>'+
			'<tpl for="nodes">'+
			'<tr><td>{reg_number}</td><td>{status_desc}</td><td>{name}</td><td>{klgi}</td></tr>'+
			'</tpl></table></p>'
		),
		column: [
			{name: "Номер", field: 'reg_number', width: 1},
			{name: "Обозначение", field: 'number', width: 1},
			{name: "Статус", field: 'waiting', width: 1,
			 renderer: function (c){
				if(!c) return '<font color="green">Готово к принятию</font>';
				else return '<b><font color="red">Ожидает документы</font></b>';
			 }
			},
			{name: "Отдел/Цех", field: 'gname', width: 1},
			{name: "Разработчик", field: 'aname', width: 1},
		],
		rowcontext: function(row, evtObj){
			if(!row.get('waiting')){
				change_id=row.get('change_id');
				project_id=row.get('project_id');
				menuNK.showAt(evtObj.getXY());
			}
			if(row.get('waiting')){
				change_id=row.get('change_id');
				project_id=row.get('project_id');
				menuCH.showAt(evtObj.getXY());
			}
		},
	});

	this.container=new Ext.Container({
		layout: 'hbox',
		layoutConfig: {
			align : 'stretch',
		},
		defaults: {flex:1, hideBorders:true},
		items:[
			enterGrid.grid
		],
	});
	
	this.refresh=enterGrid.load;
}
// next module

function Temp(){
	var object=this;
	
	var menu=function() {
		var node_id=0, node_klgi=0;
		
		var view_file={
			itemId:'view',
			text:'Посмотреть файл',
			handler:function(){
				CoreAJAX({
					url:'/file/getAllFile/',
					param:{'node_id':node_id},
					succ: function(res){
						if ( res.file ){
							var win = window.open(
								"/media/out/"+res.file,
								"",
								"width:27cm,status=no"
							);
							win.focus();
						}
					}
				});
			}
		}
		
		var nk_print=function(){
			CoreConfirm(
				'Вы уверены, что '+node_klgi+' передан в НК?',
				function (){
					CoreAJAXResponser({ url:'aj_TStoragePrint', param:{'node':node_id} });
					index.temp.refresh();
				}
			);
		}
		
		var nk_apply=function(){
			CoreConfirm(
				'Вы уверены, '+node_klgi+' прошёл НК?',
				function (){
					CoreAJAXResponser({ url:'aj_TStorageApply', param:{'node':node_id} });
					index.temp.refresh();
				}
			);
		}
		
		var nk_cancel=function(){
			CoreConfirm(
				'Вы уверены, '+node_klgi+' не прошёл НК?',
				function (){
					CoreAJAXResponser({ url:'aj_TStorageCancel', param:{'node':node_id} });
					index.temp.refresh();
				}
			);
		}
		
		var menuNK=new Ext.menu.Menu({
			items: [
				{itemId:'nk_apply',
				 text:'Пройден НК -> Поместить в архив',
				 handler:function(){
					index.form_temp_apply.view(node_id);
				 }
				},
				{itemId:'nk_cancel',
				 text:'Не пройден НК -> Аннулировать',
				 handler:nk_cancel,
				},
				view_file
			]
		});
		
                var menuNKChange=new Ext.menu.Menu({
			items: [
				{itemId:'nk_apply',
				 text:'Пройден НК -> Ожидать извещения',
				 handler:nk_apply
				},
				{itemId:'nk_cancel',
				 text:'Не пройден НК -> Аннулировать',
				 handler:nk_cancel,
				},
				view_file
			]
		});
		
		var menuPrint=new Ext.menu.Menu({
			items: [
				{itemId:'nk_print',
				 text:'Передан в НК',
				 handler:nk_print,
				},
				view_file
			]
		});
		
		var menuNew=new Ext.menu.Menu({
			items: [
				{itemId:'nk_new',
				 text:'Заменить файл',
				 handler:function(){
					index.form_temp_new.view(node_id);
				 },
				}
			]
		});
		
		this.view=function(type,id,klgi,XY){
			var menu;
			
			if(type && id && klgi){
				node_id=id;
				node_klgi=klgi;
				if(type=='o') menu=menuPrint;
				else if(type=='p') menu=menuNK;
				else if(type=='c') menu=menuNew;
				else if(type=='i') menu=menuNKChange;
				
				if(menu) menu.showAt(XY);
			}
		}
	}
	
	var enterMenu=new menu();
	
	var enterGrid=new LibGrid({
		title: 'Поступления',
		url: "aj_TStorageList",
		fields: ['reg_number','status','status_desc','node_id','pr_name','p_name','name','t_name',
			'klgi','gname','aname','f_name','sheets'],
		page: index.temp_config.result_page,
		search: true,
		column: [
			{name: "Номер", field: 'reg_number', width: 1},
			{name: "Статус", field: 'status_desc', width: 1},
			{name: "Проект", field: 'pr_name', width: 1},
			{name: "Применение", field: 'p_name', width: 1},
			{name: "Наименование", field: 'name', width: 1},
			{name: "Обозначение", field: 'klgi', width: 1},
			{name: "Отдел/Цех", field: 'gname', width: 1},
			{name: "Разработчик", field: 'aname', width: 1},
			{name: "Формат", field: 'f_name', width: 1},
			{name: "Кол-во листов", field: 'sheets', width: 1}
		],
		rowcontext: function(row, evtObj){
			enterMenu.view(row.get('status'),row.get('node_id'),row.get('klgi'),evtObj.getXY());
		},
	});

	this.container=new Ext.Container({
		layout: 'hbox',
		layoutConfig: {
			align : 'stretch',
		},
		defaults: {flex:1, hideBorders:true},
		items:[
			enterGrid.grid
		],
	});
	
	this.refresh=enterGrid.load;
}
// next module

function Time(){
	var object=this;
	
	this.container=new Ext.Container({
		layout:'border',
		items:[
			index.time_search.region,
			index.time_result.region
		],
	});
}
// next module

function Viewer(){
	var object=this;
	
	this.container=new Ext.Container({
		layout: 'border',
		defaults: {split: true, hideBorders:true},
		items:[
			{title:'Навигация',
			 region:'west',
			 layout: 'fit',
			 animCollapse:false,
			 collapsible: true,
			 collapseMode: 'mini',
			 width: 300,
			 minWidth: 200,
			 maxWidth: 400,
			 items: index.viewer_navigation.container
			},
			{title:'Изделие',
			 region:'center',
			 layout: 'fit',
			 items: index.viewer_folder.container,
			},
			{title:'Документ',
			 width:400,
			 region:'east',
			 layout: 'fit',
			 items: index.viewer_document.container
			},
		],
	});

}
// next module

function Worklog(){
	var store=new Ext.data.ArrayStore({
		idIndex: 0,
		fields: ['time', 'msg']
	});
	
	var add=function(msg,type){
		var color, m;
		if(!type) color='black';
		else if(type=='ok') color='green';
		else if(type=='sys') color='red';
		else color='yellow';
		
		m = new store.recordType({
			time: (new Date()).format('G:i:s'),
			msg: '<font color="'+color+'">'+msg+'</font>'
		});
		store.insert(0, m);
	}
	
	var list=new Ext.list.ListView({
		store: store,
		autoScroll: true,
		emptyText: 'Сообщений не было',
		columns: [
			{header: "Время", width:.10, dataIndex: 'time'},
			{header: "Сообщение", dataIndex: 'msg'},
		]
	});
	
	this.container=list;
	this.add=add;
	
	add('Начало работы','ok');
}
// next module

Ext.EventManager.on("body", 'selectstart', function(e){ e.stopEvent(); });
Ext.EventManager.on("body", 'contextmenu', function(e){ e.stopEvent(); });

index.form_login=new FormLogin();
index.form_download=new FormDownload();
index.form_file=new FormFile();
index.form_project=new FormProject();
index.form_folder=new FormFolder();
index.form_document=new FormDocument();
index.form_copy=new FormCopy();
index.form_move=new FormMove();
index.form_temp_new=new FormTStorageFile();
index.form_temp_apply=new FormTStorageApply();
index.form_tchange_apply=new FormTChangeApply();
index.form_files_new=new FormFilesFile();
index.form_files_change=new FormFilesChange();

index.worklog=new Worklog();

index.check=new Check();
index.offline=new Offline();
index.monitor_items=[]

if(archive_groups.viewer){
	index.viewer_config={'navigation_page':20}
	index.viewer_menu=new ViewerMenu();
	index.viewer_navigation=new ViewerNavigation();
	index.viewer_folder=new ViewerFolder();
	index.viewer_document=new ViewerDocument();
	index.viewer=new Viewer();
	index.monitor_items.push({title: 'Архив', items: index.viewer.container });
}

if(archive_groups.temp){
	index.temp_config={'result_page':20}
	index.temp=new Temp();
	index.monitor_items.push({title: 'Временное хранилище', items: index.temp.container });
}

if(archive_groups.tchange){
	index.tchange_config={'result_page':20}
	index.tchange=new TChange();
	index.monitor_items.push({title: 'Утверждение извещений', items: index.tchange.container });
}

if(archive_groups.tchange){
	index.change_config={'result_page':20}
	index.change=new Change();
	index.monitor_items.push({title: 'Журнал Извещений', items: index.change.container });
}

if(archive_groups.inv){
	index.inv_config={'result_page':20}
	index.inv=new Inv();
	index.monitor_items.push({title: 'Журнал Инв.Номеров', items: index.inv.container });
}


if(archive_groups.report){
	index.report_config={'result_page':20}
	index.report_menu=new ReportMenu();
	index.report_search=new ReportSearch();
	index.report_result=new ReportResult();
	index.report=new Report();
	index.monitor_items.push({title: 'Отчёты', items: index.report.container });
}

if(archive_groups.syslog){
	index.syslog_config={'result_page':20}
	index.syslog_search=new SyslogSearch();
	index.syslog_result=new SyslogResult();
	index.syslog=new Syslog();
	index.monitor_items.push({title: 'Системный Журнал', items: index.syslog.container });
}

if(archive_groups.dict){
	index.dict=new Dict();
	index.monitor_items.push({title: 'Справочники', items: index.dict.container });
}

if(archive_groups.time){
	index.time_config={'result_page':20}
	index.time_search=new TimeSearch();
	index.time_result=new TimeResult();
	index.time=new Time();
	index.monitor_items.push({title: 'Сессии пользователей', items: index.time.container });
}

if(archive_groups.files){
	index.files_config={'result_page':20}
	index.files=new Files();
	index.monitor_items.push({title: 'Работа с файлами', items: index.files.container });
}

if(archive_groups.admin){
	index.monitor_items.push({title: 'Интерфейс администратора', html: '<iframe width=100% height=100% src="/admin">' });
}

index.monitor_panel=new Ext.TabPanel({
	region: 'center',
	viewConfig: {forceFit:true},
	defaults:{layout: 'fit'},
	frame: false,
	activeTab: 0,
	items: index.monitor_items,
});
	
index.monitor=new Ext.Viewport({
	layout: 'border',
	title: "Архив документов",
	defaults: { split: true },
	items: [
		index.monitor_panel,
		{title: "Сообщения",
		 region: 'south',
		 height: 150,
		 minHeight: 100,
		 maxHeight: 200,
		 layout: 'fit',
		 items: index.worklog.container
		},
		{xtype:'container',
		 region	: 'north',
		 height: 40,
		 style:'background-image:url("./media/back.png"); background-repeat:repeat;',
		 html:	'<table width=100%><tr>'+
		 	'<td><img src="./media/logo_name.png" style="padding-top:5px;padding-left:15px;"></td>'+
		 	'<td align="right">'+Ext.get('user-tools').dom.innerHTML+'</td>'+
		 	'</tr></table>',
		 split: false,
		}
	]
});

if(archive_groups.viewer){
	index.viewer_folder.view(1,'Node');
	index.viewer_document.view(1);
}
});

