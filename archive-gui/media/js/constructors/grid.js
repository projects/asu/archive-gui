//Генератор обработчика вывода имени поля запроса
function LibGenParamName(par){
	if(par.param_rename){
		var names=par.names, param=par.param_rename;
		return function (req){ return names[req[param]]; }
	}
	else if(par.name){
		var name=par.name;
		return function (req){ return name; }
	}
	else{
		return function (req){ return 'None Name'; }
	}
}

//Генератор обработчика вывода значения поля запроса
function LibGenParamValue(par){
	var param=par.param;
	if(par.type=='id'){
		var store=par.store;
		if(par.store_field){
			var s_field=par.store_field;
			return function(req){
				var s_id=store.findExact(s_field,req[param]);
				return store.getAt(s_id).data.name;
			}
		}
		else{
			return function (req){ return store.getById(req[param]).data.name; }
		}
	}
	else if(par.type=='text'){
		return function (req){ return req[param]; }
	}
	else if(par.type=='date'){
		return function (req){ return new Date(req[param]*1000).format('d-m-Y'); }
	}
	else if(par.type=='date_a'){
		return function (req){ return new Date(req[param]*1000).format('d-m-Y H:i:s'); }
	}
	else{
		return function (req){ return 'None Value'; }
	}
}

//Генератор обработчика вывода имени колонки в зависимости от запроса
function LibGenParamCol(col){
	var name=col.name, names=col.names;
	return function (val){
		if(val) name=names[val];
		return name;
	}
}
/*	conf={
		title:заголовок панельки,
		url:откуда брать рез-ты,
		fields:[поля],
		+-search:Использовать-ли встроенную строку поиска(если да - нет метода load, за ненадобностью),
		+-page:кол-во на страницу(false-не использовать),
		+-expander:темплей для экспандера(false-не использовать),
		column:[колонки
			{
				name:заголовок
				field:поле,
				+-width:ширина
				+-renderer:функция рендера
			}
		],
		loader: func(object,res,req_all) Функция, работающая при загрузке
		rowclick: func(row, event) Событие RowClick,
		rowcontext: func(row, event) Событие RowClick,
	}
*/

//Генератор таблиц
function LibGrid(conf){
	var object=this;

//	//Добавление Пагинации к запросу
	var paging=function(obj){
		obj.limit=conf.page;
		obj.start=0;
		return obj;
	}

//	//Хранилище
	this.store=new Ext.data.JsonStore({
		url: conf.url,
		root: "records",
		totalProperty: "total",
		fields: conf.fields,
	});
	if(conf.page) object.store.baseParams=paging({});

//	//Доп.информация о строке
	var grid_expander=false, columns=[];
	if(conf.expander){
		grid_expander=new Ext.grid.RowExpander({tpl: conf.expander});
		columns=[grid_expander];
	}

//	//Колонки
	for(i=0;i<conf.column.length;i++){
		var col=conf.column[i];
		var res={header:col.name, dataIndex:col.field, sortable: true};
		if(col.width) res.width=col.width;
		if(col.renderer) res.renderer=col.renderer;
		columns.push(res);
	}
	this.column=new Ext.grid.ColumnModel(columns);

//	//Строка поиска
	var grid_tbar=false;
	if(conf.search) {
		grid_tbar=[
			'Поиск: ',
			' ',
			new Ext.ux.form.SearchField({store: object.store})
		];
	}

//	//Постраничная навигация
	var grid_bbar=false;
	if(conf.page){
		grid_bbar=new Ext.PagingToolbar({
			pageSize: conf.page,
			store: object.store,
			displayInfo: true,
			displayMsg: '{0} - {1} из {2}',
			emptyMsg: "Нет объектов",
		});
	}

//	//Событие-Левый клик на записи
	var grid_e_rowclick=function(grid, rowIndex, evtObj){
		evtObj.stopEvent();
		if(conf.rowclick) conf.rowclick(object.store.getAt(rowIndex), evtObj);
	};

//	//Событие-Правый клик на записи
	var grid_e_rowcontext=function(grid, rowIndex, evtObj){
		evtObj.stopEvent();
		if(conf.rowcontext) conf.rowcontext(object.store.getAt(rowIndex), evtObj);
	}

//	//Таблица
	this.grid=new Ext.grid.GridPanel ({
		title: conf.title,
		store: object.store,
		colModel: object.column,
		
		viewConfig: {forceFit:true},
		autoScroll: true,
		columnLines: true,
		
		enableColumnHide: false,
		enableColumnMove: false,
		enableDragDrop: false,
		enableHdMenu: false,
		
		plugins: grid_expander,
		tbar: grid_tbar,
		bbar: grid_bbar,
		
		listeners: {
			rowclick: grid_e_rowclick,
			rowcontextmenu: grid_e_rowcontext,
		},
	});

//	//Функция загрузки
	var mask=function(){ CoreMask(object.grid.el); }
	var unmask=function(){ object.grid.doLayout(); CoreUnMask(object.grid.el); }
	this.load=function(param){
		mask();
		if(!param) param={};
		if(conf.page) param=paging(param);
		object.store.baseParams=param;
		object.store.load({callback:load_fin});
	}

//	//Завершающая стадия обработка
	var load_fin=function(res,req_all){
		if(conf.loader) conf.loader(object,res,req_all);
		unmask();
	}
}

/*	conf={
		title:заголовок таблицы,
		url:источник,
		fields:[поля],
		+-page:кол-во на страницу(false-не использовать),
		+-expander:темплей для экспандера(false-не использовать),
		column:[колонки
			{
				name:заголовок
				field:поле,
				+-width:ширина
				+-renderer:функция рендера

				+-param_rename:имя параметра, в зависимости от значения которого вычисляется заголовок колонки
				+-names:{значение:результат}
				+-param_hide:если это поле задано в запросе, то скрывается колонка
			}
		],
		loader: func(object,res,req_all) Событие Загрузка
		rowclick: func(row, event) Событие RowClick,
		rowcontext: func(row, event) Событие RowClick,

		request:[расшифровка запроса
			{
				name:описание параметра,
				param:имя параметра,
				type:
					text - просто текст,
					date - дата ДД/ММ/ГГГГ,
					date_a - date+время,
					id - запись в хранилище store, по полю id либо, если задано, по полю store_field
				+-store:хранилище откуда взять имя
				+-store_field:поле в хранилище

				+-param_rename:	имя параметра, в зависимости от значения которого вычисляется надпись
				+-names:{значение:результат}

			}
		],
	}
*/

//Конструктор таблицы - результата поиска
function ConstructorGridResult(conf){
	var object=this;
	var gen_name=LibGenParamName, gen_value=LibGenParamValue, gen_col=LibGenParamCol;
	var column_id=0;

//	//Подготовка конфигурации
	var grid_conf={
		title: conf.title+' ( ):',
		url: conf.url,
		fields: conf.fields,
		search: false,
		column: [],
	};
	if(conf.page) grid_conf.page=conf.page;
	if(conf.expander){
		column_id=1;
		grid_conf.expander=conf.expander;
	}
	if(conf.rowclick) grid_conf.rowclick=conf.rowclick;
	if(conf.rowcontext) grid_conf.rowcontext=conf.rowcontext;

//	//Присваивание полям - обработчиков, и переформирование колонок
	var params={};
	for(i=0;i<conf.request.length;i++){
		var par=conf.request[i];
		params[par.param]={'name':gen_name(par),'value':gen_value(par),'rename':[],'hide':[]};
	}
	for(i=0;i<conf.column.length;i++,column_id++){
		var col=conf.column[i];
		if(col.param_rename)
			params[col.param_rename].rename.push({'cid':column_id,'rename':gen_col(col)});
		if(col.param_hide)
			params[col.param_hide].hide.push({'cid':column_id});
		
		var conf_col={name:col.name,field:col.field};
		if(col.width) conf_col.width=col.width;
		if(col.renderer) conf_col.renderer=col.renderer;
		grid_conf.column.push(conf_col);
	}

//	//Обработчик загрузки
	var loader=function(grid_object,res,req_all){
		if(res.length==0) Ext.MessageBox.alert('Результат поиска:', 'По вашему запросу данных не найдено');
		
		var title=[];
		title.push(conf.title+' ( ');
		if(req_all && req_all.params){
			var req=req_all.params;
			for(par_name in params){
				var par=params[par_name], c_hide=false, c_rename=false;
				if(req[par_name]){
					c_hide=true, c_rename=req[par_name];
					title.push(par.name(req)+': '+par.value(req)+'; ');
				}
				
				for(i=0;i<par.rename.length;i++)
					grid_object.column.setColumnHeader(par.rename[i].cid, par.rename[i].rename(c_rename));
				for(i=0;i<par.hide.length;i++)
					grid_object.column.setHidden(par.hide[i].cid, c_hide);
			}
		}
		title.push('):');
		grid_object.grid.setTitle(title.join(''));
	}

//	//Туннель для загрузчика
	if(!conf.loader) grid_conf.loader=loader;
	else{
		grid_conf.loader=function (object,res,req_all){
			loader(object,res,req_all);
			conf.loader(object,res,req_all);
		}
	}

	return new LibGrid(grid_conf);
}