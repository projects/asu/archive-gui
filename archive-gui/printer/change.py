# -*- coding: utf-8 -*-
from django.http import HttpResponse
from django.template import Context, loader
from main.models import *
from main.function import changeChanges

def PrintChange (request):
	try:
		req_change = int(request.REQUEST['id'])
		change = Change.objects.get(id = req_change)
	except:
		return HttpResponse('ERROR')
	
	change_list = changeChanges(change)
	
	t = loader.get_template('print/change.tpl')
	c = Context({
		'date': change.date.strftime("%d-%m-%Y"),
		'inv': change.invnumber,
		'number': change.number,
		'sheets': change.sheets,
		'autor': change.user,
		'change_list': change_list
	})
	
	return HttpResponse(t.render(c), mimetype="application/xhtml+xml")