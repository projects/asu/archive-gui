# -*- coding: utf-8 -*-
from django.http import HttpResponse,HttpResponseRedirect
from django.template import Context, loader
from main.models import *
from main.function import *

def PrintOpis (request):
	try:
		req_node = int(request.REQUEST['id'])
		node = Node.objects.get(id = req_node)
	except:
		return HttpResponse('ERROR')
	
	opis_list = new_getAllTree(node, False)
        res_list = []
	for i in range(0,len(opis_list)):
		opis_node = opis_list[i]['node'].object
		res_list.append({
			'num':(i+1),
			'format':opis_node.format,
			'klgi':opis_node.klgi,
			'name':opis_node.name,
			'sheets':opis_node.sheets
		})
	
	t = loader.get_template('print/opis.tpl')
	c = Context({
		'opis_list': res_list
	})
	
	return HttpResponse(t.render(c), mimetype="application/xhtml+xml")