# -*- coding: utf-8 -*-
from django.http import HttpResponse
from django.template import Context, loader
from main.models import *

def PrintRequest (request):
	try:
		req_request = int(request.REQUEST['id'])
		request = Request.objects.get( id = req_request )
	except:
		return HttpResponse('ERROR')
	
	request_list = RequestItem.objects.filter( request = request )
	res_list = []
	i = 0
	
	for it in request_list:
		i += 1
		print i
		node = it.node.object
		res_list.append({
			'num': i,
			'format': node.format,
			'klgi': node.klgi,
			'name': node.name,
			'sheets': node.sheets
		})
	
	t = loader.get_template('print/request.tpl')
	c = Context({
		'date': request.date.strftime("%d-%m-%Y"),
		'number': request.number,
		'numberzakaz': request.numberzakaz,
		'division': request.division.name,
		'user': request.user,
		'base': request.base,
		'msg': request.comments,
		'request_list': res_list
	})
	
	return HttpResponse(t.render(c), mimetype="application/xhtml+xml")