# -*- coding: utf-8 -*-
from django.conf.urls.defaults import *
from printer import change, request, opis

urlpatterns = patterns('',
	(r'^change', change.PrintChange),
	(r'^request', request.PrintRequest),
	(r'^opis', opis.PrintOpis),
)

