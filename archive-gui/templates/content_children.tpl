{"records": [
{% for item in list %}
{
	'id': '{{ item.id }}',
	'klgi': '{{ item.object.klgi }}',
	'text': '{{ item.object.name }}',
	'name': '{{ item.object.name }}',
	't_name': '{{ item.object.type }}',
{% if item.lastoperation %}
	{% if item.lastoperation.change %}
	'ch_number': '{{ item.lastoperation.change.number}}',
	'ch_data': '{{ item.lastoperation.change.date}}',
	{% else %}
	'ch_number': '-',
	{% endif %}
{% else %}
	'ch_number': '<font color=red><b>Нет</b></font>',
{% endif %}
},
{% endfor %}
]}
