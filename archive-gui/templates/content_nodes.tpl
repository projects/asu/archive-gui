{"records": [
{% for item in list %}
{
	"id": "{{ item.id }}",
	"pr_id": "ID проекта",
	"pr_name": "{{ item.project }}",
	"p_id": "ID родителя",
	"p_name": "{% if item.parent.object.klgi %}{{ item.parent.object.klgi }} {% endif %}{{ item.parent }}",
	"inv": "Инвентарный номер",
	"date": "20/10/2001",
{% if item.object.type.kind.canhavechildren %}
	'havechildren' : true,
{% else %}
	'havechildren' : false,
{% endif %}

{% if item.id == item.project.id %}
	'isproject' : true,
{% else %}
	'isproject' : false,
{% endif %}
},
{% endfor %}
]
}