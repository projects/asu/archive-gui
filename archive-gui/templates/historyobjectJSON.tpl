{records: [
{% for item in list %}
{
	'id': '{{ item.id }}',
	'op_name': '{{ item.operation.name }}',
	'date': '{{ item.date }}',
	'ch_id': '{{ item.change.id }}',
	'ch_number': '{{ item.change.number }}',
	'reg_number': '{{ item.reg_number }}',

},
{% endfor %}
]
}