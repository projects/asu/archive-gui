<html>
<link rel="stylesheet" type="text/css" href="media/ext/resources/css/ext-all.css">
<link rel="stylesheet" type="text/css" href="media/ext/examples/ux/fileuploadfield/css/fileuploadfield.css">
<script type="text/javascript" src="media/ext/adapter/ext/ext-base.js"></script>
<script type="text/javascript" src="media/ext/ext-all.js"></script>

<script type="text/javascript" src="media/ext/examples/ux/fileuploadfield/FileUploadField.js"></script>
<script type="text/javascript" src="media/ext/examples/ux/SearchField.js"></script>
<!-- <script type="text/javascript" src="media/ext/examples/ux/BufferView.js"></script>  -->
<!-- <script type="text/javascript" src="media/ext/examples/ux/TabCloseMenu.js"></script>  -->

<script type="text/javascript" src="media/js/Core.js"></script>
<script type="text/javascript" src="media/js/Form/Info/document.js"></script>
<script type="text/javascript" src="media/js/Form/Lib/document.js"></script>
<script type="text/javascript" src="media/js/Form/Lib/file.js"></script>
<script type="text/javascript" src="media/js/Form/Lib/info.js"></script>
<script type="text/javascript" src="media/js/Form/Lib/klgi.js"></script>
<script type="text/javascript" src="media/js/Form/Lib/node.js"></script>
<script type="text/javascript" src="media/js/Form/Lib/window.js"></script>
<script type="text/javascript" src="media/js/Form/document.js"></script>
<script type="text/javascript" src="media/js/Form/download.js"></script>
<script type="text/javascript" src="media/js/Form/file.js"></script>
<script type="text/javascript" src="media/js/Form/folder.js"></script>
<script type="text/javascript" src="media/js/Viewer/Lib/Bar.js"></script>
<script type="text/javascript" src="media/js/Viewer/Lib/Menu.js"></script>
<script type="text/javascript" src="media/js/Viewer/document.js"></script>
<script type="text/javascript" src="media/js/Viewer/folder.js"></script>
<script type="text/javascript" src="media/js/Viewer/navigation.js"></script>


<!-- <script type="text/javascript" src="media/ext/RowExpander.js"></script>  -->
<!-- <script type="text/javascript" src="media/ext/grid-plugins.js"></script>  -->

	
<script type="text/javascript">
var index={};
index.form_download=new FormDownload();
index.form_file=new FormFile();
index.form_project={'view':function(){ alert('none form_project'); }}
index.form_folder=new FormFolder();
index.form_document=new FormDocument();

index.viewer_bar=new ViewerBar();
index.viewer_menu=new ViewerMenu();
index.viewer_navigation=new ViewerNavigation();
index.viewer_folder=new ViewerFolder();
index.viewer_document={'view': function(id){ updateRightPanel(id); }};


/*
// var cont = new Ext.Panel({
// 	collapsible:true,
// 	autoLoad	: 'contest.html'
// });
*/

Ext.BLANK_IMAGE_URL = 'media/ext/resources/images/default/s.gif'

var onConfirmDelete = Ext.emptyFn;
var onDelete = Ext.emptyFn;
var onCompleteEdit = Ext.emptyFn;
var onEdit = Ext.emptyFn;
var onCompleteAdd = Ext.emptyFn;
// var onCopy = Ext.emptyFn;



// store of parent object
var remoteJsonProjects = new Ext.data.JsonStore({
    root	: 'records',
    baseParams	: {
	      column: 'id'
	      },
    fields	:[
	  {
	      id	: "id",
	      mapping	: "id"
	  },{
	      id	: "name",
	      mapping	: "name"
	  }],

      url: "aj_getParentObjectJSON",
//       setBaseParam : {sdsd: 1},
       listeners: {beforeload : function (st, ob) { st.setBaseParam ('project', 2); }} // !!!! надо как то понимать какой проект предустановлен в форме
  
});

var comboParent = new Ext.form.ComboBox({
    id: "comboParent",
    name: "fparent",
    store: remoteJsonProjects,
    triggerAction: 'name',
    emptyText:'Type any part of projects name...',
    forceSelection:true,
    minChars: 1,
    valueField: 'id',
    fieldLabel: 'Parent',
    loadingText: 'Querying...',
});



// store of project
var remoteJsonProjects = new Ext.data.JsonStore({
    root	: 'records',
    baseParams	: {
	      column: 'id'
	      },
    fields	:[
	  {
	      id	: "id",
	      mapping	: "id"
	  },{
	      id	: "name",
	      mapping	: "name"
	  }],
    proxy	:  new Ext.data.HttpProxy({
		url	: "aj_getProjectsJSON"
		
    })
  
});

var comboProject = new Ext.form.ComboBox({
    id: "comboProject",
    name: "fproject",
    store: remoteJsonProjects,
    triggerAction: 'name',
    emptyText:'Type any part of projects name...',
    forceSelection:true,
    minChars: 1,
    valueField: 'id',
    fieldLabel: 'Project',
    loadingText: 'Querying...',
});


// store type of objects
var storetype = new Ext.data.ArrayStore({
    fields: ['id', 'name'],
    data :   [ {% for item in listtypes %}{% if forloop.last %}[{{ item.id }},'[{{item.id}}] {{ item.name}}' ]{% else %}[{{ item.id }},'[{{item.id}}] {{ item.name}}' ],{% endif %}{% endfor %}  ]

});
var combotype = new Ext.form.ComboBox({
    id: "comboType",
    fieldLabel: 'Objects type',
    name: "ftype",
    store: storetype,
    displayField:'name',
    typeAhead: true,
    mode: 'local',
    triggerAction: 'all',
    emptyText:'Select a type ...',
    selectOnFocus:true,
    valueField: 'id',
});

//---------------------------------------------------------------
var onChangeSuccessOrFail = function (form, action){
// 	var formPanel = Ext.getCmp("changeFormPanel");
	var ffw = Ext.getCmp("changeFormPanelWindow");
	ffw.el.unmask();	


	var result = action.result;
	if (result.success) {
		ffw.close();
		Ext.MessageBox.alert("Success", action.result.msg);
	} else{
		Ext.MessageBox.alert("Failer", action.result.msg);
	}
}
 
var ChangeSubmitHandler = function (){
	var ff = Ext.getCmp("changeFormPanel");
	var ffw = Ext.getCmp("changeFormPanelWindow");
	ffw.el.mask ("Please wait", "x-mask-loading");
// 	alert ("ChangeSubmitHandler");	

	
	ff.getForm().submit({
		url		: "aj_saveChange",
		success		: onChangeSuccessOrFail,
		failure		: onChangeSuccessOrFail,
		timeout		: 100000,
	});


}

//---------------------------------------------------------------
var onCopySuccessOrFail = function (form, action){
	var ffw = Ext.getCmp("copyFormPanelWindow");
	ffw.el.unmask();	

	var ff = Ext.getCmp("newFormPanel");
	var ffw = Ext.getCmp("newFormPanelWindow");
	ffw.el.mask ("Please wait", "x-mask-loading");
	

	var result = action.result;
	if (result.success) {
		Ext.MessageBox.alert("Success", action.result.msg);
		ffw.close();
	} else{
		Ext.MessageBox.alert("Failer", action.result.msg);
	}
}
 
var CopySubmitHandler = function (){
	var ff = Ext.getCmp("copyFormPanel");
	ff.el.mask ("Please wait", "x-mask-loading");

	
	ff.getForm().submit({
		url		: "aj_saveCopyObject",
		success		: onCopySuccessOrFail,
		failure		: onCopySuccessOrFail	
	});
	ff.el.unmask();

}

//---------------------------------------------------------------

var onNewSuccessOrFail = function (form, action){
/*	var formPanel = Ext.getCmp("newFormPanel");
// 	formPanel.el.unmask();*/
	var ffw = Ext.getCmp("newFormPanelWindow");
	ffw.el.unmask();	

	var result = action.result;
	if (result.success) {
 		Ext.MessageBox.alert("Данные сохранены", action.result.msg);
		ffw.close();
		tp = Ext.getCmp("tree");
		cn = tp.getNodeById(result.node);
		nn = tp.getNodeById(result.newnode);
		tl = tp.getLoader();

		tl.load(cn);
		cn.expand(true, true);

		
	} else{
		Ext.MessageBox.alert("Ошибка при сохранении", action.result.msg);
	}
}	

 //---------------------------------------------------------------
var NewSubmitHandler = function (){
	var ff = Ext.getCmp("newFormPanel");
	var ffw = Ext.getCmp("newFormPanelWindow");
	ffw.el.mask ("Please wait", "x-mask-loading");

	ff.getForm().submit({
		url		: "aj_saveNewObject",
		success		: onNewSuccessOrFail,
		failure		: onNewSuccessOrFail,
// 		failure		: Ext.MessageBox.alert("ERROR", "Some error happend."),
	});
}

 //---------------------------------------------------------------
var NewFolderSubmitHandler = function (){
	var ff = Ext.getCmp("newFormPanel");
	var ffw = Ext.getCmp("newFormPanelWindow");
	ffw.el.mask ("Please wait", "x-mask-loading");

	ff.getForm().submit({
		url		: "aj_saveNewObjectFolder",
		success		: onNewSuccessOrFail,
		failure		: onNewSuccessOrFail,
// 		failure		: Ext.MessageBox.alert("ERROR", "Some error happend."),
	});
}


//---------------------------------------------------------------
//form new object
// var newform = new Ext.FormPanel({
//         frame:true,
//         width: 350,
// 	border:false,
//         defaults: {width: 230},
//         defaultType: 'textfield',
// 	closeAction:'close',
// 	id	: "newFormPanel",
// 
//         items: [
// 	    combotype,
// 	    comboProject,
// 	    comboParent,
// 	    {
//                 fieldLabel: 'Object name',
//                 name: 'fname',
//                 allowBlank:false
//             },{
//                 fieldLabel: 'Commets',
//                 name: 'fcomment',
// 		allowBlank:false
//             }
//         ]
//     });

//---------------------------------------------------------------
//form copy object
/*
var copyform = new Ext.FormPanel({
        frame:true,
        width: 350,
	border:false,
        defaults: {width: 230},
        defaultType: 'textfield',
	id	: "copyFormPanel",

        items: [{
                fieldLabel: 'new project',
                name: 'fnewproject',
		allowBlank:false
	    },{
                fieldLabel: 'new parent',
                name: 'fidnewparent',
		allowBlank:false
	    },{
                fieldLabel: '',
                name: 'fidparent',
		allowBlank:false
	    }
	
        ]
    });
*/


var changeform = new Ext.FormPanel({
        frame:true,
        width: 350,
	border:false,
        defaults: {width: 230},
        defaultType: 'textfield',
	id	: "changeFormPanel",

        items: [{
                fieldLabel: 'project',
                name: 'fproject',
		allowBlank:false
	    }
	
        ]
    });



//---------------------------------------------------------------

function create_newform(){
	var win = new Ext.Window({
		id : "newFormPanelWindow",
                layout:'fit',
		title : "Добавление нового изделия",
                width:800,
                 height:450,
                plain: true,
		modal: true,
                items: new Ext.FormPanel({
        		frame:true,
//         		width: 1,
			border:false,
        		defaults: {width: 200},
        		defaultType: 'textfield',
			id	: "newFormPanel",
			closeAction:'close',
			autoDestroy: true,
// 			fileUpload: true,
        		items: [
		//---------------------------------------
 				{
 				xtype: 'container',
 				layout: 'hbox',
 				width: 750,
				
 				items: [
 					{xtype: 'fieldset',
 					title: "Основные данные",
 					flex:  1,
					anchor: '-10',
					height:200,
					defaults: {width: 230},
        				defaultType: 'textfield',
 					width: 400,
 					items: [
						{
						fieldLabel: 'Наименование',
						name: 'fname',
						allowBlank:false
						},

				//---------------------------------------
						new Ext.form.ComboBox({
							id: "comboType",
							fieldLabel: 'Тип изделия',
							name: "fctype",
							store: new Ext.data.JsonStore({
								root	: 'records',
								baseParams	: {
									column: 'id'
									},
								fields	:[
									{
									name	: "id"
									},{
									name	: "name"
									}],
								
								proxy: new Ext.data.HttpProxy({
										url: 'aj_getTypeObjectJSON'
									}),
		
								listeners: 	{beforeload : function (st, ob) { 
											c = Ext.getCmp("ftypekind");
											v = c.el.getValue();
											st.setBaseParam ('ftypekind', v); 
											}, 
										
									} 
								
							}),
// 							mode: 'remote',
							triggerAction: 'all',
							emptyText:'Выберите тип изделия...',
							selectOnFocus:true,
							minChars: 0,
							valueField: "id",
							displayField: "name",
							hiddenName: "fidtype",
							forceSelection:true,
							listeners:{
// 								select : function(set){
// 									fklgiadd = Ext.getCmp("fklgi");
// 									fklgiadd.setValue(fklgiadd.getValue()+"aa");
// 									}
							}
						}),
/*					{
 					xtype: 'container',
 					layout: 'hbox',
  					width: 350,
					defaultType: 'textfield',
 					items: [*/
						{
						fieldLabel: 'Номер КЛГИ',
						id: 'fklgi',
						name: 'fklgi',
// 						width: 150,
						allowBlank: false
						},
// 						{
// 						id: 'fklgiadd',
// 						name: 'fklgiadd',
// 						width: 30,
// 						allowBlank: false
// 						},
// 					]}, // end container
						{
						xtype: 'textarea',
						fieldLabel: 'Описание',
						name: 'fitemcomment',
						allowBlank: true,
						},
					] // items fieldset 1

				    }, //'fieldset 1
					
				    {xtype: 'fieldset',
 					title: "Применение",
 					flex:  1,
					anchor: '-10',
					height:200,
					defaultType: 'textfield',
 					items: [
// 						{
// 						xtype: 'hidden',
// 						id: 'fidproject',
// 						name: 'fidproject'
// 						},
						new Ext.form.ComboBox({
						id: "comboProject",
						name: "fcproject",
						store: new Ext.data.JsonStore({
							root	: 'records',
							baseParams	: {
								column: 'id'
								},
							fields	:[
								{
								name	: "id"
								},{
								name	: "name"
								}],
							
							proxy: new Ext.data.HttpProxy({
								url: 'aj_getProjectsJSON'
								}),
		
							
						}),
						triggerAction: 'all',
						emptyText:'Наберите часть названия...',
						forceSelection: true,
						minChars: 1,
						valueField: 'id',
						displayField: "name",
						hiddenName: "fidproject",
						fieldLabel: 'Проект',
						loadingText: 'Идет запрос на сервер...',
						selectOnFocus: true,
						listeners: {change: function(){
										c = Ext.getCmp("comboParent");
										pr = c.setValue("Project changed, type again");
									}
								}
						}),

						new Ext.form.ComboBox({
						id: "comboParent",
						name: "fcparent",
						store: new Ext.data.JsonStore({
							root	: 'records',
							baseParams	: {
								column: 'id'
								},
							fields	:[
								{
								name	: "id"
								},{
								name	: "name"
								}],
							
							proxy: new Ext.data.HttpProxy({
								url: 'aj_getParentObjectJSON'
								}),
		
							
						}),
						triggerAction: 'query',
						emptyText:'Наберите часть названия...',
						forceSelection: true,
						minChars: 1,
						valueField: 'id',
						displayField: "name",
						hiddenName: "fidparent",
						fieldLabel: 'Применение',
						loadingText: 'Идёт запрос на сервер...',
						selectOnFocus: true,
						listeners: 	{beforeload : function (st, ob) { 
									ff = Ext.getCmp("fidproject");
									
// 									alert ("beforeload"+ff.getValue());
	// 								pr = c.el.getValue();
									st.setBaseParam ('project', ff.getValue()); 
									}
							} 
						}),
						{
						xtype: 'hidden',
						id: 'ftypekind',
						name: 'ftypekind'
						},
					//---------------------------------------
					] // items fieldset 2
				     }, //'fieldset 2
				] // items container
			   }, // container	hbox
				
// 			    {
//  				xtype: 'container',
//  				layout: 'hbox',
//  				width: 750,
// 				height: 350,
//  				items: [
//  					{xtype: 'fieldset',
//  					title: "Параметры операции поступления",
//  					flex:  1,
// 					anchor: '-10',
// 					height:200,
//         				defaultType: 'textfield',
// 					items	: [
// 						
// 						
// 						{
// 						xtype: 'datefield',
// 						fieldLabel: 'Поступление',
// 						name: 'fdata',
// 						allowBlank:false
// 						},{
// 						fieldLabel: 'Инв. номер',
// 						id:'finvnumber',
// 						name: 'finvnumber',
// 						allowBlank:false,
// 						},{
// 						xtype: 'hidden',
// 						id: 'ftypekind',
// 						name: 'ftypekind'
// 						},
// 						{xtype : 'checkbox',
// 						fieldLabel: 'Файл с данными',
// 						width: 500,
// 						id:'havefile',
//                 				name: 'havefile'},
// 						new Ext.ux.form.FileUploadField({
// 							name: 'ffile',
// 							fieldLabel: 'Файл с данными',
// 							width: 200,
// 						}),{
// 						xtype: 'textarea',
// 						fieldLabel: 'Комментарии операции',
// 						name: 'fopercomment',
// 						allowBlank: true,
// 						width: 400
// 						},
// 					
// 						] // items fieldset 2
// 				     }, //'fieldset 2
// 			   	] // items container
// 			   }, // container2	hbox
			] // items form panel
		}), // form panel
		border: false,
		autoDestroy: true,
		animateTarget: "treepanel",

                buttons: [{
                    text:'Сохранить',
                    disabled:false,
		    handler: NewFolderSubmitHandler,
			},{
                    text: 'Отмена',
                    handler: function(){
                        win.close();
                    }
                }]
            });
	return win;
};
//---------------------------------------------------------------

function create_newformdoc(){
	var win = new Ext.Window({
		id : "newFormPanelWindow",
                layout:'fit',
		title : "Добавление нового документа",
                width:800,
                 height:450,
                plain: true,
		modal: true,
                items: new Ext.FormPanel({
        		frame:true,
//         		width: 1,
			border:false,
        		defaults: {width: 200},
        		defaultType: 'textfield',
			id	: "newFormPanel",
			closeAction:'close',
			autoDestroy: true,
			fileUpload: true,
        		items: [
		//---------------------------------------
 				{
 				xtype: 'container',
 				layout: 'hbox',
 				width: 750,
				
 				items: [
 					{xtype: 'fieldset',
 					title: "Основные данные",
 					flex:  1,
					anchor: '-10',
					height:200,
					defaults: {width: 230},
        				defaultType: 'textfield',
 					width: 400,
 					items: [
						{
						fieldLabel: 'Наименование',
						name: 'fname',
						allowBlank:false
						},

				//---------------------------------------
						new Ext.form.ComboBox({
							id: "comboType",
							fieldLabel: 'Код документа',
							name: "fctype",
							store: new Ext.data.JsonStore({
								root	: 'records',
								baseParams	: {
									column: 'id'
									},
								fields	:[
									{
									name	: "id"
									},{
									name	: "name"
									}],
								
								proxy: new Ext.data.HttpProxy({
										url: 'aj_getTypeObjectJSON'
									}),
		
								listeners: 	{beforeload : function (st, ob) { 
											c = Ext.getCmp("ftypekind");
											v = c.el.getValue();
											st.setBaseParam ('ftypekind', v); 
											}, 
										
									} 
								
							}),
// 							mode: 'remote',
							triggerAction: 'all',
							emptyText:'Выберите код...',
							selectOnFocus:true,
							minChars: 0,
							valueField: "id",
							displayField: "name",
							hiddenName: "fidtype",
							forceSelection:true,
							listeners:{
								select : function(set){
									fklgiadd = Ext.getCmp("fklgi");
									fklgiadd.setValue(fklgiadd.getValue()+"aa");
									}
							}
						}),
/*					{
 					xtype: 'container',
 					layout: 'hbox',
  					width: 350,
					defaultType: 'textfield',
 					items: [*/
						{
						fieldLabel: 'Номер КЛГИ',
						id: 'fklgi',
						name: 'fklgi',
// 						width: 150,
						allowBlank: false
						},
// 						{
// 						id: 'fklgiadd',
// 						name: 'fklgiadd',
// 						width: 30,
// 						allowBlank: false
// 						},
// 					]}, // end container
						{
						xtype: 'textarea',
						fieldLabel: 'Описание',
						name: 'fitemcomment',
						allowBlank: true,
						},
					] // items fieldset 1

				    }, //'fieldset 1
					
				    {xtype: 'fieldset',
 					title: "Применение",
 					flex:  1,
					anchor: '-10',
					height:200,
					defaultType: 'textfield',
 					items: [
// 						{
// 						xtype: 'hidden',
// 						id: 'fidproject',
// 						name: 'fidproject'
// 						},
						new Ext.form.ComboBox({
						id: "comboProject",
						name: "fcproject",
						store: new Ext.data.JsonStore({
							root	: 'records',
							baseParams	: {
								column: 'id'
								},
							fields	:[
								{
								name	: "id"
								},{
								name	: "name"
								}],
							
							proxy: new Ext.data.HttpProxy({
								url: 'aj_getProjectsJSON'
								}),
		
							
						}),
						triggerAction: 'all',
						emptyText:'Наберите часть названия...',
						forceSelection: true,
						minChars: 1,
						valueField: 'id',
						displayField: "name",
						hiddenName: "fidproject",
						fieldLabel: 'Проект',
						loadingText: 'Идет запрос на сервер...',
						selectOnFocus: true,
						listeners: {change: function(){
										c = Ext.getCmp("comboParent");
										pr = c.setValue("Project changed, type again");
									}
								}
						}),

						new Ext.form.ComboBox({
						id: "comboParent",
						name: "fcparent",
						store: new Ext.data.JsonStore({
							root	: 'records',
							baseParams	: {
								column: 'id'
								},
							fields	:[
								{
								name	: "id"
								},{
								name	: "name"
								}],
							
							proxy: new Ext.data.HttpProxy({
								url: 'aj_getParentObjectJSON'
								}),
		
							
						}),
						triggerAction: 'query',
						emptyText:'Наберите часть названия...',
						forceSelection: true,
						minChars: 1,
						valueField: 'id',
						displayField: "name",
						hiddenName: "fidparent",
						fieldLabel: 'Применение',
						loadingText: 'Идёт запрос на сервер...',
						selectOnFocus: true,
						listeners: 	{beforeload : function (st, ob) { 
									ff = Ext.getCmp("fidproject");
									
									alert ("beforeload"+ff.getValue());
	// 								pr = c.el.getValue();
									st.setBaseParam ('project', ff.getValue()); 
									}
							} 
						}),
					//---------------------------------------
					] // items fieldset 2
				     }, //'fieldset 2
				] // items container
			   }, // container	hbox
				
			    {
 				xtype: 'container',
 				layout: 'hbox',
 				width: 750,
				height: 350,
 				items: [
 					{xtype: 'fieldset',
 					title: "Параметры операции поступления",
 					flex:  1,
					anchor: '-10',
					height:200,
        				defaultType: 'textfield',
					items	: [
						
						
						{
						xtype: 'datefield',
						fieldLabel: 'Поступление',
						name: 'fdata',
						allowBlank:false
						},{
						fieldLabel: 'Инв. номер',
						id:'finvnumber',
						name: 'finvnumber',
						allowBlank:false,
						},{
						xtype: 'hidden',
						id: 'ftypekind',
						name: 'ftypekind'
						},
						{xtype : 'checkbox',
						fieldLabel: 'Файл с данными',
						width: 500,
						id:'havefile',
                				name: 'havefile'},
						new Ext.ux.form.FileUploadField({
							name: 'ffile',
							fieldLabel: 'Файл с данными',
							width: 200,
						}),{
						xtype: 'textarea',
						fieldLabel: 'Комментарии операции',
						name: 'fopercomment',
						allowBlank: true,
						width: 400
						},
					
						] // items fieldset 2
				     }, //'fieldset 2
			   	] // items container
			   }, // container2	hbox
			] // items form panel
		}), // form panel
		border: false,
		autoDestroy: true,
		animateTarget: "treepanel",

                buttons: [{
                    text:'Сохранить',
                    disabled:false,
		    handler: NewSubmitHandler,
			},{
                    text: 'Отмена',
                    handler: function(){
                        win.close();
                    }
                }]
            });
	return win;
};

//---------------------------------------------------------------

function create_changeform(){
	var win = new Ext.Window({
		id : "changeFormPanelWindow",
                layout:'fit',
		title : "Ввод извещения",
                width:800,
                 height:450,
                plain: true,
		modal: true,
                items: new Ext.FormPanel({
        		frame:true,
//         		width: 1,
			border:false,
        		defaults: {width: 200},
        		defaultType: 'textfield',
			id	: "changeFormPanel",
			closeAction:'close',
			autoDestroy: true,
			fileUpload: true,
        		items: [
		//---------------------------------------
 				{
 				xtype: 'container',
 				layout: 'hbox',
 				width: 750,
				
 				items: [
 					{xtype: 'fieldset',
 					title: "Основные данные",
 					flex:  1,
					anchor: '-10',
					height:120,
        				defaultType: 'textfield',
 					items: [
						{
						fieldLabel: 'Номер документа',
						name: 'fnumber',
						allowBlank:false
						},
						{
						xtype: 'datefield',
						fieldLabel: 'Поступление',
						name: 'fdata',
						allowBlank:false
						},{
						fieldLabel: 'Инв. номер',
						id:'finvnumber',
						name: 'finvnumber',
						allowBlank:false,
						},

					] // items fieldset 1

				    }, //'fieldset 1
					
				    {xtype: 'fieldset',
 					title: "Применение",
 					flex:  1,
					anchor: '-10',
					height:120,
					defaultType: 'textfield',
 					items: [
// 						{
// 						xtype: 'hidden',
// 						id: 'fidproject',
// 						name: 'fidproject'
// 						},
						new Ext.form.ComboBox({
						id: "comboProject",
						name: "fcproject",
						store: new Ext.data.JsonStore({
							root	: 'records',
							baseParams	: {
								column: 'id'
								},
							fields	:[
								{
								name	: "id"
								},{
								name	: "name"
								}],
							
							proxy: new Ext.data.HttpProxy({
								url: 'aj_getProjectsJSON'
								}),
		
							
						}),
						triggerAction: 'all',
						emptyText:'Наберите часть названия...',
						forceSelection: true,
						minChars: 1,
						valueField: 'id',
						displayField: "name",
						hiddenName: "fidproject",
						fieldLabel: 'Проект',
						loadingText: 'Идет запрос на сервер...',
						selectOnFocus: true,
						listeners: {
// 								change: function(){
// 										c = Ext.getCmp("comboParent");
// 										pr = c.setValue("Project changed, type again");
// 									}
								}
						}),

					//---------------------------------------
					] // items fieldset 2
				     }, //'fieldset 2
				] // items container
			   }, // container	hbox

			    {
 				xtype: 'container',
				id:	"cont_files",
//  				layout: 'vbox',
 				width: 800,
 				items: [

/* 					{xtype: 'fieldset',
 					title: "Дополнительны параметры",
 					flex:  1,
					anchor: '-10',
					height:200,
        				defaultType: 'textfield',
					items	: [*/
						
						
// 						new Ext.ux.form.FileUploadField({
// 							id: 'ffile1',
// 							name: 'ffile1',
// 							fieldLabel: 'Файл с данными',
// // 							width: 200,
// 							
// 						}),

						] // items fieldset 2
//				     }, //'fieldset 2
			   //	] // items container
			   }, // container2	hbox

						{
						name: 'fcounter',
						fieldLabel: 'Итого строк',
						id: 'fcounter',
						width: 30,
						value: 0,
						allowBlank:false
						},
						 new Ext.Button({
							text	: "Добавить строку",
							width: 200,
							handler: function(){
									// меняем счетчик файлов
									counter = Ext.getCmp("fcounter");
									n = parseInt(counter.getValue());
									n = n+1;
									counter.setValue(n);
									var f = {
 											xtype: 'container',
											layout:'hbox',
 											layoutConfig: {
                                      			  					padding:'5',
                                        							align:'middle'
                                    							},
                                    							defaults:{margins:'0 5 0 0'},
											width: 700,
											items: [
										
										/*xtype: 'textfield',
										fieldLabel: 'sdsdsdsdsdsd',
 										id:'finvnumber'+n,
 										name: 'finvnumber'+n,
										allowBlank:false,
										width: 200,
										},*/
					new Ext.form.ComboBox({
						flex:1,
						id: "comboParent"+n,
						name: "fcparent"+n,
						//width: 200,
						store: new Ext.data.JsonStore({
							root	: 'records',
							baseParams	: {
								column: 'id'
								},
							fields	:[
								{
								name	: "id"
								},{
								name	: "name"
								}],
							
							proxy: new Ext.data.HttpProxy({
								url: 'aj_getParentObjectJSON'
								}),
							listeners:{
								beforeload : function (st, ob) { 
									
									ff = Ext.getCmp("comboProject");
// 									alert (ff.getRawValue());
// 									alert ("beforeload"+ff.getValue());
// 											pr = c.el.getValue();
									st.setBaseParam ('idproject', ff.getValue()); 
								}
							}
							
						}),// end store
						triggerAction: 'all',
						emptyText:'Наберите часть названия...',
						forceSelection: true,
						minChars: 1,
						valueField: 'id',
						displayField: "name",
						hiddenName: "fidp"+n,
						fieldLabel: 'Применение',
						loadingText: 'Идёт запрос на сервер...',
						selectOnFocus: true,

					}),// end comboParent

					new Ext.ux.form.FileUploadField({
						flex:1,
						id: 'ffile'+n,
						name: 'ffile'+n,
						fieldLabel: 'Файл с данными',
						//width: 200,
					}),
									] // end items container
										}; // end var to add

									c = Ext.getCmp("cont_files");
									c.add(f);
									c.doLayout(); 

									} // end function handler 
						}),	// end button
			] // items form panel
		}), // form panel
		border: false,
		autoDestroy: true,
//		animateTarget: "treepanel",

                buttons: [{
                    text:'Сохранить',
                    disabled:false,
		    handler: ChangeSubmitHandler,
			},{
                    text: 'Отмена',
                    handler: function(){
                        win.close();
                    }
                }]
            });
	return win;
};

//---------------------------------------------------------------

function create_copyform(){
	var win = new Ext.Window({
		id : "copyFormPanelWindow",
                layout:'fit',
		title : "Copy object to other project",
                width:500,
                height:300,
                plain: true,
		modal: true,
                items: new Ext.FormPanel({
        		frame:true,
        		width: 350,
			border:false,
        		defaults: {width: 230},
        		defaultType: 'textfield',
			id	: "copyFormPanel",
			closeAction:'close',
			autoDestroy: true,
        		items: [
		//---------------------------------------
				new Ext.form.ComboBox({
				id: "comboProject",
				name: "fcproject",
				store: remoteJsonProjects,
				triggerAction: 'name',
				emptyText:'Type any part of projects name...',
				forceSelection:true,
				minChars: 1,
				valueField: 'id',
				fieldLabel: 'Проект',
				loadingText: 'Querying...',
				listeners: {change: function(){
								c = Ext.getCmp("comboParent");
								pr = c.setValue("Project changed, type again");
							}
						}
				}),
		//---------------------------------------
				new Ext.form.ComboBox({
					id: "comboParent",
					name: "fcparent",
					store: new Ext.data.JsonStore({
						root	: 'records',
						baseParams	: {
							column: 'id'
							},
						fields	:[
							{
							id	: "id",
							mapping	: "id"
							},{
							id	: "name",
							mapping	: "name"
							}],
						
						url: "aj_getParentObjectJSON",
						listeners: 	{beforeload : function (st, ob) { 
										c = Ext.getCmp("comboProject");
										pr = c.el.getValue();
										st.setBaseParam ('project', pr); 
										}
								} // !!!! надо как то понимать какой проект предустановлен в форме
						
					}),
					triggerAction: 'query',
					emptyText:'Type any part of projects name...',
					forceSelection:true,
					minChars: 1,
					valueField: 'id',
					fieldLabel: 'Применяемость',
					loadingText: 'Querying...',
				}),
				{
					fieldLabel: 'Object name',
					id: "fname",
					name: 'fname',
					allowBlank:false,
				},
				{
					xtype		: 'checkboxgroup',
					fieldLabel	: 'Type copy',
					items		:[
							{boxLabel	:"all tree",
							 inputValue	:"all",
							id		: 'fchcopytree',
							name		: 'fchcopytree',
							}]
				
				}
			]
		}),
		border: false,
		autoDestroy: true,
		animateTarget: "treepanel",

                buttons: [{
                    text:'Submit',
                    disabled:false,
		    handler: CopySubmitHandler,
			},{
                    text: 'Close',
                    handler: function(){
                        win.close();
                    }
                }]
            });
	return win;
};


//---------------------------------------------------------------

var buildCtxMenu = function(node) {
	return new Ext.menu.Menu({
		items: [
			{
				itemId : 'add_doc',
			//  	handler: action_new
				hidden:true,
			},			
			{
				itemId : 'add_folder',
			//  	handler: action_new
				hidden:true,
			},
			{
				itemId : 'edit_folder',
				//handler: onEdit,
				hidden:true,
			},
			{
				itemId : 'edit',
				handler: onEdit,
				hidden:true,
			},
			{
				itemId : 'delete',
				handler: onDelete,
				hidden:true,
			},
			{
				itemId : 'copy',
//				handler: action_copy
				hidden:true,
			},
			{
				itemId : 'change',
// 				handler: action_change
				hidden:true,
			}
		]
	});
}

//------------------------------
function getProjectByNode (node){
	Ext.Ajax.request({
   		url: 'aj_getProjectByNodeJSON',
		params: { node: node.id },
   		success: function ( result, request) { 
			try {
				var jsonData = Ext.util.JSON.decode(result.responseText);
				if (jsonData.success == 'false'){
					Ext.MessageBox.alert('ERROR', 'Project has not being found.');
				}else{
					pr = Ext.getCmp("comboParent");
					pj = Ext.getCmp("comboProject");
// 					pj.getStore().load();
// 					prid.setValue(jsonData.id);
					pr.setValue(node.id);
					pr.setRawValue(node.text);
// 					pr.disable()

					pj.setValue(jsonData.id);
					pj.setRawValue(jsonData.name);
// 					pj.disable()
		
// 					alert (jsonData.name);
				}
			}catch (err) {
				Ext.MessageBox.alert('ERROR', 'Could not decode ' + stringData)	;
			}
		},
  		failure: function() { Ext.MessageBox.alert( "ERROR","Lost connection with server.");}
	});
}


//------------------------------
function getKlgiByNode (node){
	Ext.Ajax.request({
   		url: 'aj_getKlgiByNodeJSON',
		params: { node: node.id },
   		success: function ( result, request) { 
			try {
				var jsonData = Ext.util.JSON.decode(result.responseText);
				if (jsonData.success == 'false'){
					Ext.MessageBox.alert('ERROR', 'Project has not being found.');
				}else{

					fklgi = Ext.getCmp("fklgi");
					if (jsonData.klgi != 'None'){
						fklgi.setValue(jsonData.klgi);
// 						fklgi.disable();
					}else{
						
					}
				}
			}catch (err) {
				Ext.MessageBox.alert('ERROR', 'Could not decode ' + stringData)	;
			}
		},
  		failure: function() { Ext.MessageBox.alert( "ERROR","Lost connection with server.");}
	});
}





var onCtxMenu = function(node, evtObj) {
// 	node.select();
 	evtObj.stopEvent();
// 	if (! this.ctxMenu) {
 		this.ctxMenu = buildCtxMenu();
// 	}

	var ctxMenu = this.ctxMenu;
	var addItemDocument =  ctxMenu.getComponent('add_doc');
	var addItemFolder =  ctxMenu.getComponent('add_folder');
	var editFolder = ctxMenu.getComponent('edit_folder');
	var editItem = ctxMenu.getComponent('edit');
	var copyItem = ctxMenu.getComponent('copy');
	var deleteItem =  ctxMenu.getComponent('delete');
	var changeItem = ctxMenu.getComponent('change');


	if (node.id =='1') {
// 		addItem.setText('Add Project');
//  		editItem.setText('Nope, not changing the name');
// 		deleteItem.setText('Cannot delete the root, silly');
// 		changeItem.setText('Cannot change the root, silly');
// 		addItem.enable();
		
// 		deleteItem.disable();
// 		editItem.disable();
	}
	else if (! node.leaf) {
		addItemDocument.setText('Новый документ');
		addItemDocument.show();
		addItemFolder.setText('Новое изделие');
		addItemFolder.show();
		editFolder.setText('Правка изделия');
		editFolder.show();
		//copyItem.setText('Copy to other project');
		//deleteItem.setText('Delete');
		editItem.setText('Выдать копию');
		editItem.show();
		changeItem.setText('Добавить изменение');
		changeItem.show();
		
		addItemDocument.enable();
	
		addItemDocument.on("click", function(){ point.form.document.view(node.id); }, this, {stopEvent: true});

		addItemFolder.on("click", function(){ point.form.folder.view(node.id); }, this, {stopEvent: true});
		editFolder.on("click", function(){ point.form.folder.view(node.id,true); }, this, {stopEvent: true});

		copyItem.on("click", function(){
//  						alert ("fn action_copy"+ node.text);
					
						evtObj.stopEvent();
						var winform = create_copyform();
					
						n = Ext.getCmp("fname");

						n.setValue("["+node.id+"] "+node.text);	
						
						
						winform.show();

					}, this, {stopEvent: true});

		editItem.on("click",  function(){ point.form.download.view(node.id); }, this, {stopEvent: true});
		editItem.enable();
		deleteItem.enable();
		copyItem.enable();
		changeItem.hide();
	
	}
	else {
 		//addItemDocument.setText('Add new children');
		editItem.setText('Выдать копию');
		editItem.show();
		editFolder.setText('Правка документа');
		editFolder.show();
		//deleteItem.setText('Delete');
		//copyItem.setText('Copy to other project');
		changeItem.setText('Добавить изменение');
		changeItem.show();

		copyItem.on("click", function(){
//  						alert ("fn action_copy"+ node.text);
			
						evtObj.stopEvent();
						var winform = create_copyform();
					
						n = Ext.getCmp("fname");
						ta = Ext.getCmp("fchcopytree");

						n.setValue("["+node.id+"] "+node.text);	
						
						winform.show();

					}, this, {stopEvent: true});

		editItem.on("click",  function(){ point.form.download.view(node.id); }, this, {stopEvent: true});
		editFolder.on("click", function(){ point.form.document.view(node.id,true); }, this, {stopEvent: true});

		changeItem.on("click", function(){ point.form.file.view(node.id); }, this, {stopEvent: true});

	}
		ctxMenu.showAt(evtObj.getXY() );
}


//---------------------------------------
//Handlers 


function clickTreePanelNode(node){
//	tabpanel = Ext.getCmp("tabpanel");
//	updateCentralPanel(tabpanel, node);
	if(point.panel.center) point.panel.center.view(node.id,'Node');
// 	addTab(tabpanel, node);
 	//if(point.panel.right) point.panel.right.view(node.id);
	
}


//---------------------------------------

var treeload = new Ext.tree.TreeLoader({ 
		url : 'aj_getTreeChildren', 
		id:'treeload',
		clearOnLoad : true,
		preloadChildren : true
});

var tree = {title: 'Структура по проектам',
	xtype : 'treepanel',
	autoScroll : true,
	id: 'tree',
	border	: true,
	listeners: {click: clickTreePanelNode, contextmenu: onCtxMenu },
	loader : treeload,
	root : { text : 'Проекты', id : '1', expanded : true },
	animate: false,

}

/*
var tabs = new Ext.TabPanel({
		id: 'tabpanel',
		resizeTabs:true, // turn on tab resizing
        	minTabWidth: 130,
        	tabWidth:135,
// 		anchor: '100%',
//         	enableTabScroll:false,
		border:false,
//         	defaults: {autoScroll: true},
		plugins: new Ext.ux.TabCloseMenu(),
		
    		});

var colModel = new Ext.grid.ColumnModel([
	{
 		header	: 'id',
// 		sortable: true,
		width	: 10,
// 		dataIndex: 'id',	
		
	},{	
 		header	: 'КЛГИ',
 		sortable: true,
		width	: 20,
 		dataIndex: 'klgi',
		
	},{
 		header	: 'Название',
 		sortable: true,
		width	: 30,
 		dataIndex: 'text',
		
	},{
 		header	: 'Описание',
		
	}

]);

var colModel2 = new Ext.grid.ColumnModel([
	{
 		header	: 'id',
// 		sortable: true,
		width	: 10,
// 		dataIndex: 'id',	
		
	},{
 		header	: 'КЛГИ',
// 		sortable: true,
		width	: 20,
// 		dataIndex: 'id',	
		
	},{
 		header	: 'Название',
		width	: 30,
		
	}

]);


var selModel = new Ext.grid.RowSelectionModel({
		singleSelect: true
});


var ggg = new Ext.grid.GridPanel ({
   			     	store		: new Ext.data.JsonStore({
							url : "aj_getContentChildren",
          						autoLoad : {url : "aj_getContentChildren", params: {'node': 1}},
        						reader: new Ext.data.JsonReader({
//             							root: 'records',
//             							totalProperty: 'totalCount',
            							id: 'idstore'
        						}), 
							fields	:[
            							{name: 'id', mapping: 'id'},
								{name: 'klgi', mapping: 'klgi'},
            							{name: 'text', mapping: 'text'},
            							{name: 'content', mapping: 'content'},
        							],
							}),
         			colModel	: colModel,
				view		: new Ext.grid.GridView({
							forceFit:true,
						  }),
 				autoHeight	: true,
				layout		: 'fit',	
				selModel	: selModel,
				loadMask	: true,
// 				listeners	: {rowclick	: function () {alert ("sdsd");}},
// 				 tbar: [{
// 					iconCls:'add-feed',
// 					text:'Add NEW',
// 					handler: function(){
// 							var winform = create_newform();
// 							winform.show();
// 						},
// 					scope: this
// 				      },{
// 					id:'delete',
// 					iconCls:'delete-icon',
// 					text:'Remove',
// 					handler: function(){
// 
// 						var s = this.getSelectionModel().getSelectedNode();
// 						if(s){
// 						    this.removeFeed(s.attributes.url);
// 						      }
// 						},
// 					scope: this
// 					}]
				});


var maintab =  tabs.add({
		title: 'Основное ',
		closable: false,
		items: ggg,
 		height: 1000,
});

tabs.activate(0);*/

//------------------------------------------------------------



//------------------------------------------------------------
var briefpanel = new Ext.Panel({
		id	: "briefpanel",
		title	: "Информация",
		height: 200,
		layout	: 'fit',
		
		autoLoad: {url: 'aj_getBriefInfoObjectHTML', params: {'node': 1}},
}); 

var historychangepanel = new Ext.Panel({
		id	: "historychangepanel",
		title	: "История изменений",
		layout	: 'fit',
		height: 200,
		
		autoLoad: {url: 'aj_getBriefInfoObjectHTML', params: {'node': 1}},
}); 

var historyrequestpanel = new Ext.Panel({
		id	: "historyrequestpanel",
		title	: "История запросов",
		layout	: 'fit',
		
		autoLoad: {url: 'aj_getBriefInfoObjectHTML', params: {'node': 1}},
}); 

/*
function updateCentralPanel(tabpanel, node){
	ct = tabpanel.getActiveTab();
	cg = ct.get(0);
	cs = cg.getStore();
	cs.load({params : {'node': node.id}});



}*/


function updateRightPanel(node_id){
	var panel = Ext.getCmp("briefpanel");
	hcpanel = Ext.getCmp("historychangepanel");
	hrpanel = Ext.getCmp("historyrequestpanel");	

	panel.load({url: 'aj_getBriefInfoObjectHTML', params: {'node': node_id}});
	hcpanel.load({url: 'aj_getHistoryObjectHTML', params: {'node': node_id}});
	hrpanel.load({url: 'aj_getHistoryRequestObjectHTML', params: {'node': node_id}});
}


//------------------------------------------------------------
/*
function addTab(tabs, node){

	if (node.isLeaf()){
// 		var  content = new Ext.Panel({
// 				
// 				});
	}else{
		var  content = new Ext.grid.GridPanel ({
   			     	store		: new Ext.data.JsonStore({
							url : "aj_getContentChildren",
        						autoLoad : {url : "aj_getContentChildren", params: {'node': node.id}},
        						reader: new Ext.data.JsonReader({
            							root: 'records',
            							totalProperty: 'totalCount',
            							id: 'idstore'
        						}), 
							fields	:[
            							{name: 'id', mapping: 'id'},
            							{name: 'text', mapping: 'text'},
            							{name: 'content', mapping: 'content'},
        							]
							}),
         			colModel	: colModel,
				view		: new Ext.grid.GridView({
							forceFit:true,
						  }),
 				autoHeight	: true,
				layout		: 'fit',	
				selModel	: selModel,
				loadMask	: true,
// 				listeners	: {rowclick	: function () {alert ("sdsd");}},
				 tbar: [{
					iconCls:'add-feed',
					text:'Add NEW',
					handler: function(){
							var winform = create_newform();
//							pj = Ext.getCmp("comboProject");
//							pr = Ext.getCmp("comboParent");
							//t = Ext.getCmp("ftypekind");
							//st = Ext.getCmp("comboType").getStore();
							
							//t.setValue(3);
							
							//st.load();
							
							
							winform.show();
						},
					scope: this
				      },{
					id:'delete',
					iconCls:'delete-icon',
					text:'Remove',
					handler: function(){

						var s = this.getSelectionModel().getSelectedNode();
						if(s){
						    this.removeFeed(s.attributes.url);
						      }
						},
					scope: this
					}]
				});
		}
	


        tabs.add({
            title: node.text,
            iconCls: 'tabs',
            closable:true,
	    layout: 'fit',
	    items: content,
 	}).show();


}*/

//---------------------------------------------------------
function showMainLayout(){
	//point.panel.left.container.add(tree);
	monit=new Ext.Viewport ({
		renderTo	: 'mainLayoutDiv',
		layout	: 'border',
		title	: "Архив документов",
		defaults	:{
			frame	: true,
			split	: true
		},
		items	: [{
			title	:'Навигация',
			region	: 'west',animCollapse: false,
			collapsible: true,
			collapseMode: 'mini',
			width		: 350,
			minWidth	: 200,
			maxWidth	: 400,
			layout: 'fit',
			items : index.viewer_navigation.container

			},{
			region	: 'center',
			layout:'fit',
			frame	: false,
			items	: index.viewer_folder.container
			},{
			title	: "Сообщения",
			region	: 'south',
			height: 120,
			},{
			xtype:'container',
			region	: 'north',
			height: 40,
			id: "northpanel",
			style:'background-image:url("./media/back.png"); background-repeat:repeat;',
			html:'<img src="./media/logo_name.png"style="padding-top:5px;padding-left:15px;">',
			split: false,
			},{
			region	: 'east',
			width: 300,
			items: [briefpanel, historychangepanel, historyrequestpanel]
			}
			
		]
		
	});//.show();
}


function doJSON(stringData) {
		try {
			var jsonData = Ext.util.JSON.decode(stringData);
			if (jsonData.success == 'false'){
				Ext.MessageBox.alert('ERROR', 'Critical error happend. Call administrator. ');
			}
		}
		catch (err) {
			Ext.MessageBox.alert('ERROR', 'Could not decode ' + stringData)	;
		}
	}
 

var checkCriticalError = function(){
	Ext.Ajax.request({
   		url: 'aj_checkCriticProblem',
   		success: function ( result, request) { 
				doJSON(result.responseText);},
   		failure: function() { Ext.MessageBox.alert( "ERROR","Lost connection with server.");},
		})
}

Ext.onReady ( function (){
	showMainLayout ();
	index.viewer_folder.view(1,'Node');	
	// привязываем основные события
 	Ext.TaskMgr.start({
     		run: checkCriticalError,
     		interval: 10000
 	});

});
</script>
<body>
<div id="mainLayoutdiv"></div>
<iframe id="downloadZIP" src="" style="display:none; dvsibility:hiidden"></iframe>
<iframe id="downloadPDF" src="" style="display:none; visibility:hidden"></iframe>
</body>
</html>