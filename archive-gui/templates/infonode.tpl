{ 'obj':{
	"id" 	: "{{ item.object.id }}",
	"name"	: "{{ item.object }}",
	"klgi"	: "{{ item.object.klgi }}",
	"desc"	: "{{ item_comments }}",
	"t_id"	: "{{ item.object.type.id }}",
	"t_name": "{{ item.object.type }}",
	},
 'node':{
	"id" 	: "{{ item.id }}",
	"p_id"	: "{{ item.parent.id }}",
	"p_name": "{% if item.parent.object.klgi %}{{ item.parent.object.klgi }} {% endif %}{{ item.parent }}",
	"pr_id"	: "{{ item.project.id }}",
	"pr_name": "{{ item.project }}",

	"date": "Отдел один",
	"a_group": "{{ item.object.division.id }}",
	"gname": "{{ item.object.division }}",
	"a_name": "{{ item.object.developer.id }}",
	"aname": "{{ item.object.developer }}",
	"f_id"	: "{{ item.object.format.id }}",
	"f_name": "{{ item.object.format }}",
	"sheets": "{{ item.object.sheets }}",
	
{% if item.status == "s" %}
{% else %}
	'reg': '{{ item.reg_number }}',
{% endif %}

	
	"inv":"{{item.object.invnumber.number}}",
{% if item.lastoperation.change %}
	'ch_number': '{{ item.lastoperation.change.number}}',
{% else %}
	'ch_number': 'Original',
{% endif %}


	"d_add"	: "{{added}}",
	"d_edit": "{{edited}}",

{% if item.object.type.kind.canhavechildren %}
	"havechildren" : true,
{% else %}
	"havechildren" : false,
{% endif %}

{% if item.id == item.project.id %}
	'isproject' : true,
{% else %}
	'isproject' : false,
{% endif %}

	}
}
