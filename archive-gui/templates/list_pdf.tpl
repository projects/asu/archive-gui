<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html>
<head> 
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
</head>

<h1>Ведомость запроса из архива</h1>

<table border=1  style="weight: 100%; border-collapse: collapse;" align=center>
<tr>
<th align=center>№ п/п</th>
<th align=center>Номер КЛГИ</th>
<th align=center>Номер изменения</th>
<th align=center>Наименование</th>
<th align=center>Тип</th>
<th align=center>Формат</th>
<th align=center>Листов</th>
<th align=center>Имя файла</th>
<th align=center>Тип файла</th>
<th align=center>Hash файла</th>

</tr>
{% for item in list %}
<tr>
<td align=center>{{ forloop.counter }}</td>
<td >
{%for l in item.level %}-{%endfor%}
	{{ item.object.klgi }}</td>
<td align=center>	{{ item.lastoperation.change.number}}</td>
<td >	{{ item.object.name }}</td>
<td align=center>	{{ item.object.type.name }}/{{ item.object.type.kind.name }}</td>
<td align=center>	{{ item.object.format.name }}</td>
<td align=center>	{{ item.object.sheets }}</td>
<td align=center>	{{ item.object.klgi}}{{item.lastoperation.file.ext }}</td>
<td align=center>	{{ item.lastoperation.file.mime }}</td>
<td align=center>	{{ item.lastoperation.file.hash }}</td>

</tr>
{% endfor %}
</table>

</html>