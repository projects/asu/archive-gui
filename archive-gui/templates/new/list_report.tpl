{
"total": "{{ total }}",
"records": [
{% for item in list %}
	{
		"id": "{{ item.id }}",
		"type": "{{ type }}",
		"date": "{{ item.date }}",
		"number": "{{ item.number }}",
		"inv": "{{ item.inv }}",
		"group": "{{ item.division }}",
		"autor": "{{ item.user }}",
		"format": "{{ item.format }}",
		"sheets": "{{ item.sheets }}",
		"base": "{{ item.base }}",
		"msg": "{{ item.comments }}",
	},
{% endfor %}
]
}