
{
"total": "{{ total }}",
"records": [
{% for item in list %}
	{
		"id": "{{ item.id }}",
		"name": "{{ item.test.name }}",
		"idtest": "{{ item.test.id }}",
		"date": "{{ item.date }}",
		"status_message": "{{ item.status_message }}",
		"status_code": "{{ item.status_code }}",
		"items": [ 
		{% for testitem in item.testitems %}
			{ "label": "{{ testitem.label }}", "name": "{{ testitem.name }}" }
		{% endfor %}
		]
	},
{% endfor %}
]
}