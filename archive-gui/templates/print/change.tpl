<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:svg="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<head>
<style type="text/css">
body {
	margin: 0px; padding: 0px;
}
td {
	width:5mm;
	height:5mm;
	border:1px solid black;
}
.size_set{
	visibility:hidden;
	border:0px;
}
</style>
</head> 
<body>
<table style="border-collapse: collapse;font-size:10pt;text-align:center;width:275mm;height:205mm;">
<tbody>
<tr>
<td class="size_set">&nbsp;</td>
<td colspan="4" rowspan="25" class="size_set">&nbsp;</td>
<td colspan="4" rowspan="3">ФГУП<br />"ЦНИИ СЭТ"</td>
<td colspan="8">ИЗВЕЩЕНИЕ</td>
<td colspan="12">Обозначение</td>
<td colspan="18">Причина</td>
<td colspan="3">Шифр</td>
<td colspan="2">Лист</td>
<td colspan="3">Листов</td>
</tr>
<tr>
<td class="size_set">&nbsp;</td>
<td colspan="8" rowspan="2">Инв. №{{inv}}</td>
<td colspan="12" rowspan="2">{{number}}</td>
<td colspan="18" rowspan="2">&nbsp;</td>
<td colspan="3" rowspan="2">&nbsp;</td>
<td colspan="2" rowspan="2">&nbsp;</td>
<td colspan="3" rowspan="2">{{sheets}}</td>
</tr>
<tr>
<td class="size_set">&nbsp;</td>
</tr>
<tr>
<td class="size_set">&nbsp;</td>
<td colspan="4" rowspan="2">{{division}}</td>
<td colspan="4" rowspan="2">Дата<br />выпуска</td>
<td colspan="4" rowspan="2">{{date}}</td>
<td colspan="2" rowspan="2">Срок.<br />изм.</td>
<td colspan="4" rowspan="2">&nbsp;</td>
<td colspan="6" rowspan="2">&nbsp;</td>
<td colspan="4" rowspan="2">Срок дей-<br />ствия ПИ</td>
<td colspan="6" rowspan="2">&nbsp;</td>
<td colspan="16">Указание о внедрении</td>
</tr>
<tr>
<td class="size_set">&nbsp;</td>
<td colspan="16" rowspan="5">&nbsp;</td>
</tr>
<tr>
<td class="size_set">&nbsp;</td>
<td colspan="4" rowspan="4">Указание<br />о заделе</td>
<td colspan="30" rowspan="4">&nbsp;</td>
</tr>
<tr>
<td class="size_set">&nbsp;</td>
</tr>
<tr>
<td class="size_set">&nbsp;</td>
</tr>
<tr>
<td class="size_set">&nbsp;</td>
</tr>
<tr>
<td class="size_set">&nbsp;</td>
<td colspan="36">Содержание изменения</td>
<td colspan="14">Применяемость</td>
</tr>
<tr>
<td class="size_set">&nbsp;</td>
<td colspan="36" rowspan="26" style="text-align:left;vertical-align:top;padding-left:50px;">
<br />
<!-- START вывод задетых объектов-->
{% for item in change_list %}
	{{item.object.klgi}}<br />
{% endfor %}
<!-- END вывод задетых объектов-->
</td>
<td colspan="14">&nbsp;</td>
</tr>
<tr>
<td class="size_set">&nbsp;</td>
<td colspan="14">&nbsp;</td>
</tr>
<tr>
<td class="size_set">&nbsp;</td>
<td colspan="14">&nbsp;</td>
</tr>
<tr>
<td class="size_set">&nbsp;</td>
<td colspan="14">&nbsp;</td>
</tr>
<tr>
<td class="size_set">&nbsp;</td>
<td colspan="14">&nbsp;</td>
</tr>
<tr>
<td class="size_set">&nbsp;</td>
<td colspan="14">&nbsp;</td>
</tr>
<tr>
<td class="size_set">&nbsp;</td>
<td colspan="14">&nbsp;</td>
</tr>
<tr>
<td class="size_set">&nbsp;</td>
<td colspan="14">&nbsp;</td>
</tr>
<tr>
<td class="size_set">&nbsp;</td>
<td colspan="14">&nbsp;</td>
</tr>
<tr>
<td class="size_set">&nbsp;</td>
<td colspan="14">&nbsp;</td>
</tr>
<tr>
<td class="size_set">&nbsp;</td>
<td colspan="14">&nbsp;</td>
</tr>
<tr>
<td class="size_set">&nbsp;</td>
<td colspan="14">&nbsp;</td>
</tr>
<tr>
<td class="size_set">&nbsp;</td>
<td colspan="14">&nbsp;</td>
</tr>
<tr>
<td class="size_set">&nbsp;</td>
<td colspan="14">&nbsp;</td>
</tr>
<tr>
<td class="size_set">&nbsp;</td>
<td colspan="14">&nbsp;</td>
</tr>
<tr>
<td class="size_set">&nbsp;</td>
<td rowspan="3">&nbsp;</td>
<td rowspan="3">&nbsp;</td>
<td rowspan="3">&nbsp;</td>
<td rowspan="3">&nbsp;</td>
<td colspan="14">&nbsp;</td>
</tr>
<tr>
<td class="size_set">&nbsp;</td>
<td colspan="14">&nbsp;</td>
</tr>
<tr>
<td class="size_set">&nbsp;</td>
<td colspan="14">&nbsp;</td>
</tr>
<tr>
<td class="size_set">&nbsp;</td>
<td rowspan="3">&nbsp;</td>
<td rowspan="3">&nbsp;</td>
<td rowspan="3">&nbsp;</td>
<td rowspan="3">&nbsp;</td>
<td colspan="14">Разослать</td>
</tr>
<tr>
<td class="size_set">&nbsp;</td>
<td colspan="14">&nbsp;</td>
</tr>
<tr>
<td class="size_set">&nbsp;</td>
<td colspan="14">&nbsp;</td>
</tr>
<tr>
<td class="size_set">&nbsp;</td>
<td rowspan="6">&nbsp;</td>
<td rowspan="6">&nbsp;</td>
<td rowspan="6">&nbsp;</td>
<td rowspan="6">&nbsp;</td>
<td colspan="14">&nbsp;</td>
</tr>
<tr>
<td class="size_set">&nbsp;</td>
<td colspan="14">&nbsp;</td>
</tr>
<tr>
<td class="size_set">&nbsp;</td>
<td colspan="14">&nbsp;</td>
</tr>
<tr>
<td class="size_set">&nbsp;</td>
<td colspan="14">&nbsp;</td>
</tr>
<tr>
<td class="size_set">&nbsp;</td>
<td colspan="14">&nbsp;</td>
</tr>
<tr>
<td class="size_set">&nbsp;</td>
<td colspan="6">Составил</td>
<td colspan="6">Проверил</td>
<td colspan="6">Т. контр</td>
<td colspan="6">Н. контр</td>
<td colspan="6">Утвердил</td>
<td colspan="6">Предст. заказчика</td>
<td colspan="14">&nbsp;</td>
</tr>
<tr>
<td class="size_set">&nbsp;</td>
<td rowspan="3">&nbsp;</td>
<td rowspan="3">&nbsp;</td>
<td rowspan="3">&nbsp;</td>
<td rowspan="3">&nbsp;</td>
<td colspan="4">{{autor}}</td>
<td colspan="2" rowspan="2">&nbsp;</td>
<td colspan="4">&nbsp;</td>
<td colspan="2" rowspan="2">&nbsp;</td>
<td colspan="4">&nbsp;</td>
<td colspan="2" rowspan="2">&nbsp;</td>
<td colspan="4">&nbsp;</td>
<td colspan="2" rowspan="2">&nbsp;</td>
<td colspan="4">&nbsp;</td>
<td colspan="2" rowspan="2">&nbsp;</td>
<td colspan="6">&nbsp;</td>
<td colspan="14">Приложение</td>
</tr>
<tr>
<td class="size_set">&nbsp;</td>
<td colspan="4">&nbsp;</td>
<td colspan="4">&nbsp;</td>
<td colspan="4">&nbsp;</td>
<td colspan="4">&nbsp;</td>
<td colspan="4">&nbsp;</td>
<td colspan="6">&nbsp;</td>
<td colspan="14" rowspan="2">&nbsp;</td>
</tr>
<tr>
<td class="size_set">&nbsp;</td>
<td colspan="8">Подлинник исправил</td>
<td colspan="6">&nbsp;</td>
<td colspan="10">Контр. копию исправил</td>
<td colspan="6">&nbsp;</td>
<td colspan="6">&nbsp;</td>
</tr>
<tr class="size_set">
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
</tbody>
</table>


</body> 
</html> 