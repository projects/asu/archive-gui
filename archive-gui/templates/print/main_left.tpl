<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:svg="http://www.w3.org/2000/svg">
<head>
<style type="text/css">

@font-face {
	font-family: 'Times';
	font-size: 3mm;
}

@font-face {
	font-family: 'gost';
	
	src: url('gost.ttf') format('truetype');
}

html, body, table, tbody, th, tr, td {
	font-family: 'gost', 'Gost type A','Nimbus Sans L';
	font-style: italic;
	font-size: 4mm;
	text-align: center;
	vertical-align: bottom;
	outline: 0;
	border: 0;
	margin: 0px;
	padding: 0px;
}
table {
	border-collapse: collapse;
	border-spacing: 0;
	border:2px solid black;
}
tr,td {
	border: 2px solid black;
}

td.light{
	outline: 0px;
}
td {
}
</style>
</head>
<body>

<table>
 <tr>
  <td style="width: 5mm; height: 35mm;">
   <svg:svg version="1.1" width="5mm" height="34mm">
    <svg:text x='-17mm' y='4.5mm' transform="rotate(-90)" text-anchor='middle' font-family="gost" font-size="5mm">
     Подп. и дата
    </svg:text>
   </svg:svg>
  </td>
  <td style="width: 7mm; height: 35mm;">&nbsp;</td>
 </tr>
 <tr>
  <td style="width: 5mm; height: 25mm;">
   <svg:svg version="1.1" width="5mm" height="24mm">
    <svg:text x='-12mm' y='4.5mm' transform="rotate(-90)" text-anchor='middle' font-family="gost" font-size="5mm">
     Инв. № дубл.
    </svg:text>
   </svg:svg>
  </td>
  <td style="width: 7mm; height: 25mm;">&nbsp;</td>
 </tr>
 <tr>
  <td style="width: 5mm; height: 25mm;">
   <svg:svg version="1.1" width="5mm" height="24mm">
    <svg:text x='-12mm' y='4.5mm' transform="rotate(-90)" text-anchor='middle' font-family="gost" font-size="5mm">
     Взам. инв. №
    </svg:text>
   </svg:svg>
  </td>
  <td style="width: 7mm; height: 25mm;">&nbsp;</td>
 </tr>
 <tr>
  <td style="width: 5mm; height: 35mm;">
   <svg:svg version="1.1" width="5mm" height="34mm">
    <svg:text x='-17mm' y='4.5mm' transform="rotate(-90)" text-anchor='middle' font-family="gost" font-size="5mm">
     Подп. и дата
    </svg:text>
   </svg:svg>
  </td>
  <td style="width: 7mm; height: 35mm;">&nbsp;</td>
 </tr>
 <tr>
  <td style="width: 5mm; height: 25mm;">
   <svg:svg version="1.1" width="5mm" height="24mm">
    <svg:text x='-12mm' y='4.5mm' transform="rotate(-90)" text-anchor='middle' font-family="gost" font-size="5mm">
     Инв. № подл.
    </svg:text>
   </svg:svg>
  </td>
  <td style="width: 7mm; height: 25mm;">&nbsp;</td>
 </tr>
</table>
</body> 
</html> 