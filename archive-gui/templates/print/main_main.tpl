<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:svg="http://www.w3.org/2000/svg">
<head>
<style type="text/css">

@font-face {
	font-family: 'Times';
	font-size: 3mm;
}

@font-face {
	font-family: 'gost';
	
	src: url('/media/gost.ttf') format('truetype');
}

html, body, table, tbody, th, tr, td {
	font-family: 'gost', 'Gost type A','Nimbus Sans L';
	font-style: italic;
	font-size: 4mm;
	text-align: center;
	vertical-align: bottom;
	outline: 0;
	border: 0;
	margin: 0px;
	padding: 0px;
}
table {
	border-collapse: collapse;
	border-spacing: 0;
	border:2px solid black;
}
tr,td {
	border: 2px solid black;
}

td.light{
	outline: 0px;
}
td {
}
</style>
</head>
<body>
<table>
 <tr>
  <td style="width: 6mm; height: 20mm;">
   <svg:svg version="1.1" width="6mm" height="19mm">
    <svg:text x='-10mm' y='5mm' transform="rotate(-90)" text-anchor='middle' font-family="gost" font-size="5mm">
     № строки
    </svg:text>
   </svg:svg>
  </td>
  <td style="width: 8mm; height: 20mm;">
   <svg:svg version="1.1" width="8mm" height="19mm">
    <svg:text x='-10mm' y='6mm' transform="rotate(-90)" text-anchor='middle' font-family="gost" font-size="5mm">
     Формат
    </svg:text>
   </svg:svg>
  </td>
  <td style="width: 70mm; height: 20mm; vertical-align: middle; font-size: 8mm;">Обозначение</td>
  <td style="width: 63mm; height: 20mm; vertical-align: middle; font-size: 8mm;">Наименование</td>
  <td style="width: 10mm; height: 20mm;">
   <svg:svg version="1.1" width="8mm" height="19mm">
    <svg:text x='-10mm' y='6mm' transform="rotate(-90)" text-anchor='middle' font-family="gost" font-size="5mm">
     Кол.листов
    </svg:text>
   </svg:svg>
  </td>
  <td style="width: 10mm; height: 19mm; vertical-align: middle; font-size: 8mm;">№<br />экз.</td>
  <td style="width: 22mm; height: 19mm; vertical-align: middle; font-size: 8mm;">Приме-<br />чание</td>
 </tr>
{% for item in opis_list %}
 <tr>
  <td style="width: 6mm; height: 8mm;">{{item.num}}</td>
  <td style="width: 8mm; height: 8mm;">{{item.format}}</td>
  <td style="width: 70mm; height: 8mm;">{{item.klgi}}</td>
  <td style="width: 63mm; height: 8mm;">{{item.name}}</td>
  <td style="width: 10mm; height: 8mm;">{{item.sheets}}</td>
  <td style="width: 10mm; height: 8mm;">&nbsp;</td>
  <td style="width: 22mm; height: 8mm;">&nbsp;</td>
 </tr>
{% endfor %}
</table>

</body> 
</html> 