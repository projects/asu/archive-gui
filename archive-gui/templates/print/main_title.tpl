<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<style type="text/css">

@font-face {
	font-family: 'Times';
	font-size: 3mm;
}

@font-face {
	font-family: 'gost';
	
	src: url('gost.ttf') format('truetype');
}

html, body, table, tbody, th, tr, td {
	font-family: 'gost', 'Gost type A','Nimbus Sans L';
	font-style: italic;
	font-size: 4mm;
	text-align: center;
	vertical-align: bottom;
	outline: 0;
	border: 0;
	margin: 0px;
	padding: 0px;
}
table {
	border-collapse: collapse;
	border-spacing: 0;
	border:2px solid black;
}
tr,td {
	border: 2px solid black;
}

td.light{
	outline: 0px;
}
td {
}
</style>
</head>
<body>
<table>
 <tr>
  <td style="width: 7mm; height: 5mm;">&nbsp;</td>
  <td style="width: 10mm; height: 5mm;">&nbsp;</td>
  <td style="width: 23mm; height: 5mm;">&nbsp;</td>
  <td style="width: 15mm; height: 5mm;">&nbsp;</td>
  <td style="width: 10mm; height: 5mm;">&nbsp;</td>
  <td style="width: 120mm; height: 15mm;" colspan="6" rowspan="3">&nbsp;</td>
 </tr>
 <tr>
  <td style="width: 7mm; height: 5mm;">&nbsp;</td>
  <td style="width: 10mm; height: 5mm;">&nbsp;</td>
  <td style="width: 23mm; height: 5mm;">&nbsp;</td>
  <td style="width: 15mm; height: 5mm;">&nbsp;</td>
  <td style="width: 10mm; height: 5mm;">&nbsp;</td>
 </tr>
 <tr>
  <td style="width: 7mm; height: 5mm;" valign="bottom">Изм.</td>
  <td style="width: 10mm; height: 5mm;">Лист</td>
  <td style="width: 23mm; height: 5mm;">№ докум.</td>
  <td style="width: 15mm; height: 5mm;">Подп.</td>
  <td style="width: 10mm; height: 5mm;">Дата</td>
 </tr>
 <tr>
  <td style="width: 17mm; height: 5mm; text-align: left;" colspan="2">&nbsp;Разраб.</td>
  <td style="width: 23mm; height: 5mm;">&nbsp;</td>
  <td style="width: 15mm; height: 5mm;">&nbsp;</td>
  <td style="width: 10mm; height: 5mm;">&nbsp;</td>
  <td style="width: 70mm; height: 25mm;" rowspan="5">&nbsp;</td>
  <td style="width: 15mm; height: 5mm;" colspan="3">Лит.</td>
  <td style="width: 15mm; height: 5mm;">Лист</td>
  <td style="width: 20mm; height: 5mm;">Листов</td>
 </tr>
 <tr>
  <td style="width: 17mm; height: 5mm; text-align: left;" colspan="2">&nbsp;Пров.</td>
  <td style="width: 23mm; height: 5mm;">&nbsp;</td>
  <td style="width: 15mm; height: 5mm;">&nbsp;</td>
  <td style="width: 10mm; height: 5mm;">&nbsp;</td>
  <td style="width: 5mm; height: 5mm;">&nbsp;</td>
  <td style="width: 5mm; height: 5mm;">&nbsp;</td>
  <td style="width: 5mm; height: 5mm;">&nbsp;</td>
  <td style="width: 15mm; height: 5mm;">&nbsp;</td>
  <td style="width: 20mm; height: 5mm;">&nbsp;</td>
 </tr>
 <tr>
  <td style="width: 17mm; height: 5mm;" colspan="2">&nbsp;</td>
  <td style="width: 23mm; height: 5mm;">&nbsp;</td>
  <td style="width: 15mm; height: 5mm;">&nbsp;</td>
  <td style="width: 10mm; height: 5mm;">&nbsp;</td>
  <td style="width: 50mm; height: 15mm;" colspan="5" rowspan="3">&nbsp;</td>
 </tr>
 <tr>
  <td style="width: 17mm; height: 5mm; text-align: left;" colspan="2">&nbsp;Н.контр.</td>
  <td style="width: 23mm; height: 5mm;">&nbsp;</td>
  <td style="width: 15mm; height: 5mm;">&nbsp;</td>
  <td style="width: 10mm; height: 5mm;">&nbsp;</td>
 </tr>
 <tr>
  <td style="width: 17mm; height: 5mm; text-align: left;" colspan="2">&nbsp;Утв.</td>
  <td style="width: 23mm; height: 5mm;">&nbsp;</td>
  <td style="width: 15mm; height: 5mm;">&nbsp;</td>
  <td style="width: 10mm; height: 5mm;">&nbsp;</td>
 </tr>
</table>
</body> 
</html> 