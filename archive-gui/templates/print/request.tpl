<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:svg="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
<head>
<style type="text/css">

@font-face {
	font-family: 'Times';
	font-size: 3mm;
}

@font-face {
	font-family: 'gost';
	
	src: url('/media/gost.ttf') format('truetype');
}

html, body, table, tbody, th, tr, td {
	font-family: 'gost', 'Gost type A','Nimbus Sans L';
	font-style: italic;
	font-size: 4mm;
	text-align: center;
	vertical-align: bottom;
	outline: 0;
	border: 0;
	margin: 0px;
	padding: 0px;
}
table {
	border-collapse: collapse;
	border-spacing: 0;
	border:2px solid black;
}
tr,td {
	border: 2px solid black;
}

td.light{
	outline: 0px;
}
td {
}
table#niz tr {
	border: 0;
}
table#niz td {
	font-size: 5mm;
	border: 0;
}
table#niz {
	width: 100%;
	border: 0;
}
</style>
</head>
<body>
<div style="width:157mm">
<h1 style="text-align:center">Наряд на изготовление копий № {{number}}</h1>
<table><tbody>
 <tr>
    <td style="width: 60mm; height: 20mm; vertical-align: middle; font-size: 8mm;">Отдел/Цех</td>
    <td style="width: 60mm; height: 20mm; vertical-align: middle; font-size: 8mm;">Основание</td>
    <td style="width: 37mm; height: 20mm; vertical-align: middle; font-size: 8mm;">Заказ №</td>
 </tr>
 <tr>
    <td style="width: 60mm; height: 8mm;">{{division}}</td>
    <td style="width: 60mm; height: 8mm;">{{base}}</td>
    <td style="width: 37mm; height: 8mm;">{{numberzakaz}}</td>
 </tr>
</tbody></table>
<table><tbody>
 <tr>
  <td style="width: 6mm; height: 20mm;">
   <svg:svg version="1.1" width="6mm" height="19mm">
    <svg:text x='-10mm' y='4mm' transform="rotate(-90)" text-anchor='middle' font-family="GOST type A" font-size="5mm">
     № стр
    </svg:text>
   </svg:svg>
  </td>
  <td style="width: 10mm; height: 20mm;">
   <svg:svg version="1.1" width="8mm" height="19mm">
    <svg:text x='-10mm' y='6mm' transform="rotate(-90)" text-anchor='middle' font-family="gost" font-size="5mm">
     Формат
    </svg:text>
   </svg:svg>
  </td>
  <td style="width: 70mm; height: 20mm; vertical-align: middle; font-size: 8mm;">Обозначение</td>
  <td style="width: 63mm; height: 20mm; vertical-align: middle; font-size: 8mm;">Наименование</td>
  <td style="width: 10mm; height: 20mm;">
   <svg:svg version="1.1" width="8mm" height="19mm">
    <svg:text x='-10mm' y='6mm' transform="rotate(-90)" text-anchor='middle' font-family="gost" font-size="5mm">
     Кол.лис
    </svg:text>
   </svg:svg>
  </td>
 </tr>
{% for item in request_list %}
 <tr>
  <td style="width: 6mm; height: 8mm;">{{item.num}}</td>
  <td style="width: 8mm; height: 8mm;">{{item.format}}</td>
  <td style="width: 70mm; height: 8mm;">{{item.klgi}}</td>
  <td style="width: 63mm; height: 8mm;">{{item.name}}</td>
  <td style="width: 10mm; height: 8mm;">{{item.sheets}}</td>
 </tr>
{% endfor %}
</tbody></table>
<p></p>
<table id="niz" >
    <tr>
	<td style="text-align:left;">
	    <p>Затребовал {{user}}</p>
	</td>
	<td style="text-align:right;">
	    <p> Подпись __________</p>
	</td>
    </tr>
    <tr>
	<td style="text-align:left;">
	    <p >Дата {{date}}</p>
	</td>
    </tr>
</table>
</div>
</body> 
</html> 