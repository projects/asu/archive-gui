<html>
<link rel="stylesheet" type="text/css" href="media/ext/resources/css/ext-all.css">
<link rel="stylesheet" type="text/css" href="media/ext/examples/ux/fileuploadfield/css/fileuploadfield.css">
<script type="text/javascript" src="media/ext/adapter/ext/ext-base.js"></script>
<script type="text/javascript" src="media/ext/ext-all.js"></script>

<script type="text/javascript" src="media/ext/examples/ux/fileuploadfield/FileUploadField.js"></script>
<script type="text/javascript" src="media/ext/examples/ux/SearchField.js"></script>
<script type="text/javascript" src="media/ext/examples/ux/BufferView.js"></script>
<script type="text/javascript" src="media/ext/TabCloseMenu.js"></script>

<!-- <script type="text/javascript" src="media/ext/RowExpander.js"></script>  -->
<!-- <script type="text/javascript" src="media/ext/grid-plugins.js"></script>  -->

	
<script type="text/javascript">

var ds = new Ext.data.JsonStore({
				  url : "aj_getGridNavigationJSON",
// 				  autoLoad : {url : "aj_getGridNavigationJSON", params: {'query': "", 'start': 0, 'limit': 30}},
				  reader: new Ext.data.JsonReader({
// 					  root: 'records',
// 					  totalProperty: 'totalCount',
 					  id: 'idstore_nav'
				  }), 
				  fields	:[
					  	{name: 'id', mapping: 'id'},
						{name: 'klgi', mapping: 'klgi'},
					  	{name: 'text', mapping: 'text'},
					  ],
				 baseParams: {'limit' : 11}
				  });

var gridnav = new Ext.grid.GridPanel ({
   			     	store		: ds,
         			colModel	: colModel2,
// 				view		: new Ext.grid.GridView({
// 							forceFit:true,
// 						  }),
				viewConfig: { forceFit: true, enableRowBody:true,  showPreview:true,},
 				height		: 300,
// 				layout		: 'fit',
				selModel	: selModel,
				loadMask	: true,
				frame		:true,
    				trackMouseOver:false,
//  				autoExpandColumn: 'topic',
// 				listeners	: {rowclick	: function () {alert ("sdsd");}}
  				view: new Ext.ux.grid.BufferView({
		    // custom row height
		    			rowHeight: 30,
		    // render rows as they come into viewable area.
		    			scrollDelay: false
	    				}),
// 				items :	
// 					new Ext.DataView({
// // 						tpl: resultTpl,
// 						store: ds,
// // 						itemSelector: 'div.search-item'
// 						}),
					
				  tbar: [
				    'Search: ', ' ',
				      new Ext.ux.form.SearchField({
					  store: ds,
// 					  width:320
				      })
				  ],

				  bbar: new Ext.PagingToolbar({
				      store: ds,
				      pageSize: 20,
				      displayInfo: true,
				      displayMsg: '{0} - {1} из {2}',
				      emptyMsg: "Нет объектов"
				  })

				});


</script>


<div id="testdiv"></div>

</html>