[
{% for item in list %}
{
	'id': '{{ item.id }}',
	'klgi': '{{ item.object.klgi }}',
	'text': '{{ item.object.name }}',
	'name': '{{ item.object.name }}',

{% if item.object.type.kind.canhavechildren %}
	'leaf' : false,
	'havechildren' : true,
{% else %}
	'leaf' : true,
	'havechildren' : false,
{% endif %}

{% if item.id == item.project.id %}
	'isproject' : true,
{% else %}
	'isproject' : false,
{% endif %}
},
{% endfor %}
]
