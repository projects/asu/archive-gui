[
{% for item in list %}
{
	'text': '{{ item }}',
	'id': '{{ item.id }}',

		{% if item.object.type.kind.canhavechildren %}
		'leaf' : false,
	{% else %}
		'leaf' : true,
	{% endif %}	


},
{% endfor %}
]
