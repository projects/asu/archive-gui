[
{% for item in list %}
{
	"name": "{{ item.name }}",
	"id": "{{ item.id }}",
	"short": "{{ item.shortname }}",
},
{% endfor %}
]