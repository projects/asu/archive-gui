<html> 
<head> 
 <title>123</title> 
 <link rel="stylesheet" type="text/css" href="media/ext/resources/css/ext-all.css"/> 
 <script type="text/javascript" src="media/ext/adapter/ext/ext-base.js"></script> 
 <script type="text/javascript" src="media/ext/ext-all.js"></script> 
 <script type="text/javascript" src="media/ext/examples/ux/TabScrollerMenu.js"></script>
 <script type="text/javascript" src="media/ext/examples/ux/RowExpander.js"></script>
 <script type="text/javascript">

Ext.apply(Ext.form.VTypes, {
	daterange: function(val, field) {
		var date = field.parseDate(val);
		if (!date)  return;

		if (field.startDateField && (!this.dateRangeMax || (date.getTime() != this.dateRangeMax.getTime()))) {
			var start = Ext.getCmp(field.startDateField);
			start.setMaxValue(date);
			start.validate();
			this.dateRangeMax = date;
			}
		else if (field.endDateField && (!this.dateRangeMin || (date.getTime() != this.dateRangeMin.getTime()))) {
			var end = Ext.getCmp(field.endDateField);
			end.setMinValue(date);
			end.validate();
			this.dateRangeMin = date;
		}
		return true;
    }
});

Ext.QuickTips.init();

/**** Заглушки*///
var data_obj=[
[1,'25-11-05',1,0,'Проверка 1 Проверка 1 Проверка 1 Проверка 1 Проверка 1 Проверка 1 Проверка 1 Проверка 1 Проверка 1 '],
[4,'25-11-05',1,1,'Проверка 1'],
[5,'25-11-05',1,2,'Проверка 1'],
[8,'25-11-05',1,'0','Проверка 1'],
[9,'25-11-05',1,'0','Проверка 1'],

];


var store_obj=new Ext.data.ArrayStore({ fields: ['id','date','tid','status_code','status_message'] });

store_obj.loadData(data_obj);

//Типы объектов
var tobjCombo={
	id:'tobj-combo',
	xtype: 'combo',
	store: new Ext.data.JsonStore({
		url: 'aj_getHealthObjectTypesJSON',
		root: 'records',
		fields: ['id', 'name'],
		autoLoad:true,
	}),
	displayField: 'name',
	valueField: 'id',
	mode: 'remote',
	editable: false,
	triggerAction: 'all',

	fieldLabel: 'Тип объектов',
	emptyText: 'Любой',
	listeners: {
		select: function() { Ext.getCmp('obj-combo').clearValue(); }
	},
}
//Объекты
var objCombo={
	id:'obj-combo',
	xtype: 'combo',
	store: new Ext.data.JsonStore({
		url: 'aj_getHealthObjectsJSON',
		root: 'records',
		fields: ['id', 'name'],
		autoLoad:true,
	}),
	displayField: 'name',
	valueField: 'id',
	mode: 'remote',
	editable: false,
	triggerAction: 'all',

	fieldLabel: 'Объект',
	emptyText: 'Любой',
	listeners: {
		select: function() { Ext.getCmp('tobj-combo').clearValue(); }
	},
}

//Типы тестов
var ttestCombo={
	id:'ttest-combo',
	xtype: 'combo',
	store: new Ext.data.JsonStore({
		url: 'aj_getTestTypesJSON',
		root: 'records',
		fields: ['id', 'name'],
		autoLoad:true,
	}),
	displayField: 'name',
	valueField: 'id',
	mode: 'remote',
	editable: false,
	triggerAction: 'all',

	fieldLabel: 'Тип тестов',
	emptyText: 'Любой',
}




//Поиск - Выбор объекта
var objGroup={
	xtype: 'fieldset',
	style: 'padding:0 5px; margin:0 5px;',
	title: 'Объект',
	items:[tobjCombo, objCombo]
}
//Поиск - Выбор теста
var testGroup={
	xtype: 'fieldset',
	style: 'padding:0 5px; margin:0 5px;',
	title: 'Тест',
	items:[
		ttestCombo,
		{xtype: 'checkboxgroup',
		 id:'result-check',
		 fieldLabel: 'Результат',
		 items: [
			{boxLabel: '<font color="green">OK</font>', name: 'res0', checked: true},
			{boxLabel: '<font color="orange">Warning</font>', name: 'res1', checked: true},
			{boxLabel: '<b><font color="red">Error</font></b>', name: 'res2', checked: true},
		 ],
		}
	],
	width:'350'
}
//Поиск - Выбор даты
function dateAutoSet(){
	var dateDate=new Date();
	Ext.getCmp('date_from').setValue(new Date(dateDate.getTime()-1000*24*60*60*7));
	Ext.getCmp('date_to').setValue(dateDate);
}


var dateGroup={
	xtype: 'fieldset',
	style: 'padding:0 5px; margin:0 5px;',
	title: 'Период',
	items:[
		{xtype: 'datefield',
		 id:'date_from',
		 name:'date_from',
		 fieldLabel: 'От',
		 vtype: 'daterange',
		 endDateField: 'date_to',
		 editable: false,
		},
		{xtype: 'datefield',
		 id:'date_to',
		 name: 'date_to',
		 fieldLabel: 'До',
		 vtype: 'daterange',
		 startDateField: 'date_from',
		 editable: false,
		 listeners:{
			afterrender: function (){ dateAutoSet(); }
		 }
		}
	],
}

var resultID=0;
var messageExpander=new Ext.grid.RowExpander({tpl: new Ext.Template('<p><br><b>Сообщение</b>: {status_message}</p>')});
function codeRender(c){
	if(c==0) return '<font color="green">OK</font>';
	else if(c==1) return '<font color="orange">Warning</font>';
	else if(c==2) return '<b><font color="red">Error</font></b>';
}
//Шаблон массива результата поиска
var resultStore={
	url: 'aj_getTestResultsJSON',
	root: 'records',
	totalProperty: 'results',
	fields: ['id','date','idtest','name','status_code','status_message'],
}
//Шаблон результата поиска
var resultGrid={
	columns:[
		 messageExpander,
		 {header: "ID", width: 10, sortable: true, dataIndex: 'id'},
		 {header: "Дата", sortable: true, dataIndex: 'date'},
		 {header: "Тест", sortable: true, dataIndex: 'name'},
		 {header: "Результат", sortable: true, dataIndex: 'status_code',renderer:codeRender},
	],
	viewConfig: {forceFit:true},
	closable : true,
	autoScroll: true,
	plugins: messageExpander,
	bbar:{
		xtype:'paging',
		pageSize: 5,
		displayInfo: true,
		displayMsg: '{0} - {1} из {2}',
		emptyMsg: "Нет объектов",
	}
}
//Обработка поиска
function searchLoad(r,store,msg){
	if(r.length==0) Ext.MessageBox.alert('Результат поиска:', 'По вашему запросу данных не найдено');
	else{
		var rG=resultGrid;
		resultID++;

		if(msg=='') msg='<br>Не заданы';

		rG.id='resTab_'+resultID;
		rG.title='Поиск №'+resultID;
		rG.tabTip='<b>Параметры поиска</b>:<table>'+msg+'</table>';
		rG.bbar.store= rG.store=store;
		Ext.getCmp('results').add(new Ext.grid.GridPanel(rG)).show();
	}
}
//Кнопка очистки поиска
var searchRButton={
	text: 'Сбросить',
	handler: function() {
		Ext.getCmp('tobj-combo').clearValue();
		Ext.getCmp('obj-combo').clearValue();
		Ext.getCmp('ttest-combo').clearValue();
		Ext.getCmp('result-check').setValue([true,true,true]);
		dateAutoSet();
	}
}
//Кнопка поиска
var searchButton={
	text: 'Поиск',
	handler: function() {
		var msg='';
		var param={};

		var c_tobj=Ext.getCmp('tobj-combo').getValue();
		var c_obj=Ext.getCmp('obj-combo').getValue();
		var c_ttest=Ext.getCmp('ttest-combo').getValue();

		var res=Ext.getCmp('result-check').getValue();
		var date_from=Ext.getCmp('date_from').getValue();
		var date_to=Ext.getCmp('date_to').getValue();

		if(c_obj!=''){
			param.obj=c_obj;
			msg+='<br>Объект: '+Ext.getCmp('obj-combo').getRawValue();
		}
		else if(c_tobj!=''){
			param.tobj=c_tobj;
			msg+='<br>Тип Объекта: '+Ext.getCmp('tobj-combo').getRawValue();
		}

		if(c_ttest!=''){
			param.ttest=c_ttest;
			msg+='<br>Тип теста: '+Ext.getCmp('ttest-combo').getRawValue();
		}


		for(var i=0; i< res.length; i++) param[res[i].name]=1;

		if(date_from!=''){
			param.dateFrom=parseInt(date_from.getTime()/1000);
			msg+='<br>С:&nbsp;&nbsp;&nbsp;'+new Date(date_from).format('d/m/Y');
		}
		if(date_to!=''){
			param.dateTo=parseInt(date_to.getTime()/1000+24*60*60-1);
			msg+='<br>До:&nbsp;'+new Date(date_to).format('d/m/Y');
		}

		var store=new Ext.data.JsonStore(resultStore);
		store.load({params:param, callback:function (v){ searchLoad(v,store,msg); } });
	}
}






//Стартовая закладка
var t_obj=new Ext.grid.GridPanel({
	title: 'Пробный результат',
	id:'resTab',
	border:true,
	store: store_obj,
	columns:[
		 messageExpander,
		 {header: "ID", width: 10, sortable: true, dataIndex: 'id'},
		 {header: "Дата", sortable: true, dataIndex: 'data'},
		 {header: "Тест", sortable: true, dataIndex: 'test_id'},
		 {header: "Результат", sortable: true, dataIndex: 'status_code',renderer:codeRender},
	],
	viewConfig: {forceFit:true},
	tabTip:'21213',
	plugins: messageExpander,
});



{//Визуальные блоки
var searchRegion={
	region: 'north',
	title: 'Поиск',
	animCollapse: false,
	collapsible: true,
	height: 150,
	layout: 'form',
	items:[
		{layout: 'column',
		 border: false,
		 items:[objGroup,testGroup,dateGroup],
		 anchor:'100% 100%'
		}
	],
	buttons:[searchRButton,searchButton]
}

var resultRegion={
	xtype: 'tabpanel',
	id: 'results',
	region: 'center',
	title: 'Результат',
	activeTab: 0,
	resizeTabs: true,
	minTabWidth: 100,
	tabWidth: 150,
	enableTabScroll: true,
	defaults: {autoScroll: true},
	items: [t_obj]
}

var infoRegion={
	title: 'Дополнительная информация',
	region: 'east',
	collapsible: true,
	collapsed: true,
	animCollapse: false,
	html: '<p>доп инф-я</p>'
}

}

Ext.onReady(function() {
	var container = new Ext.Viewport({
	 layout: 'border',
	 items: [
		searchRegion,
		{defaults:{split:true},
		 region: 'center',
		 layout: 'border',
		 items:[resultRegion,infoRegion]
		}
	 ]
	});
});

</script> 
</head> 
<body> 
</body> 
</html> 