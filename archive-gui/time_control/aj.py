# -*- coding: utf-8 -*-
from django.http import HttpResponse
from django.contrib.auth.models import User
from func.func_list import *
from models import *
import simplejson

def aj_UserList (request):
	l = User.objects.all()
	res = []
	for user in l:
		res.append({'id':user.id, 'name':user.username})
	
	return HttpResponse(simplejson.dumps(res))

def aj_getUserSession (request):
	res_all = UserSession.objects.order_by('finish').reverse()
	
	try:
		res_all = res_all.filter(user__id = int(request.REQUEST['user']))
	except:
		pass
	
	try:
		request.REQUEST['closed0']
	except:
		res_all = res_all.exclude(status = 'o')
	
	try: 
		request.REQUEST['closed1']
	except:
		res_all = res_all.exclude(status__in = ['a','c'])
	
	res_date = ListFilterDate(request, res_all)
	res_page = ListPaging(request, res_date)
	
	res_row = []
	num = res_page['start']
	for it in res_page['list']:
		num += 1
		it_closed = it.closed
		if it.status == 'o':
			it_closed = False
		res_row.append({
			'num': num,
			'id': it.id,
			'start': it.start.strftime("%d-%m-%Y %H:%M:%S"),
			'finish': it.finish.strftime("%d-%m-%Y %H:%M:%S"),
			'user': it.user.username,
			'closed': it_closed,
		})
	
	res = {'total':res_page['total'], 'records':res_row}
	
	return HttpResponse(simplejson.dumps(res))