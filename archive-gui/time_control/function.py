# -*- coding: utf-8 -*-
from datetime import datetime
from models import *

def UserSessionStart (request, user):
	us = UserSession(
		user = user,
		start = datetime.now(),
		finish = datetime.now(),
	)
	us.save()
	request.session['UserSession'] = us.id
	
	return us

def UserSessionGet (request):
	if request.user.id <> None:
		request.session.set_expiry(settings.SESSION_EXPIRY)
		try:
			us = UserSession.objects.get(id = int(request.session['UserSession']), user = request.user)
		except:
			us = UserSessionStart ( request, request.user )
		else:
			us.refresh(request)
			if us.status <> 'o':
				us = False
	else:
		us = False
	
	return us