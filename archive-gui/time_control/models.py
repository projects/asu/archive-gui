# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User
from django.contrib import auth
from datetime import datetime, timedelta
import settings

class UserSessionClosed (object):
	def __get__(self, obj, obj_class):
		res = True
		if obj.status == 'o':
			delta = timedelta(0, settings.SESSION_EXPIRY)
			if (obj.finish + delta) < datetime.now():
				obj.status = 'a'
				obj.save()
			else:
				res = False
		return res

class UserSession (models.Model):
	user = models.ForeignKey (User)
	start = models.DateTimeField ()
	finish = models.DateTimeField ()
	
	status_choices = (
		('o', u'Открыта'),
		('a', u'Истечение времени'),
		('c', u'Выход'),
	)
	status = models.CharField(max_length = 1, choices = status_choices, default = 'o')
	closed = UserSessionClosed()

	def refresh(self, request):
		if self.status == 'o':
			if self.closed == False:
				self.finish = datetime.now()
				self.save()
			else:
				auth.logout(request)

	def close(self, request, status):
		self.status = status
		self.save()
		auth.logout(request)