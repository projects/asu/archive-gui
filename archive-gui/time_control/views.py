# -*- coding: utf-8 -*-
from django.contrib import auth
from function import *

def LogIn (request, username, password):
	success = 'init'
	user = auth.authenticate(username = username, password = password)
	
	if user is not None:
		if user.is_active:
			auth.login(request, user)
			request.session.set_expiry(settings.SESSION_EXPIRY)
			
			UserSessionStart(request, user)
			success = 'true'
		else:
			success = 'no_activ'
	else:
		success = 'no_user'
	
	return success

def LogOut (request, status):
	success = 'init'
	
	us = UserSessionGet(request)
	
	if us <> False:
		us.close(request, status)
		success = 'true'
	else:
		success = 'no_session'
	
	return success