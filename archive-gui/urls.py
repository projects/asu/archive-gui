# -*- coding: utf-8 -*-
from django.conf.urls.defaults import *

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

import settings

urlpatterns = patterns('',
	(r'media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_FOLDER}),
	(r'media_admin/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.ADMIN_MEDIA_PREFIX}),

	(r'^archive/', include('main.urls')),
	(r'^printer/', include('printer.urls')),
	(r'^file/', include('file.urls')),
	#(r'^accounts/login/$', 'django.contrib.auth.views.login',{'template_name': 'login.html'}),
	(r'^accounts/login/$', 'main.views.mylogin',{'template_name': 'login.html'}),
	(r'^accounts/logout/$', 'main.views.mylogout',{'template_name': 'logout.html'}),
	(r'^accounts/profile/$', 'main.views.viewmain'),
	(r'^admin/', include(admin.site.urls)),
)

